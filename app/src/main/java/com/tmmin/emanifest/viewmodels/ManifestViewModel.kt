package com.tmmin.emanifest.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.tmmin.emanifest.models.requests.assign.RequestAssign
import com.tmmin.emanifest.models.requests.assign.RequestAssignCombo
import com.tmmin.emanifest.models.requests.manifest.RequestManifestDetail
import com.tmmin.emanifest.models.requests.manifest.RequestManifestEnd
import com.tmmin.emanifest.models.requests.manifest.RequestManifestEndCheck
import com.tmmin.emanifest.models.requests.manifest.RequestManifestList
import com.tmmin.emanifest.models.requests.manifest.RequestManifestStart
import com.tmmin.emanifest.models.requests.manifest.completeness.RequestManifestCompletenessItem
import com.tmmin.emanifest.models.requests.manifest.others.RequestManifestOthers
import com.tmmin.emanifest.models.responses.assign.check.ResponseAssignCheck
import com.tmmin.emanifest.models.responses.assign.combo.ResponseAssignCombo
import com.tmmin.emanifest.models.responses.assign.post.ResponseAssignPost
import com.tmmin.emanifest.models.responses.manifest.check.ResponseManifestEndCheck
import com.tmmin.emanifest.models.responses.manifest.completeness.ResponseManifestCompleteness
import com.tmmin.emanifest.models.responses.manifest.detail.ResponseManifestDetail
import com.tmmin.emanifest.models.responses.manifest.end.ResponseManifestEnd
import com.tmmin.emanifest.models.responses.manifest.list.ResponseManifestList
import com.tmmin.emanifest.models.responses.manifest.others.ResponseManifestOthersTotal
import com.tmmin.emanifest.models.responses.manifest.start.ResponseManifestStart
import com.tmmin.emanifest.network.RequestState
import com.tmmin.emanifest.repositories.ManifestRepository
import kotlinx.coroutines.launch
import org.json.JSONException
import org.json.JSONObject
import retrofit2.HttpException
import retrofit2.Response
import java.net.UnknownHostException

class ManifestViewModel() : ViewModel() {
    val messageNoInternet = "There was a internet connection issue. Please try again !"
    private val repository: ManifestRepository = ManifestRepository()
    var isMoreRecord = false
    var pageListNumber = 1
    private var responseManifestList: ResponseManifestList? = null
    private var mutableResponseManifestList = MutableLiveData<RequestState<ResponseManifestList?>>()
    var manifestListResponse: LiveData<RequestState<ResponseManifestList?>> = mutableResponseManifestList

    private var responseManifestOthersListEO: ResponseManifestList? = null
    private var mutableResponseManifestOthersListEO = MutableLiveData<RequestState<ResponseManifestList?>>()
    var manifestOthersListEOResponse: LiveData<RequestState<ResponseManifestList?>> = mutableResponseManifestOthersListEO
    var responseManifestOthersListEOSearch: ResponseManifestList? = null
    private var mutableResponseManifestOthersListEOSearch = MutableLiveData<RequestState<ResponseManifestList?>>()
    var manifestOthersListEOSearchResponse: LiveData<RequestState<ResponseManifestList?>> = mutableResponseManifestOthersListEOSearch
    private var responseManifestOthersListShortage: ResponseManifestList? = null
    private var mutableResponseManifestOthersListShortage = MutableLiveData<RequestState<ResponseManifestList?>>()
    var manifestOthersListShortageResponse: LiveData<RequestState<ResponseManifestList?>> = mutableResponseManifestOthersListShortage
    private var responseManifestOthersListShortageSearch: ResponseManifestList? = null
    private var mutableResponseManifestOthersListShortageSearch = MutableLiveData<RequestState<ResponseManifestList?>>()
    var manifestOthersListShortageSearchResponse: LiveData<RequestState<ResponseManifestList?>> = mutableResponseManifestOthersListShortageSearch
    private var responseManifestOthersListHistory: ResponseManifestList? = null
    private var mutableResponseManifestOthersListHistory = MutableLiveData<RequestState<ResponseManifestList?>>()
    var manifestOthersListHistoryResponse: LiveData<RequestState<ResponseManifestList?>> = mutableResponseManifestOthersListHistory
    private var responseManifestOthersListHistorySearch: ResponseManifestList? = null
    private var mutableResponseManifestOthersListHistorySearch = MutableLiveData<RequestState<ResponseManifestList?>>()
    var manifestOthersListHistorySearchResponse: LiveData<RequestState<ResponseManifestList?>> = mutableResponseManifestOthersListHistorySearch

    fun getManifestDetail(auth: String, requestManifestDetail: RequestManifestDetail): LiveData<RequestState<ResponseManifestDetail>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.getDetailManifest(auth, requestManifestDetail)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun startManifest(auth: String, requestManifestStart: RequestManifestStart): LiveData<RequestState<ResponseManifestStart>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.postManifestPreparation(auth, requestManifestStart)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun getCheckEndManifest(auth: String, requestManifestEndCheck: RequestManifestEndCheck): LiveData<RequestState<ResponseManifestEndCheck>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.getCheckManifest(auth, requestManifestEndCheck)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun postEndManifest(auth: String, requestManifestEnd: RequestManifestEnd): LiveData<RequestState<ResponseManifestEnd>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.postEndManifest(auth, requestManifestEnd)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun postManifestCompletenessAdd(auth: String, requestManifestCompleteness: List<RequestManifestCompletenessItem>): LiveData<RequestState<ResponseManifestCompleteness>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.postManifestCompletenessAdd(auth, requestManifestCompleteness)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun postManifestCompletenessCancel(auth: String, requestManifestCompleteness: List<RequestManifestCompletenessItem>): LiveData<RequestState<ResponseManifestCompleteness>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.postManifestCompletenessCancel(auth, requestManifestCompleteness)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun getTotalManifestOthers(auth: String, requestManifestOthers: RequestManifestOthers): LiveData<RequestState<ResponseManifestOthersTotal>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.getTotalManifestOthers(auth, requestManifestOthers)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun getComboManifestOthers(auth: String, requestAssignCombo: RequestAssignCombo): LiveData<RequestState<ResponseAssignCombo>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.getComboManifestOthers(auth, requestAssignCombo)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun getCheckManifestAssign(auth: String, requestAssign: RequestAssign): LiveData<RequestState<ResponseAssignCheck>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.getCheckManifestAssign(auth, requestAssign)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun postManifestAssign(auth: String, requestAssign: RequestAssign): LiveData<RequestState<ResponseAssignPost>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.postManifestAssign(auth, requestAssign)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun getManifestList(isStarted: Boolean, auth: String, requestManifestList: RequestManifestList) {
        viewModelScope.launch {
            if (isStarted) {
                pageListNumber = 1
                responseManifestList = null
                isMoreRecord = false
            }
            Log.e("ReqManNumber", "" + pageListNumber)
            Log.e("ReqManMore", "" + isMoreRecord)
            requestManifestList.pageNumber = pageListNumber
            requestManifestList.pageSize = 10
            mutableResponseManifestList.postValue(RequestState.Loading)
            try {
                val response = repository.getListManifest(auth, requestManifestList)
                mutableResponseManifestList.postValue(handleManifestList(response))
            } catch (e: UnknownHostException) {
                mutableResponseManifestList.postValue(RequestState.Error(messageNoInternet))
            }
        }
    }

    private fun handleManifestList(response: Response<ResponseManifestList>): RequestState<ResponseManifestList?> {
        return if (response.isSuccessful) {
            response.body()?.let {
                isMoreRecord = it.paginationInfo?.moreRecord ?: false
                if (isMoreRecord == true) {
                    pageListNumber++
                }
                if (responseManifestList == null) responseManifestList = it else {
                    val oldData = responseManifestList?.data
                    val newData = it.data
                    oldData?.addAll(newData)
                }
            }
            RequestState.Success(responseManifestList ?: response.body())
        } else RequestState.Error(try {
            response.errorBody()?.string()?.let {
                JSONObject(it).get("message")
            }
        } catch (e: JSONException) {
            e.localizedMessage
        } as String)
    }

    fun getListManifestEO(auth: String, requestManifestOthers: RequestManifestOthers) {
        viewModelScope.launch {
            mutableResponseManifestOthersListEO.postValue(RequestState.Loading)
            try {
                val response = repository.getListManifestEO(auth, requestManifestOthers)
                mutableResponseManifestOthersListEO.postValue(handleListManifestEO(response))
            } catch (e: UnknownHostException) {
                mutableResponseManifestOthersListEO.postValue(RequestState.Error(messageNoInternet))
            }
        }
    }

    private fun handleListManifestEO(response: Response<ResponseManifestList>): RequestState<ResponseManifestList?> {
        return if (response.isSuccessful) {
            response.body()?.let {
                responseManifestOthersListEO = it
            }
            RequestState.Success(responseManifestOthersListEO ?: response.body())
        } else RequestState.Error(try {
            response.errorBody()?.string()?.let {
                JSONObject(it).get("message")
            }
        } catch (e: JSONException) {
            e.localizedMessage
        } as String)
    }

    fun searchListManifestEO(auth: String, requestManifestOthers: RequestManifestOthers) {
        viewModelScope.launch {
            mutableResponseManifestOthersListEOSearch.postValue(RequestState.Loading)
            try {
                val response = repository.searchListManifestEO(auth, requestManifestOthers)
                mutableResponseManifestOthersListEOSearch.postValue(handleSearchListManifestEO(response))
            } catch (e: UnknownHostException) {
                mutableResponseManifestOthersListEOSearch.postValue(RequestState.Error(messageNoInternet))
            }
        }
    }

    private fun handleSearchListManifestEO(response: Response<ResponseManifestList>): RequestState<ResponseManifestList?> {
        return if (response.isSuccessful) {
            response.body()?.let {
                responseManifestOthersListEOSearch = it
            }
            RequestState.Success(responseManifestOthersListEOSearch ?: response.body())
        } else RequestState.Error(try {
            response.errorBody()?.string()?.let {
                JSONObject(it).get("message")
            }
        } catch (e: JSONException) {
            e.localizedMessage
        } as String)
    }

    fun getListManifestShortage(auth: String, requestManifestOthers: RequestManifestOthers) {
        viewModelScope.launch {
            mutableResponseManifestOthersListShortage.postValue(RequestState.Loading)
            try {
                val response = repository.getListManifestShortage(auth, requestManifestOthers)
                mutableResponseManifestOthersListShortage.postValue(handleListManifestShortage(response))
            } catch (e: UnknownHostException) {
                mutableResponseManifestOthersListShortage.postValue(RequestState.Error(messageNoInternet))
            }
        }
    }

    private fun handleListManifestShortage(response: Response<ResponseManifestList>): RequestState<ResponseManifestList?> {
        return if (response.isSuccessful) {
            response.body()?.let {
                responseManifestOthersListShortage = it
            }
            RequestState.Success(responseManifestOthersListShortage ?: response.body())
        } else RequestState.Error(try {
            response.errorBody()?.string()?.let {
                JSONObject(it).get("message")
            }
        } catch (e: JSONException) {
            e.localizedMessage
        } as String)
    }

    fun searchListManifestShortage(auth: String, requestManifestOthers: RequestManifestOthers) {
        viewModelScope.launch {
            mutableResponseManifestOthersListShortageSearch.postValue(RequestState.Loading)
            try {
                val response = repository.searchListManifestShortage(auth, requestManifestOthers)
                mutableResponseManifestOthersListShortageSearch.postValue(handleSearchListManifestShortage(response))
            } catch (e: UnknownHostException) {
                mutableResponseManifestOthersListShortageSearch.postValue(RequestState.Error(messageNoInternet))
            }
        }
    }

    private fun handleSearchListManifestShortage(response: Response<ResponseManifestList>): RequestState<ResponseManifestList?> {
        return if (response.isSuccessful) {
            response.body()?.let {
                responseManifestOthersListShortageSearch = it
            }
            RequestState.Success(responseManifestOthersListShortageSearch ?: response.body())
        } else RequestState.Error(try {
            response.errorBody()?.string()?.let {
                JSONObject(it).get("message")
            }
        } catch (e: JSONException) {
            e.localizedMessage
        } as String)
    }

    fun getListManifestHistory(auth: String, requestManifestOthers: RequestManifestOthers) {
        viewModelScope.launch {
            mutableResponseManifestOthersListHistory.postValue(RequestState.Loading)
            try {
                val response = repository.getListManifestHistory(auth, requestManifestOthers)
                mutableResponseManifestOthersListHistory.postValue(handleListManifestHistory(response))
            } catch (e: UnknownHostException) {
                mutableResponseManifestOthersListHistory.postValue(RequestState.Error(messageNoInternet))
            }
        }
    }

    private fun handleListManifestHistory(response: Response<ResponseManifestList>): RequestState<ResponseManifestList?> {
        return if (response.isSuccessful) {
            response.body()?.let {
                responseManifestOthersListHistory = it
            }
            RequestState.Success(responseManifestOthersListHistory ?: response.body())
        } else RequestState.Error(try {
            response.errorBody()?.string()?.let {
                JSONObject(it).get("message")
            }
        } catch (e: JSONException) {
            e.localizedMessage
        } as String)
    }

    fun searchListManifestHistory(auth: String, requestManifestOthers: RequestManifestOthers) {
        viewModelScope.launch {
            mutableResponseManifestOthersListHistorySearch.postValue(RequestState.Loading)
            try {
                val response = repository.searchListManifestHistory(auth, requestManifestOthers)
                mutableResponseManifestOthersListHistorySearch.postValue(handleSearchListManifestHistory(response))
            } catch (e: UnknownHostException) {
                mutableResponseManifestOthersListHistorySearch.postValue(RequestState.Error(messageNoInternet))
            }
        }
    }

    private fun handleSearchListManifestHistory(response: Response<ResponseManifestList>): RequestState<ResponseManifestList?> {
        return if (response.isSuccessful) {
            response.body()?.let {
                responseManifestOthersListHistorySearch = it
            }
            RequestState.Success(responseManifestOthersListHistorySearch ?: response.body())
        } else RequestState.Error(try {
            response.errorBody()?.string()?.let {
                JSONObject(it).get("message")
            }
        } catch (e: JSONException) {
            e.localizedMessage
        } as String)
    }
}