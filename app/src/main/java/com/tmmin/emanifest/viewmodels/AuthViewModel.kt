package com.tmmin.emanifest.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.tmmin.emanifest.models.requests.auth.RequestLogin
import com.tmmin.emanifest.models.requests.auth.RequestLogout
import com.tmmin.emanifest.models.requests.auth.RequestRefreshToken
import com.tmmin.emanifest.models.requests.auth.RequestSupplierInfo
import com.tmmin.emanifest.models.responses.auth.logout.ResponseLogout
import com.tmmin.emanifest.models.responses.auth.profile.ResponseSupplierInfo
import com.tmmin.emanifest.models.responses.auth.supplierLogin.ResponseLoginSupplier
import com.tmmin.emanifest.models.responses.dockCode.ResponseDockCode
import com.tmmin.emanifest.network.RequestState
import com.tmmin.emanifest.repositories.AuthRepository
import org.json.JSONException
import org.json.JSONObject
import retrofit2.HttpException

class AuthViewModel : ViewModel() {
    val messageNoInternet = "There was a internet connection issue. Please try again !"
    private val repository: AuthRepository = AuthRepository()

    fun loginSupplier(requestLogin: RequestLogin): LiveData<RequestState<ResponseLoginSupplier>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.loginSupplier(requestLogin)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun getSupplierInfo(auth: String, requestSupplierInfo: RequestSupplierInfo): LiveData<RequestState<ResponseSupplierInfo>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.getSupplierInfo(auth, requestSupplierInfo)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun logout(requestLogout: RequestLogout): LiveData<RequestState<ResponseLogout>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.logout(requestLogout)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun refreshToken(auth: String, requestRefreshToken: RequestRefreshToken): LiveData<RequestState<ResponseLoginSupplier>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.refreshToken(auth, requestRefreshToken)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun getSettingDockCode(auth: String, subTitle: String): LiveData<RequestState<ResponseDockCode>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.getSettingDockCode(auth, subTitle)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }
}