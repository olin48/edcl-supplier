package com.tmmin.emanifest.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.tmmin.emanifest.models.requests.skid.RequestSKIDCreate
import com.tmmin.emanifest.models.requests.skid.RequestSKIDList
import com.tmmin.emanifest.models.requests.skid.RequestSkidDetail
import com.tmmin.emanifest.models.responses.skid.create.ResponseSKIDCreate
import com.tmmin.emanifest.models.responses.skid.detail.ResponseSKIDDetail
import com.tmmin.emanifest.models.responses.skid.list.ResponseSKIDList
import com.tmmin.emanifest.network.RequestState
import com.tmmin.emanifest.repositories.SKIDRepository
import org.json.JSONException
import org.json.JSONObject
import retrofit2.HttpException

class SKIDViewModel() : ViewModel() {
    val messageNoInternet = "There was a internet connection issue. Please try again !"
    private val repository: SKIDRepository = SKIDRepository()

    fun getSKIDList(auth: String, requestSKIDList: RequestSKIDList): LiveData<RequestState<ResponseSKIDList>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.getListSKID(auth, requestSKIDList)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun createSKID(auth: String, requestSKIDCreate: RequestSKIDCreate): LiveData<RequestState<ResponseSKIDCreate>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.createSKID(auth, requestSKIDCreate)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun getSKIDDetail(auth: String, requestSkidDetail: RequestSkidDetail): LiveData<RequestState<ResponseSKIDDetail>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.getDetailSKID(auth, requestSkidDetail)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }
}