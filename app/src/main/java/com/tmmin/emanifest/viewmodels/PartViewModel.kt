package com.tmmin.emanifest.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.tmmin.emanifest.models.requests.part.RequestPartList
import com.tmmin.emanifest.models.responses.part.list.ResponsePartList
import com.tmmin.emanifest.network.RequestState
import com.tmmin.emanifest.repositories.PartRepository
import org.json.JSONException
import org.json.JSONObject
import retrofit2.HttpException

class PartViewModel() : ViewModel() {
    val messageNoInternet = "There was a internet connection issue. Please try again !"
    private val repository: PartRepository = PartRepository()

    fun getPartList(auth: String, requestPartList: RequestPartList): LiveData<RequestState<ResponsePartList>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.getListPart(auth, requestPartList)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }
}