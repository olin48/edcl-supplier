package com.tmmin.emanifest.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.tmmin.emanifest.models.requests.assign.RequestAssign
import com.tmmin.emanifest.models.requests.assign.RequestAssignCombo
import com.tmmin.emanifest.models.requests.job.RequestJobList
import com.tmmin.emanifest.models.requests.job.RequestJobSupplierTotal
import com.tmmin.emanifest.models.requests.job.driver.RequestDriverPickup
import com.tmmin.emanifest.models.responses.assign.assigned.ResponseAssignedDriver
import com.tmmin.emanifest.models.responses.assign.check.ResponseAssignCheck
import com.tmmin.emanifest.models.responses.assign.combo.ResponseAssignCombo
import com.tmmin.emanifest.models.responses.assign.post.ResponseAssignPost
import com.tmmin.emanifest.models.responses.job.combo.ResponseJobCombo
import com.tmmin.emanifest.models.responses.job.current.ResponseJobSupplierTotal
import com.tmmin.emanifest.models.responses.job.driver.check.ResponseDriverPickupCheck
import com.tmmin.emanifest.models.responses.job.driver.post.ResponseDriverPickup
import com.tmmin.emanifest.models.responses.job.list.ResponseJobList
import com.tmmin.emanifest.network.RequestState
import com.tmmin.emanifest.repositories.JobRepository
import kotlinx.coroutines.launch
import org.json.JSONException
import org.json.JSONObject
import retrofit2.HttpException
import retrofit2.Response

class JobViewModel() : ViewModel() {
    val messageNoInternet = "There was a internet connection issue. Please try again !"
    private val repository: JobRepository = JobRepository()

    var isMoreRecordHistory = false
    var isMoreSearchRecordHistory = false
    var isMoreRecord = false
    var isMoreSearchRecord = false
    var isMoreRecordSupProgress = false
    var isMoreSearchRecordSupProgress = false
    var isMoreRecordSupReady = false
    var isMoreSearchRecordSupReady = false
    var isMoreRecordDelMilkrun = false
    var isMoreSearchRecordDelMilkrun = false
    var isMoreRecordDelDirect = false
    var isMoreSearchRecordDelDirect = false
    var isMoreRecordDirectPreparation = false
    var isMoreSearchRecordDirectPreparation = false
    var isMoreRecordDriverDelDirect = false
    var isMoreSearchRecordDriverDelDirect = false

    var pageNumberHistory = 1
    var pageSearchNumberHistory = 1
    var pageNumber = 1
    var pageSearchNumber = 1
    var pageNumberSupProgress = 1
    var pageSearchNumberSupProgress = 1
    var pageNumberSupReady = 1
    var pageSearchNumberSupReady = 1
    var pageNumberDelMilkrun = 1
    var pageSearchNumberDelMilkrun = 1
    var pageNumberDelDirect = 1
    var pageSearchNumberDelDirect = 1
    var pageNumberDirectPreparation = 1
    var pageSearchNumberDirectPreparation = 1
    var pageNumberDriverDelDirect = 1
    var pageSearchNumberDriverDelDirect = 1

    private var responseJobList: ResponseJobList? = null
    private var mutableResponseJobList = MutableLiveData<RequestState<ResponseJobList?>>()
    var jobListResponse: LiveData<RequestState<ResponseJobList?>> = mutableResponseJobList
    private var responseJobListSearch: ResponseJobList? = null
    private var mutableResponseJobListSearch = MutableLiveData<RequestState<ResponseJobList?>>()
    var jobListSearchResponse: LiveData<RequestState<ResponseJobList?>> = mutableResponseJobListSearch

    fun getJobList(mode: Int, isStarted: Boolean, auth: String, requestJobList: RequestJobList) {
        viewModelScope.launch {
            when (mode) {
                1 -> {
                    if (isStarted) {
                        pageNumberSupProgress = 1
                        responseJobList = null
                        isMoreRecordSupProgress = false
                    }
                    requestJobList.pageNumber = pageNumberSupProgress
                    requestJobList.pageSize = 10
                    mutableResponseJobList.postValue(RequestState.Loading)
                    try {
                        val response = repository.getJobSupplierProgressList(auth, requestJobList)
                        mutableResponseJobList.postValue(handleJobList(response))
                        isMoreRecordSupProgress = response.body()?.paginationTotalInfo?.moreRecord ?: false
                        if (isMoreRecordSupProgress == true) {
                            pageNumberSupProgress++
                        }
                    } catch (e: HttpException) {
                        try {
                            mutableResponseJobList.postValue(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
                        } catch (e: JSONException) {
                            mutableResponseJobList.postValue(RequestState.Error(messageNoInternet))
                        }
                    } catch (e: Exception) {
                        mutableResponseJobList.postValue(RequestState.Error(messageNoInternet))
                    }
                }

                2 -> {
                    if (isStarted) {
                        pageNumberSupReady = 1
                        responseJobList = null
                        isMoreRecordSupReady = false
                    }
                    requestJobList.pageNumber = pageNumberSupReady
                    requestJobList.pageSize = 10
                    mutableResponseJobList.postValue(RequestState.Loading)
                    try {
                        val response = repository.getJobSupplierReadyList(auth, requestJobList)
                        mutableResponseJobList.postValue(handleJobList(response))
                        isMoreRecordSupReady = response.body()?.paginationTotalInfo?.moreRecord ?: false
                        if (isMoreRecordSupReady == true) {
                            pageNumberSupReady++
                        }
                    } catch (e: HttpException) {
                        try {
                            mutableResponseJobList.postValue(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
                        } catch (e: JSONException) {
                            mutableResponseJobList.postValue(RequestState.Error(messageNoInternet))
                        }
                    } catch (e: Exception) {
                        mutableResponseJobList.postValue(RequestState.Error(messageNoInternet))
                    }
                }

                3 -> {
                    if (isStarted) {
                        pageNumberDelMilkrun = 1
                        responseJobList = null
                        isMoreRecordDelMilkrun = false
                    }
                    requestJobList.pageNumber = pageNumberDelMilkrun
                    requestJobList.pageSize = 10
                    mutableResponseJobList.postValue(RequestState.Loading)
                    try {
                        val response = repository.getJobDeliverMilkrunList(auth, requestJobList)
                        mutableResponseJobList.postValue(handleJobList(response))
                        isMoreRecordDelMilkrun = response.body()?.paginationTotalInfo?.moreRecord ?: false
                        if (isMoreRecordDelMilkrun == true) {
                            pageNumberDelMilkrun++
                        }
                    } catch (e: HttpException) {
                        try {
                            mutableResponseJobList.postValue(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
                        } catch (e: JSONException) {
                            mutableResponseJobList.postValue(RequestState.Error(messageNoInternet))
                        }
                    } catch (e: Exception) {
                        mutableResponseJobList.postValue(RequestState.Error(messageNoInternet))
                    }
                }

                4 -> {
                    if (isStarted) {
                        pageNumberDelDirect = 1
                        responseJobList = null
                        isMoreRecordDelDirect = false
                    }
                    requestJobList.pageNumber = pageNumberDelDirect
                    requestJobList.pageSize = 10
                    mutableResponseJobList.postValue(RequestState.Loading)
                    try {
                        val response = repository.getJobDeliverDirectList(auth, requestJobList)
                        mutableResponseJobList.postValue(handleJobList(response))
                        isMoreRecordDelDirect = response.body()?.paginationTotalInfo?.moreRecord ?: false
                        if (isMoreRecordDelDirect == true) {
                            pageNumberDelDirect++
                        }
                    } catch (e: HttpException) {
                        try {
                            mutableResponseJobList.postValue(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
                        } catch (e: JSONException) {
                            mutableResponseJobList.postValue(RequestState.Error(messageNoInternet))
                        }
                    } catch (e: Exception) {
                        mutableResponseJobList.postValue(RequestState.Error(messageNoInternet))
                    }
                }

                5 -> {
                    if (isStarted) {
                        pageNumberDirectPreparation = 1
                        responseJobList = null
                        isMoreRecordDirectPreparation = false
                    }
                    requestJobList.pageNumber = pageNumberDirectPreparation
                    requestJobList.pageSize = 10
                    mutableResponseJobList.postValue(RequestState.Loading)
                    try {
                        val response = repository.getJobDirectPreparation(auth, requestJobList)
                        mutableResponseJobList.postValue(handleJobList(response))
                        isMoreRecordDirectPreparation = response.body()?.paginationTotalInfo?.moreRecord ?: false
                        if (isMoreRecordDirectPreparation == true) {
                            pageNumberDirectPreparation++
                        }
                    } catch (e: HttpException) {
                        try {
                            mutableResponseJobList.postValue(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
                        } catch (e: JSONException) {
                            mutableResponseJobList.postValue(RequestState.Error(messageNoInternet))
                        }
                    } catch (e: Exception) {
                        mutableResponseJobList.postValue(RequestState.Error(messageNoInternet))
                    }
                }

                6 -> {
                    if (isStarted) {
                        pageNumberHistory = 1
                        responseJobList = null
                        isMoreRecordHistory = false
                    }
                    requestJobList.pageNumber = pageNumberHistory
                    requestJobList.pageSize = 10
                    mutableResponseJobList.postValue(RequestState.Loading)
                    try {
                        val response = repository.getJobHistory(auth, requestJobList)
                        mutableResponseJobList.postValue(handleJobList(response))
                        isMoreRecordHistory = response.body()?.paginationInfo?.moreRecord ?: false
                        if (isMoreRecordHistory == true) {
                            pageNumberHistory++
                        }
                    } catch (e: HttpException) {
                        try {
                            mutableResponseJobList.postValue(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
                        } catch (e: JSONException) {
                            mutableResponseJobList.postValue(RequestState.Error(messageNoInternet))
                        }
                    } catch (e: Exception) {
                        mutableResponseJobList.postValue(RequestState.Error(messageNoInternet))
                    }
                }

                7 -> {
                    if (isStarted) {
                        pageNumberDriverDelDirect = 1
                        responseJobList = null
                        isMoreRecordDriverDelDirect = false
                    }
                    requestJobList.pageNumber = pageNumberDriverDelDirect
                    requestJobList.pageSize = 10
                    mutableResponseJobList.postValue(RequestState.Loading)
                    try {
                        val response = repository.getJobDriverDeliverDirectList(auth, requestJobList)
                        mutableResponseJobList.postValue(handleJobList(response))
                        isMoreRecordDriverDelDirect = response.body()?.paginationTotalInfo?.moreRecord ?: false
                        if (isMoreRecordDriverDelDirect == true) {
                            pageNumberDriverDelDirect++
                        }
                    } catch (e: HttpException) {
                        try {
                            mutableResponseJobList.postValue(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
                        } catch (e: JSONException) {
                            mutableResponseJobList.postValue(RequestState.Error(messageNoInternet))
                        }
                    } catch (e: Exception) {
                        mutableResponseJobList.postValue(RequestState.Error(messageNoInternet))
                    }
                }

                else -> {
                    if (isStarted) {
                        pageNumber = 1
                        responseJobList = null
                        isMoreRecord = false
                    }
                    requestJobList.pageNumber = pageNumber
                    requestJobList.pageSize = 10
                    mutableResponseJobList.postValue(RequestState.Loading)
                    try {
                        val response = repository.getJobList(auth, requestJobList)
                        mutableResponseJobList.postValue(handleJobList(response))
                        isMoreRecord = response.body()?.paginationTotalInfo?.moreRecord ?: false
                        if (isMoreRecord == true) {
                            pageNumber++
                        }
                    } catch (e: HttpException) {
                        try {
                            mutableResponseJobList.postValue(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
                        } catch (e: JSONException) {
                            mutableResponseJobList.postValue(RequestState.Error(messageNoInternet))
                        }
                    } catch (e: Exception) {
                        mutableResponseJobList.postValue(RequestState.Error(messageNoInternet))
                    }
                }
            }
        }
    }

    private fun handleJobList(response: Response<ResponseJobList>): RequestState<ResponseJobList?> {
        if (response.isSuccessful) {
            response.body()?.let {
                if (responseJobList == null) responseJobList = it else {
                    val oldData = responseJobList?.data
                    val newData = it.data
                    oldData?.addAll(newData)
                }
            }
            return RequestState.Success(responseJobList ?: response.body())
        } else {
            try {
                return RequestState.Error(response.errorBody()?.string()?.let { JSONObject(it).get("message") } as String)
            } catch (e: JSONException) {
                return (RequestState.Error(messageNoInternet))
            } catch (e: Exception) {
                return (RequestState.Error(messageNoInternet))
            }
        }
    }

    fun getJobListSearch(mode: Int, isStarted: Boolean, auth: String, requestJobList: RequestJobList) {
        viewModelScope.launch {
            when (mode) {
                1 -> {
                    if (isStarted) {
                        pageSearchNumberSupProgress = 1
                        responseJobListSearch = null
                        isMoreSearchRecordSupProgress = false
                    }
                    requestJobList.pageNumber = pageSearchNumberSupProgress
                    requestJobList.pageSize = 10
                    mutableResponseJobListSearch.postValue(RequestState.Loading)
                    try {
                        val response = repository.getJobSupplierProgressList(auth, requestJobList)
                        mutableResponseJobListSearch.postValue(handleJobListSearch(response))
                        isMoreSearchRecordSupProgress = response.body()?.paginationTotalInfo?.moreRecord ?: false
                        if (isMoreSearchRecordSupProgress == true) {
                            pageSearchNumberSupProgress++
                        }
                    } catch (e: HttpException) {
                        try {
                            mutableResponseJobListSearch.postValue(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
                        } catch (e: JSONException) {
                            mutableResponseJobListSearch.postValue(RequestState.Error(messageNoInternet))
                        }
                    } catch (e: Exception) {
                        mutableResponseJobListSearch.postValue(RequestState.Error(messageNoInternet))
                    }
                }

                2 -> {
                    if (isStarted) {
                        pageSearchNumberSupReady = 1
                        responseJobListSearch = null
                        isMoreSearchRecordSupReady = false
                    }
                    requestJobList.pageNumber = pageSearchNumberSupReady
                    requestJobList.pageSize = 10
                    mutableResponseJobListSearch.postValue(RequestState.Loading)
                    try {
                        val response = repository.getJobSupplierReadyList(auth, requestJobList)
                        mutableResponseJobListSearch.postValue(handleJobListSearch(response))
                        isMoreSearchRecordSupReady = response.body()?.paginationTotalInfo?.moreRecord ?: false
                        if (isMoreSearchRecordSupReady == true) {
                            pageSearchNumberSupReady++
                        }
                    } catch (e: HttpException) {
                        try {
                            mutableResponseJobListSearch.postValue(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
                        } catch (e: JSONException) {
                            mutableResponseJobListSearch.postValue(RequestState.Error(messageNoInternet))
                        }
                    } catch (e: Exception) {
                        mutableResponseJobListSearch.postValue(RequestState.Error(messageNoInternet))
                    }
                }

                3 -> {
                    if (isStarted) {
                        pageSearchNumberDelMilkrun = 1
                        responseJobListSearch = null
                        isMoreSearchRecordDelMilkrun = false
                    }
                    requestJobList.pageNumber = pageSearchNumberDelMilkrun
                    requestJobList.pageSize = 10
                    mutableResponseJobListSearch.postValue(RequestState.Loading)
                    try {
                        val response = repository.getJobDeliverMilkrunList(auth, requestJobList)
                        mutableResponseJobListSearch.postValue(handleJobListSearch(response))
                        isMoreSearchRecordDelMilkrun = response.body()?.paginationTotalInfo?.moreRecord ?: false
                        if (isMoreSearchRecordDelMilkrun == true) {
                            pageSearchNumberDelMilkrun++
                        }
                    } catch (e: HttpException) {
                        try {
                            mutableResponseJobListSearch.postValue(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
                        } catch (e: JSONException) {
                            mutableResponseJobListSearch.postValue(RequestState.Error(messageNoInternet))
                        }
                    } catch (e: Exception) {
                        mutableResponseJobListSearch.postValue(RequestState.Error(messageNoInternet))
                    }
                }

                4 -> {
                    if (isStarted) {
                        pageSearchNumberDelDirect = 1
                        responseJobListSearch = null
                        isMoreSearchRecordDelDirect = false
                    }
                    requestJobList.pageNumber = pageSearchNumberDelDirect
                    requestJobList.pageSize = 10
                    mutableResponseJobListSearch.postValue(RequestState.Loading)
                    try {
                        val response = repository.getJobDeliverDirectList(auth, requestJobList)
                        mutableResponseJobListSearch.postValue(handleJobListSearch(response))
                        isMoreSearchRecordDelDirect = response.body()?.paginationTotalInfo?.moreRecord ?: false
                        if (isMoreSearchRecordDelDirect == true) {
                            pageSearchNumberDelDirect++
                        }
                    } catch (e: HttpException) {
                        try {
                            mutableResponseJobListSearch.postValue(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
                        } catch (e: JSONException) {
                            mutableResponseJobListSearch.postValue(RequestState.Error(messageNoInternet))
                        }
                    } catch (e: Exception) {
                        mutableResponseJobListSearch.postValue(RequestState.Error(messageNoInternet))
                    }
                }

                5 -> {
                    if (isStarted) {
                        pageSearchNumberDirectPreparation = 1
                        responseJobListSearch = null
                        isMoreSearchRecordDirectPreparation = false
                    }
                    requestJobList.pageNumber = pageSearchNumberDirectPreparation
                    requestJobList.pageSize = 10
                    mutableResponseJobListSearch.postValue(RequestState.Loading)
                    try {
                        val response = repository.getJobDirectPreparation(auth, requestJobList)
                        mutableResponseJobListSearch.postValue(handleJobListSearch(response))
                        isMoreSearchRecordDirectPreparation = response.body()?.paginationTotalInfo?.moreRecord ?: false
                        if (isMoreSearchRecordDirectPreparation == true) {
                            pageSearchNumberDirectPreparation++
                        }
                    } catch (e: HttpException) {
                        try {
                            mutableResponseJobListSearch.postValue(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
                        } catch (e: JSONException) {
                            mutableResponseJobListSearch.postValue(RequestState.Error(messageNoInternet))
                        }
                    } catch (e: Exception) {
                        mutableResponseJobListSearch.postValue(RequestState.Error(messageNoInternet))
                    }
                }

                6 -> {
                    if (isStarted) {
                        pageSearchNumberHistory = 1
                        responseJobListSearch = null
                        isMoreSearchRecordHistory = false
                    }
                    requestJobList.pageNumber = pageSearchNumberHistory
                    requestJobList.pageSize = 10
                    mutableResponseJobListSearch.postValue(RequestState.Loading)
                    try {
                        val response = repository.getJobHistory(auth, requestJobList)
                        mutableResponseJobListSearch.postValue(handleJobListSearch(response))
                        isMoreSearchRecordHistory = response.body()?.paginationInfo?.moreRecord ?: false
                        if (isMoreSearchRecordHistory == true) {
                            pageSearchNumberHistory++
                        }
                    } catch (e: HttpException) {
                        try {
                            mutableResponseJobListSearch.postValue(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
                        } catch (e: JSONException) {
                            mutableResponseJobListSearch.postValue(RequestState.Error(messageNoInternet))
                        }
                    } catch (e: Exception) {
                        mutableResponseJobListSearch.postValue(RequestState.Error(messageNoInternet))
                    }
                }

                6 -> {
                    if (isStarted) {
                        pageSearchNumberDriverDelDirect = 1
                        responseJobListSearch = null
                        isMoreSearchRecordDriverDelDirect = false
                    }
                    requestJobList.pageNumber = pageSearchNumberDriverDelDirect
                    requestJobList.pageSize = 10
                    mutableResponseJobListSearch.postValue(RequestState.Loading)
                    try {
                        val response = repository.getJobDriverDeliverDirectList(auth, requestJobList)
                        mutableResponseJobListSearch.postValue(handleJobListSearch(response))
                        isMoreSearchRecordDriverDelDirect = response.body()?.paginationInfo?.moreRecord ?: false
                        if (isMoreSearchRecordDriverDelDirect == true) {
                            pageSearchNumberDriverDelDirect++
                        }
                    } catch (e: HttpException) {
                        try {
                            mutableResponseJobListSearch.postValue(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
                        } catch (e: JSONException) {
                            mutableResponseJobListSearch.postValue(RequestState.Error(messageNoInternet))
                        }
                    } catch (e: Exception) {
                        mutableResponseJobListSearch.postValue(RequestState.Error(messageNoInternet))
                    }
                }

                else -> {
                    if (isStarted) {
                        pageSearchNumber = 1
                        responseJobListSearch = null
                        isMoreSearchRecord = false
                    }
                    requestJobList.pageNumber = pageSearchNumber
                    requestJobList.pageSize = 10
                    mutableResponseJobListSearch.postValue(RequestState.Loading)
                    try {
                        val response = repository.getJobList(auth, requestJobList)
                        mutableResponseJobListSearch.postValue(handleJobListSearch(response))
                        isMoreSearchRecord = response.body()?.paginationTotalInfo?.moreRecord ?: false
                        if (isMoreSearchRecord == true) {
                            pageSearchNumber++
                        }
                    } catch (e: HttpException) {
                        try {
                            mutableResponseJobListSearch.postValue(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
                        } catch (e: JSONException) {
                            mutableResponseJobListSearch.postValue(RequestState.Error(messageNoInternet))
                        }
                    } catch (e: Exception) {
                        mutableResponseJobListSearch.postValue(RequestState.Error(messageNoInternet))
                    }
                }
            }
        }
    }

    private fun handleJobListSearch(response: Response<ResponseJobList>): RequestState<ResponseJobList?> {
        if (response.isSuccessful) {
            response.body()?.let {
                if (responseJobListSearch == null) responseJobListSearch = it else {
                    val oldData = responseJobListSearch?.data
                    val newData = it.data
                    oldData?.addAll(newData)
                }
            }
            return RequestState.Success(responseJobListSearch ?: response.body())
        } else {
            try {
                return RequestState.Error(response.errorBody()?.string()?.let { JSONObject(it).get("message") } as String)
            } catch (e: JSONException) {
                return (RequestState.Error(messageNoInternet))
            } catch (e: Exception) {
                return (RequestState.Error(messageNoInternet))
            }
        }
    }

    fun getJobSupplierProgressTotal(auth: String, requestJobSupplierTotal: RequestJobSupplierTotal): LiveData<RequestState<ResponseJobSupplierTotal>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.getJobSupplierProgressTotal(auth, requestJobSupplierTotal)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun getJobCombo(auth: String, subtitle: String): LiveData<RequestState<ResponseJobCombo>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.getJobCombo(auth, subtitle)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun getAssignCombo(auth: String, requestAssignCombo: RequestAssignCombo): LiveData<RequestState<ResponseAssignCombo>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.getAssignCombo(auth, requestAssignCombo)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun getAssignedDriver(auth: String, requestAssign: RequestAssign): LiveData<RequestState<ResponseAssignedDriver>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.getAssignedDriver(auth, requestAssign)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun getAssignCheck(auth: String, requestAssign: RequestAssign): LiveData<RequestState<ResponseAssignCheck>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.getAssignCheck(auth, requestAssign)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun postAssign(auth: String, requestAssign: RequestAssign): LiveData<RequestState<ResponseAssignPost>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.postAssign(auth, requestAssign)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun getDriverJobCheck(auth: String, requestDriverPickup: RequestDriverPickup): LiveData<RequestState<ResponseDriverPickupCheck>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.getDriverJobCheck(auth, requestDriverPickup)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun postDriverJobConfirm(auth: String, requestDriverPickup: RequestDriverPickup): LiveData<RequestState<ResponseDriverPickup>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.postDriverJobConfirm(auth, requestDriverPickup)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }
}