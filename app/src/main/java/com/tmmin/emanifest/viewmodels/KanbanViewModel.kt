package com.tmmin.emanifest.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.tmmin.emanifest.models.requests.kanban.cancel.RequestKanbanCancel
import com.tmmin.emanifest.models.requests.kanban.check.RequestKanbanCheck
import com.tmmin.emanifest.models.requests.kanban.create.RequestKanbanCreate
import com.tmmin.emanifest.models.requests.kanban.detail.RequestKanbanDetail
import com.tmmin.emanifest.models.requests.kanban.list.RequestKanbanList
import com.tmmin.emanifest.models.requests.kanban.total.RequestKanbanTotal
import com.tmmin.emanifest.models.responses.kanban.cancel.ResponseKanbanCancel
import com.tmmin.emanifest.models.responses.kanban.check.ResponseKanbanCheck
import com.tmmin.emanifest.models.responses.kanban.create.ResponseKanbanCreate
import com.tmmin.emanifest.models.responses.kanban.detail.ResponseKanbanDetail
import com.tmmin.emanifest.models.responses.kanban.list.ResponseKanbanList
import com.tmmin.emanifest.models.responses.kanban.manifest.ResponseKanbanManifestList
import com.tmmin.emanifest.models.responses.kanban.total.ResponseKanbanTotal
import com.tmmin.emanifest.network.RequestState
import com.tmmin.emanifest.repositories.KanbanRepository
import org.json.JSONException
import org.json.JSONObject
import retrofit2.HttpException

class KanbanViewModel() : ViewModel() {
    val messageNoInternet = "There was a internet connection issue. Please try again !"
    private val repository: KanbanRepository = KanbanRepository()

    fun getTotalKanban(auth: String, requestKanbanTotal: RequestKanbanTotal): LiveData<RequestState<ResponseKanbanTotal>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.getTotalKanban(auth, requestKanbanTotal)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun getDetailKanban(auth: String, requestKanbanDetail: RequestKanbanDetail): LiveData<RequestState<ResponseKanbanDetail>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.getDetailKanban(auth, requestKanbanDetail)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun getCheckKanban(auth: String, requestKanbanCheck: RequestKanbanCheck): LiveData<RequestState<ResponseKanbanCheck>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.getCheckKanban(auth, requestKanbanCheck)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun createKanban(auth: String, requestKanbanCreate: RequestKanbanCreate): LiveData<RequestState<ResponseKanbanCreate>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.createKanban(auth, requestKanbanCreate)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun postCancelKanban(auth: String, requestKanbanCancel: RequestKanbanCancel): LiveData<RequestState<ResponseKanbanCancel>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.postCancelKanban(auth, requestKanbanCancel)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun getListKanban(auth: String, requestKanbanList: RequestKanbanList): LiveData<RequestState<ResponseKanbanList>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.getListKanban(auth, requestKanbanList)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun getListKanbanManifest(auth: String, requestKanbanList: RequestKanbanList): LiveData<RequestState<ResponseKanbanManifestList>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.getListKanbanManifest(auth, requestKanbanList)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }
}