package com.tmmin.emanifest.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.tmmin.emanifest.models.requests.order.RequestOrderCombo
import com.tmmin.emanifest.models.requests.order.RequestOrderRoute
import com.tmmin.emanifest.models.requests.order.RequestOrderStatus
import com.tmmin.emanifest.models.responses.order.combo.ResponseOrderCombo
import com.tmmin.emanifest.models.responses.order.route.ResponseOrderRoute
import com.tmmin.emanifest.models.responses.order.status.ResponseOrderStatus
import com.tmmin.emanifest.network.RequestState
import com.tmmin.emanifest.repositories.OrderRepository
import org.json.JSONException
import org.json.JSONObject
import retrofit2.HttpException

class OrderViewModel() : ViewModel() {
    val messageNoInternet = "There was a internet connection issue. Please try again !"
    private val repository: OrderRepository = OrderRepository()

    fun getOrderCombo(auth: String, requestOrderCombo: RequestOrderCombo): LiveData<RequestState<ResponseOrderCombo>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.getOrderCombo(auth, requestOrderCombo)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun getOrderStatus(auth: String, requestOrderStatus: RequestOrderStatus): LiveData<RequestState<ResponseOrderStatus>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.getOrderStatus(auth, requestOrderStatus)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun getOrderRoute(auth: String, requestOrderRoute: RequestOrderRoute): LiveData<RequestState<ResponseOrderRoute>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.getOrderRoute(auth, requestOrderRoute)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }

    fun getOrderLastPosition(auth: String, requestOrderRoute: RequestOrderRoute): LiveData<RequestState<ResponseOrderRoute>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repository.getOrderLastPosition(auth, requestOrderRoute)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            try {
                emit(RequestState.Error(e.response()?.errorBody()?.string()?.let { JSONObject(it).get("message") } as String))
            } catch (e: JSONException) {
                emit(RequestState.Error(messageNoInternet))
            }
        } catch (e: Exception) {
            emit(RequestState.Error(messageNoInternet))
        }
    }
}