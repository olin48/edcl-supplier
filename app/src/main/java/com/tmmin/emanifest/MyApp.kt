package com.tmmin.emanifest

import android.app.Application
import com.mazenrashed.printooth.Printooth
import org.conscrypt.Conscrypt
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import java.security.Security

class MyApp : Application(), KodeinAware {

    override fun onCreate() {
        super.onCreate()
        Security.insertProviderAt(Conscrypt.newProvider(), 1)
        Printooth.init(this)
    }

    override val kodein = Kodein.lazy {
//        import(androidXModule(this@MVVMApplication))
//        bind() from singleton { ConnectionInterceptor(instance()) }
//        bind() from singleton { MyApi(instance()) }
//        bind() from singleton { PreferenceHelper(instance()) }
//        bind() from singleton { AuthRepository(instance()) }
//        bind() from provider { AuthFactory(instance()) }
    }
}