package com.tmmin.emanifest.dialogs

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.OnClickListener
import android.view.View.OnTouchListener
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.tmmin.emanifest.R
import com.tmmin.emanifest.databinding.DialogPickupBinding
import com.tmmin.emanifest.listeners.PickupListener

class DialogPickup(context: Context, pickupListener: PickupListener) : BottomSheetDialogFragment(), OnClickListener, OnTouchListener {
    private var dialogPickupBinding: DialogPickupBinding? = null
    private val binding get() = dialogPickupBinding!!
    var listener = pickupListener
    var thisContext = context
    var modeSelected = 0
    var modeComplete = 1
    var modeShortage = 2
    var modeOverflow = 3

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)
        setCancelable(false)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialogPickupBinding = DialogPickupBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnClose.setOnClickListener(this)
        binding.btnComplete.setOnClickListener(this)
        binding.btnShortage.setOnClickListener(this)
        binding.btnOverflow.setOnClickListener(this)
        binding.btnSwipe.setOnTouchListener(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        dialogPickupBinding = null
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btnClose -> dismiss()
            binding.btnComplete -> selectMode(modeComplete)
            binding.btnShortage -> selectMode(modeShortage)
            binding.btnOverflow -> selectMode(modeOverflow)
        }
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        when (v) {
            binding.btnSwipe -> {
                var initialX = 0f
                when (event?.action) {
                    MotionEvent.ACTION_DOWN -> {
                        initialX = v.x
                    }

                    MotionEvent.ACTION_MOVE -> {
                        val newX = event.rawX - v.width
                        if (newX > binding.tvDescSwipe.left && newX + v.width < binding.tvDescSwipe.right) {
                            v.x = newX
                        }
                    }

                    MotionEvent.ACTION_UP -> {
                        if (event.rawX >= binding.tvDescSwipe.right) {
                            v.x = initialX
                            submit()
                        } else {
                            v.x = initialX
                        }
                    }
                }
            }
        }
        return true
    }

    private fun selectMode(mode: Int) {
        when (mode) {
            modeComplete -> {
                modeSelected = modeComplete
                binding.ivComplete.setImageResource(R.drawable.ic_checked)
                binding.ivShortage.setImageResource(R.drawable.ic_unchecked)
                binding.ivOverflow.setImageResource(R.drawable.ic_unchecked)
                binding.etNote.setText("COMPLETE")
            }

            modeShortage -> {
                modeSelected = modeShortage
                binding.ivComplete.setImageResource(R.drawable.ic_unchecked)
                binding.ivShortage.setImageResource(R.drawable.ic_checked)
                binding.ivOverflow.setImageResource(R.drawable.ic_unchecked)
                binding.etNote.setText("SHORTAGE")
            }

            modeOverflow -> {
                modeSelected = modeOverflow
                binding.ivComplete.setImageResource(R.drawable.ic_unchecked)
                binding.ivShortage.setImageResource(R.drawable.ic_unchecked)
                binding.ivOverflow.setImageResource(R.drawable.ic_checked)
                binding.etNote.setText("OVERFLOW")
            }
        }
    }

    private fun submit() {
        var message = binding.etNote.text.toString()
        if (modeSelected == 0) {
            Toast.makeText(thisContext, "Please Select a Status First !", Toast.LENGTH_SHORT).show()
        } else if (message.isEmpty()) {
            Toast.makeText(thisContext, "Please Put a Note First !", Toast.LENGTH_SHORT).show()
        } else {
            listener.onClicked(modeSelected, message)
            dismiss()
        }
    }
}