package com.tmmin.emanifest.dialogs

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import com.tmmin.emanifest.R
import com.tmmin.emanifest.listeners.OptionListener
import kotlinx.android.synthetic.main.dialog_info.btnCancel
import kotlinx.android.synthetic.main.dialog_info.btnYes
import kotlinx.android.synthetic.main.dialog_info.civHeader
import kotlinx.android.synthetic.main.dialog_info.tvInfoDesc
import kotlinx.android.synthetic.main.dialog_info.tvInfoHeader

class DialogInfo(activity: Activity, mode: Int, infoTitle: String, infoDesc: String, optionListener: OptionListener) : Dialog(activity) {
    companion object{
        val MODE_NORMAL = 0
        val MODE_SUCCESS = 1
        val MODE_FAILED = 2
    }
    var optionListener = optionListener
    var infoTitle = infoTitle
    var infoDesc = infoDesc
    var activity = activity
    var mode = mode

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_info)
        setCancelable(false)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)

        tvInfoHeader.setText(infoTitle)
        tvInfoDesc.setText(infoDesc)
        if (mode == MODE_NORMAL) {
            civHeader.setImageResource(R.mipmap.ic_launcher)
        } else if (mode == MODE_SUCCESS) {
            civHeader.setImageResource(R.drawable.ic_success)
        } else if (mode == MODE_FAILED) {
            civHeader.setImageResource(R.drawable.ic_failed)
        }
        btnCancel.setOnClickListener {
            optionListener.onClicked(false)
            dismiss()
        }
        btnYes.setOnClickListener {
            optionListener.onClicked(true)
            dismiss()
        }
    }
}