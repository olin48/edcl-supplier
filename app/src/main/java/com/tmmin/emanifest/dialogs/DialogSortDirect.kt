package com.tmmin.emanifest.dialogs

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.tmmin.emanifest.R
import com.tmmin.emanifest.adapters.SortDirectAdapter
import com.tmmin.emanifest.databinding.DialogSortDirectBinding
import com.tmmin.emanifest.listeners.AssignListener
import com.tmmin.emanifest.listeners.SortDirectListener


class DialogSortDirect(context: Context, var list: ArrayList<String>, var sortBySelected: Int, var sortListener: SortDirectListener) : Dialog(context), View.OnClickListener {
    private lateinit var binding: DialogSortDirectBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.dialog_sort_direct, null, false)
        setContentView(binding.root)
        setup()
    }

    private fun setup() {
        setCancelable(true)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        var sortDirectAdapter = SortDirectAdapter(context, list, sortBySelected, object : AssignListener {
            override fun onClicked(index: Int) {
                sortBySelected = index
            }
        })
        binding.rcySort.layoutManager = LinearLayoutManager(context)
        binding.rcySort.adapter = sortDirectAdapter
        binding.tvRemove.setOnClickListener(this)
        binding.btnOK.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btnOK -> {
                sortListener.onClicked(true, sortBySelected)
                dismiss()
            }

            binding.tvRemove -> {
                sortBySelected = 0
                sortListener.onClicked(false, sortBySelected)
                dismiss()
            }
        }
    }
}