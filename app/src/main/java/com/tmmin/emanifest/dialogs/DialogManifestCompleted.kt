package com.tmmin.emanifest.dialogs

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import com.tmmin.emanifest.R
import kotlinx.android.synthetic.main.dialog_manifest_completed.btnDone

class DialogManifestCompleted(activity: Activity) : Dialog(activity) {
    var activity = activity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_manifest_completed)
        setCancelable(false)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        btnDone.setOnClickListener {
            activity?.let {
                it.onBackPressed()
            }
            dismiss()
        }
    }
}