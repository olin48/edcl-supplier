package com.tmmin.emanifest.dialogs

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.tmmin.emanifest.R
import com.tmmin.emanifest.helpers.PreferenceHelper
import com.tmmin.emanifest.models.responses.settings.plant.DataItem
import com.tmmin.emanifest.util.Constant
import kotlinx.android.synthetic.main.dialog_kanban.btnOK
import kotlinx.android.synthetic.main.dialog_plant.spPlant

class DialogPlant(activity: Activity, dataItem: List<DataItem>) : Dialog(activity) {
    private lateinit var sharedPref: PreferenceHelper
    var activity = activity
    var items = dataItem
    var plantSelected = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_plant)
        setCancelable(false)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        sharedPref = PreferenceHelper(activity)
        val adapter = ArrayAdapter(activity, android.R.layout.simple_spinner_item, items)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spPlant.adapter = adapter
        spPlant.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                plantSelected = position
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
        btnOK.setOnClickListener {
            savePlant()
        }
    }

    private fun savePlant() {
        with(sharedPref) {
            put(Constant.PROFILE_SUPPLIER_PLANT, items.get(plantSelected).value)
        }
        dismiss()
    }
}