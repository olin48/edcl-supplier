package com.tmmin.emanifest.dialogs

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.tmmin.emanifest.R
import com.tmmin.emanifest.databinding.DialogPrintingBinding
import kotlinx.android.synthetic.main.dialog_manifest_completed.btnDone

class DialogPrinting(activity: Activity) : Dialog(activity), View.OnClickListener {
    private lateinit var binding: DialogPrintingBinding
    var activity = activity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.dialog_printing, null, false)
        setContentView(binding.root)
        setup()
    }

    private fun setup() {
        setCancelable(false)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        Glide.with(activity).asGif().load(R.raw.gif_printer).into(binding.ivPrinter)
        binding.btnDone.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btnDone -> {
                dismiss()
            }
        }
    }
}