package com.tmmin.emanifest.dialogs

import android.app.Dialog
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.tmmin.emanifest.R
import com.tmmin.emanifest.databinding.DialogTestBinding
import java.io.File

class DialogSlipFile(context: Context, var file: File) : Dialog(context) {
    private var context = context;
    private lateinit var binding: DialogTestBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.dialog_test, null, false)
        setContentView(binding.root)
        setup()
    }

    private fun setup() {
        setCancelable(true)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        Glide.with(context).load(file).into(binding.ivPreview)
    }
}