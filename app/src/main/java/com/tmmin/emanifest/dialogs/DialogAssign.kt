package com.tmmin.emanifest.dialogs

import android.R
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.tmmin.emanifest.databinding.DialogAssignBinding
import com.tmmin.emanifest.listeners.AssignListener

class DialogAssign(context: Context, items: List<String>, driverName: String, assignListener: AssignListener) : BottomSheetDialogFragment() {
    private var dialogAssignBinding: DialogAssignBinding? = null
    private val binding get() = dialogAssignBinding!!
    var listener = assignListener
    var thisContext = context
    var selected = 0
    var items = items
    var driverSelected = driverName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, com.tmmin.emanifest.R.style.CustomBottomSheetDialogTheme)
        setCancelable(false)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialogAssignBinding = DialogAssignBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = ArrayAdapter(thisContext, R.layout.simple_spinner_item, items)
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
        binding.spType.adapter = adapter
        binding.spType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                selected = position
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }

        binding.btnClose.setOnClickListener {
            dismiss()
        }
        binding.btnSubmit.setOnClickListener {
            if (selected != 0) {
                listener.onClicked(selected - 1)
                dismiss()
            }
        }
        setup()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        dialogAssignBinding = null
    }

    private fun setup() {
        if (!driverSelected.isEmpty()) {
            var indexDriver = -1
            binding.tvTitle.setText("Edit Assign Driver")
            indexDriver = items.indexOfFirst { it.equals(driverSelected, true) }
            if (indexDriver != -1) {
                binding.spType.setSelection(indexDriver)
            }
        }
    }
}