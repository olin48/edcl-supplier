package com.tmmin.emanifest.dialogs

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import com.tmmin.emanifest.R
import com.tmmin.emanifest.listeners.OptionListener
import kotlinx.android.synthetic.main.dialog_kanban.btnCancel
import kotlinx.android.synthetic.main.dialog_kanban.btnOK
import kotlinx.android.synthetic.main.dialog_kanban.btnYes
import kotlinx.android.synthetic.main.dialog_kanban.layoutButtonBasic
import kotlinx.android.synthetic.main.dialog_kanban.layoutButtonMultiple
import kotlinx.android.synthetic.main.dialog_kanban.tvTitle

class DialogKanban(activity: Activity, mode: Int, infoTitle: String, optionListener: OptionListener) : Dialog(activity) {
    companion object {
        val MODE_BASIC = 0
        val MODE_MULTIPLE = 1
        val MODE_ASSIGN = 2
    }

    var optionListener = optionListener
    var infoTitle = infoTitle
    var activity = activity
    var mode = mode

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_kanban)
        setCancelable(false)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)

        tvTitle.setText(infoTitle)
        if (mode == MODE_ASSIGN) {
            layoutButtonBasic.visibility = View.GONE
            layoutButtonMultiple.visibility = View.VISIBLE
            btnYes.setText("Confirm")
        } else if (mode == MODE_MULTIPLE) {
            layoutButtonBasic.visibility = View.GONE
            layoutButtonMultiple.visibility = View.VISIBLE
        } else {
            layoutButtonBasic.visibility = View.VISIBLE
            layoutButtonMultiple.visibility = View.GONE
        }
        btnOK.setOnClickListener {
            optionListener.onClicked(false)
            dismiss()
        }
        btnCancel.setOnClickListener {
            optionListener.onClicked(false)
            dismiss()
        }
        btnYes.setOnClickListener {
            optionListener.onClicked(true)
            dismiss()
        }
    }
}