package com.tmmin.emanifest.dialogs

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.tmmin.emanifest.R
import com.tmmin.emanifest.adapters.SortAdapter
import com.tmmin.emanifest.databinding.DialogSortBinding
import com.tmmin.emanifest.listeners.AssignListener
import com.tmmin.emanifest.listeners.SortListener
import com.tmmin.emanifest.models.others.ModelCombo
import com.tmmin.emanifest.models.responses.job.combo.DataItem
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale


class DialogSort(context: Context, var list: ArrayList<DataItem>, var modelCombo: ModelCombo, var sortListener: SortListener) : Dialog(context), View.OnClickListener {
    private lateinit var binding: DialogSortBinding
    var calendarFrom = Calendar.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.dialog_sort, null, false)
        setContentView(binding.root)
        setup()
    }

    private fun setup() {
        setCancelable(true)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        binding.etDateFrom.setText(modelCombo.dateFromSelected)
        binding.etDateTo.setText(modelCombo.dateToSelected)
        var sortAdapter = SortAdapter(context, list, modelCombo.sortBySelected, object : AssignListener {
            override fun onClicked(index: Int) {
                modelCombo.sortBySelected = index
            }
        })
        binding.rcySort.layoutManager = LinearLayoutManager(context)
        binding.rcySort.adapter = sortAdapter
        binding.etDateFrom.setOnClickListener(this)
        binding.etDateTo.setOnClickListener(this)
        binding.tvRemove.setOnClickListener(this)
        binding.btnOK.setOnClickListener(this)
    }

    private fun showDateFromPicker(onDateSelected: (day: Int, month: Int, year: Int) -> Unit) {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val datePickerDialog = DatePickerDialog(context, { _, selectedYear, selectedMonth, selectedDay ->
            onDateSelected(selectedDay, selectedMonth, selectedYear)
        }, year, month, day)
        datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
        datePickerDialog.show()
    }

    private fun showDateToPicker(onDateSelected: (day: Int, month: Int, year: Int) -> Unit) {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val datePickerDialog = DatePickerDialog(context, { _, selectedYear, selectedMonth, selectedDay ->
            onDateSelected(selectedDay, selectedMonth, selectedYear)
        }, year, month, day)
        datePickerDialog.datePicker.minDate = calendarFrom.timeInMillis
        datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
        datePickerDialog.show()
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.etDateFrom -> {
                showDateFromPicker { day, month, year ->
                    calendarFrom.set(year, month, day)
                    val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                    modelCombo.dateFromSelected = dateFormat.format(calendarFrom.time)
                    binding.etDateFrom.setText(modelCombo.dateFromSelected)
                }
            }

            binding.etDateTo -> {
                if (modelCombo.dateFromSelected.isEmpty()) {
                    Toast.makeText(context, context.getString(R.string.InfoSortDate), Toast.LENGTH_SHORT).show()
                } else {
                    showDateToPicker { day, month, year ->
                        val calendar = Calendar.getInstance()
                        calendar.set(year, month, day)
                        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                        modelCombo.dateToSelected = dateFormat.format(calendar.time)
                        binding.etDateTo.setText(modelCombo.dateToSelected)
                    }
                }
            }

            binding.btnOK -> {
                sortListener.onClicked(true, modelCombo.sortBySelected, modelCombo.dateFromSelected, modelCombo.dateToSelected)
                dismiss()
            }

            binding.tvRemove -> {
                modelCombo.sortBySelected = 3
                modelCombo.dateToSelected = ""
                modelCombo.dateFromSelected = ""
                binding.etDateFrom.setText(modelCombo.dateFromSelected)
                binding.etDateTo.setText(modelCombo.dateToSelected)
                sortListener.onClicked(false, modelCombo.sortBySelected, modelCombo.dateFromSelected, modelCombo.dateToSelected)
                dismiss()
            }
        }
    }
}