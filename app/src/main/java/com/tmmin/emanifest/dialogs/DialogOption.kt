package com.tmmin.emanifest.dialogs

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import com.tmmin.emanifest.R
import com.tmmin.emanifest.databinding.DialogOptionBinding
import com.tmmin.emanifest.listeners.OptionListener

class DialogOption(context: Context, infoTitle: String, optionListener: OptionListener) : Dialog(context), View.OnClickListener {
    private lateinit var binding: DialogOptionBinding
    var optionListener = optionListener
    var infoTitle = infoTitle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.dialog_option, null, false)
        setContentView(binding.root)
        setup()
    }

    private fun setup() {
        window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setCancelable(false)
        binding.tvInfoHeader.setText(infoTitle)
        binding.btnCancel.setOnClickListener(this)
        binding.btnYes.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btnCancel -> {
                dismiss()
                optionListener.onClicked(false)
            }

            binding.btnYes -> {
                dismiss()
                optionListener.onClicked(true)
            }
        }
    }
}