package com.tmmin.emanifest.dialogs

import android.app.Activity
import android.app.Dialog
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.google.zxing.BarcodeFormat
import com.google.zxing.WriterException
import com.google.zxing.qrcode.QRCodeWriter
import com.mazenrashed.printooth.Printooth
import com.mazenrashed.printooth.ui.ScanningActivity
import com.tmmin.emanifest.R
import com.tmmin.emanifest.databinding.DialogSlipBinding
import com.tmmin.emanifest.helpers.PreferenceHelper
import com.tmmin.emanifest.listeners.OptionListener
import com.tmmin.emanifest.listeners.SlipPreviewListener
import com.tmmin.emanifest.models.others.ModelSlip
import com.tmmin.emanifest.util.Constant

class DialogSlip(activity: Activity, modelSlip: ModelSlip, listener: SlipPreviewListener) : Dialog(activity), View.OnClickListener {
    private lateinit var binding: DialogSlipBinding
    lateinit var sharedPref: PreferenceHelper
    var slipPreviewListener = listener
    var modelSlip = modelSlip
    var context = activity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.dialog_slip, null, false)
        setContentView(binding.root)
        setup()
    }

    private fun setup() {
        setCancelable(true)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        var qrCodeBitmap = generateQRCode(modelSlip.kanbanId, 500, 500)
        binding.ivQRCode.setImageBitmap(qrCodeBitmap)
        binding.btnCancel.setOnClickListener(this)
        binding.btnBrowse.setOnClickListener(this)
        binding.btnPrint.setOnClickListener(this)
        sharedPref = PreferenceHelper(context)
        var mode = sharedPref.getInt(Constant.SET_PAPER)
        if (mode == 0) {
            binding.tvPaper.setText("Sticker")
        } else {
            binding.tvPaper.setText("Continous")
        }
        binding.tvSuppCodePlant.setText(modelSlip.supCodePlant)
        binding.tvDockCode.setText(modelSlip.dockCode)
        binding.tvSupDockName.setText(modelSlip.suppName)
        binding.tvSupDockCode.setText(modelSlip.suppDockCode)
        binding.tvOrderNumber.setText(modelSlip.orderNumber)
        binding.tvManifestNumber.setText(modelSlip.manifestNumber)
        binding.tvRoute.setText(modelSlip.routeCycle)
        binding.tvPlaneNumber.setText(modelSlip.planeNumber)
        binding.tvConveyance.setText(modelSlip.conveyance)
        binding.tvKanbanCode.setText(modelSlip.kanbanCode)
        initDevices()
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btnCancel -> {
                slipPreviewListener.onCanceled()
                dismiss()
            }

            binding.btnBrowse -> {
                if (isBluetoothEnabled()) {
                    if (Printooth.hasPairedPrinter()) {
                        Printooth.removeCurrentPrinter()
                    } else {
                        context.startActivityForResult(Intent(context, ScanningActivity::class.java), ScanningActivity.SCANNING_FOR_PRINTER)
                        initDevices()
                    }
                } else {
                    var dialogInfo = DialogInfo(context, DialogInfo.MODE_FAILED, context.getString(R.string.MessageError), context.getString(R.string.InfoReqBluetooh), object : OptionListener {
                        override fun onClicked(isApproved: Boolean) {}
                    })
                    dialogInfo.show()
                }
            }

            binding.btnPrint -> {
                if (Printooth.getPairedPrinter() == null) {
                    Toast.makeText(context, context.getString(R.string.ErrorNoPrinter), Toast.LENGTH_SHORT).show()
                } else {
                    binding.layoutContent.post {
                        val bitmap = Bitmap.createBitmap(binding.layoutContent.width, binding.layoutContent.height, Bitmap.Config.ARGB_8888)
                        val canvas = Canvas(bitmap)
                        binding.layoutContent.draw(canvas)
                        slipPreviewListener.onConfirmed(bitmap)
                        dismiss()
                    }
                }
            }
        }
    }

    fun generateQRCode(text: String, width: Int, height: Int): Bitmap? {
        val writer = QRCodeWriter()
        return try {
            val bitMatrix = writer.encode(text, BarcodeFormat.QR_CODE, width, height)
            val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
            for (x in 0 until width) {
                for (y in 0 until height) {
                    bitmap.setPixel(x, y, if (bitMatrix[x, y]) Color.BLACK else Color.TRANSPARENT)
                }
            }
            bitmap
        } catch (e: WriterException) {
            e.printStackTrace()
            null
        }
    }

    fun initDevices() {
        if (Printooth.getPairedPrinter() == null) {
            binding.tvDevice.setText("No Printer Selected")
        } else {
            if (Printooth.hasPairedPrinter()) {
                binding.tvDevice.setText(Printooth.getPairedPrinter()?.name ?: "-")
            } else {
                binding.tvDevice.setText("No Printer Selected")
            }
        }
    }

    fun isBluetoothEnabled(): Boolean {
        val bluetoothAdapter: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()
        return bluetoothAdapter?.isEnabled == true
    }
}