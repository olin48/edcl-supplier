package com.tmmin.emanifest.network

import com.tmmin.emanifest.models.responses.settings.plant.ResponseSettingPlant
import com.tmmin.emanifest.util.ConstantAPI
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface SettingsApiService {
    @GET(ConstantAPI.API_SETTING_PLANT)
    suspend fun getSupplierPlant(@Header("Authorization") authorization: String, @Query("supplierCd") supplierCd: String, @Query("subTitle") subTitle: String): Response<ResponseSettingPlant>
}