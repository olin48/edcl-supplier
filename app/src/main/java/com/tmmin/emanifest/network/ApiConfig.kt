package com.tmmin.emanifest.network

import com.tmmin.emanifest.BuildConfig
import com.tmmin.emanifest.util.ConstantAPI
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.X509TrustManager

class ApiConfig {
    companion object {
        val timeoutSecond: Long = 60

        private fun getSSLSocketFactory(trustManager: X509TrustManager): SSLSocketFactory {
            val sslContext = SSLContext.getInstance("TLS")
            sslContext.init(null, arrayOf(trustManager), SecureRandom())
            return sslContext.socketFactory
        }

        private fun getTrustManager(): X509TrustManager {
            val trustManager = object : X509TrustManager {
                override fun checkClientTrusted(chain: Array<out X509Certificate>?, authType: String?) {

                }

                override fun checkServerTrusted(chain: Array<out X509Certificate>?, authType: String?) {
                    try {
                        chain?.let {
                            for (cert in it) {
                                cert.checkValidity()
                            }
                        }
                    } catch (e: CertificateException) {
                        throw CertificateException("Invalid certificate: ${e.message}")
                    }
                }

                override fun getAcceptedIssuers(): Array<X509Certificate> {
                    return arrayOf()
                }
            }
            return trustManager
        }

        fun getAuthService(): AuthApiService {
            val trustManager = getTrustManager()
            val sslSocketFactory = getSSLSocketFactory(trustManager)
            val loggingInterceptor = if (BuildConfig.DEBUG) HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
            else HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE)
            val client = OkHttpClient.Builder().sslSocketFactory(sslSocketFactory, trustManager).connectTimeout(timeoutSecond, TimeUnit.SECONDS).writeTimeout(timeoutSecond, TimeUnit.SECONDS).readTimeout(timeoutSecond, TimeUnit.SECONDS).addInterceptor(loggingInterceptor).build()
            val retrofit = Retrofit.Builder().baseUrl(BuildConfig.BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(client).build()
            return retrofit.create(AuthApiService::class.java)
        }

        fun getSettingsService(): SettingsApiService {
            val trustManager = getTrustManager()
            val sslSocketFactory = getSSLSocketFactory(trustManager)
            val loggingInterceptor = if (BuildConfig.DEBUG) HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
            else HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE)
            val client = OkHttpClient.Builder().sslSocketFactory(sslSocketFactory, trustManager).connectTimeout(timeoutSecond, TimeUnit.SECONDS).writeTimeout(timeoutSecond, TimeUnit.SECONDS).readTimeout(timeoutSecond, TimeUnit.SECONDS).addInterceptor(loggingInterceptor).build()
            val retrofit = Retrofit.Builder().baseUrl(BuildConfig.BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(client).build()
            return retrofit.create(SettingsApiService::class.java)
        }

        fun getJobService(): JobApiService {
            val trustManager = getTrustManager()
            val sslSocketFactory = getSSLSocketFactory(trustManager)
            val loggingInterceptor = if (BuildConfig.DEBUG) HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
            else HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE)
            val client = OkHttpClient.Builder().sslSocketFactory(sslSocketFactory, trustManager).connectTimeout(timeoutSecond, TimeUnit.SECONDS).writeTimeout(timeoutSecond, TimeUnit.SECONDS).readTimeout(timeoutSecond, TimeUnit.SECONDS).addInterceptor(loggingInterceptor).build()
            val retrofit = Retrofit.Builder().baseUrl(BuildConfig.BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(client).build()
            return retrofit.create(JobApiService::class.java)
        }

        fun getManifestService(): ManifestApiService {
            val trustManager = getTrustManager()
            val sslSocketFactory = getSSLSocketFactory(trustManager)
            val loggingInterceptor = if (BuildConfig.DEBUG) HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
            else HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE)
            val client = OkHttpClient.Builder().sslSocketFactory(sslSocketFactory, trustManager).connectTimeout(timeoutSecond, TimeUnit.SECONDS).writeTimeout(timeoutSecond, TimeUnit.SECONDS).readTimeout(timeoutSecond, TimeUnit.SECONDS).addInterceptor(loggingInterceptor).build()
            val retrofit = Retrofit.Builder().baseUrl(BuildConfig.BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(client).build()
            return retrofit.create(ManifestApiService::class.java)
        }

        fun getKanbanService(): KanbanApiService {
            val trustManager = getTrustManager()
            val sslSocketFactory = getSSLSocketFactory(trustManager)
            val loggingInterceptor = if (BuildConfig.DEBUG) HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
            else HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE)
            val client = OkHttpClient.Builder().sslSocketFactory(sslSocketFactory, trustManager).connectTimeout(timeoutSecond, TimeUnit.SECONDS).writeTimeout(timeoutSecond, TimeUnit.SECONDS).readTimeout(timeoutSecond, TimeUnit.SECONDS).addInterceptor(loggingInterceptor).build()
            val retrofit = Retrofit.Builder().baseUrl(BuildConfig.BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(client).build()
            return retrofit.create(KanbanApiService::class.java)
        }

        fun getPartService(): PartApiService {
            val trustManager = getTrustManager()
            val sslSocketFactory = getSSLSocketFactory(trustManager)
            val loggingInterceptor = if (BuildConfig.DEBUG) HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
            else HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE)
            val client = OkHttpClient.Builder().sslSocketFactory(sslSocketFactory, trustManager).connectTimeout(timeoutSecond, TimeUnit.SECONDS).writeTimeout(timeoutSecond, TimeUnit.SECONDS).readTimeout(timeoutSecond, TimeUnit.SECONDS).addInterceptor(loggingInterceptor).build()
            val retrofit = Retrofit.Builder().baseUrl(BuildConfig.BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(client).build()
            return retrofit.create(PartApiService::class.java)
        }

        fun getSKIDService(): SKIDApiService {
            val trustManager = getTrustManager()
            val sslSocketFactory = getSSLSocketFactory(trustManager)
            val loggingInterceptor = if (BuildConfig.DEBUG) HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
            else HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE)
            val client = OkHttpClient.Builder().sslSocketFactory(sslSocketFactory, trustManager).connectTimeout(timeoutSecond, TimeUnit.SECONDS).writeTimeout(timeoutSecond, TimeUnit.SECONDS).readTimeout(timeoutSecond, TimeUnit.SECONDS).addInterceptor(loggingInterceptor).build()
            val retrofit = Retrofit.Builder().baseUrl(BuildConfig.BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(client).build()
            return retrofit.create(SKIDApiService::class.java)
        }

        fun getOrderService(): OrderApiService {
            val trustManager = getTrustManager()
            val sslSocketFactory = getSSLSocketFactory(trustManager)
            val loggingInterceptor = if (BuildConfig.DEBUG) HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
            else HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE)
            val client = OkHttpClient.Builder().sslSocketFactory(sslSocketFactory, trustManager).connectTimeout(timeoutSecond, TimeUnit.SECONDS).writeTimeout(timeoutSecond, TimeUnit.SECONDS).readTimeout(timeoutSecond, TimeUnit.SECONDS).addInterceptor(loggingInterceptor).build()
            val retrofit = Retrofit.Builder().baseUrl(BuildConfig.BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(client).build()
            return retrofit.create(OrderApiService::class.java)
        }
    }
}