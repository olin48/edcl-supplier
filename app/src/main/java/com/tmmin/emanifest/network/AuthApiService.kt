package com.tmmin.emanifest.network

import com.tmmin.emanifest.models.requests.auth.RequestLogin
import com.tmmin.emanifest.models.requests.auth.RequestLogout
import com.tmmin.emanifest.models.requests.auth.RequestRefreshToken
import com.tmmin.emanifest.models.responses.auth.logout.ResponseLogout
import com.tmmin.emanifest.models.responses.auth.profile.ResponseSupplierInfo
import com.tmmin.emanifest.models.responses.auth.supplierLogin.ResponseLoginSupplier
import com.tmmin.emanifest.models.responses.dockCode.ResponseDockCode
import com.tmmin.emanifest.util.ConstantAPI
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Query

interface AuthApiService {
    @POST(ConstantAPI.API_AUTH_LOGIN_SUPPLIER)
    suspend fun loginSupplier(@Body requestLogin: RequestLogin): ResponseLoginSupplier

    @GET(ConstantAPI.API_AUTH_INFO)
    suspend fun getSupplierInfo(@Header("Authorization") authorization: String, @Query("supplierCd") supplierCd: String, @Query("supplierPlant") supplierPlant: String, @Query("subTitle") subTitle: String): ResponseSupplierInfo

    @POST(ConstantAPI.API_AUTH_LOGOUT)
    suspend fun logout(@Body requestLogout: RequestLogout): ResponseLogout

    @POST(ConstantAPI.API_AUTH_REFRESH_TOKEN)
    suspend fun refreshToken(@Header("Authorization") authorization: String, @Body requestRefreshToken: RequestRefreshToken): ResponseLoginSupplier

    @GET(ConstantAPI.API_AUTH_DOCK_CODE)
    suspend fun getSettingDockCode(@Header("Authorization") authorization: String, @Query("subTitle") subTitle: String): ResponseDockCode
}