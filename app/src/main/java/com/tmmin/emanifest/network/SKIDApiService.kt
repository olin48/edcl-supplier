package com.tmmin.emanifest.network

import com.tmmin.emanifest.models.requests.skid.RequestSKIDCreate
import com.tmmin.emanifest.models.responses.skid.create.ResponseSKIDCreate
import com.tmmin.emanifest.models.responses.skid.detail.ResponseSKIDDetail
import com.tmmin.emanifest.models.responses.skid.list.ResponseSKIDList
import com.tmmin.emanifest.util.ConstantAPI
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Query

interface SKIDApiService {
    @GET(ConstantAPI.API_SKID_LIST)
    suspend fun getListSKID(@Header("Authorization") authorization: String, @Query("manifestNo") deliveryNo: String, @Query("subTitle") supplierCd: String): ResponseSKIDList

    @POST(ConstantAPI.API_SKID_CREATE)
    suspend fun createSKID(@Header("Authorization") authorization: String, @Body requestSKIDCreate: RequestSKIDCreate): ResponseSKIDCreate

    @GET(ConstantAPI.API_SKID_DETAIL)
    suspend fun getDetailSKID(@Header("Authorization") authorization: String, @Query("skidNo") skidNo: String, @Query("subTitle") subTitle: String): ResponseSKIDDetail
}