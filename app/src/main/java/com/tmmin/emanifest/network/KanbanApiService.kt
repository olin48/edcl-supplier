package com.tmmin.emanifest.network

import com.tmmin.emanifest.models.requests.kanban.cancel.RequestKanbanCancel
import com.tmmin.emanifest.models.requests.kanban.create.RequestKanbanCreate
import com.tmmin.emanifest.models.responses.kanban.cancel.ResponseKanbanCancel
import com.tmmin.emanifest.models.responses.kanban.check.ResponseKanbanCheck
import com.tmmin.emanifest.models.responses.kanban.create.ResponseKanbanCreate
import com.tmmin.emanifest.models.responses.kanban.detail.ResponseKanbanDetail
import com.tmmin.emanifest.models.responses.kanban.list.ResponseKanbanList
import com.tmmin.emanifest.models.responses.kanban.manifest.ResponseKanbanManifestList
import com.tmmin.emanifest.models.responses.kanban.total.ResponseKanbanTotal
import com.tmmin.emanifest.util.ConstantAPI
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Query

interface KanbanApiService {
    @GET(ConstantAPI.API_KANBAN_TOTAL)
    suspend fun getTotalKanban(
        @Header("Authorization") authorization: String,
        @Query("ManifestNo") manifestNo: String,
    ): ResponseKanbanTotal

    @GET(ConstantAPI.API_KANBAN_DETAIL)
    suspend fun getDetailKanban(@Header("Authorization") authorization: String, @Query("supplierCd") supplierCd: String, @Query("supplierPlant") supplierPlant: String, @Query("kanbanId") kanbanId: String, @Query("kanbanCd") kanbanCd: String, @Query("subTitle") subTitle: String): ResponseKanbanDetail

    @GET(ConstantAPI.API_KANBAN_CHECK)
    suspend fun getCheckKanban(@Header("Authorization") authorization: String, @Query("manifestNo") manifestNo: String, @Query("itemNo") itemNo: Int, @Query("seqNo") seqNo: Int, @Query("skidNo") skidNo: String, @Query("qty") qty: Int, @Query("processId") processId: Long, @Query("subTitle") supplierCd: String): ResponseKanbanCheck

    @POST(ConstantAPI.API_KANBAN_ADD)
    suspend fun createKanban(@Header("Authorization") authorization: String, @Body requestKanbanCreate: RequestKanbanCreate): ResponseKanbanCreate

    @POST(ConstantAPI.API_KANBAN_CANCEL)
    suspend fun postCancelKanban(@Header("Authorization") authorization: String, @Body requestKanbanCancel: RequestKanbanCancel): ResponseKanbanCancel

    @GET(ConstantAPI.API_KANBAN_LIST)
    suspend fun getListKanban(@Header("Authorization") authorization: String, @Query("manifestNo") manifestNo: String, @Query("itemNo") itemNo: String, @Query("skidNo") skidNo: String, @Query("subTitle") supplierCd: String): ResponseKanbanList

    @GET(ConstantAPI.API_KANBAN_MANIFEST_LIST)
    suspend fun getListKanbanManifest(@Header("Authorization") authorization: String, @Query("manifestNo") manifestNo: String, @Query("subTitle") supplierCd: String): ResponseKanbanManifestList
}