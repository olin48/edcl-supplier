package com.tmmin.emanifest.network

import com.tmmin.emanifest.models.requests.assign.RequestAssign
import com.tmmin.emanifest.models.requests.job.driver.RequestDriverPickup
import com.tmmin.emanifest.models.responses.assign.assigned.ResponseAssignedDriver
import com.tmmin.emanifest.models.responses.assign.check.ResponseAssignCheck
import com.tmmin.emanifest.models.responses.assign.combo.ResponseAssignCombo
import com.tmmin.emanifest.models.responses.assign.post.ResponseAssignPost
import com.tmmin.emanifest.models.responses.job.combo.ResponseJobCombo
import com.tmmin.emanifest.models.responses.job.current.ResponseJobSupplierTotal
import com.tmmin.emanifest.models.responses.job.driver.check.ResponseDriverPickupCheck
import com.tmmin.emanifest.models.responses.job.driver.post.ResponseDriverPickup
import com.tmmin.emanifest.models.responses.job.list.ResponseJobList
import com.tmmin.emanifest.util.ConstantAPI
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Query

interface JobApiService {
    @GET(ConstantAPI.API_JOB_HISTORY)
    suspend fun getJobHistory(@Header("Authorization") authorization: String, @Query("supplierCd") supplierCd: String, @Query("supplierPlant") supplierPlant: String, @Query("pickupDtFrom") pickupDtFrom: String, @Query("pickupDtTo") PickupDtTo: String, @Query("sortBy") sortBy: String, @Query("keyword") keyword: String, @Query("pageNumber") pageNumber: Int, @Query("pageSize") pageSize: Int, @Query("subTitle") subTitle: String): Response<ResponseJobList>

    @GET(ConstantAPI.API_JOB_SUPPLIER_PREPARATION)
    suspend fun getJobList(@Header("Authorization") authorization: String, @Query("supplierCd") supplierCd: String, @Query("supplierPlant") supplierPlant: String, @Query("pickupDtFrom") pickupDtFrom: String, @Query("pickupDtTo") PickupDtTo: String, @Query("sortBy") sortBy: String, @Query("keyword") keyword: String, @Query("pageNumber") pageNumber: Int, @Query("pageSize") pageSize: Int, @Query("subTitle") subTitle: String): Response<ResponseJobList>

    @GET(ConstantAPI.API_JOB_DIRECT_PREPARATION)
    suspend fun getJobDirectPreparation(@Header("Authorization") authorization: String, @Query("supplierCd") supplierCd: String, @Query("supplierPlant") supplierPlant: String, @Query("pickupDtFrom") pickupDtFrom: String, @Query("pickupDtTo") PickupDtTo: String, @Query("sortBy") sortBy: String, @Query("keyword") keyword: String, @Query("pageNumber") pageNumber: Int, @Query("pageSize") pageSize: Int, @Query("subTitle") subTitle: String): Response<ResponseJobList>

    @GET(ConstantAPI.API_JOB_SUPPLIER_PROGRESS)
    suspend fun getJobSupplierProgressList(@Header("Authorization") authorization: String, @Query("supplierCd") supplierCd: String, @Query("supplierPlant") supplierPlant: String, @Query("pickupDtFrom") pickupDtFrom: String, @Query("pickupDtTo") PickupDtTo: String, @Query("sortBy") sortBy: String, @Query("keyword") keyword: String, @Query("DeliveryType") deliveryType: String, @Query("pageNumber") pageNumber: Int, @Query("pageSize") pageSize: Int, @Query("subTitle") subTitle: String): Response<ResponseJobList>

    @GET(ConstantAPI.API_JOB_SUPPLIER_READY)
    suspend fun getJobSupplierReadyList(@Header("Authorization") authorization: String, @Query("supplierCd") supplierCd: String, @Query("supplierPlant") supplierPlant: String, @Query("pickupDtFrom") pickupDtFrom: String, @Query("pickupDtTo") PickupDtTo: String, @Query("sortBy") sortBy: String, @Query("keyword") keyword: String, @Query("DeliveryType") deliveryType: String, @Query("pageNumber") pageNumber: Int, @Query("pageSize") pageSize: Int, @Query("subTitle") subTitle: String): Response<ResponseJobList>

    @GET(ConstantAPI.API_JOB_DELIVERY_MILKRUN)
    suspend fun getJobDeliverMilkrunList(@Header("Authorization") authorization: String, @Query("supplierCd") supplierCd: String, @Query("supplierPlant") supplierPlant: String, @Query("pickupDtFrom") pickupDtFrom: String, @Query("pickupDtTo") PickupDtTo: String, @Query("sortBy") sortBy: String, @Query("keyword") keyword: String, @Query("pageNumber") pageNumber: Int, @Query("pageSize") pageSize: Int, @Query("subTitle") subTitle: String): Response<ResponseJobList>

    @GET(ConstantAPI.API_JOB_DELIVERY_DIRECT)
    suspend fun getJobDeliverDirectList(@Header("Authorization") authorization: String, @Query("supplierCd") supplierCd: String, @Query("supplierPlant") supplierPlant: String, @Query("pickupDtFrom") pickupDtFrom: String, @Query("pickupDtTo") PickupDtTo: String, @Query("sortBy") sortBy: String, @Query("keyword") keyword: String, @Query("pageNumber") pageNumber: Int, @Query("pageSize") pageSize: Int, @Query("subTitle") subTitle: String): Response<ResponseJobList>

    @GET(ConstantAPI.API_JOB_SUPPLIER_TOTAL)
    suspend fun getJobSupplierProgressTotal(@Header("Authorization") authorization: String, @Query("SupplierCd") supplierCd: String, @Query("SupplierPlant") supplierPlant: String, @Query("KeywordOnProgress") keywordOnProgress: String, @Query("KeywordReadyToPickup") keywordReadyToPickup: String, @Query("SubTitle") subTitle: String): ResponseJobSupplierTotal

    @GET(ConstantAPI.API_JOB_DRIVER_DELIVERY_DIRECT)
    suspend fun getJobDriverDeliverDirectList(@Header("Authorization") authorization: String, @Query("supplierCd") supplierCd: String, @Query("supplierPlant") supplierPlant: String, @Query("pickupDtFrom") pickupDtFrom: String, @Query("pickupDtTo") PickupDtTo: String, @Query("sortBy") sortBy: String, @Query("keyword") keyword: String, @Query("pageNumber") pageNumber: Int, @Query("pageSize") pageSize: Int, @Query("subTitle") subTitle: String): Response<ResponseJobList>

    @GET(ConstantAPI.API_JOB_COMBO)
    suspend fun getJobCombo(@Header("Authorization") authorization: String, @Query("subTitle") subTitle: String): ResponseJobCombo

    @GET(ConstantAPI.API_ASSIGN_COMBO)
    suspend fun getComboAssignJob(@Header("Authorization") authorization: String, @Query("SupplierCd") supplierCd: String, @Query("SupplierPlant") supplierPlant: String, @Query("SubTitle") subTitle: String): ResponseAssignCombo

    @GET(ConstantAPI.API_ASSIGNED_DRIVER)
    suspend fun getAssignedDriver(@Header("Authorization") authorization: String, @Query("DeliveryNo") deliveryNo: String, @Query("SupplierCd") SupplierCd: String, @Query("SupplierPlant") SupplierPlant: String, @Query("SubTitle") subTitle: String): ResponseAssignedDriver

    @GET(ConstantAPI.API_ASSIGN_CHECK)
    suspend fun getCheckAssignJob(@Header("Authorization") authorization: String, @Query("DeliveryNo") deliveryNo: String, @Query("SupplierCd") SupplierCd: String, @Query("SupplierPlant") SupplierPlant: String, @Query("DriverUsername") DriverUsername: String, @Query("SubTitle") subTitle: String): ResponseAssignCheck

    @POST(ConstantAPI.API_ASSIGN_POST)
    suspend fun postAssignJob(@Header("Authorization") authorization: String, @Body requestAssign: RequestAssign): ResponseAssignPost

    @GET(ConstantAPI.API_JOB_DRIVER_CHECK_DELIVERY_DIRECT)
    suspend fun getDriverJobCheck(@Header("Authorization") authorization: String, @Query("DeliveryNo") deliveryNo: String, @Query("DirectSupplierSts") DirectSupplierSts: String, @Query("DirectSupplierNote") DirectSupplierNote: String, @Query("SupplierCd") SupplierCd: String, @Query("SupplierPlant") SupplierPlant: String, @Query("SubTitle") subTitle: String): ResponseDriverPickupCheck

    @POST(ConstantAPI.API_JOB_DRIVER_CONFIRM_DELIVERY_DIRECT)
    suspend fun postDriverJobConfirm(@Header("Authorization") authorization: String, @Body requestDriverPickup: RequestDriverPickup): ResponseDriverPickup
}









