package com.tmmin.emanifest.network

import com.tmmin.emanifest.models.responses.order.combo.ResponseOrderCombo
import com.tmmin.emanifest.models.responses.order.route.ResponseOrderRoute
import com.tmmin.emanifest.models.responses.order.status.ResponseOrderStatus
import com.tmmin.emanifest.util.ConstantAPI
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface OrderApiService {
    @GET(ConstantAPI.API_ORDER_COMBO)
    suspend fun getOrderCombo(@Header("Authorization") authorization: String, @Query("SupplierCd") supplierCd: String, @Query("SupplierPlant") supplierPlant: String, @Query("subTitle") subTitle: String): ResponseOrderCombo

    @GET(ConstantAPI.API_ORDER_STATUS)
    suspend fun getOrderStatus(@Header("Authorization") authorization: String, @Query("DeliveryNo") deliveryNo: String, @Query("subTitle") subTitle: String): ResponseOrderStatus

    @GET(ConstantAPI.API_ORDER_ROUTE)
    suspend fun getOrderRoute(@Header("Authorization") authorization: String, @Query("DeliveryNo") deliveryNo: String): ResponseOrderRoute

    @GET(ConstantAPI.API_ORDER_ROUTE)
    suspend fun getOrderLastPosition(@Header("Authorization") authorization: String, @Query("DeliveryNo") deliveryNo: String, @Query("subTitle") subTitle: String): ResponseOrderRoute
}