package com.tmmin.emanifest.network

import com.tmmin.emanifest.models.responses.order.route.ResponseOrderRoute
import com.tmmin.emanifest.util.ConstantAPI
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface OthersApiService {
    @GET(ConstantAPI.API_ORDER_ROUTE)
    suspend fun getOrderRoute(@Header("Authorization") authorization: String, @Query("DeliveryNo") deliveryNo: String, @Query("SubTitle") subTitle: String): Response<ResponseOrderRoute>
}