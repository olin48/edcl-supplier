package com.tmmin.emanifest.network

import com.tmmin.emanifest.models.requests.manifest.RequestManifestEnd
import com.tmmin.emanifest.models.requests.manifest.RequestManifestStart
import com.tmmin.emanifest.models.requests.manifest.completeness.RequestManifestCompletenessItem
import com.tmmin.emanifest.models.requests.assign.RequestAssign
import com.tmmin.emanifest.models.responses.manifest.check.ResponseManifestEndCheck
import com.tmmin.emanifest.models.responses.manifest.completeness.ResponseManifestCompleteness
import com.tmmin.emanifest.models.responses.manifest.detail.ResponseManifestDetail
import com.tmmin.emanifest.models.responses.manifest.end.ResponseManifestEnd
import com.tmmin.emanifest.models.responses.manifest.list.ResponseManifestList
import com.tmmin.emanifest.models.responses.manifest.others.ResponseManifestOthersTotal
import com.tmmin.emanifest.models.responses.assign.check.ResponseAssignCheck
import com.tmmin.emanifest.models.responses.assign.post.ResponseAssignPost
import com.tmmin.emanifest.models.responses.assign.combo.ResponseAssignCombo
import com.tmmin.emanifest.models.responses.manifest.start.ResponseManifestStart
import com.tmmin.emanifest.util.ConstantAPI
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Query

interface ManifestApiService {
    @GET(ConstantAPI.API_MANIFEST_LIST)
    suspend fun getListManifest(@Header("Authorization") authorization: String, @Query("DeliveryNo") deliveryNo: String, @Query("SupplierCd") supplierCd: String, @Query("SupplierPlant") supplierPlant: String, @Query("PageNumber") offset: Int, @Query("PageSize") pageSize: Int, @Query("SubTitle") subTitle: String): Response<ResponseManifestList>

    @GET(ConstantAPI.API_MANIFEST_DETAIL)
    suspend fun getDetailManifest(@Header("Authorization") authorization: String, @Query("manifestNo") deliveryNo: String, @Query("subTitle") supplierCd: String): ResponseManifestDetail

    @POST(ConstantAPI.API_MANIFEST_START)
    suspend fun postManifestPreparation(@Header("Authorization") authorization: String, @Body requestManifestStart: RequestManifestStart): ResponseManifestStart

    @GET(ConstantAPI.API_MANIFEST_END_CHECK)
    suspend fun getCheckManifest(@Header("Authorization") authorization: String, @Query("ManifestNo") deliveryNo: String, @Query("ProcessId") processId: String, @Query("SubTitle") subTitle: String): ResponseManifestEndCheck

    @POST(ConstantAPI.API_MANIFEST_END)
    suspend fun postEndManifest(@Header("Authorization") authorization: String, @Body requestManifestEnd: RequestManifestEnd): ResponseManifestEnd

    @POST(ConstantAPI.API_MANIFEST_COMPLETENESS_ADD)
    suspend fun postManifestCompletenessAdd(@Header("Authorization") authorization: String, @Body requestManifestCompleteness: List<RequestManifestCompletenessItem>): ResponseManifestCompleteness

    @POST(ConstantAPI.API_MANIFEST_COMPLETENESS_CANCEL)
    suspend fun postManifestCompletenessCancel(@Header("Authorization") authorization: String, @Body requestManifestCompleteness: List<RequestManifestCompletenessItem>): ResponseManifestCompleteness

    @GET(ConstantAPI.API_MANIFEST_OTHERS_TOTAL)
    suspend fun getTotalManifestOthers(@Header("Authorization") authorization: String, @Query("SupplierCd") supplierCd: String, @Query("SupplierPlant") supplierPlant: String, @Query("SubTitle") subTitle: String): ResponseManifestOthersTotal

    @GET(ConstantAPI.API_MANIFEST_OTHERS_EO)
    suspend fun getListManifestEO(@Header("Authorization") authorization: String, @Query("SupplierCd") supplierCd: String, @Query("SupplierPlant") supplierPlant: String, @Query("Keyword") keyword: String, @Query("SubTitle") subTitle: String): Response<ResponseManifestList>

    @GET(ConstantAPI.API_MANIFEST_OTHERS_SHORTAGE)
    suspend fun getListManifestShortage(@Header("Authorization") authorization: String, @Query("SupplierCd") supplierCd: String, @Query("SupplierPlant") supplierPlant: String, @Query("Keyword") keyword: String, @Query("SubTitle") subTitle: String): Response<ResponseManifestList>

    @GET(ConstantAPI.API_MANIFEST_OTHERS_HISTORY)
    suspend fun getListManifestHistory(@Header("Authorization") authorization: String, @Query("SupplierCd") supplierCd: String, @Query("SupplierPlant") supplierPlant: String, @Query("SortBy") sortBy: String, @Query("Keyword") keyword: String, @Query("SubTitle") subTitle: String): Response<ResponseManifestList>

    @GET(ConstantAPI.API_MANIFEST_OTHERS_COMBO)
    suspend fun getComboManifestOthers(@Header("Authorization") authorization: String, @Query("SupplierCd") supplierCd: String, @Query("SupplierPlant") supplierPlant: String, @Query("SubTitle") subTitle: String): ResponseAssignCombo

    @GET(ConstantAPI.API_MANIFEST_OTHERS_ASSIGN_CHECK)
    suspend fun getCheckManifestAssign(@Header("Authorization") authorization: String, @Query("DeliveryNo") deliveryNo: String, @Query("ManifestNo") manifestNo: String, @Query("SubTitle") subTitle: String): ResponseAssignCheck

    @POST(ConstantAPI.API_MANIFEST_OTHERS_ASSIGN_POST)
    suspend fun postManifestAssign(@Header("Authorization") authorization: String, @Body requestAssign: RequestAssign): ResponseAssignPost
}