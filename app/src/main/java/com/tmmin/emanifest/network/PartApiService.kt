package com.tmmin.emanifest.network

import com.tmmin.emanifest.models.responses.part.list.ResponsePartList
import com.tmmin.emanifest.util.ConstantAPI
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface PartApiService {
    @GET(ConstantAPI.API_PART_LIST)
    suspend fun getListPart(@Header("Authorization") authorization: String, @Query("manifestNo") deliveryNo: String, @Query("subTitle") supplierCd: String): ResponsePartList
}