package com.tmmin.emanifest.listeners


interface OptionListener {
    fun onClicked(isApproved: Boolean)
}