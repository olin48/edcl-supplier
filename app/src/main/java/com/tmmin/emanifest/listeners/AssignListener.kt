package com.tmmin.emanifest.listeners

interface AssignListener {
    fun onClicked(index: Int)
}