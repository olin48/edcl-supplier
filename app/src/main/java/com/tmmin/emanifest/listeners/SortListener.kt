package com.tmmin.emanifest.listeners

interface SortListener {
    fun onClicked(isFiltered: Boolean, index: Int, dateFrom: String, dateTo: String)
}