package com.tmmin.emanifest.listeners

interface PickupListener {
    fun onClicked(mode: Int, message: String)
}