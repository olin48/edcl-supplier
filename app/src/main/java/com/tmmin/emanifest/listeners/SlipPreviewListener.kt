package com.tmmin.emanifest.listeners

import android.graphics.Bitmap

interface SlipPreviewListener {
    fun onCanceled()
    fun onConfirmed(bitmap: Bitmap)
}