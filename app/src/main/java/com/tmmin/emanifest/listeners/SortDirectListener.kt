package com.tmmin.emanifest.listeners

interface SortDirectListener {
    fun onClicked(isFiltered: Boolean, index: Int)
}