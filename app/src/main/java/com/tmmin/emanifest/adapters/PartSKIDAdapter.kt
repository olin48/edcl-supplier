package com.tmmin.emanifest.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TableLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.tmmin.emanifest.R
import com.tmmin.emanifest.features.operator.main.OperatorMainActivity
import com.tmmin.emanifest.features.operator.kanban.ManifestKanbanDetailFragment
import com.tmmin.emanifest.models.others.ModelNavbar
import com.tmmin.emanifest.models.responses.skid.detail.DataItem

class PartSKIDAdapter(operatorMainActivity: OperatorMainActivity, var modelNavbar: ModelNavbar, var list: ArrayList<DataItem>) : RecyclerView.Adapter<PartSKIDAdapter.ViewHolder>() {
    var parentActivity = operatorMainActivity;

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.layout_part_skid, viewGroup, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        viewHolder.tvPartNo.text = ((i + 1).toString() + ". " + list[i].partNo)
        viewHolder.tvManifestNo.text = (list[i].manifestNo ?: "").toString()
        viewHolder.tvKanbanNo.text = (list[i].totalScannedKanban ?: "0").toString() + "/" + (list[i].totalKanban ?: "0").toString()
        viewHolder.tvKanbanNo.setBackgroundColor(getColor(list[i].totalScannedKanban ?: 0, list[i].totalKanban ?: 0))
        viewHolder.tableLayout.setOnClickListener {
            parentActivity?.let {
                it.moveFragment(ManifestKanbanDetailFragment.newInstance(paramNavbar = Gson().toJson(modelNavbar), paramPart = Gson().toJson(list.get(i))))
            }
        }
    }

    private fun getColor(kanbanActual: Int, kanbanPlant: Int): Int {
        if (kanbanActual == 0) {
            return ContextCompat.getColor(parentActivity, R.color.ColorPink)
        } else if (kanbanActual == kanbanPlant) {
            return ContextCompat.getColor(parentActivity, R.color.ColorGreen)
        } else {
            return ContextCompat.getColor(parentActivity, R.color.ColorYellow)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tableLayout: TableLayout = itemView.findViewById(R.id.tableLayout)
        val tvPartNo: TextView = itemView.findViewById(R.id.tvPartNo)
        val tvManifestNo: TextView = itemView.findViewById(R.id.tvManifestNo)
        val tvKanbanNo: TextView = itemView.findViewById(R.id.tvKanbanNo)
    }
}