package com.tmmin.emanifest.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.tmmin.emanifest.R
import com.tmmin.emanifest.features.driver.main.DriverMainActivity
import com.tmmin.emanifest.features.driver.manifest.ManifestListFragment
import com.tmmin.emanifest.helpers.StringHelper
import com.tmmin.emanifest.models.others.ModelNavbar
import com.tmmin.emanifest.models.responses.job.list.DataItem

class EDCLJobAdapter(var isModeHistory: Boolean = false, var activity: DriverMainActivity) : RecyclerView.Adapter<EDCLJobAdapter.ViewHolder>() {
    val statusIncomplete = "1"
    val statusComplete = "2"
    val statusPickedUp = "4"
    var stringHelper = StringHelper()

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.layout_job, viewGroup, false)
        return ViewHolder(v)
    }

    private val differCallback = object : DiffUtil.ItemCallback<DataItem>() {
        override fun areItemsTheSame(oldItem: DataItem, newItem: DataItem): Boolean = oldItem == newItem
        override fun areContentsTheSame(oldItem: DataItem, newItem: DataItem): Boolean = oldItem == newItem
    }

    val differ = AsyncListDiffer(this, differCallback)

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        with(differ.currentList[i]) {
            viewHolder.tvJobType.visibility = View.GONE
            viewHolder.tvRouteId.text = route + " - " + rate
            viewHolder.tvDateTime.text = stringHelper.convertDateTime(arrivalPlanDt ?: "-")
            viewHolder.tvSupplier.text = lpName
            viewHolder.tvStatus.text = deliveryPreparationDescription ?: "-"
            viewHolder.ivStatusBackground.setColorFilter(getStatusColor(deliveryPreparationStatus ?: ""))
            viewHolder.ivStatusAction.setImageResource(getStatusIcon(deliveryPreparationStatus ?: ""))
            viewHolder.itemView.setOnClickListener {
                var modelNavbar = ModelNavbar(deliveryNo = deliveryNo ?: "", route = route ?: "", rate = rate ?: "", partner = lpName ?: "", deliveryPreparationStatus = deliveryPreparationStatus ?: "0", date = stringHelper.convertDateTime(arrivalPlanDt ?: "-"))
                activity.moveFragment(ManifestListFragment.newInstance(Gson().toJson(modelNavbar), isModeHistory))
            }
        }
    }

    private fun getStatusColor(status: String): Int {
        var color = ContextCompat.getColor(activity, R.color.ColorOrange)
        when (status) {
            statusIncomplete -> color = ContextCompat.getColor(activity, R.color.ColorRed)
            statusComplete -> color = ContextCompat.getColor(activity, R.color.ColorGreen)
            statusPickedUp -> color = ContextCompat.getColor(activity, R.color.ColorGreen)
        }
        return color
    }

    private fun getStatusIcon(status: String): Int {
        var icon = R.drawable.ic_eye
        when (status) {
            statusIncomplete -> icon = R.drawable.ic_edit_box
            statusComplete -> icon = R.drawable.ic_edit_box
            statusPickedUp -> icon = R.drawable.ic_eye
        }
        return icon
    }

    override fun getItemCount(): Int = differ.currentList.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvStatus: TextView = itemView.findViewById(R.id.tvStatus)
        val tvJobType: TextView = itemView.findViewById(R.id.tvJobType)
        val tvRouteId: TextView = itemView.findViewById(R.id.tvRouteId)
        val tvDateTime: TextView = itemView.findViewById(R.id.tvDateTime)
        val tvSupplier: TextView = itemView.findViewById(R.id.tvSupplier)
        val ivStatusBackground: ImageView = itemView.findViewById(R.id.ivStatusBackground)
        val ivStatusAction: ImageView = itemView.findViewById(R.id.ivStatusAction)
    }
}