package com.tmmin.emanifest.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.tmmin.emanifest.features.operator.manifest.others.ManifestOthersEOFragment
import com.tmmin.emanifest.features.operator.manifest.others.ManifestOthersHistoryFragment
import com.tmmin.emanifest.features.operator.manifest.others.ManifestOthersShortageFragment

private const val NUM_TABS = 3

public class PagerManifestAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) : FragmentStateAdapter(fragmentManager, lifecycle) {

    override fun getItemCount(): Int {
        return NUM_TABS
    }

    override fun createFragment(position: Int): Fragment {
        when (position) {
            1 -> return ManifestOthersShortageFragment()
            2 -> return ManifestOthersHistoryFragment()
        }
        return ManifestOthersEOFragment()
    }
}