package com.tmmin.emanifest.adapters

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.tmmin.emanifest.R
import com.tmmin.emanifest.features.operator.main.OperatorMainActivity
import com.tmmin.emanifest.features.operator.manifest.detail.ManifestDetailFragment
import com.tmmin.emanifest.helpers.DataHelper
import com.tmmin.emanifest.models.responses.manifest.list.DataItem

class ManifestAdapter(var activity: Activity, var isModeHistory: Boolean = false, var isTransporter: Boolean = false) : RecyclerView.Adapter<ManifestAdapter.ViewHolder>() {
    val statusPreparation = "0"
    val statusIncomplete = "1"
    val statusComplete = "2"
    val statusNoOrder = "3"
    val statusPickedUp = "4"
    val statusReceived = "5"

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.layout_manifest, viewGroup, false)
        return ViewHolder(v)
    }

    private val differCallback = object : DiffUtil.ItemCallback<DataItem>() {
        override fun areItemsTheSame(oldItem: DataItem, newItem: DataItem): Boolean = oldItem == newItem
        override fun areContentsTheSame(oldItem: DataItem, newItem: DataItem): Boolean = oldItem == newItem
    }

    val differ = AsyncListDiffer(this, differCallback)

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        with(differ.currentList[i]) {
            totalSKID?.let {
                viewHolder.tvTotalSKID.text = totalSKID.toString()
            } ?: run {
                viewHolder.tvTotalSKID.text = "-"
            }
            viewHolder.tvStatus.text = supplierPreparationDescription ?: "-"
            viewHolder.tvManifestNumber.text = manifestNo
            viewHolder.tvOrderNo.text = orderNo
            viewHolder.tvKanbanTotal.text = (totalScannedKanban ?: 0).toString() + "/" + (totalKanban ?: 0).toString()
            viewHolder.tvDockCode.text = dockCd
            viewHolder.tvPreparedBy.text = lastPreparedBy ?: "-"
            viewHolder.ivStatusBackground.setColorFilter(getStatusColor(supplierPreparationStatus ?: ""))
            if (isModeHistory) {
                viewHolder.ivStatusAction.setImageResource(R.drawable.ic_eye)
            } else {
                viewHolder.ivStatusAction.setImageResource(getStatusIcon(supplierPreparationStatus ?: ""))
            }
            viewHolder.itemView.setOnClickListener {
                if (activity is OperatorMainActivity) {
                    (activity as OperatorMainActivity).moveFragment(ManifestDetailFragment.newInstance(manifestNo ?: "", isModeHistory, isTransporter))
                } else {
                    val intent = Intent(activity, OperatorMainActivity::class.java)
                    intent.putExtra(DataHelper.DATA_NAVTO_MANIFEST_DETAIL, true)
                    intent.putExtra(DataHelper.DATA_MANIFEST_NO, manifestNo ?: "")
                    intent.putExtra(DataHelper.DATA_IS_HISTORY, isModeHistory)
                    intent.putExtra(DataHelper.DATA_IS_TRANSPORTER, isTransporter)
                    activity.startActivity(intent)
                }
            }
            if (supplierPreparationStatus.equals("3")) {
                viewHolder.ivStatusAction.visibility = View.INVISIBLE
            }
        }
    }

    override fun getItemCount(): Int = differ.currentList.size

    private fun getStatusColor(status: String): Int {
        var color = ContextCompat.getColor(activity, R.color.ColorOrange)
        when (status) {
            statusPreparation -> color = ContextCompat.getColor(activity, R.color.ColorLime)
            statusIncomplete -> color = ContextCompat.getColor(activity, R.color.ColorRed)
            statusComplete -> color = ContextCompat.getColor(activity, R.color.ColorGreen)
            statusNoOrder -> color = ContextCompat.getColor(activity, R.color.ColorYellow)
            statusPickedUp -> color = ContextCompat.getColor(activity, R.color.ColorGreen)
            statusReceived -> color = ContextCompat.getColor(activity, R.color.ColorOrange)
        }
        return color
    }

    private fun getStatusIcon(status: String): Int {
        var icon = R.drawable.ic_eye
        when (status) {
            statusPreparation -> icon = R.drawable.ic_arrow_right
            statusIncomplete -> icon = R.drawable.ic_edit_box
            statusComplete -> icon = R.drawable.ic_edit_box
            statusNoOrder -> icon = R.drawable.ic_arrow_right
        }
        return icon
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ivStatusBackground: ImageView = itemView.findViewById(R.id.ivStatusBackground)
        val ivStatusAction: ImageView = itemView.findViewById(R.id.ivStatusAction)
        val tvStatus: TextView = itemView.findViewById(R.id.tvStatus)
        val tvManifestNumber: TextView = itemView.findViewById(R.id.tvManifestNumber)
        val tvOrderNo: TextView = itemView.findViewById(R.id.tvOrderNumber)
        val tvKanbanTotal: TextView = itemView.findViewById(R.id.tvKanbanTotal)
        val tvDockCode: TextView = itemView.findViewById(R.id.tvDockCode)
        val tvTotalSKID: TextView = itemView.findViewById(R.id.tvTotalSkid)
        val tvPreparedBy: TextView = itemView.findViewById(R.id.tvPreparedBy)
    }
}