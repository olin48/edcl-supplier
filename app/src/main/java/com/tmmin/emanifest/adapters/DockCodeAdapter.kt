package com.tmmin.emanifest.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.tmmin.emanifest.R
import com.tmmin.emanifest.models.responses.dockCode.DataItem

class DockCodeAdapter(context: Context, var list: MutableList<DataItem>) : RecyclerView.Adapter<DockCodeAdapter.ViewHolder>() {
    var context = context;

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.layout_dock_code, viewGroup, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        viewHolder.tvDockCode.text = list[i].value ?: ""
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvDockCode: TextView = itemView.findViewById(R.id.tvDockCode)
    }
}