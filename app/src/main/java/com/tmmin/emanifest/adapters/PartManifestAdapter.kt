package com.tmmin.emanifest.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.tmmin.emanifest.R
import com.tmmin.emanifest.models.responses.part.list.DataItem

class PartManifestAdapter(context: Context, var list: ArrayList<DataItem>) : RecyclerView.Adapter<PartManifestAdapter.ViewHolder>() {
    var context = context;

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.layout_part_manifest, viewGroup, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        viewHolder.tvPartNo.text = ((i + 1).toString() + ". " + list[i].partNo)
        viewHolder.tvUniqNo.text = (list[i].uniqueNo ?: "-").toString()
        viewHolder.tvPcs.text = (list[i].piecesPerKanban ?: "-").toString()
        viewHolder.tvKanbanNo.text = (list[i].totalKanbanActual ?: "0").toString() + "/" + (list[i].totalKanbanPlan ?: "0").toString()
        viewHolder.tvKanbanNo.setBackgroundColor(getColor(list[i].totalKanbanActual ?: 0, list[i].totalKanbanPlan ?: 0))
    }

    private fun getColor(kanbanActual: Int, kanbanPlant: Int): Int {
        if (kanbanActual == 0) {
            return ContextCompat.getColor(context, R.color.ColorPink)
        } else if (kanbanActual == kanbanPlant) {
            return ContextCompat.getColor(context, R.color.ColorGreen)
        } else {
            return ContextCompat.getColor(context, R.color.ColorYellow)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvPartNo: TextView = itemView.findViewById(R.id.tvPartNo)
        val tvUniqNo: TextView = itemView.findViewById(R.id.tvUniqNo)
        val tvPcs: TextView = itemView.findViewById(R.id.tvPcs)
        val tvKanbanNo: TextView = itemView.findViewById(R.id.tvKanbanNo)
    }
}