package com.tmmin.emanifest.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.tmmin.emanifest.R
import com.tmmin.emanifest.models.responses.order.status.DataItem

class JobTrackingAdapter(context: Context, var list: ArrayList<DataItem>) : RecyclerView.Adapter<JobTrackingAdapter.ViewHolder>() {
    var context = context;

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.layout_job_tracking, viewGroup, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        var suppPlant = list.get(i).supplierPlant ?: ""
        var suppCode = list.get(i).supplierCD ?: ""
        var suppDescription = ""
        if (!suppCode.isEmpty() && !suppPlant.isEmpty()) {
            suppDescription = "(" + suppCode + "-" + suppPlant + ") "
        }
        var arrivalActual = list.get(i).arrivalActualDt ?: ""
        var departureActual = list.get(i).departureActualDt ?: ""
        viewHolder.tvStatusTime.text = list[i].arrivalPlanDt ?: ""
        if ((list.get(i).isTmmin ?: "0").equals("1")) {
            viewHolder.tvStatusTitle.setText("Order delivered")
        } else {
            viewHolder.tvStatusTitle.text = suppDescription + list[i].description ?: ""
        }
        if (!arrivalActual.isEmpty() && !departureActual.isEmpty()) {
            viewHolder.bgStatus.backgroundTintList = ContextCompat.getColorStateList(context, R.color.ColorBlue)
            viewHolder.ivStatus.setImageResource(R.drawable.ic_check)
            viewHolder.ivStatus.visibility = View.VISIBLE
        } else if (!arrivalActual.isEmpty() && departureActual.isEmpty()) {
            viewHolder.bgStatus.backgroundTintList = ContextCompat.getColorStateList(context, R.color.ColorWhite)
            viewHolder.ivStatus.visibility = View.GONE
        } else {
            viewHolder.bgStatus.backgroundTintList = ContextCompat.getColorStateList(context, R.color.ColorOrange)
            viewHolder.ivStatus.setImageResource(R.drawable.ic_dash)
            viewHolder.ivStatus.visibility = View.VISIBLE
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val bgStatus: RelativeLayout = itemView.findViewById(R.id.bgStatus)
        val ivStatus: ImageView = itemView.findViewById(R.id.ivStatus)
        val tvStatusTitle: TextView = itemView.findViewById(R.id.tvStatusTitle)
        val tvStatusTime: TextView = itemView.findViewById(R.id.tvStatusTime)
    }
}