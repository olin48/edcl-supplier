package com.tmmin.emanifest.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.tmmin.emanifest.features.operator.job.supplier.JobSupplierProgressFragment
import com.tmmin.emanifest.features.operator.job.supplier.JobSupplierReadyFragment

private const val NUM_TABS = 2

public class PagerJobSupplierAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) : FragmentStateAdapter(fragmentManager, lifecycle) {

    override fun getItemCount(): Int {
        return NUM_TABS
    }

    override fun createFragment(position: Int): Fragment {
        when (position) {
            1 -> return JobSupplierReadyFragment()
        }
        return JobSupplierProgressFragment()
    }
}