package com.tmmin.emanifest.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.tmmin.emanifest.R
import com.tmmin.emanifest.models.responses.kanban.list.DataItem

class KanbanAdapter(context: Context, var list: ArrayList<DataItem>) : RecyclerView.Adapter<KanbanAdapter.ViewHolder>() {
    var context = context;

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.layout_kanban, viewGroup, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        viewHolder.tvKanbanNo.text = ((i + 1).toString() + ". " + list.get(i).kanbanCd)
        viewHolder.tvQty.text = (list[i].totalQuantityPlan ?: "").toString()
        viewHolder.cbScan.isChecked = list.get(i).isScanned ?: false
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvKanbanNo: TextView = itemView.findViewById(R.id.tvKanbanNo)
        val tvQty: TextView = itemView.findViewById(R.id.tvQty)
        val cbScan: CheckBox = itemView.findViewById(R.id.cbScan)
    }
}