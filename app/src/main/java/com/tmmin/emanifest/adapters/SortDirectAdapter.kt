package com.tmmin.emanifest.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.tmmin.emanifest.R
import com.tmmin.emanifest.listeners.AssignListener
import com.tmmin.emanifest.listeners.SortDirectListener
import com.tmmin.emanifest.models.responses.job.combo.DataItem

class SortDirectAdapter(context: Context, var list: ArrayList<String>, var sortBySelected: Int, var assignListener: AssignListener) : RecyclerView.Adapter<SortDirectAdapter.ViewHolder>() {
    var holders = ArrayList<ViewHolder>()
    var context = context;

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.layout_sort, viewGroup, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        holders.add(viewHolder)
        if ((i + 1) == sortBySelected) {
            viewHolder.ivCheck.visibility = View.VISIBLE
        } else {
            viewHolder.ivCheck.visibility = View.GONE
        }
        viewHolder.tvTitle.text = list[i]
        viewHolder.itemView.setOnClickListener {
            selectSort(i)
            assignListener.onClicked(i + 1)
        }
    }

    private fun selectSort(index: Int) {
        for (i in 0 until holders.size) {
            if (i == index) {
                holders.get(i).ivCheck.visibility = View.VISIBLE
            } else {
                holders.get(i).ivCheck.visibility = View.GONE
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvTitle: TextView = itemView.findViewById(R.id.tvTitle)
        val ivCheck: ImageView = itemView.findViewById(R.id.ivCheck)
    }
}