package com.tmmin.emanifest.adapters

import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.tmmin.emanifest.R
import com.tmmin.emanifest.features.operator.job.supplier.JobSupplierActivity
import com.tmmin.emanifest.features.operator.main.OperatorMainActivity
import com.tmmin.emanifest.features.operator.manifest.list.ManifestListFragment
import com.tmmin.emanifest.helpers.DataHelper
import com.tmmin.emanifest.helpers.StringHelper
import com.tmmin.emanifest.models.others.ModelNavbar
import com.tmmin.emanifest.models.responses.job.list.DataItem

class JobAdapter(var isModeHistory: Boolean = false, var isShowType: Boolean = false, var isTransporter: Boolean = false, var activity: Activity) : RecyclerView.Adapter<JobAdapter.ViewHolder>() {
    val statusPreparation = "0"
    val statusIncomplete = "1"
    val statusComplete = "2"
    val statusOverflow = "3"
    val statusPickedUp = "4"
    val statusReceived = "5"

    var stringHelper = StringHelper()

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.layout_job, viewGroup, false)
        return ViewHolder(v)
    }

    private val differCallback = object : DiffUtil.ItemCallback<DataItem>() {
        override fun areItemsTheSame(oldItem: DataItem, newItem: DataItem): Boolean = oldItem == newItem
        override fun areContentsTheSame(oldItem: DataItem, newItem: DataItem): Boolean = oldItem == newItem
    }

    val differ = AsyncListDiffer(this, differCallback)

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        with(differ.currentList[i]) {
            var isNoOrder = isNoOrder ?: false
            var isDirect: Boolean
            if (deliveryType.equals("1")) {
                isDirect = true
                viewHolder.tvJobType.text = "Direct Supplier"
            } else {
                isDirect = false
                viewHolder.tvJobType.text = "Milkrun"
            }
            if (isShowType) {
                viewHolder.tvJobType.visibility = View.VISIBLE
            } else {
                viewHolder.tvJobType.visibility = View.GONE
            }
            viewHolder.tvRouteId.text = route + " - " + rate
            viewHolder.tvDateTime.text = stringHelper.convertDateTime(arrivalPlanDt ?: "-")
            viewHolder.tvSupplier.text = lpName
            viewHolder.tvStatus.text = deliveryPreparationDescription ?: "-"
            viewHolder.ivStatusBackground.setColorFilter(getStatusColor(deliveryPreparationStatus ?: ""))
            viewHolder.ivStatusAction.setImageResource(getStatusIcon(deliveryPreparationStatus ?: ""))
            viewHolder.itemView.setOnClickListener {
                Log.e("JobSelected", "" + Gson().toJson(differ.currentList[i]))
                var modelNavbar = ModelNavbar(deliveryNo = deliveryNo ?: "", route = route ?: "", rate = rate ?: "", partner = lpName ?: "", deliveryPreparationStatus = deliveryPreparationStatus ?: "0", date = stringHelper.convertDateTime(arrivalPlanDt ?: "-"))
                if (activity is OperatorMainActivity) {
                    //Regular
                    (activity as OperatorMainActivity).moveFragment(ManifestListFragment.newInstance(Gson().toJson(modelNavbar), isModeHistory, isDirect, isTransporter, isNoOrder))
                } else if (activity is JobSupplierActivity) {
                    //Non Regular Job Supplier Activity
                    var isReady: Boolean
                    if (deliveryType.equals("2")) {
                        isReady = false
                    } else if (deliveryPreparationStatus.equals("0")) {
                        isReady = false
                    } else {
                        isReady = true
                    }
                    val intent = Intent(activity, OperatorMainActivity::class.java)
                    intent.putExtra(DataHelper.DATA_NAVBAR, Gson().toJson(modelNavbar))
                    intent.putExtra(DataHelper.DATA_NAVTO_MANIFEST_LIST, isDirect)
                    intent.putExtra(DataHelper.DATA_IS_TRANSPORTER, isTransporter)
                    intent.putExtra(DataHelper.DATA_IS_NO_ORDER, isNoOrder)
                    intent.putExtra(DataHelper.DATA_IS_SUPPLIER_PREPARATION, isReady)
                    activity.startActivity(intent)
                } else {
                    //Non Regular
                    val intent = Intent(activity, OperatorMainActivity::class.java)
                    intent.putExtra(DataHelper.DATA_NAVBAR, Gson().toJson(modelNavbar))
                    intent.putExtra(DataHelper.DATA_NAVTO_MANIFEST_LIST, isDirect)
                    intent.putExtra(DataHelper.DATA_IS_TRANSPORTER, isTransporter)
                    intent.putExtra(DataHelper.DATA_IS_NO_ORDER, isNoOrder)
                    activity.startActivity(intent)
                }
            }
        }
    }

    private fun getStatusColor(status: String): Int {
        var color = ContextCompat.getColor(activity, R.color.ColorOrange)
        when (status) {
            statusPreparation -> color = ContextCompat.getColor(activity, R.color.ColorLime)
            statusIncomplete -> color = ContextCompat.getColor(activity, R.color.ColorRed)
            statusComplete -> color = ContextCompat.getColor(activity, R.color.ColorGreen)
            statusOverflow -> color = ContextCompat.getColor(activity, R.color.ColorRed)
            statusPickedUp -> color = ContextCompat.getColor(activity, R.color.ColorGreen)
            statusReceived -> color = ContextCompat.getColor(activity, R.color.ColorOrange)
        }
        return color
    }

    private fun getStatusIcon(status: String): Int {
        var icon = R.drawable.ic_eye
        when (status) {
            statusPreparation -> icon = R.drawable.ic_arrow_right
            statusIncomplete -> icon = R.drawable.ic_edit_box
            statusComplete -> icon = R.drawable.ic_edit_box
        }
        return icon
    }

    override fun getItemCount(): Int = differ.currentList.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvStatus: TextView = itemView.findViewById(R.id.tvStatus)
        val tvJobType: TextView = itemView.findViewById(R.id.tvJobType)
        val tvRouteId: TextView = itemView.findViewById(R.id.tvRouteId)
        val tvDateTime: TextView = itemView.findViewById(R.id.tvDateTime)
        val tvSupplier: TextView = itemView.findViewById(R.id.tvSupplier)
        val ivStatusBackground: ImageView = itemView.findViewById(R.id.ivStatusBackground)
        val ivStatusAction: ImageView = itemView.findViewById(R.id.ivStatusAction)
    }
}