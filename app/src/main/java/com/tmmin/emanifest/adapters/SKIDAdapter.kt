package com.tmmin.emanifest.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.activity.viewModels
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.tmmin.emanifest.R
import com.tmmin.emanifest.dialogs.DialogInfo
import com.tmmin.emanifest.dialogs.DialogLoading
import com.tmmin.emanifest.features.operator.main.OperatorMainActivity
import com.tmmin.emanifest.features.operator.kanban.ManifestKanbanScanOfflineFragment
import com.tmmin.emanifest.helpers.PreferenceHelper
import com.tmmin.emanifest.listeners.OptionListener
import com.tmmin.emanifest.models.others.ModelNavbar
import com.tmmin.emanifest.models.requests.manifest.RequestManifestStart
import com.tmmin.emanifest.models.requests.skid.RequestSkidDetail
import com.tmmin.emanifest.models.responses.skid.list.DataItem
import com.tmmin.emanifest.network.RequestState
import com.tmmin.emanifest.util.Constant
import com.tmmin.emanifest.viewmodels.ManifestViewModel
import com.tmmin.emanifest.viewmodels.SKIDViewModel
import net.cachapa.expandablelayout.ExpandableLayout
import java.util.Locale

class SKIDAdapter(var operatorMainActivity: OperatorMainActivity, var lifecycleOwner: LifecycleOwner, var modelNavbar: ModelNavbar, var list: ArrayList<DataItem>, var isModeHistory: Boolean) : RecyclerView.Adapter<SKIDAdapter.ViewHolder>() {
    lateinit var partAdapter: PartSKIDAdapter
    val skidViewModel: SKIDViewModel by operatorMainActivity.viewModels()
    val manifestViewModel: ManifestViewModel by operatorMainActivity.viewModels()
    var dialogLoading = DialogLoading(operatorMainActivity)
    var sharedPref: PreferenceHelper
    var authToken: String
    var locale: Locale

    init {
        sharedPref = PreferenceHelper(operatorMainActivity)
        authToken = sharedPref.getString(Constant.AUTH_TOKEN_ACCESS) ?: ""
        locale = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            operatorMainActivity.resources.configuration.locales.get(0)
        } else {
            operatorMainActivity.resources.configuration.locale
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.layout_skid, viewGroup, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        var isScannedByLP = list.get(i).isScannedByLP ?: false
        if (isModeHistory) {
            viewHolder.btnEdit.visibility = View.INVISIBLE
        } else {
            if (isScannedByLP) {
                viewHolder.btnEdit.visibility = View.INVISIBLE
            } else {
                viewHolder.btnEdit.visibility = View.VISIBLE
            }
        }
        Log.e("IsHistory", "" + isModeHistory)
        Log.e("IsScanned", "" + isScannedByLP)
        viewHolder.tvSKIDNumber.text = list.get(i).skidNo ?: "-"
        viewHolder.tvPartTotalKanban.text = (list.get(i).totalKanban ?: 0).toString()
        viewHolder.itemView.setOnClickListener {
            modelNavbar.skidNo = list.get(i).skidNo ?: ""
            if (viewHolder.layoutExpandSKID.isExpanded) {
                viewHolder.layoutExpandSKID.setExpanded(false)
                viewHolder.btnExpand.setImageResource(R.drawable.ic_arrow_up)
            } else {
                if (viewHolder.rcySKIDPart.adapter == null) {
                    var requestSkidDetail = RequestSkidDetail(skidNo = list.get(i).skidNo ?: "", subTitle = locale.language)
                    skidViewModel.getSKIDDetail(authToken, requestSkidDetail).observe(lifecycleOwner) {
                        if (it != null) {
                            when (it) {
                                is RequestState.Loading -> {
                                    viewHolder?.tvInfoPart?.visibility = View.GONE
                                    viewHolder?.rcySKIDPart?.visibility = View.GONE
                                    viewHolder?.pbPart?.visibility = View.VISIBLE
                                }

                                is RequestState.Success -> {
                                    it.data?.let { data ->
                                        Log.e("RespPart", "" + Gson().toJson(it.data))
                                        var partList = ArrayList<com.tmmin.emanifest.models.responses.skid.detail.DataItem>()
                                        var size = data.data?.size ?: 0
                                        if (size > 0) {
                                            partList.clear()
                                            for (part in data.data!!) {
                                                if (part != null) {
                                                    partList.add(part)
                                                }
                                            }
                                            partAdapter = PartSKIDAdapter(operatorMainActivity!!, modelNavbar, partList)
                                            viewHolder?.rcySKIDPart?.layoutManager = LinearLayoutManager(operatorMainActivity)
                                            viewHolder?.rcySKIDPart?.adapter = partAdapter
                                            viewHolder?.tvInfoPart?.visibility = View.GONE
                                            viewHolder?.rcySKIDPart?.visibility = View.VISIBLE
                                            viewHolder?.pbPart?.visibility = View.GONE
                                        } else {
                                            viewHolder?.tvInfoPart?.setText("No Parts")
                                            viewHolder?.tvInfoPart?.visibility = View.VISIBLE
                                            viewHolder?.rcySKIDPart?.visibility = View.GONE
                                            viewHolder?.pbPart?.visibility = View.GONE
                                        }
                                    }
                                }

                                is RequestState.Error -> {
                                    viewHolder?.tvInfoPart?.setText(it.message)
                                    viewHolder?.tvInfoPart?.visibility = View.VISIBLE
                                    viewHolder?.rcySKIDPart?.visibility = View.GONE
                                    viewHolder?.pbPart?.visibility = View.GONE
                                }
                            }
                        }
                    }
                }
                viewHolder.layoutExpandSKID.setExpanded(true)
                viewHolder.btnExpand.setImageResource(R.drawable.ic_arrow_down)
            }
        }
        viewHolder.btnEdit.setOnClickListener {
            modelNavbar.skidNo = list.get(i).skidNo ?: ""
            var isStarted = sharedPref.getString(modelNavbar.manifestNo ?: "") ?: ""
            if (isStarted.isEmpty() || isStarted.equals("0")) {
                postStartManifest()
            } else {
                startEditScan()
            }
        }
    }

    private fun startEditScan() {
        operatorMainActivity?.let {
            it.moveAndDestroyFragment(ManifestKanbanScanOfflineFragment.newInstance(Gson().toJson(modelNavbar)))
        }
    }

    private fun postStartManifest() {
        dialogLoading.show()
        var reqManifestStart = RequestManifestStart(manifestNo = modelNavbar.manifestNo, subTitle = locale.language)
        manifestViewModel.startManifest(authToken, reqManifestStart).observe(lifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {
                        dialogLoading.show()
                    }

                    is RequestState.Success -> {
                        Log.e("RespManStart", "" + Gson().toJson(it.data))
                        dialogLoading.dismiss()
                        it.data?.let { data ->
                            sharedPref.put((modelNavbar.manifestNo ?: ""), (data?.data?.processIdOutput ?: 0).toString())
                            startEditScan()
                        }
                    }

                    is RequestState.Error -> {
                        dialogLoading.dismiss()
                        var dialogInfo = DialogInfo(operatorMainActivity, DialogInfo.MODE_FAILED, operatorMainActivity.getString(R.string.MessageError), it.message, object : OptionListener {
                            override fun onClicked(isApproved: Boolean) {}
                        })
                        dialogInfo.show()
                    }
                }
            }
        }
    }


    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val layoutExpandSKID: ExpandableLayout = itemView.findViewById(R.id.layoutExpandSKID)
        val rcySKIDPart: RecyclerView = itemView.findViewById(R.id.rcySKIDPart)
        val pbPart: ProgressBar = itemView.findViewById(R.id.pbPart)
        val tvInfoPart: TextView = itemView.findViewById(R.id.tvInfoPart)
        val tvPartTotalKanban: TextView = itemView.findViewById(R.id.tvPartTotalKanban)
        val tvSKIDNumber: TextView = itemView.findViewById(R.id.tvSKIDNumber)
        val btnExpand: ImageView = itemView.findViewById(R.id.btnExpand)
        val btnEdit: ImageView = itemView.findViewById(R.id.btnEdit)
    }
}