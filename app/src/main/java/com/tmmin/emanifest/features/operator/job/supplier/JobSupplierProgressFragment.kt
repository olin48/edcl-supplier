package com.tmmin.emanifest.features.operator.job.supplier

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.tmmin.emanifest.R
import com.tmmin.emanifest.adapters.JobAdapter
import com.tmmin.emanifest.databinding.FragmentJobSupplierProgressListBinding
import com.tmmin.emanifest.dialogs.DialogLoading
import com.tmmin.emanifest.dialogs.DialogSortDirect
import com.tmmin.emanifest.listeners.SortDirectListener
import com.tmmin.emanifest.models.requests.job.RequestJobList
import com.tmmin.emanifest.network.RequestState
import com.tmmin.emanifest.util.BaseFragment
import com.tmmin.emanifest.util.Constant
import com.tmmin.emanifest.viewmodels.JobViewModel
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class JobSupplierProgressFragment : BaseFragment(), View.OnClickListener {
    private val jobViewModel: JobViewModel by viewModels()
    private lateinit var binding: FragmentJobSupplierProgressListBinding
    private lateinit var jobSearchAdapter: JobAdapter
    private lateinit var jobListAdapter: JobAdapter
    private lateinit var dialogLoading: DialogLoading
    private var parentActivity: JobSupplierActivity? = null
    var layoutManager: LinearLayoutManager? = null
    var scrollState: Parcelable? = null
    var runnable: Runnable? = null
    val handler = Handler()
    var isSearchMode = false
    var query = ""
    var isNotifShowed = false
    var isNotifSearchShowed = false
    var sortBySelected = 3

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_job_supplier_progress_list, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
    }

    override fun onResume() {
        super.onResume()
        getJob(true)
    }

    override fun onPause() {
        super.onPause()
        sortBySelected = 0
        binding.etSearch.setText("")
        binding.btnSort.setColorFilter(ContextCompat.getColor(requireActivity(), R.color.ColorPurple))
        binding.btnSort.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.ColorTransparent))
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is JobSupplierActivity) {
            parentActivity = context
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btnSearchClose -> {
                binding.etSearch.setText("")
            }

            binding.btnSort -> {
                sortJob()
            }
        }
    }

    private fun setup() {
        parentActivity = activity as? JobSupplierActivity
        binding.btnSearchClose.setOnClickListener(this)
        binding.btnSort.setOnClickListener(this)
        layoutManager = LinearLayoutManager(requireActivity())
        dialogLoading = DialogLoading(requireActivity())
        dialogLoading.setCancelable(true)
        handleSearchJob()
        handleGetJob()
        binding.etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                runnable?.let { handler.removeCallbacks(it) }
            }

            override fun afterTextChanged(s: Editable?) {
                binding.rcyJob.visibility = View.VISIBLE
                binding.tvInfoJob.visibility = View.GONE
                if (s.toString().length == 0) {
                    isSearchMode = false
                    binding.btnSearchClose.visibility = View.GONE
                    binding.btnSearchSubmit.visibility = View.VISIBLE
                    binding.rcyJob.adapter = jobListAdapter
                } else {
                    isSearchMode = true
                    binding.btnSearchClose.visibility = View.VISIBLE
                    binding.btnSearchSubmit.visibility = View.GONE
                    binding.rcyJob.adapter = jobSearchAdapter
                    runnable = Runnable {
                        searchJob(true)
                    }
                    handler.postDelayed(runnable!!, 2000)
                }
            }
        })
    }

    private fun resetData() {
        layoutManager = null
        scrollState = null
        layoutManager = LinearLayoutManager(requireActivity())
        jobListAdapter = JobAdapter(false, true, false, parentActivity!!)
        jobSearchAdapter = JobAdapter(false, true, false, parentActivity!!)
        binding.rcyJob.adapter = jobListAdapter
        binding.rcyJob.layoutManager = layoutManager
        binding.rcyJob.clearOnScrollListeners()
        binding.rcyJob.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1)) {
                    scrollState = layoutManager?.onSaveInstanceState()
                    if (isSearchMode == true) {
                        if (jobViewModel.isMoreSearchRecordSupProgress == true) {
                            searchJob(false)
                        } else {
                            if (!isNotifSearchShowed) {
                                Toast.makeText(requireActivity(), getString(R.string.InfoMaxData), Toast.LENGTH_SHORT).show()
                                isNotifSearchShowed = true
                            }
                        }
                    } else {
                        if (jobViewModel.isMoreRecordSupProgress == true) {
                            getJob(false)
                        } else {
                            if (!isNotifShowed) {
                                Toast.makeText(requireActivity(), getString(R.string.InfoMaxData), Toast.LENGTH_SHORT).show()
                                isNotifShowed = true
                            }
                        }
                    }
                }
            }
        })
    }

    private fun getJob(isStarted: Boolean) {
        if (isStarted) {
            binding.rcyJob.smoothScrollToPosition(0)
            binding.layoutLoading.visibility = View.VISIBLE
            binding.tvInfoJob.visibility = View.GONE
            binding.rcyJob.visibility = View.GONE
            binding.layoutLoading.startShimmer()
            resetData()
        } else {
            dialogLoading.show()
        }
        var deliveryType: String
        if (sortBySelected == 1) {
            deliveryType = "1"
        } else if (sortBySelected == 2) {
            deliveryType = "2"
        } else {
            deliveryType = ""
        }
        var requestJobList = RequestJobList(deliveryType = deliveryType, sortBy = "3", supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE) ?: "", supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT) ?: "", pickupDtFrom = "", pickupDtTo = "", subTitle = locale.language)
        jobViewModel.getJobList(1, isStarted, sharedPref.getString(Constant.AUTH_TOKEN_ACCESS) ?: "", requestJobList)
    }

    private fun searchJob(isStarted: Boolean) {
        Log.e("JobSearch", "" + isSearchMode)
        query = binding.etSearch.text.toString()
        if (!query.isEmpty()) {
            if (isStarted) {
                binding.rcyJob.smoothScrollToPosition(0)
                binding.layoutLoading.visibility = View.VISIBLE
                binding.tvInfoJob.visibility = View.GONE
                binding.rcyJob.visibility = View.GONE
                binding.layoutLoading.startShimmer()
            } else {
                dialogLoading.show()
            }
            var deliveryType: String
            if (sortBySelected == 1) {
                deliveryType = "1"
            } else if (sortBySelected == 2) {
                deliveryType = "2"
            } else {
                deliveryType = ""
            }
            var requestJobList = RequestJobList(deliveryType = deliveryType, keyword = query, sortBy = "3", supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE) ?: "", supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT) ?: "", pickupDtFrom = "", pickupDtTo = "", subTitle = locale.language)
            jobViewModel.getJobListSearch(1, isStarted, sharedPref.getString(Constant.AUTH_TOKEN_ACCESS) ?: "", requestJobList)
        }
    }

    private fun sortJob() {
        var dataList = arrayListOf("Direct", "Milkrun")
        var dialogSortDirect = DialogSortDirect(requireActivity(), dataList, sortBySelected, object : SortDirectListener {
            override fun onClicked(isFiltered: Boolean, index: Int) {
                if (index == 0) {
                    binding.btnSort.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.ColorTransparent))
                    binding.btnSort.setColorFilter(ContextCompat.getColor(requireActivity(), R.color.ColorPurple))
                } else {
                    binding.btnSort.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.ColorPurple))
                    binding.btnSort.setColorFilter(ContextCompat.getColor(requireActivity(), R.color.ColorWhite))
                }
                sortBySelected = index
                if (isSearchMode) {
                    searchJob(true)
                } else {
                    getJob(true)
                }
            }
        })
        dialogSortDirect.show()
    }

    private fun handleGetJob() {
        jobViewModel.jobListResponse.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespJobProgress", "" + Gson().toJson(it.data))
                        dialogLoading.dismiss()
                        binding.layoutLoading.stopShimmer()
                        var size = it.data?.data?.size ?: 0
                        Log.e("Size", "" + size)
                        if (size > 0) {
                            it.data?.data?.let { data -> jobListAdapter?.differ?.submitList(data.toList()) }
                            binding.rcyJob.adapter = jobListAdapter
                            binding.rcyJob.visibility = View.VISIBLE
                            binding.layoutLoading.visibility = View.GONE
                            binding.tvInfoJob.visibility = View.GONE

                            try {
                                scrollState?.let {
                                    layoutManager?.onRestoreInstanceState(it)
                                }
                            } catch (e: Exception) {
                            }
                        } else {
                            binding.rcyJob.visibility = View.GONE
                            binding.layoutLoading.visibility = View.GONE
                            binding.tvInfoJob.visibility = View.VISIBLE
                            binding.tvInfoJob.text = "No Job"
                        }
                    }

                    is RequestState.Error -> {
                        Log.e("ErrJobProgress", "" + it.message)
                        dialogLoading.dismiss()
                        binding.layoutLoading.stopShimmer()
                        binding.rcyJob.visibility = View.GONE
                        binding.layoutLoading.visibility = View.GONE
                        binding.tvInfoJob.visibility = View.VISIBLE
                        binding.tvInfoJob.text = it.message
                    }
                }
            }
        })
    }

    private fun handleSearchJob() {
        jobViewModel.jobListSearchResponse.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespJobProgressSearch", "" + Gson().toJson(it.data))
                        dialogLoading.dismiss()
                        binding.layoutLoading.stopShimmer()
                        var size = it.data?.data?.size ?: 0
                        if (size > 0) {
                            it.data?.data?.let { data -> jobSearchAdapter?.differ?.submitList(data.toList()) }
                            binding.rcyJob.adapter = jobSearchAdapter
                            binding.rcyJob.visibility = View.VISIBLE
                            binding.layoutLoading.visibility = View.GONE
                            binding.tvInfoJob.visibility = View.GONE

                            try {
                                scrollState?.let {
                                    layoutManager?.onRestoreInstanceState(it)
                                }
                            } catch (e: Exception) {
                            }
                        } else {
                            binding.rcyJob.visibility = View.GONE
                            binding.layoutLoading.visibility = View.GONE
                            binding.tvInfoJob.visibility = View.VISIBLE
                            binding.tvInfoJob.text = "Job Not Found"
                        }
                    }

                    is RequestState.Error -> {
                        dialogLoading.dismiss()
                        binding.layoutLoading.stopShimmer()
                        binding.tvInfoJob.text = it.message
                        binding.rcyJob.visibility = View.GONE
                        binding.layoutLoading.visibility = View.GONE
                        binding.tvInfoJob.visibility = View.VISIBLE
                    }
                }
            }
        })
    }
}