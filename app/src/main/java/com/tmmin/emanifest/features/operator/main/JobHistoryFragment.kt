package com.tmmin.emanifest.features.operator.main

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.tmmin.emanifest.R
import com.tmmin.emanifest.adapters.JobAdapter
import com.tmmin.emanifest.databinding.FragmentJobHistoryBinding
import com.tmmin.emanifest.dialogs.DialogInfo
import com.tmmin.emanifest.dialogs.DialogLoading
import com.tmmin.emanifest.dialogs.DialogSort
import com.tmmin.emanifest.listeners.OptionListener
import com.tmmin.emanifest.listeners.SortListener
import com.tmmin.emanifest.models.others.ModelCombo
import com.tmmin.emanifest.models.requests.job.RequestJobList
import com.tmmin.emanifest.network.RequestState
import com.tmmin.emanifest.util.BaseFragment
import com.tmmin.emanifest.util.Constant
import com.tmmin.emanifest.viewmodels.JobViewModel
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class JobHistoryFragment : BaseFragment(), View.OnClickListener {
    private lateinit var dateTime: String
    private val jobViewModel: JobViewModel by viewModels()
    private lateinit var binding: FragmentJobHistoryBinding
    private lateinit var jobSearchAdapter: JobAdapter
    private lateinit var jobListAdapter: JobAdapter
    private lateinit var dialogLoading: DialogLoading
    var parentActivity: OperatorMainActivity? = null
    var runnable: Runnable? = null
    val handler = Handler()
    var isSearchMode = false
    var query = ""
    var isNotifShowed = false
    var isNotifSearchShowed = false
    var layoutManager: LinearLayoutManager? = null
    var scrollState: Parcelable? = null
    var dateFromSelected = ""
    var dateToSelected = ""
    var sortBySelected = 3

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_job_history, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OperatorMainActivity) {
            parentActivity = context
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.layoutToolbar.btnBack -> {
                parentActivity?.let {
                    it.onBackPressed()
                }
            }

            binding.btnSearchClose -> {
                binding.etSearch.setText("")
            }

            binding.btnSort -> {
                sortJob()
            }
        }
    }

    private fun setup() {
        binding.layoutToolbar.tvTitle.visibility = View.VISIBLE
        binding.layoutToolbar.btnBack.visibility = View.VISIBLE
        binding.layoutToolbar.tvTitle.text = "Job History"
        parentActivity = activity as? OperatorMainActivity
        binding.layoutToolbar.btnBack.setOnClickListener(this)
        binding.btnSearchClose.setOnClickListener(this)
        binding.btnSort.setOnClickListener(this)
        layoutManager = LinearLayoutManager(requireActivity())
        dialogLoading = DialogLoading(requireActivity())
        dialogLoading.setCancelable(true)
        resetData()
        handleSearchJobHistory()
        handleGetJobHistory()
        getDateTime()
        getJobHistory(true)
        binding.etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                runnable?.let { handler.removeCallbacks(it) }
            }

            override fun afterTextChanged(s: Editable?) {
                binding.rcyJob.visibility = View.VISIBLE
                binding.tvInfoJob.visibility = View.GONE
                if (s.toString().length == 0) {
                    isSearchMode = false
                    binding.btnSearchClose.visibility = View.GONE
                    binding.btnSearchSubmit.visibility = View.VISIBLE
                    binding.rcyJob.adapter = jobListAdapter
                } else {
                    isSearchMode = true
                    binding.btnSearchClose.visibility = View.VISIBLE
                    binding.btnSearchSubmit.visibility = View.GONE
                    binding.rcyJob.adapter = jobSearchAdapter
                    runnable = Runnable {
                        searchJobHistory(true)
                    }
                    handler.postDelayed(runnable!!, 2000)
                }
            }
        })
    }

    private fun getDateTime() {
        val currentDate = Date()
        val formatter = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        dateTime = formatter.format(currentDate)
    }

    private fun resetData() {
        jobListAdapter = JobAdapter(true, true, false, parentActivity!!)
        jobSearchAdapter = JobAdapter(true, true, false, parentActivity!!)
        binding.rcyJob.adapter = jobListAdapter
        binding.rcyJob.layoutManager = layoutManager
        binding.rcyJob.addOnScrollListener(scrollListener)
    }

    private fun getJobHistory(isStarted: Boolean) {
        if (isStarted) {
            binding.rcyJob.smoothScrollToPosition(0)
            binding.layoutLoading.visibility = View.VISIBLE
            binding.tvInfoJob.visibility = View.GONE
            binding.rcyJob.visibility = View.GONE
            binding.layoutLoading.startShimmer()
        } else {
            dialogLoading.show()
        }
        var requestJobList = RequestJobList(sortBy = sortBySelected.toString(), keyword = "", pickupDtFrom = dateFromSelected, pickupDtTo = dateToSelected, supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE) ?: "", supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT) ?: "", subTitle = locale.language)
        jobViewModel.getJobList(6, isStarted, sharedPref.getString(Constant.AUTH_TOKEN_ACCESS) ?: "", requestJobList)
    }

    private fun searchJobHistory(isStarted: Boolean) {
        query = binding.etSearch.text.toString()
        if (!query.isEmpty()) {
            if (isStarted) {
                binding.rcyJob.smoothScrollToPosition(0)
                binding.layoutLoading.visibility = View.VISIBLE
                binding.tvInfoJob.visibility = View.GONE
                binding.rcyJob.visibility = View.GONE
                binding.layoutLoading.startShimmer()
            } else {
                dialogLoading.show()
            }
            var requestJobList = RequestJobList(sortBy = sortBySelected.toString(), keyword = query, pickupDtFrom = dateFromSelected, pickupDtTo = dateToSelected, supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE) ?: "", supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT) ?: "", subTitle = locale.language)
            jobViewModel.getJobListSearch(6, isStarted, sharedPref.getString(Constant.AUTH_TOKEN_ACCESS) ?: "", requestJobList)
        }
    }

    private fun sortJob() {
        dialogLoading.show()
        jobViewModel.getJobCombo(authToken, locale.language).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        dialogLoading.dismiss()
                        it.data?.let { data ->
                            var size = data.data?.size ?: 0
                            if (size > 0) {
                                var modelCombo = ModelCombo(sortBySelected = sortBySelected, dateFromSelected = dateFromSelected, dateToSelected = dateToSelected)
                                var dialogSort = DialogSort(requireActivity(), data.data, modelCombo, object : SortListener {
                                    override fun onClicked(isFiltered: Boolean, index: Int, dateFrom: String, dateTo: String) {
                                        sortBySelected = (data.data.get(index).value ?: "3").toInt()
                                        dateFromSelected = dateFrom
                                        dateToSelected = dateTo
                                        if (isFiltered) {
                                            binding.btnSort.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.ColorPurple))
                                            binding.btnSort.setColorFilter(ContextCompat.getColor(requireActivity(), R.color.ColorWhite))
                                            if (isSearchMode) {
                                                searchJobHistory(true)
                                            } else {
                                                getJobHistory(true)
                                            }
                                        } else {
                                            binding.btnSort.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.ColorTransparent))
                                            binding.btnSort.setColorFilter(ContextCompat.getColor(requireActivity(), R.color.ColorPurple))
                                            getJobHistory(true)
                                        }
                                    }
                                })
                                dialogSort.show()
                            } else {
                                var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), it.data?.message ?: getString(R.string.ErrorSort), object : OptionListener {
                                    override fun onClicked(isApproved: Boolean) {}
                                })
                                dialogInfo.show()
                            }
                        }
                    }

                    is RequestState.Error -> {
                        dialogLoading.dismiss()
                        var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), it.message, object : OptionListener {
                            override fun onClicked(isApproved: Boolean) {}
                        })
                        dialogInfo.show()
                    }
                }
            }
        }
    }

    private fun handleGetJobHistory() {
        jobViewModel.jobListResponse.observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespJobHistory", "" + Gson().toJson(it.data))
                        dialogLoading.dismiss()
                        binding.layoutLoading.stopShimmer()
                        var size = it.data?.data?.size ?: 0
                        if (size > 0) {
                            it.data?.data?.let { data -> jobListAdapter?.differ?.submitList(data.toList()) }
                            binding.rcyJob.adapter = jobListAdapter
                            binding.rcyJob.visibility = View.VISIBLE
                            binding.layoutLoading.visibility = View.GONE
                            binding.tvInfoJob.visibility = View.GONE
                            try {
                                scrollState?.let {
                                    layoutManager?.onRestoreInstanceState(it)
                                }
                            } catch (e: Exception) {
                            }
                        } else {
                            binding.rcyJob.visibility = View.GONE
                            binding.layoutLoading.visibility = View.GONE
                            binding.tvInfoJob.visibility = View.VISIBLE
                            binding.tvInfoJob.text = "No History"
                        }
                    }

                    is RequestState.Error -> {
                        Log.e("ErrJobHistory", "" + it.message)
                        dialogLoading.dismiss()
                        binding.layoutLoading.stopShimmer()
                        binding.rcyJob.visibility = View.GONE
                        binding.layoutLoading.visibility = View.GONE
                        binding.tvInfoJob.visibility = View.VISIBLE
                        binding.tvInfoJob.text = it.message
                    }
                }
            }
        }
    }

    private fun handleSearchJobHistory() {
        jobViewModel.jobListSearchResponse.observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        dialogLoading.dismiss()
                        binding.layoutLoading.stopShimmer()
                        var size = it.data?.data?.size ?: 0
                        if (size > 0) {
                            it.data?.data?.let { data -> jobSearchAdapter?.differ?.submitList(data.toList()) }
                            binding.rcyJob.adapter = jobSearchAdapter
                            binding.rcyJob.visibility = View.VISIBLE
                            binding.layoutLoading.visibility = View.GONE
                            binding.tvInfoJob.visibility = View.GONE
                            try {
                                scrollState?.let {
                                    layoutManager?.onRestoreInstanceState(it)
                                }
                            } catch (e: Exception) {
                            }
                        } else {
                            binding.rcyJob.visibility = View.GONE
                            binding.layoutLoading.visibility = View.GONE
                            binding.tvInfoJob.visibility = View.VISIBLE
                            binding.tvInfoJob.text = "Job Not Found"
                        }
                    }

                    is RequestState.Error -> {
                        Log.e("ErrJobHistorySearch", "" + it.message)
                        dialogLoading.dismiss()
                        binding.layoutLoading.stopShimmer()
                        binding.tvInfoJob.text = it.message
                        binding.rcyJob.visibility = View.GONE
                        binding.layoutLoading.visibility = View.GONE
                        binding.tvInfoJob.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    private val scrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            if (!recyclerView.canScrollVertically(1)) {
                scrollState = layoutManager?.onSaveInstanceState()
                if (isSearchMode == true) {
                    if (jobViewModel.isMoreSearchRecordHistory == true) {
                        searchJobHistory(false)
                    } else {
                        if (!isNotifSearchShowed) {
                            Toast.makeText(requireActivity(), getString(R.string.InfoMaxData), Toast.LENGTH_SHORT).show()
                            isNotifSearchShowed = true
                        }
                    }
                } else {
                    if (jobViewModel.isMoreRecordHistory == true) {
                        getJobHistory(false)
                    } else {
                        if (!isNotifShowed) {
                            Toast.makeText(requireActivity(), getString(R.string.InfoMaxData), Toast.LENGTH_SHORT).show()
                            isNotifShowed = true
                        }
                    }
                }
            }
        }
    }
}