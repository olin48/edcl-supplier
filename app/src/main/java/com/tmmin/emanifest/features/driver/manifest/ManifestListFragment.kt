package com.tmmin.emanifest.features.driver.manifest

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.tmmin.emanifest.R
import com.tmmin.emanifest.adapters.EDCLManifestAdapter
import com.tmmin.emanifest.databinding.FragmentDriverManifestListBinding
import com.tmmin.emanifest.dialogs.DialogInfo
import com.tmmin.emanifest.dialogs.DialogLoading
import com.tmmin.emanifest.dialogs.DialogPickup
import com.tmmin.emanifest.features.driver.job.JobQRCodeFragment
import com.tmmin.emanifest.features.driver.main.DriverMainActivity
import com.tmmin.emanifest.helpers.DataHelper
import com.tmmin.emanifest.helpers.PreferenceHelper
import com.tmmin.emanifest.listeners.OptionListener
import com.tmmin.emanifest.listeners.PickupListener
import com.tmmin.emanifest.models.others.ModelNavbar
import com.tmmin.emanifest.models.requests.job.driver.RequestDriverPickup
import com.tmmin.emanifest.models.requests.manifest.RequestManifestList
import com.tmmin.emanifest.network.RequestState
import com.tmmin.emanifest.util.BaseFragment
import com.tmmin.emanifest.util.Constant
import com.tmmin.emanifest.viewmodels.JobViewModel
import com.tmmin.emanifest.viewmodels.ManifestViewModel

class ManifestListFragment : BaseFragment(), View.OnClickListener {
    private val viewModel: ManifestViewModel by viewModels()
    private val jobViewModel: JobViewModel by viewModels()
    private lateinit var dialogLoading: DialogLoading
    lateinit var binding: FragmentDriverManifestListBinding
    lateinit var manifestAdapter: EDCLManifestAdapter
    lateinit var modelNavbar: ModelNavbar
    var parentActivity: DriverMainActivity? = null
    var dataNavbar: String? = null
    var isHistory: Boolean = false
    var isNotifShowed = false
    var layoutManager: LinearLayoutManager? = null
    var scrollState: Parcelable? = null

    companion object {
        fun newInstance(paramNavbar: String, paramIsHistory: Boolean = false): ManifestListFragment {
            val fragment = ManifestListFragment()
            val args = Bundle()
            args.putString(DataHelper.DATA_NAVBAR, paramNavbar)
            args.putBoolean(DataHelper.DATA_IS_HISTORY, paramIsHistory)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            dataNavbar = it.getString(DataHelper.DATA_NAVBAR)
            isHistory = it.getBoolean(DataHelper.DATA_IS_HISTORY)
            modelNavbar = Gson().fromJson(dataNavbar, ModelNavbar::class.java)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_driver_manifest_list, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is DriverMainActivity) {
            parentActivity = context
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.layoutToolbar.btnBack -> {
                parentActivity?.let {
                    it.onBackPressed()
                }
            }

            binding.btnProcess -> {
                postJobLoading()
            }

            binding.btnConfirm -> {
                postJobComplete()
            }

            binding.btnQRCode -> {
                generateQRCode()
            }
        }
    }

    fun setup() {
        binding.layoutToolbar.tvTitle.visibility = View.VISIBLE
        binding.layoutToolbar.btnBack.visibility = View.VISIBLE
        binding.layoutToolbar.tvTitle.text = "Manifest List"
        binding.tvRoute.text = modelNavbar.route
        binding.tvRate.text = modelNavbar.rate
        binding.tvPartner.text = modelNavbar.partner
        binding.tvPickupDate.text = modelNavbar.date
        parentActivity = activity as? DriverMainActivity
        layoutManager = LinearLayoutManager(requireActivity())
        dialogLoading = DialogLoading(requireActivity())
        dialogLoading.setCancelable(true)
        sharedPref = PreferenceHelper(requireActivity())
        binding.layoutToolbar.btnBack.setOnClickListener(this)
        binding.btnProcess.setOnClickListener(this)
        binding.btnConfirm.setOnClickListener(this)
        binding.btnQRCode.setOnClickListener(this)
        manifestAdapter = EDCLManifestAdapter(parentActivity!!, isHistory)
        binding.rcyManifest.adapter = manifestAdapter
        binding.rcyManifest.layoutManager = layoutManager
        binding.rcyManifest.addOnScrollListener(scrollListener)
        handleGetManifests()
        if (isHistory) {
            getManifest(true)
        } else {
            initJob()
        }
    }

    private fun getManifest(isStarted: Boolean) {
        if (isStarted) {
            binding.rcyManifest.smoothScrollToPosition(0)
            binding.layoutLoading.visibility = View.VISIBLE
            binding.tvInfoManifest.visibility = View.GONE
            binding.rcyManifest.visibility = View.GONE
            binding.layoutLoading.startShimmer()
        } else {
            dialogLoading.show()
        }
        var requestManifestList = RequestManifestList(deliveryNo = modelNavbar.deliveryNo, supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT) ?: "", supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE) ?: "", subTitle = locale.language)
        viewModel.getManifestList(isStarted, sharedPref.getString(Constant.AUTH_TOKEN_ACCESS) ?: "", requestManifestList)
    }

    private fun handleGetManifests() {
        viewModel.manifestListResponse.observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespListMan", "" + Gson().toJson(it))
                        dialogLoading.dismiss()
                        binding.layoutLoading.stopShimmer()
                        var size = it.data?.data?.size ?: 0
                        if (size > 0) {
                            it.data?.data?.let { data -> manifestAdapter?.differ?.submitList(data.toList()) }
                            binding.rcyManifest.visibility = View.VISIBLE
                            binding.layoutLoading.visibility = View.GONE
                            binding.tvInfoManifest.visibility = View.GONE

                            try {
                                scrollState?.let {
                                    layoutManager?.onRestoreInstanceState(it)
                                }
                            } catch (e: Exception) {
                            }
                        } else {
                            binding.tvInfoManifest.text = "No Manifests"
                            binding.rcyManifest.visibility = View.GONE
                            binding.layoutLoading.visibility = View.GONE
                            binding.tvInfoManifest.visibility = View.VISIBLE
                        }
                    }

                    is RequestState.Error -> {
                        dialogLoading.dismiss()
                        binding.layoutLoading.stopShimmer()
                        binding.tvInfoManifest.text = it.message
                        binding.rcyManifest.visibility = View.GONE
                        binding.layoutLoading.visibility = View.GONE
                        binding.tvInfoManifest.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    private fun initJob() {
        var requestDriverPickup = RequestDriverPickup(deliveryNo = modelNavbar.deliveryNo, supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE) ?: "", supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT) ?: "", subTitle = locale.language)
        Log.e("ReqInit", "" + Gson().toJson(requestDriverPickup))
        jobViewModel.getDriverJobCheck(authToken, requestDriverPickup).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {
                        binding.btnProcess.visibility = View.GONE
                        binding.btnConfirm.visibility = View.GONE
                        binding.btnQRCode.visibility = View.GONE
                    }

                    is RequestState.Success -> {
                        Log.e("RespInit", "" + Gson().toJson(it.data))
                        var status = it.data?.data?.directSupplierStatus ?: "0"
                        showButton(status)
                        getManifest(true)
                    }

                    is RequestState.Error -> {
                        Log.e("ErrInit", "" + it.message)
                        binding.btnProcess.visibility = View.GONE
                        binding.btnConfirm.visibility = View.GONE
                        binding.btnQRCode.visibility = View.GONE
                        getManifest(true)
                    }
                }
            }
        }
    }

    private fun postJobLoading() {
        dialogLoading.show()
        var requestDriverPickup = RequestDriverPickup(deliveryNo = modelNavbar.deliveryNo, directSupplierSts = "1", directSupplierNote = "", supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE) ?: "", supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT) ?: "", subTitle = locale.language)
        jobViewModel.postDriverJobConfirm(authToken, requestDriverPickup).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespPost", "" + Gson().toJson(it.data))
                        dialogLoading.dismiss()
                        it.data?.let { data ->
                            var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_SUCCESS, getString(R.string.MessageSuccess), "", object : OptionListener {
                                override fun onClicked(isApproved: Boolean) {
                                    initJob()
                                }
                            })
                            dialogInfo.show()
                        }
                    }

                    is RequestState.Error -> {
                        Log.e("ErrPost", "" + it.message)
                        dialogLoading.dismiss()
                        var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), it.message, object : OptionListener {
                            override fun onClicked(isApproved: Boolean) {}
                        })
                        dialogInfo.show()
                    }
                }
            }
        }
    }

    private fun postJobComplete() {
        val dialogPickup = DialogPickup(requireActivity(), object : PickupListener {
            override fun onClicked(mode: Int, message: String) {
                dialogLoading.show()
                var requestDriverPickup = RequestDriverPickup(deliveryNo = modelNavbar.deliveryNo, directSupplierSts = "2", directSupplierNote = message, supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE) ?: "", supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT) ?: "", subTitle = locale.language)
                jobViewModel.postDriverJobConfirm(authToken, requestDriverPickup).observe(viewLifecycleOwner) {
                    if (it != null) {
                        when (it) {
                            is RequestState.Loading -> {}

                            is RequestState.Success -> {
                                Log.e("RespAssPost", "" + Gson().toJson(it.data))
                                dialogLoading.dismiss()
                                it.data?.let { data ->
                                    var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_SUCCESS, getString(R.string.MessageSuccess), getString(R.string.InfoSuccessConfirmJob), object : OptionListener {
                                        override fun onClicked(isApproved: Boolean) {
                                            initJob()
                                        }
                                    })
                                    dialogInfo.show()
                                }
                            }

                            is RequestState.Error -> {
                                dialogLoading.dismiss()
                                var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), it.message, object : OptionListener {
                                    override fun onClicked(isApproved: Boolean) {
                                        initJob()
                                    }
                                })
                                dialogInfo.show()
                            }
                        }
                    }
                }
            }
        })
        dialogPickup.show(requireActivity().supportFragmentManager, "DIALOG_PICKUP")
    }

    private fun showButton(status: String) {
        when (status) {
            "0" -> {
                binding.btnProcess.visibility = View.VISIBLE
                binding.btnConfirm.visibility = View.GONE
                binding.btnQRCode.visibility = View.GONE
            }

            "1" -> {
                binding.btnProcess.visibility = View.GONE
                binding.btnConfirm.visibility = View.VISIBLE
                binding.btnQRCode.visibility = View.GONE
            }

            "2" -> {
                binding.btnProcess.visibility = View.GONE
                binding.btnConfirm.visibility = View.GONE
                binding.btnQRCode.visibility = View.VISIBLE
            }
        }
    }

    private fun generateQRCode() {
        parentActivity?.let {
            it?.moveFragment(JobQRCodeFragment.newInstance(Gson().toJson(modelNavbar)))
        }
    }

    private val scrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            if (!recyclerView.canScrollVertically(1)) {
                scrollState = layoutManager?.onSaveInstanceState()
                if (viewModel.isMoreRecord) {
                    getManifest(false)
                } else {
                    if (!isNotifShowed) {
                        Toast.makeText(requireActivity(), getString(R.string.InfoMaxData), Toast.LENGTH_SHORT).show()
                        isNotifShowed = true
                    }
                }
            }
        }
    }
}