package com.tmmin.emanifest.features.driver.main

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.tmmin.emanifest.R
import com.tmmin.emanifest.databinding.ActivityMainDriverBinding
import com.tmmin.emanifest.util.BaseActivity

class DriverMainActivity : BaseActivity() {
    lateinit var binding: ActivityMainDriverBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main_driver)
        setup()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1) {
            super.onBackPressed()
            val newFragment = supportFragmentManager.findFragmentById(R.id.content)
            when (newFragment) {
                newFragment as? HomeFragment -> selectNavigation(0)
                newFragment as? JobHistoryFragment -> selectNavigation(1)
                newFragment as? ProfileFragment -> selectNavigation(2)
            }
        } else {
            finish()
        }
    }

    private fun setup() {
        binding.navigation.setOnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navHome -> {
                    moveFragment(HomeFragment())
                    true
                }

                R.id.navJobHistory -> {
                    moveFragment(JobHistoryFragment())
                    true
                }

                R.id.navProfile -> {
                    moveFragment(ProfileFragment())
                    true
                }

                else -> false
            }
        }
        binding.navigation.selectedItemId = R.id.navHome
    }

    fun selectNavigation(position: Int) {
        if (position == 0) {
            val selectedItem = binding.navigation.menu.findItem(R.id.navHome)
            selectedItem?.isChecked = true
        } else if (position == 1) {
            val selectedItem = binding.navigation.menu.findItem(R.id.navJobHistory)
            selectedItem?.isChecked = true
        } else if (position == 2) {
            val selectedItem = binding.navigation.menu.findItem(R.id.navProfile)
            selectedItem?.isChecked = true
        }
    }

    fun moveFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(R.id.content, fragment).addToBackStack(null).commit()
    }
}