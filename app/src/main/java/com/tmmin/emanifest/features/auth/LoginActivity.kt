package com.tmmin.emanifest.features.auth

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.google.gson.Gson
import com.tmmin.emanifest.BuildConfig
import com.tmmin.emanifest.R
import com.tmmin.emanifest.databinding.ActivityLoginBinding
import com.tmmin.emanifest.dialogs.DialogInfo
import com.tmmin.emanifest.dialogs.DialogLoading
import com.tmmin.emanifest.features.driver.main.DriverMainActivity
import com.tmmin.emanifest.features.operator.main.OperatorMainActivity
import com.tmmin.emanifest.features.others.PermissionsActivity
import com.tmmin.emanifest.listeners.OptionListener
import com.tmmin.emanifest.models.requests.auth.RequestLogin
import com.tmmin.emanifest.models.requests.auth.RequestSupplierInfo
import com.tmmin.emanifest.network.RequestState
import com.tmmin.emanifest.util.BaseActivity
import com.tmmin.emanifest.util.Constant
import com.tmmin.emanifest.viewmodels.AuthViewModel
import kotlinx.android.synthetic.main.activity_login.btnLogin

class LoginActivity : BaseActivity(), View.OnClickListener {
    private val authViewModel: AuthViewModel by viewModels()
    private lateinit var binding: ActivityLoginBinding
    private lateinit var dialogLoading: DialogLoading
    val ROLE_DRIVER = "Driver"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        setup()
    }

    private fun setup() {
        val versionName = BuildConfig.VERSION_NAME
        when (BuildConfig.BASE_URL) {
            BuildConfig.BASE_URL_TRIAL -> {
                binding.tvVersion.setText("V : $versionName" + " (Trial)")
            }

            BuildConfig.BASE_URL_DEV -> {
                binding.tvVersion.setText("V : $versionName" + " (Dev)")
            }

            else -> {
                binding.tvVersion.setText("V : $versionName")
            }
        }
        dialogLoading = DialogLoading(this)
        btnLogin.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.btnLogin -> {
                login()
            }
        }
    }

    private fun login() {
        val username = binding.etUsername.text.toString()
        val password = binding.etPassword.text.toString()
        if (username.isEmpty()) {
            Toast.makeText(this, "Username Cannot Be Empty !", Toast.LENGTH_SHORT).show()
        } else if (password.isEmpty()) {
            Toast.makeText(this, "Password Cannot Be Empty !", Toast.LENGTH_SHORT).show()
        } else if (!isInternetAvailable()) {
            var dialogInfo = DialogInfo(this, DialogInfo.MODE_FAILED, getString(R.string.MessageError), getString(R.string.ErrorInternetConnection), object : OptionListener {
                override fun onClicked(isApproved: Boolean) {}
            })
            dialogInfo.show()
        } else {
            val requestLogin = RequestLogin(username = username, password = password, subTitle = locale.language)
            Log.e("ReqLogin", "" + Gson().toJson(requestLogin))
            dialogLoading.show()
            authViewModel.loginSupplier(requestLogin).observe(this) {
                if (it != null) {
                    when (it) {
                        is RequestState.Loading -> {
                            dialogLoading.show()
                        }

                        is RequestState.Success -> {
                            Log.e("RespLogin", "" + Gson().toJson(it.data))
                            it.data?.let { data ->
                                sharedPref.put(Constant.AUTH_USERNAME, binding.etUsername.text.toString())
                                sharedPref.put(Constant.AUTH_PASSWORD, binding.etPassword.text.toString())
                                sharedPref.put(Constant.AUTH_TOKEN_ACCESS, "Bearer " + it.data?.data?.accessToken)
                                sharedPref.put(Constant.AUTH_TOKEN_REFRESH, it.data?.data?.refreshToken ?: "")
                                sharedPref.put(Constant.AUTH_TOKEN_EXPIRED, it.data?.data?.refreshTokenExpired ?: "")
                                sharedPref.put(Constant.PROFILE_SUPPLIER_PLANT, it.data?.data?.supplierPlant ?: "1")
                                sharedPref.put(Constant.PROFILE_SUPPLIER_CODE, data.data?.supplierCd ?: "")
                                sharedPref.put(Constant.PROFILE_SUPPLIER_ROLE, data.data?.supplierRole ?: "")
                                sharedPref.put(Constant.PROFILE_SUPPLIER_USER, data.data?.supplierUser ?: "")
                                if (hasLaserScanner()) {
                                    sharedPref.put(Constant.SET_SCANNER, 1)
                                } else {
                                    sharedPref.put(Constant.SET_SCANNER, 0)
                                }
                                getSupplierInfo()
                            }
                        }

                        is RequestState.Error -> {
                            Log.e("ErrLogin", "" + it.message)
                            dialogLoading.dismiss()
                            var dialogInfo = DialogInfo(this, DialogInfo.MODE_FAILED, getString(R.string.MessageError), it.message, object : OptionListener {
                                override fun onClicked(isApproved: Boolean) {}
                            })
                            dialogInfo.show()
                        }
                    }
                }
            }
        }
    }

    private fun getSupplierInfo() {
        var supplierRole = sharedPref.getString(Constant.PROFILE_SUPPLIER_ROLE) ?: ""
        var requestSupplierInfo = RequestSupplierInfo(supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE) ?: "", supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT) ?: "1", subTitle = locale.language)
        Log.e("ReqInfo", "" + Gson().toJson(requestSupplierInfo))
        authViewModel.getSupplierInfo(sharedPref.getString(Constant.AUTH_TOKEN_ACCESS) ?: "", requestSupplierInfo).observe(this) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespInfo", "" + Gson().toJson(it.data))
                        dialogLoading.dismiss()
                        it.data?.let { data ->
                            with(sharedPref) {
                                put(Constant.AUTH_IS_LOGGED_IN, true)
                                put(Constant.PROFILE_SUPPLIER_NAME, data.data?.supplierName ?: "")
                                put(Constant.PROFILE_SUPPLIER_Abbreviation, data.data?.supplierAbbreviation ?: "")
                            }
                            var dialogInfo = DialogInfo(this, DialogInfo.MODE_SUCCESS, getString(R.string.MessageSuccess), getString(R.string.InfoWelcome), object : OptionListener {
                                override fun onClicked(isApproved: Boolean) {
                                    if (supplierRole.equals(ROLE_DRIVER, true)) {
                                        val intent = Intent(this@LoginActivity, DriverMainActivity::class.java)
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                        startActivity(intent)
                                        finish()
                                    } else {
                                        if (hasPermissions()) {
                                            val intent = Intent(this@LoginActivity, OperatorMainActivity::class.java)
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                            startActivity(intent)
                                            finish()
                                        } else {
                                            val intent = Intent(this@LoginActivity, PermissionsActivity::class.java)
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                            startActivity(intent)
                                            finish()
                                        }
                                    }
                                }
                            })
                            dialogInfo.show()
                        }
                    }

                    is RequestState.Error -> {
                        Log.e("ErrInfo", "" + it.message)
                        dialogLoading.dismiss()
                        var dialogInfo = DialogInfo(this, DialogInfo.MODE_FAILED, getString(R.string.MessageError), getString(R.string.ErrorServer), object : OptionListener {
                            override fun onClicked(isApproved: Boolean) {}
                        })
                        dialogInfo.show()
                    }
                }
            }
        }
    }

    private fun permissions(): Array<String>? {
        val permission: Array<String>
        permission = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
            arrayOf(Manifest.permission.CAMERA, Manifest.permission.BLUETOOTH_CONNECT, Manifest.permission.BLUETOOTH_SCAN, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            arrayOf(Manifest.permission.CAMERA, Manifest.permission.READ_MEDIA_IMAGES, Manifest.permission.BLUETOOTH_CONNECT, Manifest.permission.BLUETOOTH_SCAN, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            arrayOf(Manifest.permission.CAMERA, Manifest.permission.BLUETOOTH_CONNECT, Manifest.permission.BLUETOOTH_SCAN, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
        } else {
            arrayOf(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
        }
        return permission
    }

    private fun hasPermissions(): Boolean {
        return permissions()!!.all {
            ContextCompat.checkSelfPermission(this, it) == PackageManager.PERMISSION_GRANTED
        }
    }
}