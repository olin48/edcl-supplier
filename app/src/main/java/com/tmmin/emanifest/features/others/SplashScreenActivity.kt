package com.tmmin.emanifest.features.others

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.tmmin.emanifest.R
import com.tmmin.emanifest.features.auth.LoginActivity
import com.tmmin.emanifest.features.driver.main.DriverMainActivity
import com.tmmin.emanifest.features.operator.main.OperatorMainActivity
import com.tmmin.emanifest.models.requests.auth.RequestRefreshToken
import com.tmmin.emanifest.network.RequestState
import com.tmmin.emanifest.util.BaseActivity
import com.tmmin.emanifest.util.Constant
import com.tmmin.emanifest.viewmodels.AuthViewModel
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class SplashScreenActivity : BaseActivity() {
    private val authViewModel: AuthViewModel by viewModels()
    val ROLE_DRIVER = "Driver"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        setup()
    }

    private fun setup() {
        if (sharedPref.getBoolean(Constant.AUTH_IS_LOGGED_IN)) {
            val date = sharedPref.getString(Constant.AUTH_TOKEN_EXPIRED) ?: ""
            try {
                if (isDateNowGreaterThan(date)) {
                    getNewToken()
                } else {
                    redirectPage(true)
                }
            } catch (e: Exception) {
                redirectPage(false)
            }
        } else {
            redirectPage(false)
        }
    }

    private fun permissions(): Array<String>? {
        val permission: Array<String>
        permission = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            arrayOf(Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.BLUETOOTH_CONNECT, Manifest.permission.BLUETOOTH_SCAN)
        } else {
            arrayOf(Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
        }
        return permission
    }

    private fun hasPermissions(): Boolean {
        return permissions()!!.all {
            ContextCompat.checkSelfPermission(this, it) == PackageManager.PERMISSION_GRANTED
        }
    }

    fun isDateNowGreaterThan(dateString: String): Boolean {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val dateToCompare: Date = dateFormat.parse(dateString) ?: return false
        val now = Date()
        return now.after(dateToCompare)
    }

    private fun redirectPage(isAuthorized: Boolean) {
        Handler().postDelayed({
            if (isAuthorized) {
                var supplierRole = sharedPref.getString(Constant.PROFILE_SUPPLIER_ROLE) ?: ""
                if (supplierRole.equals(ROLE_DRIVER, true)) {
                    val intent = Intent(this@SplashScreenActivity, DriverMainActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                    finish()
                } else {
                    if (hasPermissions()) {
                        val intent = Intent(this@SplashScreenActivity, OperatorMainActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                        finish()
                    } else {
                        val intent = Intent(this@SplashScreenActivity, PermissionsActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                        finish()
                    }
                }
            } else {
                val intent = Intent(this@SplashScreenActivity, LoginActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
                finish()
            }
        }, 1500)
    }

    private fun getNewToken() {
        var requestRefreshToken = RequestRefreshToken(refreshToken = sharedPref.getString(Constant.AUTH_TOKEN_REFRESH) ?: "")
        authViewModel.refreshToken(authToken, requestRefreshToken).observe(this) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespRefreshToken", "" + Gson().toJson(it.data))
                        sharedPref.put(Constant.AUTH_TOKEN_ACCESS, "Bearer " + it.data?.data?.accessToken)
                        sharedPref.put(Constant.AUTH_TOKEN_REFRESH, it.data?.data?.refreshToken ?: "")
                        sharedPref.put(Constant.AUTH_TOKEN_EXPIRED, it.data?.data?.refreshTokenExpired ?: "")
                        redirectPage(true)
                    }

                    is RequestState.Error -> {
                        Log.e("ErrRefreshToken", "" + it.message)
                        redirectPage(false)
                    }
                }
            }
        }
    }
}