package com.tmmin.emanifest.features.operator.main

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.tmmin.emanifest.R
import com.tmmin.emanifest.databinding.ActivityMainBinding
import com.tmmin.emanifest.features.operator.kanban.ManifestKanbanScanFragment
import com.tmmin.emanifest.features.operator.kanban.ManifestKanbanScanOfflineFragment
import com.tmmin.emanifest.features.operator.manifest.detail.ManifestDetailFragment
import com.tmmin.emanifest.features.operator.manifest.list.ManifestListFragment
import com.tmmin.emanifest.helpers.DataHelper
import com.tmmin.emanifest.util.BaseActivity

class OperatorMainActivity : BaseActivity() {
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setup()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1) {
            val fragment = supportFragmentManager.findFragmentById(R.id.content)
            if (fragment is ManifestKanbanScanFragment || fragment is ManifestKanbanScanOfflineFragment) {
                return
            } else {
                super.onBackPressed()
                val newFragment = supportFragmentManager.findFragmentById(R.id.content)
                when (newFragment) {
                    newFragment as? HomeFragment -> selectNavigation(0)
                    newFragment as? JobListFragment -> selectNavigation(1)
                    newFragment as? TrackOrderFragment -> selectNavigation(2)
                    newFragment as? JobHistoryFragment -> selectNavigation(3)
                    newFragment as? ProfileFragment -> selectNavigation(4)
                }
            }
        } else {
            finish()
        }
        val fragment = supportFragmentManager.findFragmentById(R.id.content)
        if (fragment is ManifestKanbanScanFragment || fragment is ManifestKanbanScanOfflineFragment) {
            binding.navigation.visibility = View.GONE
        } else {
            binding.navigation.visibility = View.VISIBLE
        }
    }

    private fun setup() {
        binding.navigation.setOnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navHome -> {
                    moveFragment(HomeFragment())
                    true
                }

                R.id.navJobList -> {
                    moveFragment(JobListFragment())
                    true
                }

                R.id.navTrackOrder -> {
                    moveFragment(TrackOrderFragment())
                    true
                }

                R.id.navJobHistory -> {
                    moveFragment(JobHistoryFragment())
                    true
                }

                R.id.navProfile -> {
                    moveFragment(ProfileFragment())
                    true
                }

                else -> false
            }
        }
        if (intent == null) {
            binding.navigation.selectedItemId = R.id.navHome
        } else {
            if (intent.hasExtra(DataHelper.DATA_NAVTO_MANIFEST_DETAIL)) {
                val dataManifestNo = intent.getStringExtra(DataHelper.DATA_MANIFEST_NO)
                val dataIsHistory = intent.getBooleanExtra(DataHelper.DATA_IS_HISTORY, false)
                val dataIsDirect = intent.getBooleanExtra(DataHelper.DATA_IS_DIRECT, false)
                moveFragment(ManifestDetailFragment.newInstance(dataManifestNo ?: "", dataIsHistory, dataIsDirect))
            } else if (intent.hasExtra(DataHelper.DATA_NAVTO_MANIFEST_LIST)) {
                val dataNavbar = intent.getStringExtra(DataHelper.DATA_NAVBAR) ?: ""
                val dataIsDirect = intent.getBooleanExtra(DataHelper.DATA_NAVTO_MANIFEST_LIST, false)
                val dataIsTransporter = intent.getBooleanExtra(DataHelper.DATA_IS_TRANSPORTER, false)
                val dataIsNoOrder = intent.getBooleanExtra(DataHelper.DATA_IS_NO_ORDER, false)
                val dataIsSupplierPreparation = intent.getBooleanExtra(DataHelper.DATA_IS_SUPPLIER_PREPARATION, false)
                moveFragment(ManifestListFragment.newInstance(dataNavbar, false, dataIsDirect, dataIsTransporter, dataIsNoOrder, dataIsSupplierPreparation))
            } else if (intent.hasExtra(DataHelper.DATA_NAVTO_FRAGMENT)) {
                val fragmentType = intent.getIntExtra(DataHelper.DATA_NAVTO_FRAGMENT, 0)
                when (fragmentType) {
                    0 -> moveFragment(HomeFragment())
                    1 -> moveFragment(JobListFragment())
                    2 -> moveFragment(TrackOrderFragment())
                    3 -> moveFragment(JobHistoryFragment())
                    4 -> moveFragment(ProfileFragment())
                }
                selectNavigation(fragmentType)
            } else {
                binding.navigation.selectedItemId = R.id.navHome
            }
        }
    }

    fun selectNavigation(position: Int) {
        if (position == 0) {
            val selectedItem = binding.navigation.menu.findItem(R.id.navHome)
            selectedItem?.isChecked = true
        } else if (position == 1) {
            val selectedItem = binding.navigation.menu.findItem(R.id.navJobList)
            selectedItem?.isChecked = true
        } else if (position == 2) {
            val selectedItem = binding.navigation.menu.findItem(R.id.navTrackOrder)
            selectedItem?.isChecked = true
        } else if (position == 3) {
            val selectedItem = binding.navigation.menu.findItem(R.id.navJobHistory)
            selectedItem?.isChecked = true
        } else if (position == 4) {
            val selectedItem = binding.navigation.menu.findItem(R.id.navProfile)
            selectedItem?.isChecked = true
        }
    }

    fun moveFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(R.id.content, fragment).addToBackStack(null).commit()
    }

    fun moveAndDestroyFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(R.id.content, fragment).addToBackStack(null).commit()
    }

    fun enableNavigation(isEnable: Boolean) {
        binding.navigation.isEnabled = isEnable
    }
}