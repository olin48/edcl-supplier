package com.tmmin.emanifest.features.operator.settings

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.tmmin.emanifest.R
import com.tmmin.emanifest.databinding.ActivitySettingsBinding

class SettingsActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var binding: ActivitySettingsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_settings)
        setup()
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.layoutToolbar.btnBack -> {finish()}
            binding.btnSettingProfile -> {}
            binding.btnSettingKanban -> {
                val intent = Intent(this@SettingsActivity, SettingKanbanActivity::class.java)
                startActivity(intent)
            }
            binding.btnSettingPassword -> {}
        }
    }

    private fun setup(){
        binding.layoutToolbar.tvTitle.visibility = View.VISIBLE
        binding.layoutToolbar.btnBack.visibility = View.VISIBLE
        binding.layoutToolbar.tvTitle.text = "Settings"
        binding.layoutToolbar.btnBack.setOnClickListener(this)
        binding.btnSettingProfile.setOnClickListener(this)
        binding.btnSettingKanban.setOnClickListener(this)
        binding.btnSettingPassword.setOnClickListener(this)
    }
}