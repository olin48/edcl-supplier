package com.tmmin.emanifest.features.operator.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import com.google.gson.Gson
import com.tmmin.emanifest.R
import com.tmmin.emanifest.databinding.FragmentHomeBinding
import com.tmmin.emanifest.features.operator.job.delivery.JobDeliveryFragment
import com.tmmin.emanifest.features.operator.job.supplier.JobSupplierActivity
import com.tmmin.emanifest.features.operator.job.transporter.JobDeliverActivity
import com.tmmin.emanifest.features.operator.manifest.others.ManifestOthersActivity
import com.tmmin.emanifest.helpers.DataHelper
import com.tmmin.emanifest.models.requests.job.RequestJobSupplierTotal
import com.tmmin.emanifest.models.requests.manifest.others.RequestManifestOthers
import com.tmmin.emanifest.network.RequestState
import com.tmmin.emanifest.util.BaseFragment
import com.tmmin.emanifest.util.Constant
import com.tmmin.emanifest.viewmodels.AuthViewModel
import com.tmmin.emanifest.viewmodels.JobViewModel
import com.tmmin.emanifest.viewmodels.ManifestViewModel
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class HomeFragment : BaseFragment(), View.OnClickListener {
    val titleJobSupplier = arrayListOf("ON PROGRESS", "READY TO PICKUP")
    private val manifestViewModel: ManifestViewModel by viewModels()
    private val authViewModel: AuthViewModel by viewModels()
    private val jobViewModel: JobViewModel by viewModels()
    private lateinit var binding: FragmentHomeBinding
    private lateinit var dateTime: String
    var parentActivity: OperatorMainActivity? = null
    var totalOthersShortage = 0
    var totalOthersEO = 0
    var totalOthers = 0
    var totalJobProgress = 0
    var totalJobReady = 0
    var isGetJobTotalSuccess = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OperatorMainActivity) {
            parentActivity = context
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btnJobList -> {
                parentActivity?.let {
                    it.binding?.navigation?.selectedItemId = R.id.navJobList
                }
            }

            binding.btnJobHistory -> {
                parentActivity?.let {
                    it.binding?.navigation?.selectedItemId = R.id.navJobHistory
                }
            }

            binding.btnJobDelivery -> {
                parentActivity?.let {
                    it?.moveFragment(JobDeliveryFragment())
                }
            }

            binding.btnJobManifest -> {
                val dataList = arrayListOf("EO (" + totalOthersEO + ")", "Shortage (" + totalOthersShortage + ")", "History")
                parentActivity?.selectNavigation(0)
                val intent = Intent(parentActivity, ManifestOthersActivity::class.java)
                intent.putStringArrayListExtra(DataHelper.DATA_LIST, dataList)
                startActivity(intent)
            }

            binding.btnCurrentSupplier -> {
                if (isGetJobTotalSuccess) {
                    titleJobSupplier[0] = "ON PROGRESS (" + totalJobProgress + ")"
                    titleJobSupplier[1] = "READY TO PICKUP (" + totalJobReady + ")"
                }
                parentActivity?.selectNavigation(0)
                val intent = Intent(parentActivity, JobSupplierActivity::class.java)
                intent.putStringArrayListExtra(DataHelper.DATA_LIST, titleJobSupplier)
                startActivity(intent)
            }

            binding.btnCurrentDeliver -> {
                parentActivity?.selectNavigation(0)
                val intent = Intent(parentActivity, JobDeliverActivity::class.java)
                startActivity(intent)
            }
        }
    }

    private fun setup() {
        parentActivity = activity as? OperatorMainActivity
        binding.tvCompany.text = sharedPref.getString(Constant.PROFILE_SUPPLIER_NAME)
        binding.tvSuppCode.text = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE)
        binding.tvSuppDiv.text = " - "
        binding.tvSuppPlant.text = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT)
        binding.btnJobList.visibility = View.VISIBLE
        binding.btnJobHistory.visibility = View.VISIBLE
        binding.tvTitleManifest.visibility = View.VISIBLE
        binding.btnJobManifest.visibility = View.VISIBLE
        binding.btnJobList.setOnClickListener(this)
        binding.btnJobHistory.setOnClickListener(this)
        binding.btnCurrentSupplier.setOnClickListener(this)
        binding.btnJobManifest.setOnClickListener(this)
        binding.btnCurrentDeliver.setOnClickListener(this)
        binding.btnJobDelivery.setOnClickListener(this)
        getDateTime()
        getSettingDockCode()
        getJobSupplierTotal()
        getManifestOthersTotal()
        Log.e("GetDockCodes", sharedPref.getString(Constant.SET_PRINTER) ?: "")
    }

    private fun getDateTime() {
        val currentDate = Date()
        val formatter = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        dateTime = formatter.format(currentDate)
    }

    private fun getManifestOthersTotal() {
        binding.layoutLoading.startShimmer()
        binding.layoutData.visibility = View.GONE
        binding.layoutLoading.visibility = View.VISIBLE
        var requestManifestOthers = RequestManifestOthers(supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE) ?: "", supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT) ?: "", subTitle = locale.language)
        manifestViewModel.getTotalManifestOthers(sharedPref.getString(Constant.AUTH_TOKEN_ACCESS) ?: "", requestManifestOthers).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {
                        binding.layoutManifestOthers.visibility = View.VISIBLE
                        binding.tvInfoManifestOthers.visibility = View.GONE
                    }

                    is RequestState.Success -> {
                        Log.e("RespManOthers", "" + Gson().toJson(it.data))
                        binding.layoutLoading.stopShimmer()
                        binding.layoutData.visibility = View.VISIBLE
                        binding.layoutLoading.visibility = View.GONE
                        it.data?.let { data ->
                            totalOthers = data?.data?.totalOthersManifest ?: 0
                            totalOthersEO = data?.data?.totalEOManifest ?: 0
                            totalOthersShortage = data?.data?.totalShortageManifest ?: 0
                            binding.tvTotalManifest.setText(totalOthers.toString())
                        }
                    }

                    is RequestState.Error -> {
                        binding.layoutLoading.stopShimmer()
                        binding.layoutData.visibility = View.VISIBLE
                        binding.layoutLoading.visibility = View.GONE
                        binding.layoutManifestOthers.visibility = View.GONE
                        binding.tvInfoManifestOthers.visibility = View.VISIBLE
                        binding.tvInfoManifestOthers.setText(it.message ?: getString(R.string.ErrorServer))
                    }
                }
            }
        }
    }

    private fun getJobSupplierTotal() {
        isGetJobTotalSuccess = false
        binding.btnCurrentSupplier.isEnabled = false
        binding.btnCurrentSupplier.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.ColorPlaceholder)
        var requestJobSupplierTotal = RequestJobSupplierTotal(supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE) ?: "", supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT) ?: "", keywordOnProgress = "", keywordReadyToPickup = "", subTitle = locale.language)
        jobViewModel.getJobSupplierProgressTotal(sharedPref.getString(Constant.AUTH_TOKEN_ACCESS) ?: "", requestJobSupplierTotal).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespJobSupTotal", "" + Gson().toJson(it.data))
                        binding.btnCurrentSupplier.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.ColorWhite)
                        binding.btnCurrentSupplier.isEnabled = true
                        isGetJobTotalSuccess = true
                        it.data.data?.let {
                            totalJobProgress = it.totalDeliveryOnProgress ?: 0
                            totalJobReady = it.totalDeliveryReadyToPickup ?: 0
                        }
                    }

                    is RequestState.Error -> {
                        Log.e("ErrJobSupTotal", "" + it.message)
                        binding.btnCurrentSupplier.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.ColorWhite)
                        binding.btnCurrentSupplier.isEnabled = true
                        isGetJobTotalSuccess = false
                    }
                }
            }
        }
    }

    private fun getSettingDockCode() {
        authViewModel.getSettingDockCode(authToken, locale.language).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespSetDockCode", "" + Gson().toJson(it.data))
                        it.data.let {
                            sharedPref.put(Constant.SET_PRINTER, Gson().toJson(it))
                        }
                    }

                    is RequestState.Error -> {
                        Log.e("ErrSetDockCode", "" + it.message)
                    }
                }
            }
        }
    }
}