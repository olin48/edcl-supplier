package com.tmmin.emanifest.features.operator.job.transporter

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.tmmin.emanifest.R
import com.tmmin.emanifest.adapters.JobAdapter
import com.tmmin.emanifest.databinding.FragmentJobDeliverDirectListBinding
import com.tmmin.emanifest.dialogs.DialogInfo
import com.tmmin.emanifest.dialogs.DialogLoading
import com.tmmin.emanifest.listeners.OptionListener
import com.tmmin.emanifest.models.requests.job.RequestJobList
import com.tmmin.emanifest.network.RequestState
import com.tmmin.emanifest.util.BaseFragment
import com.tmmin.emanifest.util.Constant
import com.tmmin.emanifest.viewmodels.JobViewModel
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class JobDeliverDirectFragment : BaseFragment(), View.OnClickListener {
    private lateinit var dateTime: String
    private val jobViewModel: JobViewModel by viewModels()
    private lateinit var binding: FragmentJobDeliverDirectListBinding
    private lateinit var jobSearchAdapter: JobAdapter
    private lateinit var jobListAdapter: JobAdapter
    private lateinit var dialogLoading: DialogLoading
    private var parentActivity: JobDeliverActivity? = null
    var layoutManager: LinearLayoutManager? = null
    var scrollState: Parcelable? = null
    var runnable: Runnable? = null
    val handler = Handler()
    var isSearchMode = false
    var query = ""
    var isNotifShowed = false
    var isNotifSearchShowed = false
    var sortBySelected = 3

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_job_deliver_direct_list, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        setup()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is JobDeliverActivity) {
            parentActivity = context
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btnSearchClose -> {
                binding.etSearch.setText("")
            }

            binding.btnSort -> {
                sortJob()
            }
        }
    }

    private fun setup() {
        parentActivity = activity as? JobDeliverActivity
        binding.btnSearchClose.setOnClickListener(this)
        binding.btnSort.setOnClickListener(this)
        layoutManager = LinearLayoutManager(requireActivity())
        dialogLoading = DialogLoading(requireActivity())
        dialogLoading.setCancelable(true)
        resetData()
        handleSearchJob()
        handleGetJob()
        getDateTime()
        getJob(true)
        binding.etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                runnable?.let { handler.removeCallbacks(it) }
            }

            override fun afterTextChanged(s: Editable?) {
                binding.rcyJob.visibility = View.VISIBLE
                binding.tvInfoJob.visibility = View.GONE
                if (s.toString().length == 0) {
                    isSearchMode = false
                    binding.btnSearchClose.visibility = View.GONE
                    binding.btnSearchSubmit.visibility = View.VISIBLE
                    binding.rcyJob.adapter = jobListAdapter
                } else {
                    isSearchMode = true
                    binding.btnSearchClose.visibility = View.VISIBLE
                    binding.btnSearchSubmit.visibility = View.GONE
                    binding.rcyJob.adapter = jobSearchAdapter
                    runnable = Runnable {
                        searchJob(true)
                    }
                    handler.postDelayed(runnable!!, 2000)
                }
            }
        })
    }

    private fun resetData() {
        jobListAdapter = JobAdapter(false, false, true, parentActivity!!)
        jobSearchAdapter = JobAdapter(false, false, true, parentActivity!!)
        binding.rcyJob.adapter = jobListAdapter
        binding.rcyJob.layoutManager = layoutManager
        binding.rcyJob.addOnScrollListener(scrollListener)
    }

    private fun getDateTime() {
        val currentDate = Date()
        val formatter = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        dateTime = formatter.format(currentDate)
    }

    private fun getJob(isStarted: Boolean) {
        if (isStarted) {
            binding.rcyJob.smoothScrollToPosition(0)
            binding.layoutLoading.visibility = View.VISIBLE
            binding.tvInfoJob.visibility = View.GONE
            binding.rcyJob.visibility = View.GONE
            binding.layoutLoading.startShimmer()
        } else {
            dialogLoading.show()
        }
        var requestJobList = RequestJobList(sortBy = sortBySelected.toString(), supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE) ?: "", supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT) ?: "", pickupDtFrom = "", pickupDtTo = "", subTitle = locale.language)
        jobViewModel.getJobList(4, isStarted, sharedPref.getString(Constant.AUTH_TOKEN_ACCESS) ?: "", requestJobList)
    }

    private fun searchJob(isStarted: Boolean) {
        query = binding.etSearch.text.toString()
        if (!query.isEmpty()) {
            if (isStarted) {
                binding.rcyJob.smoothScrollToPosition(0)
                binding.layoutLoading.visibility = View.VISIBLE
                binding.tvInfoJob.visibility = View.GONE
                binding.rcyJob.visibility = View.GONE
                binding.layoutLoading.startShimmer()
            } else {
                dialogLoading.show()
            }
            var requestJobList = RequestJobList(keyword = query, sortBy = sortBySelected.toString(), supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE) ?: "", supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT) ?: "", pickupDtFrom = "", pickupDtTo = "", subTitle = locale.language)
            jobViewModel.getJobListSearch(4, isStarted, sharedPref.getString(Constant.AUTH_TOKEN_ACCESS) ?: "", requestJobList)
        }
    }

    private fun sortJob() {
        dialogLoading.show()
        jobViewModel.getJobCombo(authToken, locale.language).observe(viewLifecycleOwner){
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        dialogLoading.dismiss()
                        it.data?.let { data ->
                            var size = data.data?.size ?: 0
                            if (size > 0) {
//                                var dialogSort = DialogSort(requireActivity(), data.data, sortBySelected, object : SortListener {
//                                    override fun onClicked(index: Int, dateFrom: String, dateTo: String) {
//                                        resetData()
//                                        dialogLoading.show()
//                                        var sortBy = data.data.get(index).value ?: ""
//                                        var requestJobList = RequestJobList(sortBy = sortBy, supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE) ?: "", supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT) ?: "", pickupDtFrom = dateFrom, pickupDtTo = dateTo, subTitle = locale.language)
//                                        jobViewModel.getJobList(4, true, sharedPref.getString(Constant.AUTH_TOKEN_ACCESS) ?: "", requestJobList)
//                                    }
//                                })
//                                dialogSort.show()
                            } else {
                                var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), it.data?.message ?: getString(R.string.ErrorSort), object : OptionListener {
                                    override fun onClicked(isApproved: Boolean) {}
                                })
                                dialogInfo.show()
                            }
                        }
                    }

                    is RequestState.Error -> {
                        dialogLoading.dismiss()
                        var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), it.message, object : OptionListener {
                            override fun onClicked(isApproved: Boolean) {}
                        })
                        dialogInfo.show()
                    }
                }
            }
        }
    }

    private fun handleGetJob() {
        jobViewModel.jobListResponse.observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespJobList", "" + Gson().toJson(it.data))
                        dialogLoading.dismiss()
                        binding.layoutLoading.stopShimmer()
                        var size = it.data?.data?.size ?: 0
                        if (size > 0) {
                            it.data?.data?.let { data -> jobListAdapter?.differ?.submitList(data.toList()) }
                            binding.rcyJob.adapter = jobListAdapter
                            binding.rcyJob.visibility = View.VISIBLE
                            binding.layoutLoading.visibility = View.GONE
                            binding.tvInfoJob.visibility = View.GONE

                            try {
                                scrollState?.let {
                                    layoutManager?.onRestoreInstanceState(it)
                                }
                            } catch (e: Exception) { }
                        } else {
                            binding.rcyJob.visibility = View.GONE
                            binding.layoutLoading.visibility = View.GONE
                            binding.tvInfoJob.visibility = View.VISIBLE
                            binding.tvInfoJob.text = "No Job"
                        }
                    }

                    is RequestState.Error -> {
                        dialogLoading.dismiss()
                        binding.layoutLoading.stopShimmer()
                        binding.rcyJob.visibility = View.GONE
                        binding.layoutLoading.visibility = View.GONE
                        binding.tvInfoJob.visibility = View.VISIBLE
                        binding.tvInfoJob.text = it.message
                    }
                }
            }
        }
    }

    private fun handleSearchJob() {
        jobViewModel.jobListSearchResponse.observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        dialogLoading.dismiss()
                        binding.layoutLoading.stopShimmer()
                        var size = it.data?.data?.size ?: 0
                        if (size > 0) {
                            it.data?.data?.let { data -> jobSearchAdapter?.differ?.submitList(data.toList()) }
                            binding.rcyJob.adapter = jobSearchAdapter
                            binding.rcyJob.visibility = View.VISIBLE
                            binding.layoutLoading.visibility = View.GONE
                            binding.tvInfoJob.visibility = View.GONE

                            try {
                                scrollState?.let {
                                    layoutManager?.onRestoreInstanceState(it)
                                }
                            } catch (e: Exception) { }
                        } else {
                            binding.rcyJob.visibility = View.GONE
                            binding.layoutLoading.visibility = View.GONE
                            binding.tvInfoJob.visibility = View.VISIBLE
                            binding.tvInfoJob.text = "Job Not Found"
                        }
                    }

                    is RequestState.Error -> {
                        dialogLoading.dismiss()
                        binding.layoutLoading.stopShimmer()
                        binding.tvInfoJob.text = it.message
                        binding.rcyJob.visibility = View.GONE
                        binding.layoutLoading.visibility = View.GONE
                        binding.tvInfoJob.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    private val scrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            if (!recyclerView.canScrollVertically(1)) {
                scrollState = layoutManager?.onSaveInstanceState()
                if (isSearchMode == true) {
                    if (jobViewModel.isMoreSearchRecordDelDirect == true) {
                        searchJob(false)
                    } else {
                        if (!isNotifSearchShowed) {
                            Toast.makeText(requireActivity(), getString(R.string.InfoMaxData), Toast.LENGTH_SHORT).show()
                            isNotifSearchShowed = true
                        }
                    }
                } else {
                    Log.e("ScrollMore", "" + jobViewModel.isMoreRecordDelDirect)
                    if (jobViewModel.isMoreRecordDelDirect == true) {
                        getJob(false)
                    } else {
                        if (!isNotifShowed) {
                            Toast.makeText(requireActivity(), getString(R.string.InfoMaxData), Toast.LENGTH_SHORT).show()
                            isNotifShowed = true
                        }
                    }
                }
            }
        }
    }
}