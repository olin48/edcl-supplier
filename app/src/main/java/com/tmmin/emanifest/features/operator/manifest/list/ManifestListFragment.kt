package com.tmmin.emanifest.features.operator.manifest.list

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.tmmin.emanifest.R
import com.tmmin.emanifest.adapters.ManifestAdapter
import com.tmmin.emanifest.databinding.FragmentManifestListBinding
import com.tmmin.emanifest.dialogs.DialogAssign
import com.tmmin.emanifest.dialogs.DialogInfo
import com.tmmin.emanifest.dialogs.DialogKanban
import com.tmmin.emanifest.dialogs.DialogLoading
import com.tmmin.emanifest.dialogs.DialogManifestCompleted
import com.tmmin.emanifest.features.operator.main.OperatorMainActivity
import com.tmmin.emanifest.helpers.DataHelper
import com.tmmin.emanifest.helpers.PreferenceHelper
import com.tmmin.emanifest.listeners.AssignListener
import com.tmmin.emanifest.listeners.OptionListener
import com.tmmin.emanifest.models.others.ModelNavbar
import com.tmmin.emanifest.models.requests.assign.RequestAssign
import com.tmmin.emanifest.models.requests.assign.RequestAssignCombo
import com.tmmin.emanifest.models.requests.manifest.RequestManifestEnd
import com.tmmin.emanifest.models.requests.manifest.RequestManifestEndCheck
import com.tmmin.emanifest.models.requests.manifest.RequestManifestList
import com.tmmin.emanifest.models.requests.manifest.RequestManifestStart
import com.tmmin.emanifest.network.RequestState
import com.tmmin.emanifest.util.BaseFragment
import com.tmmin.emanifest.util.Constant
import com.tmmin.emanifest.viewmodels.JobViewModel
import com.tmmin.emanifest.viewmodels.ManifestViewModel

class ManifestListFragment : BaseFragment(), View.OnClickListener {
    private val viewModel: ManifestViewModel by viewModels()
    private val jobViewModel: JobViewModel by viewModels()
    private lateinit var dialogLoading: DialogLoading
    lateinit var binding: FragmentManifestListBinding
    lateinit var manifestAdapter: ManifestAdapter
    lateinit var modelNavbar: ModelNavbar
    var parentActivity: OperatorMainActivity? = null
    var dataNavbar: String? = null
    var isHistory: Boolean = false
    var isDirect: Boolean = false
    var isTransporter: Boolean = false
    var isNoOrder: Boolean = false
    var isSupplierPreparation: Boolean = false
    var isNotifShowed = false
    var layoutManager: LinearLayoutManager? = null
    var scrollState: Parcelable? = null
    var manifestSelected = ""
    var driverSelected = ""

    companion object {
        fun newInstance(paramNavbar: String, paramIsHistory: Boolean = false, paramIsDirect: Boolean = false, paramIsTransporter: Boolean = false, paramIsNoOrder: Boolean = false, paramIsSupplierPreparation: Boolean = false): ManifestListFragment {
            val fragment = ManifestListFragment()
            val args = Bundle()
            args.putString(DataHelper.DATA_NAVBAR, paramNavbar)
            args.putBoolean(DataHelper.DATA_IS_HISTORY, paramIsHistory)
            args.putBoolean(DataHelper.DATA_IS_DIRECT, paramIsDirect)
            args.putBoolean(DataHelper.DATA_IS_TRANSPORTER, paramIsTransporter)
            args.putBoolean(DataHelper.DATA_IS_NO_ORDER, paramIsNoOrder)
            args.putBoolean(DataHelper.DATA_IS_SUPPLIER_PREPARATION, paramIsSupplierPreparation)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            dataNavbar = it.getString(DataHelper.DATA_NAVBAR)
            isHistory = it.getBoolean(DataHelper.DATA_IS_HISTORY)
            isDirect = it.getBoolean(DataHelper.DATA_IS_DIRECT)
            isTransporter = it.getBoolean(DataHelper.DATA_IS_TRANSPORTER)
            isNoOrder = it.getBoolean(DataHelper.DATA_IS_NO_ORDER)
            isSupplierPreparation = it.getBoolean(DataHelper.DATA_IS_SUPPLIER_PREPARATION)
            modelNavbar = Gson().fromJson(dataNavbar, ModelNavbar::class.java)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_manifest_list, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
    }

    override fun onResume() {
        super.onResume()
        getManifest(true)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OperatorMainActivity) {
            parentActivity = context
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.layoutToolbar.btnBack -> {
                parentActivity?.let {
                    it.onBackPressed()
                }
            }

            binding.btnSubmit -> {
                submitJob()
            }

            binding.btnAssign -> {
                getAssignCombo()
            }

            binding.btnEditDriver -> {
                getAssignCombo()
            }
        }
    }

    fun setup() {
        binding.layoutToolbar.tvTitle.visibility = View.VISIBLE
        binding.layoutToolbar.btnBack.visibility = View.VISIBLE
        binding.layoutToolbar.tvTitle.text = "Manifest List"
        binding.tvRoute.text = modelNavbar.route
        binding.tvRate.text = modelNavbar.rate
        binding.tvPartner.text = modelNavbar.partner
        binding.tvPickupDate.text = modelNavbar.date
        binding.btnSubmit.visibility = View.GONE
        binding.btnAssign.visibility = View.GONE
        binding.btnEditDriver.visibility = View.GONE
        if (isHistory) {
            binding.btnSubmit.visibility = View.GONE
            binding.btnAssign.visibility = View.GONE
            binding.btnEditDriver.visibility = View.GONE
        } else if (isSupplierPreparation) {
            getAssignedDriver()
        } else {
            if (isNoOrder) {
                binding.btnSubmit.visibility = View.VISIBLE
                binding.btnAssign.visibility = View.GONE
                binding.btnEditDriver.visibility = View.GONE
            }
        }
        parentActivity = activity as? OperatorMainActivity
        layoutManager = LinearLayoutManager(requireActivity())
        dialogLoading = DialogLoading(requireActivity())
        dialogLoading.setCancelable(true)
        sharedPref = PreferenceHelper(requireActivity())
        binding.layoutToolbar.btnBack.setOnClickListener(this)
        binding.btnSubmit.setOnClickListener(this)
        binding.btnAssign.setOnClickListener(this)
        binding.btnEditDriver.setOnClickListener(this)
        manifestAdapter = ManifestAdapter(parentActivity!!, isHistory, isTransporter)
        binding.rcyManifest.adapter = manifestAdapter
        binding.rcyManifest.layoutManager = layoutManager
        binding.rcyManifest.addOnScrollListener(scrollListener)
        handleGetManifests()
    }

    private val scrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            if (!recyclerView.canScrollVertically(1)) {
                scrollState = layoutManager?.onSaveInstanceState()
                if (viewModel.isMoreRecord == true) {
                    getManifest(false)
                } else {
                    if (!isNotifShowed) {
                        Toast.makeText(requireActivity(), getString(R.string.InfoMaxData), Toast.LENGTH_SHORT).show()
                        isNotifShowed = true
                    }
                }
            }
        }
    }

    private fun getManifest(isStarted: Boolean) {
        if (isStarted) {
            binding.rcyManifest.smoothScrollToPosition(0)
            binding.layoutLoading.visibility = View.VISIBLE
            binding.tvInfoManifest.visibility = View.GONE
            binding.rcyManifest.visibility = View.GONE
            binding.layoutLoading.startShimmer()
        } else {
            dialogLoading.show()
        }
        var requestManifestList = RequestManifestList(deliveryNo = modelNavbar.deliveryNo, supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT) ?: "", supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE) ?: "", subTitle = locale.language)
        Log.e("ReqManList", "" + Gson().toJson(requestManifestList))
        viewModel.getManifestList(isStarted, sharedPref.getString(Constant.AUTH_TOKEN_ACCESS) ?: "", requestManifestList)
    }

    private fun handleGetManifests() {
        viewModel.manifestListResponse.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespListMan", "" + Gson().toJson(it))
                        dialogLoading.dismiss()
                        binding.layoutLoading.stopShimmer()
                        var size = it.data?.data?.size ?: 0
                        Log.e("Size", "" + size)
                        if (size > 0) {
                            it.data?.data?.let { data -> manifestAdapter?.differ?.submitList(data.toList()) }
                            binding.rcyManifest.visibility = View.VISIBLE
                            binding.layoutLoading.visibility = View.GONE
                            binding.tvInfoManifest.visibility = View.GONE
                            if (isNoOrder) {
                                manifestSelected = it.data?.data?.get(0)?.manifestNo ?: ""
                            }
                            try {
                                scrollState?.let {
                                    layoutManager?.onRestoreInstanceState(it)
                                }
                            } catch (e: Exception) {
                            }
                        } else {
                            binding.tvInfoManifest.text = "No Manifests"
                            binding.rcyManifest.visibility = View.GONE
                            binding.layoutLoading.visibility = View.GONE
                            binding.tvInfoManifest.visibility = View.VISIBLE
                        }
                    }

                    is RequestState.Error -> {
                        dialogLoading.dismiss()
                        binding.layoutLoading.stopShimmer()
                        binding.tvInfoManifest.text = it.message
                        binding.rcyManifest.visibility = View.GONE
                        binding.layoutLoading.visibility = View.GONE
                        binding.tvInfoManifest.visibility = View.VISIBLE
                    }
                }
            }
        })
    }

    private fun getAssignCombo() {
        dialogLoading.show()
        var requestAssignCombo = RequestAssignCombo(supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE) ?: "", supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT) ?: "", subTitle = locale.language)
        jobViewModel.getAssignCombo(authToken, requestAssignCombo).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespAssOption", "" + Gson().toJson(it.data))
                        dialogLoading.dismiss()
                        var size = it.data?.data?.size ?: 0
                        if (size > 0) {
                            val items = mutableListOf<String>()
                            items.clear()
                            items.add("Select Driver")
                            for (driver in it.data?.data!!) {
                                var name = driver?.label ?: ""
                                items.add(name)
                            }
                            val dialogAssign = DialogAssign(requireActivity(), items, driverSelected, object : AssignListener {
                                override fun onClicked(index: Int) {
                                    getAssignCheck(it.data.data?.get(index)?.value ?: "")
                                }
                            })
                            dialogAssign.show(requireActivity().supportFragmentManager, "DIALOG_ASSIGN")
                        } else {
                            var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), "Option Not Found !", object : OptionListener {
                                override fun onClicked(isApproved: Boolean) {}
                            })
                            dialogInfo.show()
                        }
                    }

                    is RequestState.Error -> {
                        dialogLoading.dismiss()
                        var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), it.message, object : OptionListener {
                            override fun onClicked(isApproved: Boolean) {}
                        })
                        dialogInfo.show()
                    }
                }
            }
        }
    }

    private fun getAssignCheck(driverName: String) {
        dialogLoading.show()
        var requestAssign = RequestAssign(supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE) ?: "", supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT) ?: "", deliveryNo = modelNavbar.deliveryNo, subTitle = locale.language, driverUsername = driverName)
        jobViewModel.getAssignCheck(authToken, requestAssign).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespAssCheck", "" + Gson().toJson(it.data))
                        dialogLoading.dismiss()
                        it.data?.let { data ->
                            var message = (data.data?.messageType ?: "")
                            when {
                                message.equals("I", ignoreCase = true) -> {
                                    postAssign(driverName)
                                }

                                message.equals("C", ignoreCase = true) -> {
                                    var dialogKanban = DialogKanban(requireActivity(), DialogKanban.MODE_ASSIGN, it.data?.data?.message ?: getString(R.string.ErrorServer), object : OptionListener {
                                        override fun onClicked(isApproved: Boolean) {
                                            if (isApproved == true) {
                                                postAssign(driverName)
                                            }
                                        }
                                    })
                                    dialogKanban.show()
                                }

                                message.equals("E", ignoreCase = true) -> {
                                    var dialogKanban = DialogKanban(requireActivity(), DialogKanban.MODE_BASIC, it.data?.data?.message ?: getString(R.string.ErrorServer), object : OptionListener {
                                        override fun onClicked(isApproved: Boolean) {}
                                    })
                                    dialogKanban.show()
                                }

                                else -> {
                                    var dialogKanban = DialogKanban(requireActivity(), DialogKanban.MODE_BASIC, it.data?.data?.message ?: getString(R.string.ErrorServer), object : OptionListener {
                                        override fun onClicked(isApproved: Boolean) {}
                                    })
                                    dialogKanban.show()
                                }
                            }
                        }
                    }

                    is RequestState.Error -> {
                        dialogLoading.dismiss()
                        var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), it.message, object : OptionListener {
                            override fun onClicked(isApproved: Boolean) {}
                        })
                        dialogInfo.show()
                    }
                }
            }
        }
    }

    private fun postAssign(driverName: String) {
        dialogLoading.show()
        var requestAssign = RequestAssign(supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE) ?: "", supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT) ?: "", deliveryNo = modelNavbar.deliveryNo, subTitle = locale.language, driverUsername = driverName)
        jobViewModel.postAssign(authToken, requestAssign).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespAssPost", "" + Gson().toJson(it.data))
                        dialogLoading.dismiss()
                        it.data?.let { data ->
                            parentActivity?.let {
                                var dialogCompleted = DialogManifestCompleted(parentActivity!!)
                                dialogCompleted.show()
                            }
                        } ?: run {
                            var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), getString(R.string.ErrorProcessManifest), object : OptionListener {
                                override fun onClicked(isApproved: Boolean) {}
                            })
                            dialogInfo.show()
                        }
                    }

                    is RequestState.Error -> {
                        dialogLoading.dismiss()
                        var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), it.message, object : OptionListener {
                            override fun onClicked(isApproved: Boolean) {}
                        })
                        dialogInfo.show()
                    }
                }
            }
        }
    }

    private fun submitJob() {
        if (manifestSelected.isEmpty()) {
            return
        } else {
            dialogLoading.show()
            var processId = sharedPref.getString(manifestSelected) ?: "0"
            if (processId.equals("0")) {
                startManifest()
            } else {
                checkEndManifest()
            }
        }
    }

    private fun startManifest() {
        var reqManifestStart = RequestManifestStart(manifestNo = manifestSelected, subTitle = locale.language)
        Log.e("ReqStartManifest", "" + Gson().toJson(reqManifestStart))
        viewModel.startManifest(authToken, reqManifestStart).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespManStart", "" + Gson().toJson(it.data))
                        it.data?.let { data ->
                            sharedPref.put((manifestSelected), (data?.data?.processIdOutput ?: 0).toString())
                            checkEndManifest()
                        }
                    }

                    is RequestState.Error -> {
                        Log.e("ErrManStart", "" + it.message)
                        dialogLoading.dismiss()
                        var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), it.message, object : OptionListener {
                            override fun onClicked(isApproved: Boolean) {}
                        })
                        dialogInfo.show()
                    }
                }
            }
        }
    }

    private fun checkEndManifest() {
        var processId = sharedPref.getString(manifestSelected) ?: "0"
        var requestManifestEndCheck = RequestManifestEndCheck(manifestNo = manifestSelected, processId = processId, subTitle = locale.language)
        Log.e("ReqManEndCheck", "" + Gson().toJson(requestManifestEndCheck))
        viewModel.getCheckEndManifest(authToken, requestManifestEndCheck).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespManEndCheck", "" + Gson().toJson(it.data))
                        it.data?.let { data ->
                            var message = (data.data?.messageType ?: "")
                            when {
                                message.equals("I", ignoreCase = true) -> {
                                    endManifest()
                                }

                                message.equals("C", ignoreCase = true) -> {
                                    dialogLoading.dismiss()
                                    var dialogKanban = DialogKanban(requireActivity(), DialogKanban.MODE_MULTIPLE, it.data?.data?.message ?: getString(R.string.ErrorServer), object : OptionListener {
                                        override fun onClicked(isApproved: Boolean) {
                                            if (isApproved == true) {
                                                dialogLoading.show()
                                                endManifest()
                                            }
                                        }
                                    })
                                    dialogKanban.show()
                                }

                                message.equals("E", ignoreCase = true) -> {
                                    dialogLoading.dismiss()
                                    var dialogKanban = DialogKanban(requireActivity(), DialogKanban.MODE_BASIC, it.data?.data?.message ?: getString(R.string.ErrorServer), object : OptionListener {
                                        override fun onClicked(isApproved: Boolean) {}
                                    })
                                    dialogKanban.show()
                                }

                                else -> {
                                    dialogLoading.dismiss()
                                    var dialogKanban = DialogKanban(requireActivity(), DialogKanban.MODE_BASIC, it.data?.data?.message ?: getString(R.string.ErrorServer), object : OptionListener {
                                        override fun onClicked(isApproved: Boolean) {}
                                    })
                                    dialogKanban.show()
                                }
                            }
                        }
                    }

                    is RequestState.Error -> {
                        Log.e("ErrManEndCheck", "" + it.message)
                        dialogLoading.dismiss()
                        var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), it.message, object : OptionListener {
                            override fun onClicked(isApproved: Boolean) {}
                        })
                        dialogInfo.show()
                    }
                }
            }
        }
    }

    private fun endManifest() {
        var requestManifestEnd = RequestManifestEnd(manifestNo = manifestSelected, processId = sharedPref.getString(manifestSelected), subTitle = locale.language)
        Log.e("ReqManEndPost", "" + Gson().toJson(requestManifestEnd))
        viewModel.postEndManifest(authToken, requestManifestEnd).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespManEndPost", "" + Gson().toJson(it.data))
                        dialogLoading.dismiss()
                        it.data?.let { data ->
                            parentActivity?.let {
                                var dialogCompleted = DialogManifestCompleted(parentActivity!!)
                                dialogCompleted.show()
                            }
                        } ?: run {
                            var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), getString(R.string.ErrorProcessManifest), object : OptionListener {
                                override fun onClicked(isApproved: Boolean) {}
                            })
                            dialogInfo.show()
                        }
                    }

                    is RequestState.Error -> {
                        Log.e("ErrManEndPost", "" + it.message)
                        dialogLoading.dismiss()
                        var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), it.message, object : OptionListener {
                            override fun onClicked(isApproved: Boolean) {}
                        })
                        dialogInfo.show()
                    }
                }
            }
        }
    }

    private fun getAssignedDriver() {
        var requestAssign = RequestAssign(supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE) ?: "", supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT) ?: "", deliveryNo = modelNavbar.deliveryNo, subTitle = locale.language)
        Log.e("ReqAssDriver", "" + Gson().toJson(requestAssign))
        jobViewModel.getAssignedDriver(authToken, requestAssign).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespAssDriver", "" + Gson().toJson(it.data))
                        driverSelected = it.data.data?.driverName ?: ""
                        if (driverSelected.isEmpty()) {
                            binding.btnSubmit.visibility = View.GONE
                            binding.btnAssign.visibility = View.VISIBLE
                            binding.btnEditDriver.visibility = View.GONE
                        } else {
                            binding.btnSubmit.visibility = View.GONE
                            binding.btnAssign.visibility = View.GONE
                            binding.btnEditDriver.visibility = View.VISIBLE
                        }
                    }

                    is RequestState.Error -> {
                        Log.e("ErrAssDriver", "" + it.message)
                        binding.btnAssign.visibility = View.VISIBLE
                        binding.btnEditDriver.visibility = View.GONE
                        driverSelected = ""
                    }
                }
            }
        }
    }
}