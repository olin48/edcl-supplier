package com.tmmin.emanifest.features.operator.kanban

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.tmmin.emanifest.R
import com.tmmin.emanifest.adapters.KanbanAdapter
import com.tmmin.emanifest.databinding.FragmentManifestKanbanDetailBinding
import com.tmmin.emanifest.features.operator.main.OperatorMainActivity
import com.tmmin.emanifest.helpers.DataHelper
import com.tmmin.emanifest.models.others.ModelNavbar
import com.tmmin.emanifest.models.requests.kanban.list.RequestKanbanList
import com.tmmin.emanifest.models.responses.kanban.list.DataItem
import com.tmmin.emanifest.network.RequestState
import com.tmmin.emanifest.util.BaseFragment
import com.tmmin.emanifest.viewmodels.KanbanViewModel

class ManifestKanbanDetailFragment : BaseFragment(), View.OnClickListener {
    val kanbanViewModel: KanbanViewModel by viewModels()
    lateinit var kanbanAdapter: KanbanAdapter
    lateinit var binding: FragmentManifestKanbanDetailBinding
    private var dataNavbar: String? = null
    private var dataPart: String? = null
    lateinit var responseKanbanDetail: com.tmmin.emanifest.models.responses.skid.detail.DataItem
    lateinit var modelNavbar: ModelNavbar
    var parentActivity: OperatorMainActivity? = null
    var kanbanList = ArrayList<DataItem>()
    var totalQtyActual = 0
    var totalQtyPlan = 0

    companion object {
        fun newInstance(paramNavbar: String, paramPart: String): ManifestKanbanDetailFragment {
            val fragment = ManifestKanbanDetailFragment()
            val args = Bundle()
            args.putString(DataHelper.DATA_NAVBAR, paramNavbar)
            args.putString(DataHelper.DATA_PART, paramPart)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            dataNavbar = it.getString(DataHelper.DATA_NAVBAR)
            dataPart = it.getString(DataHelper.DATA_PART)
            modelNavbar = Gson().fromJson(dataNavbar, ModelNavbar::class.java);
            responseKanbanDetail = Gson().fromJson(dataPart, com.tmmin.emanifest.models.responses.skid.detail.DataItem::class.java);
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_manifest_kanban_detail, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OperatorMainActivity) {
            parentActivity = context
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.layoutToolbar.btnBack -> {
                parentActivity?.let {
                    it.onBackPressed()
                }
            }
        }
    }

    private fun setup() {
        parentActivity = activity as? OperatorMainActivity
        binding.layoutToolbar.tvTitle.visibility = View.VISIBLE
        binding.layoutToolbar.btnBack.visibility = View.VISIBLE
        binding.layoutToolbar.tvTitle.text = "SKID Check"
        binding.tvRoute.text = modelNavbar.route
        binding.tvRate.text = modelNavbar.rate
        binding.tvPartner.text = modelNavbar.partner
        binding.tvPickupDate.text = modelNavbar.date
        binding.tvPartNo.text = responseKanbanDetail?.partNo ?: "-"
        binding.tvTotalKanban.text = (responseKanbanDetail?.totalKanban ?: 0).toString()
        binding.layoutToolbar.btnBack.setOnClickListener(this)
        getKanbanList()
    }

    private fun getKanbanList() {
        binding.layoutLoading.startShimmer()
        binding.layoutData.visibility = View.GONE
        binding.layoutLoading.visibility = View.VISIBLE
        var requestKanbanList = RequestKanbanList(manifestNo = modelNavbar.manifestNo, skidNo = modelNavbar.skidNo, itemNo = (responseKanbanDetail?.itemNo ?: 0).toString(), subTitle = locale.language)
        kanbanList.clear()
        totalQtyActual = 0
        totalQtyPlan = 0
        kanbanViewModel.getListKanban(authToken, requestKanbanList).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        binding.layoutLoading.stopShimmer()
                        binding.layoutData.visibility = View.VISIBLE
                        binding.layoutLoading.visibility = View.GONE
                        var size = it.data?.data?.size ?: 0
                        if (size > 0) {
                            for (kanban in it.data?.data!!) {
                                totalQtyPlan += kanban?.totalQuantityPlan ?: 0
                                totalQtyActual += kanban?.totalQuantityActual ?: 0
                                if (kanban != null) {
                                    kanbanList.add(kanban)
                                }
                            }
                            kanbanAdapter = KanbanAdapter(parentActivity!!, kanbanList)
                            binding.rcyKanban.layoutManager = LinearLayoutManager(requireActivity())
                            binding.rcyKanban.adapter = kanbanAdapter
                            binding.tvInfoKanban.visibility = View.GONE
                            binding.rcyKanban.visibility = View.VISIBLE
                            binding.tvTotalQtyPlan.text = totalQtyPlan.toString()
                            binding.tvTotalQtyActual.text = totalQtyActual.toString()
                        } else {
                            binding.tvInfoKanban.visibility = View.VISIBLE
                            binding.tvInfoKanban.setText("No SKID")
                            binding.rcyKanban.visibility = View.GONE
                        }
                    }

                    is RequestState.Error -> {
                        binding.layoutLoading.stopShimmer()
                        binding.layoutData.visibility = View.VISIBLE
                        binding.layoutLoading.visibility = View.GONE
                        binding.tvInfoKanban.visibility = View.VISIBLE
                        binding.rcyKanban.visibility = View.GONE
                        binding.tvInfoKanban.setText(it.message ?: getString(R.string.ErrorServer))
                    }
                }
            }
        }
    }
}