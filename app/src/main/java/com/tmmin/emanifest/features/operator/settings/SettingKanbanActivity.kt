package com.tmmin.emanifest.features.operator.settings

import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.tmmin.emanifest.R
import com.tmmin.emanifest.adapters.DockCodeAdapter
import com.tmmin.emanifest.databinding.ActivitySettingKanbanBinding
import com.tmmin.emanifest.models.responses.dockCode.DataItem
import com.tmmin.emanifest.models.responses.dockCode.ResponseDockCode
import com.tmmin.emanifest.util.BaseActivity
import com.tmmin.emanifest.util.Constant

class SettingKanbanActivity : BaseActivity(), View.OnClickListener {
    lateinit var binding: ActivitySettingKanbanBinding
    lateinit var dockCodeAdapter: DockCodeAdapter
    val dockCodes = mutableListOf<DataItem>()
    var partSelected = 0
    var scannerSelected = 0
    var paperSelected = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_setting_kanban)
        setup()
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.layoutToolbar.btnBack -> {
                finish()
            }

            binding.btnSettingScanner -> {
                if (binding.layoutExpandScanner.isExpanded) {
                    binding.layoutExpandScanner.setExpanded(false)
                    binding.btnExpandScanner.setImageResource(R.drawable.ic_arrow_up)
                } else {
                    binding.layoutExpandScanner.setExpanded(true)
                    binding.btnExpandScanner.setImageResource(R.drawable.ic_arrow_down)
                }
            }

            binding.btnSettingSlip -> {
                if (binding.layoutExpandSlip.isExpanded) {
                    binding.layoutExpandSlip.setExpanded(false)
                    binding.btnExpandSlip.setImageResource(R.drawable.ic_arrow_up)
                } else {
                    binding.layoutExpandSlip.setExpanded(true)
                    binding.btnExpandSlip.setImageResource(R.drawable.ic_arrow_down)
                }
            }

            binding.btnSettingPlant -> {
                if (binding.layoutExpandPlant.isExpanded) {
                    binding.layoutExpandPlant.setExpanded(false)
                    binding.btnExpandPlant.setImageResource(R.drawable.ic_arrow_up)
                } else {
                    binding.layoutExpandPlant.setExpanded(true)
                    binding.btnExpandPlant.setImageResource(R.drawable.ic_arrow_down)
                }
            }

            binding.btnSettingPartNo -> {
                if (binding.layoutExpandPartNo.isExpanded) {
                    binding.layoutExpandPartNo.setExpanded(false)
                    binding.btnExpandPartNo.setImageResource(R.drawable.ic_arrow_up)
                } else {
                    binding.layoutExpandPartNo.setExpanded(true)
                    binding.btnExpandPartNo.setImageResource(R.drawable.ic_arrow_down)
                }
            }

            binding.btnSettingPaper -> {
                if (binding.layoutExpandPaper.isExpanded) {
                    binding.layoutExpandPaper.setExpanded(false)
                    binding.btnExpandPaper.setImageResource(R.drawable.ic_arrow_up)
                } else {
                    binding.layoutExpandPaper.setExpanded(true)
                    binding.btnExpandPaper.setImageResource(R.drawable.ic_arrow_down)
                }
            }
        }
    }

    private fun setup() {
        binding.layoutToolbar.tvTitle.visibility = View.VISIBLE
        binding.layoutToolbar.btnBack.visibility = View.VISIBLE
        binding.layoutToolbar.tvTitle.text = "Kanban Setting"
        binding.layoutToolbar.btnBack.setOnClickListener(this)
        binding.btnSettingScanner.setOnClickListener(this)
        binding.btnSettingSlip.setOnClickListener(this)
        binding.btnSettingPaper.setOnClickListener(this)
        binding.btnSettingPlant.setOnClickListener(this)
        binding.btnSettingPartNo.setOnClickListener(this)
        binding.layoutChildSetPart.visibility = View.GONE
        binding.groupPart.setOnCheckedChangeListener { group, checkedId ->
            val radioButton: RadioButton = findViewById(checkedId)
            partSelected = binding.groupPart.indexOfChild(radioButton)
            if (partSelected == 0) {
                binding.layoutChildSetPart.visibility = View.GONE
            } else {
                binding.layoutChildSetPart.visibility = View.VISIBLE
            }
        }

        binding.groupScanner.setOnCheckedChangeListener { group, checkedId ->
            val radioButton: RadioButton = findViewById(checkedId)
            scannerSelected = binding.groupScanner.indexOfChild(radioButton)
            sharedPref.put(Constant.SET_SCANNER, scannerSelected)
        }

        binding.groupPaper.setOnCheckedChangeListener { group, checkedId ->
            val radioButton: RadioButton = findViewById(checkedId)
            paperSelected = binding.groupPaper.indexOfChild(radioButton)
            sharedPref.put(Constant.SET_PAPER, paperSelected)
        }
        dockCodeAdapter = DockCodeAdapter(this, dockCodes)
        binding.rcyDockCodes.layoutManager = LinearLayoutManager(this)
        binding.rcyDockCodes.adapter = dockCodeAdapter
        if (hasLaserScanner()) {
            binding.layoutSetScanner.visibility = View.VISIBLE
        } else {
            binding.layoutSetScanner.visibility = View.GONE
        }
        getDataDockCodes()
        getData()
    }

    private fun getData() {
        scannerSelected = sharedPref.getInt(Constant.SET_SCANNER)
        paperSelected = sharedPref.getInt(Constant.SET_PAPER)
        binding.etPlant.setText(sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT))
        binding.etPartSample.setText(sharedPref.getString(Constant.SET_PART_SAMPLE))
        binding.etDigitStart.setText(sharedPref.getString(Constant.SET_PART_START))
        binding.etDigitEnd.setText(sharedPref.getString(Constant.SET_PART_END))
        if (scannerSelected >= 0 && scannerSelected < binding.groupScanner.childCount) {
            val radioButton = binding.groupScanner.getChildAt(scannerSelected) as RadioButton
            radioButton.isChecked = true
        }
        if (partSelected >= 0 && partSelected < binding.groupPart.childCount) {
            val radioButton = binding.groupPart.getChildAt(partSelected) as RadioButton
            radioButton.isChecked = true
        }
        if (paperSelected >= 0 && paperSelected < binding.groupPaper.childCount) {
            val radioButton = binding.groupPaper.getChildAt(paperSelected) as RadioButton
            radioButton.isChecked = true
        }
    }

    private fun getDataDockCodes() {
        var jsonDockCodes = sharedPref.getString(Constant.SET_PRINTER) ?: ""
        if (jsonDockCodes.isEmpty()) {
            initDockCode(false)
        } else {
            var modelDockCode = Gson().fromJson(sharedPref.getString(Constant.SET_PRINTER), ResponseDockCode::class.java)
            var dockCodeSize = modelDockCode.data?.size ?: 0
            if (dockCodeSize > 0) {
                dockCodes.clear()
                for (i in 0 until dockCodeSize) {
                    dockCodes.add(modelDockCode.data?.get(i)!!)
                }
                dockCodeAdapter.notifyDataSetChanged()
                initDockCode(true)
            } else {
                initDockCode(false)
            }
        }
    }

    private fun initDockCode(isExist: Boolean) {
        if (isExist) {
            binding.rcyDockCodes.visibility = View.VISIBLE
            binding.tvInfoDockCodes.visibility = View.GONE
        } else {
            binding.rcyDockCodes.visibility = View.GONE
            binding.tvInfoDockCodes.visibility = View.VISIBLE
        }
    }
}