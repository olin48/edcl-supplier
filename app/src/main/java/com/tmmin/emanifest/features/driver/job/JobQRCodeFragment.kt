package com.tmmin.emanifest.features.driver.job

import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.google.gson.Gson
import com.google.zxing.BarcodeFormat
import com.google.zxing.WriterException
import com.google.zxing.qrcode.QRCodeWriter
import com.tmmin.emanifest.R
import com.tmmin.emanifest.databinding.FragmentDriverJobQrCodeBinding
import com.tmmin.emanifest.features.driver.main.DriverMainActivity
import com.tmmin.emanifest.helpers.DataHelper
import com.tmmin.emanifest.models.others.ModelNavbar
import com.tmmin.emanifest.util.BaseFragment

class JobQRCodeFragment : BaseFragment(), View.OnClickListener {
    private lateinit var binding: FragmentDriverJobQrCodeBinding
    private var parentActivity: DriverMainActivity? = null
    lateinit var modelNavbar: ModelNavbar
    var dataNavbar: String? = null

    companion object {
        fun newInstance(paramNavbar: String): JobQRCodeFragment {
            val fragment = JobQRCodeFragment()
            val args = Bundle()
            args.putString(DataHelper.DATA_NAVBAR, paramNavbar)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            dataNavbar = it.getString(DataHelper.DATA_NAVBAR)
            modelNavbar = Gson().fromJson(dataNavbar, ModelNavbar::class.java)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_driver_job_qr_code, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is DriverMainActivity) {
            parentActivity = context
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.layoutToolbar.btnBack -> {
                parentActivity?.let {
                    it.onBackPressed()
                } ?: parentActivity?.onBackPressed()
            }
        }
    }

    private fun setup() {
        binding.layoutToolbar.tvTitle.visibility = View.VISIBLE
        binding.layoutToolbar.btnBack.visibility = View.VISIBLE
        binding.layoutToolbar.tvTitle.text = "QR Code"
        binding.tvRoute.text = modelNavbar.route
        binding.tvRate.text = modelNavbar.rate
        binding.tvPartner.text = modelNavbar.partner
        binding.tvPickupDate.text = modelNavbar.date
        parentActivity = activity as? DriverMainActivity
        binding.layoutToolbar.btnBack.setOnClickListener(this)
        val qrCodeBitmap = generateQRCode(modelNavbar.deliveryNo ?: "", 500, 500)
        binding.ivQRCode.setImageBitmap(qrCodeBitmap)
    }

    fun generateQRCode(text: String, width: Int, height: Int): Bitmap? {
        val writer = QRCodeWriter()
        return try {
            val bitMatrix = writer.encode(text, BarcodeFormat.QR_CODE, width, height)
            val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
            for (x in 0 until width) {
                for (y in 0 until height) {
                    bitmap.setPixel(x, y, if (bitMatrix[x, y]) android.graphics.Color.BLACK else android.graphics.Color.TRANSPARENT)
                }
            }
            bitmap
        } catch (e: WriterException) {
            e.printStackTrace()
            null
        }
    }
}