package com.tmmin.emanifest.features.operator.kanban

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.example.tscdll.TSCActivity
import com.google.gson.Gson
import com.google.zxing.BarcodeFormat
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.DefaultDecoderFactory
import com.mazenrashed.printooth.Printooth
import com.tmmin.emanifest.R
import com.tmmin.emanifest.databinding.FragmentManifestKanbanScanBinding
import com.tmmin.emanifest.dialogs.DialogInfo
import com.tmmin.emanifest.dialogs.DialogKanban
import com.tmmin.emanifest.dialogs.DialogLoading
import com.tmmin.emanifest.dialogs.DialogOption
import com.tmmin.emanifest.dialogs.DialogPrinting
import com.tmmin.emanifest.dialogs.DialogSlip
import com.tmmin.emanifest.features.operator.main.OperatorMainActivity
import com.tmmin.emanifest.helpers.DataHelper
import com.tmmin.emanifest.listeners.OptionListener
import com.tmmin.emanifest.listeners.SlipPreviewListener
import com.tmmin.emanifest.models.others.ModelNavbar
import com.tmmin.emanifest.models.others.ModelSlip
import com.tmmin.emanifest.models.requests.kanban.list.RequestKanbanList
import com.tmmin.emanifest.models.requests.manifest.RequestManifestEnd
import com.tmmin.emanifest.models.requests.manifest.completeness.RequestManifestCompletenessItem
import com.tmmin.emanifest.models.responses.dockCode.ResponseDockCode
import com.tmmin.emanifest.models.responses.kanban.manifest.DataItem
import com.tmmin.emanifest.network.RequestState
import com.tmmin.emanifest.util.BaseFragment
import com.tmmin.emanifest.util.Constant
import com.tmmin.emanifest.viewmodels.KanbanViewModel
import com.tmmin.emanifest.viewmodels.ManifestViewModel


class ManifestKanbanScanOfflineFragment : BaseFragment(), View.OnClickListener, View.OnTouchListener {
    val kanbanViewModel: KanbanViewModel by viewModels()
    val manifestViewModel: ManifestViewModel by viewModels()
    private lateinit var dialogLoading: DialogLoading
    lateinit var binding: FragmentManifestKanbanScanBinding
    lateinit var modelNavbar: ModelNavbar
    lateinit var audioManager: AudioManager
    private var mediaPlayer: MediaPlayer? = null
    var dialogSlip: DialogSlip? = null
    var tscActivity = TSCActivity()
    var dataNavbar: String? = null
    var parentActivity: OperatorMainActivity? = null
    val REQUEST_CAMERA = 1
    var isScanning = false
    var flashOn = false
    var kanbanList: MutableList<DataItem> = mutableListOf()
    var kanbanScannedList: MutableList<RequestManifestCompletenessItem> = mutableListOf()
    var kanbanCancelList: MutableList<RequestManifestCompletenessItem> = mutableListOf()
    var kanbanAddList: MutableList<RequestManifestCompletenessItem> = mutableListOf()
    var dataOriginScanned = 0

    val dockCodes = mutableListOf<com.tmmin.emanifest.models.responses.dockCode.DataItem>()

    companion object {
        fun newInstance(paramNavbar: String): ManifestKanbanScanOfflineFragment {
            val fragment = ManifestKanbanScanOfflineFragment()
            val args = Bundle()
            args.putString(DataHelper.DATA_NAVBAR, paramNavbar)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            dataNavbar = it.getString(DataHelper.DATA_NAVBAR)
            modelNavbar = Gson().fromJson(dataNavbar, ModelNavbar::class.java);
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_manifest_kanban_scan, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (checkSelfPermission(requireActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            setup()
        } else {
            doRequestPermission()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OperatorMainActivity) {
            parentActivity = context
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            Log.e("OnDestroy", "True")
            dialogLoading.dismiss()
            tscActivity.closeport()
        } catch (e: Exception) {
        }
    }

    override fun onResume() {
        super.onResume()
        Log.e("OnResume", "True")
        binding.scanner.resume()
        mediaPlayer = MediaPlayer().apply {
            setOnCompletionListener {
                it.reset()
            }
        }
        dialogSlip?.let {
            if (dialogSlip!!.isShowing) {
                dialogSlip!!.initDevices()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        binding.scanner.pause()
        mediaPlayer?.release()
        mediaPlayer = null
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_CAMERA -> {
                setup()
            }
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btnSubmit -> {
                submitKanban()
            }

            binding.btnModeCamera -> {
                switchMode(false)
            }

            binding.btnModeScanner -> {
                switchMode(true)
            }

            binding.btnFlash -> {
                flashOn = !flashOn
                binding.scanner.barcodeView.setTorch(flashOn)
                if (flashOn) {
                    binding.btnFlash.setImageResource(R.drawable.ic_flash_on)
                } else {
                    binding.btnFlash.setImageResource(R.drawable.ic_flash_off)
                }
            }
        }
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        when (v) {
            binding.btnSwipe -> {
                if (!isScanning) {
                    var initialX = 0f
                    when (event?.action) {
                        MotionEvent.ACTION_DOWN -> {
                            initialX = v.x
                        }

                        MotionEvent.ACTION_MOVE -> {
                            val newX = event.rawX - v.width
                            if (newX > binding.tvDescSwipe.left && newX + v.width < binding.tvDescSwipe.right) {
                                v.x = newX
                            }
                        }

                        MotionEvent.ACTION_UP -> {
                            if (event.rawX >= binding.tvDescSwipe.right) {
                                v.x = initialX
                                finishScan()
                            } else {
                                v.x = initialX
                            }
                        }
                    }
                }
            }
        }
        return true
    }

    private fun setup() {
        parentActivity = activity as? OperatorMainActivity
        parentActivity?.binding?.navigation?.visibility = View.GONE
        binding.layoutToolbar.tvTitle.visibility = View.VISIBLE
        binding.layoutToolbar.btnBack.visibility = View.GONE
        binding.layoutToolbar.tvTitle.text = "Kanban Scan"
        binding.tvRoute.text = modelNavbar.route
        binding.tvRate.text = modelNavbar.rate
        binding.tvPartner.text = modelNavbar.partner
        binding.tvPickupDate.text = modelNavbar.date
        binding.tvSKIDNumber.text = modelNavbar.skidNo
        binding.btnFlash.setOnClickListener(this)
        binding.btnSubmit.setOnClickListener(this)
        binding.btnSwipe.setOnTouchListener(this)
        binding.btnModeCamera.setOnClickListener(this)
        binding.btnModeScanner.setOnClickListener(this)
        dialogLoading = DialogLoading(requireActivity())
        audioManager = requireActivity().getSystemService(Context.AUDIO_SERVICE) as AudioManager
        initScannerView()
        initScannerExternal()
        getDataDockCodes()
        getKanbanList()
        if (hasLaserScanner()) {
            Glide.with(this).asGif().load(R.raw.gif_scanner).into(binding.ivScanner)
            var mode = sharedPref.getInt(Constant.SET_SCANNER) ?: 0
            if (mode == 0) {
                switchMode(false)
            } else {
                switchMode(true)
            }
        } else {
            switchMode(false)
            binding.layoutMode.visibility = View.GONE
        }
    }

    private fun doRequestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), REQUEST_CAMERA)
        } else {
            setup()
        }
    }

    private fun initScannerExternal() {
        binding.etResultScanner.isCursorVisible = false
        binding.etResultScanner.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                val imm = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(binding.etResultScanner.windowToken, 0)
            }
        }
        binding.etResultScanner.requestFocus()
        binding.etResultScanner.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                binding.etResultScanner.removeTextChangedListener(this)
                binding.etResultScanner.setText("")
                binding.etResultScanner.addTextChangedListener(this)
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                processKanban(s.toString(), "")
            }
        })
    }

    private fun initScannerView() {
        val formats = listOf(BarcodeFormat.QR_CODE)
        binding.scanner.barcodeView.decoderFactory = DefaultDecoderFactory(formats)
        binding.scanner.resume()
        binding.scanner.decodeContinuous(object : BarcodeCallback {
            override fun barcodeResult(result: BarcodeResult) {
                if (!isScanning) {
                    isScanning = true
                    var kanban = result?.text ?: ""
                    processKanban(kanban, "")
                }
            }

            override fun possibleResultPoints(resultPoints: List<ResultPoint>) {}
        })
    }

    private fun switchMode(isExternal: Boolean) {
        binding.etResultScanner.isEnabled = isExternal
        if (isExternal) {
            binding.layoutScanExternal.visibility = View.VISIBLE
            binding.layoutScanInternal.visibility = View.GONE
            binding.btnModeCamera.visibility = View.VISIBLE
            binding.btnModeScanner.visibility = View.GONE
            binding.scanner.pause()
            binding.etResultScanner.requestFocus()
        } else {
            binding.layoutScanExternal.visibility = View.GONE
            binding.layoutScanInternal.visibility = View.VISIBLE
            binding.btnModeCamera.visibility = View.GONE
            binding.btnModeScanner.visibility = View.VISIBLE
            binding.scanner.resume()
        }
    }

    private fun submitKanban() {
        var kanban = binding.etResult.text.toString().toUpperCase()
        if (!kanban.isEmpty()) {
            isScanning = true
            processKanban("", kanban)
        }
    }

    private fun initDefaultView() {
        binding.etResult.setText("")
        binding.tvScanNumber.text = ""
        binding.tvScanDiv.text = ""
        binding.tvScanTotal.text = ""
        dataOriginScanned = 0
    }

    private fun playBeepSound() {
        val maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
        val newVolume = (maxVolume * 100)
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, newVolume, 0)
        var sound = R.raw.scanner_beep
        mediaPlayer?.apply {
            if (isPlaying) {
                stop()
                reset()
            }
            setDataSource(requireActivity(), Uri.parse("android.resource://${requireActivity().packageName}/$sound"))
            prepare()
            start()
        }
    }

    private fun playBeepSoundError() {
        val maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
        val newVolume = (maxVolume * 100)
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, newVolume, 0)
        var sound = R.raw.scanner_beep_error
        mediaPlayer?.apply {
            if (isPlaying) {
                stop()
                reset()
            }
            setDataSource(requireActivity(), Uri.parse("android.resource://${requireActivity().packageName}/$sound"))
            prepare()
            start()
        }
    }

    private fun processKanban(kanbanId: String, kanbanCd: String) {
        isScanning = true
        var indexDataOrigin = -1
        var indexDataAdd = -1
        var indexDataCancel = -1
        var indexDataAddBefore = -1
        var indexDataOriginCancel = -1

        if (kanbanCd.isEmpty()) {
            indexDataOrigin = kanbanList.indexOfFirst { it.kanbanId.equals(kanbanId, true) }
        } else {
            indexDataOrigin = kanbanList.indexOfFirst { it.kanbanCd.equals(kanbanCd, true) }
        }

        if (indexDataOrigin == -1) {
            //Checking If Kanban Is Not Exist On Manifest
            playBeepSoundError()
            var dialogKanban = DialogKanban(requireActivity(), DialogKanban.MODE_BASIC, getString(R.string.ErrorKanban), object : OptionListener {
                override fun onClicked(isApproved: Boolean) {
                    isScanning = false
                    binding.etResult.setText("")
                    binding.etResult.clearFocus()
                    binding.etResultScanner.requestFocus()
                }
            })
            dialogKanban.show()
        } else {
            var kanbanSKIDNo = kanbanList.get(indexDataOrigin).skidNo ?: "-"
            if (!kanbanSKIDNo.equals("-") && !kanbanSKIDNo.equals(modelNavbar.skidNo)) {
                //Kanban Is Belongs On Another SKID
                playBeepSoundError()
                var dialogKanban = DialogKanban(requireActivity(), DialogKanban.MODE_BASIC, getString(R.string.ErrorKanbanScanned), object : OptionListener {
                    override fun onClicked(isApproved: Boolean) {
                        isScanning = false
                        binding.etResult.setText("")
                        binding.etResult.clearFocus()
                        binding.etResultScanner.requestFocus()
                    }
                })
                dialogKanban.show()
            } else {
                //Kanban Is Not Belongs On Another SKID
                var kanban = kanbanList.get(indexDataOrigin)
                indexDataAdd = kanbanAddList.indexOfFirst { it.kanbanId.equals(kanban.kanbanId, true) }
                indexDataCancel = kanbanCancelList.indexOfFirst { it.kanbanId.equals(kanban.kanbanId, true) }
                if (indexDataCancel != -1) {
                    //Kanban Is Exist On List Cancel
                    if (indexDataOrigin == -1) {
                        isScanning = false
                        return
                    } else {
                        playBeepSound()
                        indexDataAddBefore = kanbanScannedList.indexOfFirst { it.kanbanId.equals(kanbanCancelList.get(indexDataCancel).kanbanId, true) }
                        if (indexDataAddBefore != -1) {
                            kanbanScannedList.removeAt(indexDataAddBefore)
                        }
                        indexDataOriginCancel = kanbanList.indexOfFirst { it.kanbanId.equals(kanbanCancelList.get(indexDataCancel).kanbanId, true) }
                        if (indexDataOriginCancel != -1) {
                            kanbanList.get(indexDataOriginCancel).skidNo = "-"
                        }
                        kanbanCancelList.removeAt(indexDataCancel)

                        var manifest = RequestManifestCompletenessItem(skidNo = modelNavbar.skidNo, supplierCode = kanban.supplierCd, supplierPlant = kanban.supplierPlant, manifestNo = kanban.manifestNo, kanbanId = kanban.kanbanId, itemNo = (kanban.itemNo ?: "0").toInt(), seqNo = (kanban.seqNo ?: "0").toInt())
                        kanbanAddList.add(manifest)

                        updateTotalKanban()
                        val handler = Handler(Looper.getMainLooper())
                        handler.postDelayed({
                            isScanning = false
                        }, 2000)
                    }
                } else if (indexDataAdd != -1) {
                    //Kanban Is Exist On List Add
                    playBeepSound()
                    Log.e("DeleteConfirm", "1")
                    var dialogKanban = DialogKanban(requireActivity(), DialogKanban.MODE_MULTIPLE, getString(R.string.InfoCancelKanban), object : OptionListener {
                        override fun onClicked(isApproved: Boolean) {
                            if (isApproved == true) {
                                kanbanAddList.removeAt(indexDataAdd)
                            }
                            updateTotalKanban()
                            val handler = Handler(Looper.getMainLooper())
                            handler.postDelayed({
                                isScanning = false
                            }, 2000)
                        }
                    })
                    dialogKanban.show()
                } else {
                    playBeepSound()
                    if (kanban.skidNo == null || (kanban.skidNo ?: "-").equals("-")) {
                        //Add Kanban
                        var manifest = RequestManifestCompletenessItem(skidNo = modelNavbar.skidNo, supplierCode = kanban.supplierCd, supplierPlant = kanban.supplierPlant, manifestNo = kanban.manifestNo, kanbanId = kanban.kanbanId, itemNo = (kanban.itemNo ?: "0").toInt(), seqNo = (kanban.seqNo ?: "0").toInt())
                        kanbanAddList.add(manifest)
                        updateTotalKanban()
                        val handler = Handler(Looper.getMainLooper())
                        handler.postDelayed({
                            isScanning = false
                        }, 2000)
                    } else {
                        //Cancel Kanban
                        var dialogKanban = DialogKanban(requireActivity(), DialogKanban.MODE_MULTIPLE, getString(R.string.InfoCancelKanban), object : OptionListener {
                            override fun onClicked(isApproved: Boolean) {
                                if (isApproved == true) {
                                    var manifest = RequestManifestCompletenessItem(skidNo = kanban.skidNo, supplierCode = kanban.supplierCd, supplierPlant = kanban.supplierPlant, manifestNo = kanban.manifestNo, kanbanId = kanban.kanbanId, itemNo = (kanban.itemNo ?: "0").toInt(), seqNo = (kanban.seqNo ?: "0").toInt())
                                    kanbanCancelList.add(manifest)
                                }
                                isScanning = false
                                updateTotalKanban()
                            }
                        })
                        dialogKanban.show()
                    }
                }
            }
        }
    }

    private fun updateTotalKanban() {
        var sizeOrigin = kanbanList.size
        var sizeScanned = (kanbanAddList.size + kanbanScannedList.size) - kanbanCancelList.size
        binding.tvScanNumber.setText(sizeScanned.toString())
        binding.tvScanDiv.setText("/")
        binding.tvScanTotal.setText(kanbanList.size.toString())
        if (sizeScanned == 0) {
            binding.tvScanNumber.setTextColor(ContextCompat.getColor(requireActivity(), R.color.ColorRed))
            binding.tvScanDiv.setTextColor(ContextCompat.getColor(requireActivity(), R.color.ColorRed))
            binding.tvScanTotal.setTextColor(ContextCompat.getColor(requireActivity(), R.color.ColorRed))
        } else if (sizeScanned == sizeOrigin) {
            binding.tvScanNumber.setTextColor(ContextCompat.getColor(requireActivity(), R.color.ColorGreenDark))
            binding.tvScanDiv.setTextColor(ContextCompat.getColor(requireActivity(), R.color.ColorGreenDark))
            binding.tvScanTotal.setTextColor(ContextCompat.getColor(requireActivity(), R.color.ColorGreenDark))
        } else if (sizeScanned != sizeOrigin) {
            binding.tvScanNumber.setTextColor(ContextCompat.getColor(requireActivity(), R.color.ColorYellow))
            binding.tvScanDiv.setTextColor(ContextCompat.getColor(requireActivity(), R.color.ColorYellow))
            binding.tvScanTotal.setTextColor(ContextCompat.getColor(requireActivity(), R.color.ColorYellow))
        } else {
            binding.tvScanNumber.setTextColor(ContextCompat.getColor(requireActivity(), R.color.ColorBlackSecond))
            binding.tvScanDiv.setTextColor(ContextCompat.getColor(requireActivity(), R.color.ColorBlackSecond))
            binding.tvScanTotal.setTextColor(ContextCompat.getColor(requireActivity(), R.color.ColorBlackSecond))
        }
        binding.etResult.setText("")
        binding.etResult.clearFocus()
        binding.etResultScanner.requestFocus()
    }

    private fun finishScan() {
        if (kanbanAddList.size > 0) {
            postManifestCompletenessAdd()
        } else if (kanbanCancelList.size > 0) {
            postManifestCompletenessCancel()
        } else {
            postEndManifest()
        }
    }

    private fun getDataDockCodes() {
        var jsonDockCodes = sharedPref.getString(Constant.SET_PRINTER) ?: ""
        if (!jsonDockCodes.isEmpty()) {
            var modelDockCode = Gson().fromJson(sharedPref.getString(Constant.SET_PRINTER), ResponseDockCode::class.java)
            var dockCodeSize = modelDockCode.data?.size ?: 0
            if (dockCodeSize > 0) {
                dockCodes.clear()
                for (i in 0 until dockCodeSize) {
                    dockCodes.add(modelDockCode.data?.get(i)!!)
                }
            }
        }
    }

    private fun getKanbanList() {
        isScanning = true
        initDefaultView()
        kanbanList.clear()
        dialogLoading.show()
        var requestKanbanList = RequestKanbanList(manifestNo = modelNavbar.manifestNo ?: "")
        kanbanViewModel.getListKanbanManifest(authToken, requestKanbanList).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespKanbanList", "" + Gson().toJson(it.data))
                        dialogLoading.dismiss()
                        isScanning = false
                        var size = it.data?.data?.size ?: 0
                        if (size > 0) {
                            kanbanList.clear()
                            for (kanban in it.data?.data!!) {
                                if (kanban != null) {
                                    kanbanList.add(kanban)
                                    if (!(kanban.skidNo ?: "-").equals("-")) {
                                        var manifest = RequestManifestCompletenessItem(skidNo = kanban.skidNo, supplierCode = kanban.supplierCd, supplierPlant = kanban.supplierPlant, manifestNo = kanban.manifestNo, kanbanId = kanban.kanbanId, itemNo = (kanban.itemNo ?: "0").toInt(), seqNo = (kanban.seqNo ?: "0").toInt())
                                        kanbanScannedList.add(manifest)
                                        dataOriginScanned++
                                    }
                                }
                            }
                            updateTotalKanban()
                        } else {
                            isScanning = false
                            binding.tvScanNumber.setText("")
                            binding.tvScanDiv.setText("")
                            binding.tvScanTotal.setText("")
                        }
                    }

                    is RequestState.Error -> {
                        Log.e("ErrKanbanList", "" + it.message)
                        dialogLoading.dismiss()
                        isScanning = false
                    }
                }
            }
        }
    }

    private fun postManifestCompletenessAdd() {
        dialogLoading.show()
        manifestViewModel.postManifestCompletenessAdd(authToken, kanbanAddList).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespCompleteAdd", "" + Gson().toJson(it.data))
                        dialogLoading.dismiss()
                        kanbanAddList.clear()
                        if (kanbanCancelList.size > 0) {
                            postManifestCompletenessCancel()
                        } else {
                            checkSlip()
                        }
                    }

                    is RequestState.Error -> {
                        Log.e("ErrCompleteAdd", "" + it.message)
                        dialogLoading.dismiss()
                        if (kanbanCancelList.size > 0) {
                            postManifestCompletenessCancel()
                        } else {
                            var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), getString(R.string.ErrorAddKanban), object : OptionListener {
                                override fun onClicked(isApproved: Boolean) {
                                    binding.etResult.setText("")
                                    binding.etResult.clearFocus()
                                    isScanning = false
                                }
                            })
                            dialogInfo.show()
                        }
                    }
                }
            }
        }
    }

    private fun postManifestCompletenessCancel() {
        dialogLoading.show()
        manifestViewModel.postManifestCompletenessCancel(authToken, kanbanCancelList).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespCompleteCancel", "" + Gson().toJson(it.data))
                        dialogLoading.dismiss()
                        kanbanCancelList.clear()
                        checkSlip()
                    }

                    is RequestState.Error -> {
                        Log.e("ErrCompleteCancel", "" + it.message)
                        dialogLoading.dismiss()
                        var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), getString(R.string.ErrorAddKanban), object : OptionListener {
                            override fun onClicked(isApproved: Boolean) {
                                binding.etResult.setText("")
                                binding.etResult.clearFocus()
                                isScanning = false
                            }
                        })
                        dialogInfo.show()
                    }
                }
            }
        }
    }

    private fun postEndManifest() {
        dialogLoading.show()
        var requestManifestEnd = RequestManifestEnd(manifestNo = modelNavbar.manifestNo ?: "", processId = sharedPref.getString(modelNavbar.manifestNo ?: "0") ?: "0", subTitle = locale.language)
        manifestViewModel.postEndManifest(authToken, requestManifestEnd).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespManEndPost", "" + Gson().toJson(it.data))
                        dialogLoading.dismiss()
                        checkSlip()
                    }

                    is RequestState.Error -> {
                        dialogLoading.dismiss()
                        var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), it.message, object : OptionListener {
                            override fun onClicked(isApproved: Boolean) {}
                        })
                        dialogInfo.show()
                    }
                }
            }
        }
    }

    private fun checkSlip() {
        var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_SUCCESS, getString(R.string.MessageSuccess), "", object : OptionListener {
            override fun onClicked(isApproved: Boolean) {
                var indexDockCode = -1
                indexDockCode = dockCodes.indexOfFirst { it.value.equals(modelNavbar.dockCode, true) }
                if (indexDockCode == -1) {
                    parentActivity?.let {
                        it.supportFragmentManager.popBackStack()
                    }
                } else {
                    var dialogOption = DialogOption(requireActivity(), getString(R.string.InfoReqPrintSlip), object : OptionListener {
                        override fun onClicked(isApproved: Boolean) {
                            if (isApproved) {
                                showDialogSlip()
                            } else {
                                parentActivity?.let {
                                    it.supportFragmentManager.popBackStack()
                                }
                            }
                        }
                    })
                    dialogOption.show()
                }
            }
        })
        dialogInfo.show()
    }

    private fun showDialogSlip() {
        var kanban = kanbanList.get(0)
        var routeCycle = modelNavbar.route + " - " + modelNavbar.rate
        var suppCodePlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE) + " - " + sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT)
        var modelSlip = ModelSlip(supCodePlant = suppCodePlant, dockCode = modelNavbar.dockCode ?: "-", suppName = sharedPref.getString(Constant.PROFILE_SUPPLIER_NAME) ?: "-", suppDockCode = modelNavbar.shippingDock ?: "-", orderNumber = modelNavbar.orderNo ?: "-", manifestNumber = modelNavbar.manifestNo ?: "-", routeCycle = routeCycle, planeNumber = modelNavbar.pLaneSeq ?: "-", conveyance = modelNavbar.conveyanceRoute ?: "-", kanbanCode = kanban.kanbanCd ?: "-", kanbanId = kanban.kanbanId ?: "-")
        dialogSlip = DialogSlip(requireActivity(), modelSlip, object : SlipPreviewListener {
            override fun onCanceled() {
                parentActivity?.let {
                    it.supportFragmentManager.popBackStack()
                }
            }

            override fun onConfirmed(bitmap: Bitmap) {
                if (bitmap == null) {
                    var dialogOption = DialogOption(requireActivity(), getString(R.string.ErrorPrintSlip), object : OptionListener {
                        override fun onClicked(isApproved: Boolean) {
                            if (isApproved) {
                                showDialogSlip()
                            } else {
                                parentActivity?.let {
                                    it.supportFragmentManager.popBackStack()
                                }
                            }
                        }
                    })
                    dialogOption.show()
                } else {
                    printSlip(bitmap)
                }
            }
        })
        dialogSlip?.show()
    }

    private fun printSlip(bitmap: Bitmap) {
        try {
            Log.e("TryPrint", "True")
            tscActivity.openport(Printooth.getPairedPrinter()?.address ?: "")
            var mode = sharedPref.getInt(Constant.SET_PAPER)
            val paperHeight = if (mode == 0) 103 else 122
            val yCoordinat = if (mode == 0) 35 else 145
            tscActivity.setup(68, paperHeight, 2, 10, 0, 0, 0)
            tscActivity.clearbuffer()
            tscActivity.sendbitmap_resize(0, yCoordinat, bitmap, 550, 760)
            tscActivity.printlabel(2, 1)
            var dialogPrinting = DialogPrinting(requireActivity())
            dialogPrinting.setOnDismissListener {
                parentActivity?.let {
                    it.supportFragmentManager.popBackStack()
                }
            }
            dialogPrinting.show()
        } catch (e: Exception) {
            Log.e("ErrPrint", "" + e.message)
            var dialogOption = DialogOption(requireActivity(), getString(R.string.ErrorPrintSlip), object : OptionListener {
                override fun onClicked(isApproved: Boolean) {
                    if (isApproved) {
                        printSlip(bitmap)
                    } else {
                        parentActivity?.let {
                            it.supportFragmentManager.popBackStack()
                        }
                    }
                }
            })
            dialogOption.show()
        }
    }
}