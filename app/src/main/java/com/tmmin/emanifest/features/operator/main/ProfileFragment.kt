package com.tmmin.emanifest.features.operator.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import com.tmmin.emanifest.BuildConfig
import com.tmmin.emanifest.R
import com.tmmin.emanifest.databinding.FragmentProfileBinding
import com.tmmin.emanifest.dialogs.DialogInfo
import com.tmmin.emanifest.dialogs.DialogLoading
import com.tmmin.emanifest.features.auth.LoginActivity
import com.tmmin.emanifest.features.operator.settings.SettingsActivity
import com.tmmin.emanifest.features.others.TestFragment
import com.tmmin.emanifest.listeners.OptionListener
import com.tmmin.emanifest.models.requests.auth.RequestLogout
import com.tmmin.emanifest.network.RequestState
import com.tmmin.emanifest.util.BaseFragment
import com.tmmin.emanifest.util.Constant
import com.tmmin.emanifest.viewmodels.AuthViewModel

class ProfileFragment : BaseFragment(), View.OnClickListener {
    private val authViewModel: AuthViewModel by viewModels()
    private lateinit var binding: FragmentProfileBinding
    private lateinit var dialogLoading: DialogLoading
    var parentActivity: OperatorMainActivity? = null
    val ROLE_DRIVER = "Driver"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OperatorMainActivity) {
            parentActivity = context
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btnSetAbout -> {
                parentActivity?.let {
                    it.moveFragment(TestFragment())
                }
            }

            binding.btnSettings -> {
                val intent = Intent(requireActivity(), SettingsActivity::class.java)
                startActivity(intent)
            }

            binding.btnLogout -> {
                logout()
            }
        }
    }

    private fun setup() {
        parentActivity = activity as? OperatorMainActivity
//        binding.btnSetAbout.setOnClickListener(this)
        binding.btnSettings.setOnClickListener(this)
        binding.btnLogout.setOnClickListener(this)
        binding.tvName.text = "Halo, " + sharedPref.getString(Constant.AUTH_USERNAME)
        dialogLoading = DialogLoading(requireActivity())
        dialogLoading.setCancelable(true)
        val versionName = BuildConfig.VERSION_NAME
        when (BuildConfig.BASE_URL) {
            BuildConfig.BASE_URL_TRIAL -> {
                binding.tvVersion.setText("V : $versionName" + " (Trial)")
            }

            BuildConfig.BASE_URL_DEV -> {
                binding.tvVersion.setText("V : $versionName" + " (Dev)")
            }

            else -> {
                binding.tvVersion.setText("V : $versionName")
            }
        }
        var supplierRole = sharedPref.getString(Constant.PROFILE_SUPPLIER_ROLE) ?: ""
        if (supplierRole.equals(ROLE_DRIVER, true)) {
            binding.civProfileDriver.visibility = View.VISIBLE
            binding.civProfileOperator.visibility = View.GONE
        } else {
            binding.civProfileDriver.visibility = View.GONE
            binding.civProfileOperator.visibility = View.VISIBLE
        }
    }

    private fun logout() {
        val requestLogout = RequestLogout(username = sharedPref.getString(Constant.AUTH_USERNAME) ?: "")
        authViewModel.logout(requestLogout).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {
                        dialogLoading.show()
                    }

                    is RequestState.Success -> {
                        dialogLoading.dismiss()
                        it.data?.let { data ->
                            var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_SUCCESS, getString(R.string.MessageSuccess), "", object : OptionListener {
                                override fun onClicked(isApproved: Boolean) {
                                    sharedPref.clear()
                                    val intent = Intent(requireActivity(), LoginActivity::class.java)
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                    startActivity(intent)
                                    requireActivity().finish()
                                }
                            });
                            dialogInfo.show()
                        }
                    }

                    is RequestState.Error -> {
                        dialogLoading.dismiss()
                        var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), "", object : OptionListener {
                            override fun onClicked(isApproved: Boolean) {}
                        });
                        dialogInfo.show()
                    }
                }
            }
        }
    }
}