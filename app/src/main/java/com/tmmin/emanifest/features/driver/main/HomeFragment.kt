package com.tmmin.emanifest.features.driver.main

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.tmmin.emanifest.R
import com.tmmin.emanifest.databinding.FragmentDriverHomeBinding
import com.tmmin.emanifest.features.driver.job.JobDeliveryFragment
import com.tmmin.emanifest.util.BaseFragment
import com.tmmin.emanifest.util.Constant

class HomeFragment : BaseFragment(), View.OnClickListener {
    private lateinit var binding: FragmentDriverHomeBinding
    var parentActivity: DriverMainActivity? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_driver_home, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is DriverMainActivity) {
            parentActivity = context
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btnJobDelivery -> {
                parentActivity?.let {
                    it?.moveFragment(JobDeliveryFragment())
                }
            }

            binding.btnJobHistory -> {
                parentActivity?.let {
                    it.binding?.navigation?.selectedItemId = R.id.navJobHistory
                }
            }
        }
    }

    private fun setup() {
        parentActivity = activity as? DriverMainActivity
        binding.tvName.text = sharedPref.getString(Constant.PROFILE_SUPPLIER_USER)
        binding.tvCompany.text = sharedPref.getString(Constant.PROFILE_SUPPLIER_NAME)
        binding.btnJobDelivery.setOnClickListener(this)
        binding.btnJobHistory.setOnClickListener(this)
    }
}