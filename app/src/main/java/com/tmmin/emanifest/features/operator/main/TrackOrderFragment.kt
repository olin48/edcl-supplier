package com.tmmin.emanifest.features.operator.main

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.google.gson.Gson
import com.tmmin.emanifest.R
import com.tmmin.emanifest.adapters.JobTrackingAdapter
import com.tmmin.emanifest.databinding.FragmentTrackOrderBinding
import com.tmmin.emanifest.dialogs.DialogLoading
import com.tmmin.emanifest.models.requests.order.RequestOrderCombo
import com.tmmin.emanifest.models.requests.order.RequestOrderRoute
import com.tmmin.emanifest.models.requests.order.RequestOrderStatus
import com.tmmin.emanifest.models.responses.order.combo.DataItem
import com.tmmin.emanifest.network.RequestState
import com.tmmin.emanifest.util.BaseFragment
import com.tmmin.emanifest.util.Constant
import com.tmmin.emanifest.viewmodels.OrderViewModel
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class TrackOrderFragment : BaseFragment(), View.OnClickListener, OnMapReadyCallback {
    private val orderViewModel: OrderViewModel by viewModels()
    lateinit var fusedLocationClient: FusedLocationProviderClient
    lateinit var binding: FragmentTrackOrderBinding
    lateinit var dateTime: String
    lateinit var map: GoogleMap
    var currentLatLng = LatLng(0.0, 0.0)
    var statusList = ArrayList<com.tmmin.emanifest.models.responses.order.status.DataItem>()
    val inputFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
    val outputFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    var orderData = mutableListOf<DataItem>()
    var locList = mutableListOf<LatLng>()
    var parentActivity: OperatorMainActivity? = null
    var currentDeliveryNo = ""

    companion object {
        const val LOCATION_PERMISSION_REQUEST_CODE = 2
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_track_order, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OperatorMainActivity) {
            parentActivity = context
        }
    }

    override fun onResume() {
        super.onResume()
        if (ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            setup()
        } else {
            doRequestPermission()
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btnBack -> {
                parentActivity?.let {
                    it.onBackPressed()
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            LOCATION_PERMISSION_REQUEST_CODE -> {
                setup()
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        getCurrentLocation()
        getOrderCombo()
    }

    private fun setup() {
        parentActivity = activity as? OperatorMainActivity
        binding.btnBack.setOnClickListener(this)
        getDateTime()
        initDefaultValues()
        binding.layoutLoading.startShimmer()
        binding.layoutLoading.visibility = View.VISIBLE
        binding.layoutData.visibility = View.GONE
        val mapFragment = childFragmentManager.findFragmentById(R.id.layoutMap) as SupportMapFragment
        mapFragment.getMapAsync(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        parentActivity?.enableNavigation(false)
    }

    private fun doRequestPermission() {
        requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
    }

    private fun getDateTime() {
        val currentDate = Date()
        val formatter = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        dateTime = formatter.format(currentDate)
    }

    private fun initDefaultValues() {
        binding.tvDeliveryNo.setText("-")
        binding.tvDate.setText("-")
        binding.tvPartner.setText("-")
        binding.tvDriverName.setText("-")
    }

    fun getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        } else {
            fusedLocationClient.lastLocation.addOnSuccessListener { location ->
                if (location != null) {
                    currentLatLng = LatLng(location.latitude, location.longitude)
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 15f))
                }
            }
        }
    }

    private fun editMap(isDefault: Boolean) {
        if (isDefault) {
            locList.clear()
            map.clear()
        } else {
            val polylineOptions = PolylineOptions().addAll(locList).color(R.color.ColorBlue).width(5f)
            map.addPolyline(polylineOptions)
            if (locList.isNotEmpty()) {
                getLastPosition()
            } else {
                currentLatLng.let {
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 15f))
                }
            }
        }
    }

    private fun getOrderCombo() {
        var requestOrderCombo = RequestOrderCombo(supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE) ?: "", supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT) ?: "", subTitle = locale.language)
        orderViewModel.getOrderCombo(authToken, requestOrderCombo).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        parentActivity?.enableNavigation(true)
                        binding.layoutLoading.stopShimmer()
                        binding.layoutLoading.visibility = View.GONE
                        binding.layoutData.visibility = View.VISIBLE
                        var size = it.data?.data?.size ?: 0
                        if (size > 0) {
                            Log.e("RespOrderCombo", "" + Gson().toJson(it.data))
                            orderData = it.data?.data!!
                            val items = mutableListOf<String>()
                            items.clear()
                            for (job in it.data?.data!!) {
                                if (job != null) {
                                    items.add(job.label ?: "")
                                }
                            }
                            val adapter = ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, items)
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                            binding.spOrder.visibility = View.VISIBLE
                            binding.tvInfoJob.visibility = View.GONE
                            binding.spOrder.adapter = adapter
                            binding.spOrder.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                                override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                                    try {
                                        val date: Date = inputFormat.parse(orderData.get(position).pickupDt ?: "")
                                        binding.tvDate.setText(outputFormat.format(date))
                                    } catch (e: Exception) {
                                        binding.tvDate.setText(orderData.get(position).pickupDt ?: "")
                                    }
                                    binding.tvDeliveryNo.setText(orderData.get(position).deliveryNo ?: "")
                                    binding.tvPartner.setText(orderData.get(position).lpName ?: "")
                                    binding.tvDriverName.setText(orderData.get(position).driverName ?: "")
                                    currentDeliveryNo = orderData.get(position).deliveryNo ?: ""
                                    getOrderStatus()
                                }

                                override fun onNothingSelected(parent: AdapterView<*>) {}
                            }
                        } else {
                            initDefaultValues()
                            binding.spOrder.visibility = View.GONE
                            binding.tvInfoJob.visibility = View.VISIBLE
                            binding.tvInfoJob.text = "No Order"
                            binding.tvInfoStatus.visibility = View.VISIBLE
                            binding.rcyStatus.visibility = View.GONE
                            map.addMarker(MarkerOptions().position(currentLatLng).title("Current Location"))
                        }
                    }

                    is RequestState.Error -> {
                        Log.e("ErrOrderCombo", "" + it.message)
                        initDefaultValues()
                        binding.layoutLoading.stopShimmer()
                        binding.layoutLoading.visibility = View.GONE
                        binding.layoutData.visibility = View.VISIBLE
                        parentActivity?.enableNavigation(true)
                        binding.spOrder.visibility = View.GONE
                        binding.tvInfoJob.visibility = View.VISIBLE
                        binding.tvInfoJob.setText(it.message)
                        binding.tvInfoStatus.visibility = View.VISIBLE
                        binding.rcyStatus.visibility = View.GONE
                        map.addMarker(MarkerOptions().position(currentLatLng).title("Current Location"))
                    }
                }
            }
        }
    }

    private fun getOrderStatus() {
        binding.layoutLoading.startShimmer()
        binding.layoutLoading.visibility = View.VISIBLE
        binding.layoutData.visibility = View.GONE
        statusList.clear()
        var requestOrderStatus = RequestOrderStatus(deliveryNo = currentDeliveryNo, subTitle = locale.language)
        orderViewModel.getOrderStatus(authToken, requestOrderStatus).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        parentActivity?.enableNavigation(true)
                        var size = it.data?.data?.size ?: 0
                        if (size > 0) {
                            Log.e("RespOrderStatus", "" + Gson().toJson(it.data))
                            for (status in it.data?.data!!) {
                                if (status != null) {
                                    statusList.add(status)
                                    Log.e("Status", "" + Gson().toJson(status))
                                }
                            }
                            var statusAdapter = JobTrackingAdapter(requireActivity(), statusList)
                            binding.rcyStatus.layoutManager = LinearLayoutManager(requireActivity())
                            binding.rcyStatus.adapter = statusAdapter
                            binding.rcyStatus.visibility = View.VISIBLE
                            binding.tvInfoStatus.visibility = View.GONE
                        } else {
                            binding.tvInfoStatus.visibility = View.VISIBLE
                            binding.rcyStatus.visibility = View.GONE
                            map.addMarker(MarkerOptions().position(currentLatLng).title("Current Location"))
                        }
                        getOrderRoute()
                    }

                    is RequestState.Error -> {
                        Log.e("ErrOrderStatus", "" + it.message)
                        parentActivity?.enableNavigation(true)
                        binding.rcyStatus.visibility = View.GONE
                        binding.tvInfoStatus.setText(it.message)
                        binding.tvInfoStatus.visibility = View.VISIBLE
                        getOrderRoute()
                    }
                }
            }
        }
    }

    private fun getOrderRoute() {
        var requestOrderRoute = RequestOrderRoute(deliveryNo = currentDeliveryNo, subTitle = locale.language)
        orderViewModel.getOrderRoute(authToken, requestOrderRoute).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {
                        editMap(true)
                    }

                    is RequestState.Success -> {
                        binding.layoutLoading.stopShimmer()
                        binding.layoutLoading.visibility = View.GONE
                        binding.layoutData.visibility = View.VISIBLE
                        parentActivity?.enableNavigation(true)
                        var size = it.data?.data?.size ?: 0
                        if (size > 0) {
                            Log.e("RespOrderRoute", "" + Gson().toJson(it.data))
                            for (route in it.data?.data!!) {
                                if (route != null) {
                                    val loc = LatLng((route.y ?: 0).toDouble(), (route.x ?: 0).toDouble())
                                    map.addMarker(MarkerOptions().position(loc))
                                    locList.add(loc)
                                }
                            }
                            editMap(false)
                        } else {
                            Log.e("FailOrderRoute", "" + Gson().toJson(it.data))
                            map.addMarker(MarkerOptions().position(currentLatLng).title("Current Location"))
                        }
                    }

                    is RequestState.Error -> {
                        Log.e("ErrOrderRoute", "" + it.message)
                        binding.layoutLoading.stopShimmer()
                        binding.layoutLoading.visibility = View.GONE
                        binding.layoutData.visibility = View.VISIBLE
                        parentActivity?.enableNavigation(true)
                        map.addMarker(MarkerOptions().position(currentLatLng).title("Current Location"))
                    }
                }
            }
        }
    }

    private fun getLastPosition(){
        var dialogLoading = DialogLoading(requireActivity())
        dialogLoading.setCancelable(false)
        dialogLoading.show()
        var requestOrderRoute = RequestOrderRoute(deliveryNo = currentDeliveryNo, subTitle = locale.language)
        orderViewModel.getOrderLastPosition(authToken, requestOrderRoute).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {

                    }

                    is RequestState.Success -> {
                        dialogLoading.dismiss()
                        var size = it.data?.data?.size ?: 0
                        if (size > 0) {
                            Log.e("RespOrderPosition", "" + Gson().toJson(it.data))
                            val latLng = LatLng((it.data.data?.get(size)?.y ?: 0).toDouble(), (it.data.data?.get(size)?.x ?: 0).toDouble())
                            map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))
                        } else {
                            Log.e("FailOrderPosition", "" + Gson().toJson(it.data))
                            map.addMarker(MarkerOptions().position(currentLatLng).title("Current Location"))
                        }
                    }

                    is RequestState.Error -> {
                        Log.e("ErrOrderPosition", "" + it.message)
                        map.addMarker(MarkerOptions().position(currentLatLng).title("Current Location"))
                        dialogLoading.dismiss()
                    }
                }
            }
        }
    }
}