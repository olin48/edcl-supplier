package com.tmmin.emanifest.features.operator.skid

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.tmmin.emanifest.R
import com.tmmin.emanifest.adapters.SKIDAdapter
import com.tmmin.emanifest.databinding.FragmentManifestSkidBinding
import com.tmmin.emanifest.dialogs.DialogInfo
import com.tmmin.emanifest.dialogs.DialogLoading
import com.tmmin.emanifest.features.operator.kanban.ManifestKanbanScanOfflineFragment
import com.tmmin.emanifest.features.operator.main.OperatorMainActivity
import com.tmmin.emanifest.features.operator.manifest.list.ManifestListFragment
import com.tmmin.emanifest.features.operator.manifest.others.ManifestOthersActivity
import com.tmmin.emanifest.helpers.DataHelper
import com.tmmin.emanifest.listeners.OptionListener
import com.tmmin.emanifest.models.others.ModelNavbar
import com.tmmin.emanifest.models.requests.manifest.RequestManifestStart
import com.tmmin.emanifest.models.requests.skid.RequestSKIDCreate
import com.tmmin.emanifest.models.requests.skid.RequestSKIDList
import com.tmmin.emanifest.models.responses.skid.list.DataItem
import com.tmmin.emanifest.network.RequestState
import com.tmmin.emanifest.util.BaseFragment
import com.tmmin.emanifest.util.Constant
import com.tmmin.emanifest.viewmodels.ManifestViewModel
import com.tmmin.emanifest.viewmodels.SKIDViewModel

class ManifestSKIDFragment : BaseFragment(), View.OnClickListener {
    private val manifestViewModel: ManifestViewModel by viewModels()
    private val skidViewModel: SKIDViewModel by viewModels()
    private lateinit var dialogLoading: DialogLoading
    private lateinit var skidAdapter: SKIDAdapter
    lateinit var binding: FragmentManifestSkidBinding
    var parentActivity: OperatorMainActivity? = null
    var skidList = ArrayList<DataItem>()
    lateinit var modelNavbar: ModelNavbar
    var dataNavbar: String? = null
    var dataType: String? = null
    var dataIsHistory = false

    companion object {
        fun newInstance(paramNavbar: String, paramType: String, paramIsHistory: Boolean): ManifestSKIDFragment {
            val fragment = ManifestSKIDFragment()
            val args = Bundle()
            args.putString(DataHelper.DATA_NAVBAR, paramNavbar)
            args.putString(DataHelper.DATA_TYPE, paramType)
            args.putBoolean(DataHelper.DATA_IS_HISTORY, paramIsHistory)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            dataNavbar = it.getString(DataHelper.DATA_NAVBAR)
            dataType = it.getString(DataHelper.DATA_TYPE)
            dataIsHistory = it.getBoolean(DataHelper.DATA_IS_HISTORY)
            modelNavbar = Gson().fromJson(dataNavbar, ModelNavbar::class.java);
            Log.e("ModelNavbar", "" + Gson().toJson(modelNavbar))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_manifest_skid, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OperatorMainActivity) {
            parentActivity = context
        }
    }

    override fun onDestroy() {
        Log.e("OnDestroy", "True")
        super.onDestroy()
        if (::dialogLoading.isInitialized) {
            dialogLoading.dismiss()
        }
    }

    override fun onPause() {
        Log.e("OnPause", "True")
        super.onPause()
        if (::dialogLoading.isInitialized) {
            dialogLoading.dismiss()
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.layoutToolbar.btnBack -> {
                parentActivity?.let {
                    it.onBackPressed()
                }
            }

            binding.btnCreate -> {
                parentActivity?.let {
                    createSkid()
                }
            }

            binding.btnManifest -> {
                moveManifestList()
            }
        }
    }

    private fun setup() {
        parentActivity = activity as? OperatorMainActivity
        parentActivity?.binding?.navigation?.visibility = View.VISIBLE
        binding.layoutToolbar.tvTitle.visibility = View.VISIBLE
        binding.layoutToolbar.btnBack.visibility = View.VISIBLE
        binding.layoutToolbar.tvTitle.text = "SKID List"
        dialogLoading = DialogLoading(requireActivity())
        dialogLoading.setCancelable(true)
        binding.tvRoute.text = modelNavbar.route
        binding.tvRate.text = modelNavbar.rate
        binding.tvPartner.text = modelNavbar.partner
        binding.tvPickupDate.text = modelNavbar.date
        binding.layoutToolbar.btnBack.setOnClickListener(this)
        binding.btnCreate.setOnClickListener(this)
        binding.btnManifest.setOnClickListener(this)
        if (dataIsHistory || (!dataType.equals("0"))) {
            binding.btnCreate.visibility = View.GONE
            binding.btnManifest.visibility = View.GONE
        } else {
            binding.btnCreate.visibility = View.VISIBLE
            binding.btnManifest.visibility = View.VISIBLE
        }
        getSKIDList()
    }

    private fun getSKIDList() {
        var requestPartList = RequestSKIDList(manifestNo = modelNavbar.manifestNo, subTitle = locale.language)
        skidViewModel.getSKIDList(authToken, requestPartList).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {
                        binding.layoutLoading.startShimmer()
                        binding.layoutLoading.visibility = View.VISIBLE
                        binding.tvInfoSKID.visibility = View.GONE
                        binding.rcySKID.visibility = View.GONE
                    }

                    is RequestState.Success -> {
                        Log.e("RespSKID", "" + Gson().toJson(it.data))
                        binding.layoutLoading.stopShimmer()
                        binding.layoutLoading.visibility = View.GONE
                        var size = it.data?.data?.size ?: 0
                        skidList.clear()
                        if (size > 0) {
                            for (skid in it.data?.data!!) {
                                if (skid != null) {
                                    skidList.add(skid)
                                }
                            }
                            skidAdapter = SKIDAdapter(parentActivity!!, viewLifecycleOwner, modelNavbar, skidList, dataIsHistory)
                            binding.rcySKID.layoutManager = LinearLayoutManager(requireActivity())
                            binding.rcySKID.adapter = skidAdapter
                            binding.tvInfoSKID.visibility = View.GONE
                            binding.rcySKID.visibility = View.VISIBLE
                        } else {
                            binding.tvInfoSKID.visibility = View.VISIBLE
                            binding.tvInfoSKID.setText("No SKID")
                            binding.rcySKID.visibility = View.GONE
                        }
                    }

                    is RequestState.Error -> {
                        binding.layoutLoading.stopShimmer()
                        binding.layoutLoading.visibility = View.GONE
                        binding.tvInfoSKID.visibility = View.VISIBLE
                        binding.rcySKID.visibility = View.GONE
                        binding.tvInfoSKID.setText(it.message ?: getString(R.string.ErrorServer))
                    }
                }
            }
        }
    }

    private fun createSkid() {
        var isStarted = sharedPref.getString(modelNavbar.manifestNo ?: "") ?: ""
        Log.e("ReqProcessId", isStarted)
        if (isStarted.isEmpty() || isStarted.equals("0")) {
            postStartManifest()
        } else {
            postCreateSKID()
        }
    }

    private fun postStartManifest() {
        dialogLoading.show()
        var reqManifestStart = RequestManifestStart(manifestNo = modelNavbar.manifestNo, subTitle = locale.language)
        manifestViewModel.startManifest(authToken, reqManifestStart).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {
                        dialogLoading.show()
                    }

                    is RequestState.Success -> {
                        Log.e("RespManStart", "" + Gson().toJson(it.data))
                        dialogLoading.dismiss()
                        it.data?.let { data ->
                            sharedPref.put((modelNavbar.manifestNo ?: ""), (data?.data?.processIdOutput ?: 0).toString())
                            postCreateSKID()
                        }
                    }

                    is RequestState.Error -> {
                        dialogLoading.dismiss()
                        var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), it.message, object : OptionListener {
                            override fun onClicked(isApproved: Boolean) {}
                        })
                        dialogInfo.show()
                    }
                }
            }
        }
    }

    private fun postCreateSKID() {
        dialogLoading.show()
        var processId = sharedPref.getString(modelNavbar.manifestNo ?: "") ?: ""
        var requestSKIDCreate = RequestSKIDCreate(manifestNo = modelNavbar.manifestNo, processId = processId, supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE) ?: "", supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT) ?: "", subTitle = locale.language)
        skidViewModel.createSKID(authToken, requestSKIDCreate).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {
                        dialogLoading.show()
                    }

                    is RequestState.Success -> {
                        Log.e("RespManStart", "" + Gson().toJson(it.data))
                        dialogLoading.dismiss()
                        it.data?.let { data ->
                            modelNavbar.skidNo = data?.data?.skidNoOutput ?: ""
                            startScanKanban()
                        }
                    }

                    is RequestState.Error -> {
                        dialogLoading.dismiss()
                        var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), it.message, object : OptionListener {
                            override fun onClicked(isApproved: Boolean) {}
                        })
                        dialogInfo.show()
                    }
                }
            }
        }
    }

    private fun startScanKanban() {
        parentActivity?.let {
            it.moveAndDestroyFragment(ManifestKanbanScanOfflineFragment.newInstance(Gson().toJson(modelNavbar)))
        }
    }

    private fun moveManifestList() {
        when (dataType) {
            "1" -> {
                val intent = Intent(parentActivity, ManifestOthersActivity::class.java)
                intent.putExtra(DataHelper.DATA_NAVBAR, dataNavbar)
                intent.putExtra(DataHelper.DATA_TYPE, 0)
                activity?.startActivity(intent)
            }

            "2" -> {
                val intent = Intent(parentActivity, ManifestOthersActivity::class.java)
                intent.putExtra(DataHelper.DATA_NAVBAR, dataNavbar)
                intent.putExtra(DataHelper.DATA_TYPE, 1)
                activity?.startActivity(intent)
            }

            else -> {
                parentActivity?.let {
                    it.moveFragment(ManifestListFragment.newInstance(Gson().toJson(modelNavbar), false))
                }
            }
        }
    }
}