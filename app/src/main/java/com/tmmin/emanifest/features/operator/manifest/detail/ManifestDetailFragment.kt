package com.tmmin.emanifest.features.operator.manifest.detail

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.tmmin.emanifest.R
import com.tmmin.emanifest.adapters.PartManifestAdapter
import com.tmmin.emanifest.databinding.FragmentManifestDetailBinding
import com.tmmin.emanifest.dialogs.DialogAssign
import com.tmmin.emanifest.dialogs.DialogInfo
import com.tmmin.emanifest.dialogs.DialogKanban
import com.tmmin.emanifest.dialogs.DialogLoading
import com.tmmin.emanifest.dialogs.DialogManifestCompleted
import com.tmmin.emanifest.features.operator.kanban.ManifestKanbanScanOfflineFragment
import com.tmmin.emanifest.features.operator.main.OperatorMainActivity
import com.tmmin.emanifest.features.operator.skid.ManifestSKIDFragment
import com.tmmin.emanifest.helpers.DataHelper
import com.tmmin.emanifest.helpers.PreferenceHelper
import com.tmmin.emanifest.helpers.StringHelper
import com.tmmin.emanifest.listeners.AssignListener
import com.tmmin.emanifest.listeners.OptionListener
import com.tmmin.emanifest.models.others.ModelNavbar
import com.tmmin.emanifest.models.requests.assign.RequestAssign
import com.tmmin.emanifest.models.requests.assign.RequestAssignCombo
import com.tmmin.emanifest.models.requests.manifest.RequestManifestDetail
import com.tmmin.emanifest.models.requests.manifest.RequestManifestEnd
import com.tmmin.emanifest.models.requests.manifest.RequestManifestEndCheck
import com.tmmin.emanifest.models.requests.manifest.RequestManifestStart
import com.tmmin.emanifest.models.requests.part.RequestPartList
import com.tmmin.emanifest.models.requests.skid.RequestSKIDCreate
import com.tmmin.emanifest.models.responses.manifest.detail.Data
import com.tmmin.emanifest.models.responses.part.list.DataItem
import com.tmmin.emanifest.network.RequestState
import com.tmmin.emanifest.util.BaseFragment
import com.tmmin.emanifest.util.Constant
import com.tmmin.emanifest.viewmodels.ManifestViewModel
import com.tmmin.emanifest.viewmodels.PartViewModel
import com.tmmin.emanifest.viewmodels.SKIDViewModel

class ManifestDetailFragment : BaseFragment(), View.OnClickListener {
    private lateinit var dialogLoading: DialogLoading
    private lateinit var binding: FragmentManifestDetailBinding
    private lateinit var partManifestAdapter: PartManifestAdapter
    private val manifestViewModel: ManifestViewModel by viewModels()
    private val skidViewModel: SKIDViewModel by viewModels()
    private val partViewModel: PartViewModel by viewModels()
    private var partList = ArrayList<DataItem>()
    private var parentActivity: OperatorMainActivity? = null
    private var dataManifest: Data? = null
    var modelNavbar = ModelNavbar()
    var stringHelper = StringHelper()
    val statusPreparation = "0"
    val statusIncomplete = "1"
    val statusComplete = "2"
    val statusNoOrder = "3"
    var dataIsHistory = false
    var paramIsTransporter = false
    var isOthers = false
    var isPickedUp = false
    var selectedDelivery = ""
    var flag = ""
    var dataManifestNo = ""

    companion object {
        fun newInstance(paramManifestNo: String, paramIsHistory: Boolean, paramIsTransporter: Boolean): ManifestDetailFragment {
            val fragment = ManifestDetailFragment()
            val args = Bundle()
            args.putString(DataHelper.DATA_MANIFEST_NO, paramManifestNo)
            args.putBoolean(DataHelper.DATA_IS_HISTORY, paramIsHistory)
            args.putBoolean(DataHelper.DATA_IS_TRANSPORTER, paramIsTransporter)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            dataManifestNo = it.getString(DataHelper.DATA_MANIFEST_NO) ?: ""
            dataIsHistory = it.getBoolean(DataHelper.DATA_IS_HISTORY)
            paramIsTransporter = it.getBoolean(DataHelper.DATA_IS_TRANSPORTER)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_manifest_detail, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OperatorMainActivity) {
            parentActivity = context
        }
    }

    private fun setup() {
        parentActivity = activity as? OperatorMainActivity
        parentActivity?.binding?.navigation?.visibility = View.VISIBLE
        binding.layoutToolbar.tvTitle.visibility = View.VISIBLE
        binding.layoutToolbar.btnBack.visibility = View.VISIBLE
        binding.layoutToolbar.tvTitle.text = "Manifest Detail"
        dialogLoading = DialogLoading(requireActivity())
        dialogLoading.setCancelable(true)
        sharedPref = PreferenceHelper(requireActivity())
        binding.layoutToolbar.btnBack.setOnClickListener(this)
        binding.btnListSKID.setOnClickListener(this)
        binding.btnCreateSKID.setOnClickListener(this)
        binding.btnSubmit.setOnClickListener(this)
        getDetailManifest()
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.layoutToolbar.btnBack -> {
                parentActivity?.let {
                    it.onBackPressed()
                }
            }

            binding.btnListSKID -> {
                parentActivity?.let {
                    var isDisabled: Boolean
                    if (dataIsHistory || isPickedUp) {
                        isDisabled = true
                    } else {
                        isDisabled = false
                    }
                    it.moveFragment(ManifestSKIDFragment.newInstance(Gson().toJson(modelNavbar), flag, isDisabled))
                }
            }

            binding.btnCreateSKID -> {
                var isStarted = sharedPref.getString(dataManifestNo ?: "") ?: ""
                if (isStarted.isEmpty() || isStarted.equals("0")) {
                    startManifest(false)
                } else {
                    createSKID()
                }
            }

            binding.btnSubmit -> {
                checkEndManifest()
            }
        }
    }

    private fun setValue(isSuccess: Boolean) {
        if (isSuccess == true) {
            binding.tvManifestStatus.setTextColor(getColor(dataManifest?.supplierPreparationStatus ?: "-"))
            binding.tvManifestNumber.text = dataManifest?.manifestNo ?: "-"
            binding.tvManifestStatus.text = dataManifest?.supplierPreparationDescription ?: "-"
            binding.tvSuppCode.text = dataManifest?.supplierShippingDock ?: "-"
            binding.tvPreparedBy.text = dataManifest?.lastPreparedBy ?: "-"
            binding.tvDockCode.text = dataManifest?.dockCd ?: "-"
            binding.tvOrderNumber.text = dataManifest?.orderNo ?: "-"
            binding.tvSuppDeparture.text = stringHelper.convertDateTime(dataManifest?.supplierDeparturePlanDt ?: "-")
            binding.tvSuppArrive.text = stringHelper.convertDateTime(dataManifest?.tmminArrivalPlanDt ?: "-")
            binding.tvInfoDriver.text = dataManifest?.driverName ?: "-"
            binding.tvInfoVehicle.text = dataManifest?.vehicleType ?: "-"
            binding.tvInfoVehiclePlat.text = dataManifest?.platNo ?: "-"
            binding.tvPartTotalKanban.text = (dataManifest?.totalKanbanPlan ?: 0).toString()
            binding.tvPartTotalQty.text = (dataManifest?.totalQuantityPlan ?: 0).toString()

            var status = dataManifest?.supplierPreparationStatus ?: "0"
            isPickedUp = dataManifest?.isPickedUp ?: false
            modelNavbar.deliveryNo = dataManifest?.deliveryNo ?: ""
            modelNavbar.orderNo = dataManifest?.orderNo ?: ""
            modelNavbar.manifestNo = dataManifestNo
            modelNavbar.route = dataManifest?.route ?: "-"
            modelNavbar.rate = dataManifest?.rate ?: "-"
            modelNavbar.partner = dataManifest?.lpName ?: "-"
            modelNavbar.dockCode = dataManifest?.dockCd ?: "-"
            modelNavbar.conveyanceRoute = dataManifest?.conveyanceRoute ?: "-"
            modelNavbar.pLaneSeq = dataManifest?.pLaneSeq ?: "-"
            modelNavbar.shippingDock = dataManifest?.shippingDock ?: "-"
            modelNavbar.deliveryPreparationStatus = "0"
            modelNavbar.date = stringHelper.convertDateTime(dataManifest?.supplierArrivalPlanDt ?: "-")
            flag = dataManifest?.manifestCategoryFlag ?: ""

            binding.layoutBottom.visibility = View.VISIBLE
            if (flag.equals("0", true)) {
                //Checking If Manifest Is Not Manifest Others
                binding.btnSubmit.setText("Submit")
                isOthers = false
            } else {
                //Manifest Is Manifest Others
                binding.btnSubmit.setText("Next")
                isOthers = true
                if (status.equals("0")) {
                    //Checking If Manifest Is Manifest Others And Status Is Preparation Then Set Default Title
                    if (isPickedUp == false) {
                        binding.tvRoute.visibility = View.GONE
                        binding.tvRate.visibility = View.GONE
                        modelNavbar.route = ""
                        modelNavbar.rate = ""
                        modelNavbar.partner = "-"
                        modelNavbar.date = "-"
                    }
                } else {
                    //Manifest Is Manifest Others And Status Is Not Preparation Then Set Title
                    binding.tvRoute.visibility = View.VISIBLE
                    binding.tvRate.visibility = View.VISIBLE
                }
            }

            Log.e("IsHistory", "" + dataIsHistory)
            Log.e("IsTransporter", "" + paramIsTransporter)
            Log.e("IsPickedUp", "" + isPickedUp)
            Log.e("IsOthers", "" + isOthers)
            binding.tvRoute.text = modelNavbar.route
            binding.tvRate.text = modelNavbar.rate
            binding.tvPartner.text = modelNavbar.partner
            binding.tvPickupDate.text = modelNavbar.date

            if (dataIsHistory) {
                //Checking If Manifest Is History
                binding.btnListSKID.visibility = View.VISIBLE
                binding.btnCreateSKID.visibility = View.GONE
                binding.btnSubmit.visibility = View.GONE
            } else if (paramIsTransporter) {
                //Checking If Manifest Is Direct
                binding.btnListSKID.visibility = View.VISIBLE
                binding.btnCreateSKID.visibility = View.GONE
                binding.btnSubmit.visibility = View.GONE
            } else if (isOthers) {
                //Checking If Manifest Is Others
                binding.btnCreateSKID.visibility = View.VISIBLE
                binding.btnListSKID.visibility = View.VISIBLE
                binding.btnSubmit.visibility = View.GONE
            } else if (isPickedUp) {
                //Checking If Manifest Has Been Picked Up
                binding.btnListSKID.visibility = View.VISIBLE
                binding.btnCreateSKID.visibility = View.VISIBLE
                binding.btnSubmit.visibility = View.VISIBLE
            } else {
                //Manifest Has Not Been Picked Up
                if ((dataManifest?.supplierPreparationStatus ?: "0").equals("3")) {
                    //Manifest Is Not Manifest Others And Status No Order
                    binding.btnSubmit.visibility = View.GONE
                    binding.btnListSKID.visibility = View.GONE
                    binding.btnCreateSKID.visibility = View.GONE
                } else {
                    //Manifest Is Not Manifest Others And Status Is Not No Order
                    binding.btnSubmit.visibility = View.GONE
                    binding.btnListSKID.visibility = View.VISIBLE
                    binding.btnCreateSKID.visibility = View.VISIBLE
                }
            }
        } else {
            binding.layoutBottom.visibility = View.GONE
            binding.tvManifestStatus.setTextColor(ContextCompat.getColor(requireActivity(), R.color.ColorBlackSecond))
            binding.tvRoute.text = "-"
            binding.tvRate.text = "-"
            binding.tvPartner.text = "-"
            binding.tvPickupDate.text = "-"
            binding.tvManifestNumber.text = "-"
            binding.tvManifestStatus.text = "-"
            binding.tvSuppCode.text = "-"
            binding.tvPreparedBy.text = "-"
            binding.tvDockCode.text = "-"
            binding.tvOrderNumber.text = "-"
            binding.tvSuppDeparture.text = "-"
            binding.tvSuppArrive.text = "-"
            binding.tvInfoDriver.text = "-"
            binding.tvInfoVehicle.text = "-"
            binding.tvInfoVehiclePlat.text = "-"
            binding.tvPartTotalKanban.text = "-"
            binding.tvPartTotalQty.text = "-"
        }
    }

    private fun getColor(status: String): Int {
        var color = ContextCompat.getColor(requireActivity(), R.color.ColorBlackSecond)
        when (status) {
            statusPreparation -> color = ContextCompat.getColor(requireActivity(), R.color.ColorLime)
            statusIncomplete -> color = ContextCompat.getColor(requireActivity(), R.color.ColorRed)
            statusComplete -> color = ContextCompat.getColor(requireActivity(), R.color.ColorGreenDark)
            statusNoOrder -> color = ContextCompat.getColor(requireActivity(), R.color.ColorYellow)
        }
        return color
    }

    private fun getDetailManifest() {
        binding.layoutLoading.visibility = View.VISIBLE
        binding.layoutData.visibility = View.GONE
        binding.layoutLoading.startShimmer()
        var requestManifestDetail = RequestManifestDetail(manifestNo = dataManifestNo, subTitle = locale.language)
        manifestViewModel.getManifestDetail(authToken, requestManifestDetail).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {
                        binding.layoutLoading.visibility = View.VISIBLE
                        binding.layoutData.visibility = View.GONE
                        binding.layoutLoading.startShimmer()
                        setValue(false)
                    }

                    is RequestState.Success -> {
                        Log.e("RespDetail", "" + Gson().toJson(it.data))
                        it.data?.let { data ->
                            dataManifest = data.data
                            setValue(true)
                        }
                        getParts()
                    }

                    is RequestState.Error -> {
                        Log.e("ErrDetail", "" + it.message)
                        setValue(false)
                        getParts()
                    }
                }
            }
        }
    }

    private fun getParts() {
        var requestPartList = RequestPartList(manifestNo = dataManifestNo, subTitle = locale.language)
        partViewModel.getPartList(authToken, requestPartList).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespParts", "" + Gson().toJson(it.data))
                        binding.layoutLoading.visibility = View.GONE
                        binding.layoutData.visibility = View.VISIBLE
                        binding.layoutLoading.stopShimmer()
                        var size = it.data?.data?.size ?: 0
                        if (size > 0) {
                            partList.clear()
                            for (part in it.data?.data!!) {
                                if (part != null) {
                                    partList.add(part)
                                }
                            }
                            partManifestAdapter = PartManifestAdapter(requireActivity(), partList)
                            binding.rcyPart.layoutManager = LinearLayoutManager(requireActivity())
                            binding.rcyPart.adapter = partManifestAdapter
                            binding.rcyPart.visibility = View.VISIBLE
                            binding.tvInfoPart.visibility = View.GONE
                        } else {
                            binding.rcyPart.visibility = View.GONE
                            binding.tvInfoPart.visibility = View.VISIBLE
                            binding.tvInfoPart.text = "No Parts"
                        }
                    }

                    is RequestState.Error -> {
                        Log.e("ErrParts", "" + it.message)
                        binding.layoutLoading.visibility = View.GONE
                        binding.layoutData.visibility = View.VISIBLE
                        binding.layoutLoading.stopShimmer()
                        binding.rcyPart.visibility = View.GONE
                        binding.tvInfoPart.visibility = View.VISIBLE
                        binding.tvInfoPart.text = it.message
                    }
                }
            }
        }
    }

    private fun startManifest(isSingle: Boolean) {
        dialogLoading.show()
        var reqManifestStart = RequestManifestStart(manifestNo = dataManifestNo, subTitle = locale.language)
        manifestViewModel.startManifest(authToken, reqManifestStart).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespManStart", "" + Gson().toJson(it.data))
                        dialogLoading.dismiss()
                        it.data?.let { data ->
                            sharedPref.put((dataManifestNo), (data?.data?.processIdOutput ?: 0).toString())
                            if (isSingle) {
                                checkEndManifest()
                            } else {
                                createSKID()
                            }
                        }
                    }

                    is RequestState.Error -> {
                        dialogLoading.dismiss()
                        var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), it.message, object : OptionListener {
                            override fun onClicked(isApproved: Boolean) {}
                        })
                        dialogInfo.show()
                    }
                }
            }
        }
    }

    private fun createSKID() {
        dialogLoading.show()
        var processId = sharedPref.getString(dataManifestNo ?: "0") ?: "0"
        var requestSKIDCreate = RequestSKIDCreate(manifestNo = dataManifestNo, processId = processId, supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE) ?: "", supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT) ?: "", subTitle = locale.language)
        skidViewModel.createSKID(authToken, requestSKIDCreate).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespManSkidCreate", "" + Gson().toJson(it.data))
                        dialogLoading.dismiss()
                        modelNavbar.skidNo = it.data?.data?.skidNoOutput ?: ""
                        startScanKanban()
                    }

                    is RequestState.Error -> {
                        dialogLoading.dismiss()
                        var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), it.message, object : OptionListener {
                            override fun onClicked(isApproved: Boolean) {}
                        })
                        dialogInfo.show()
                    }
                }
            }
        }
    }

    private fun startScanKanban() {
        parentActivity?.let {
            it.moveAndDestroyFragment(ManifestKanbanScanOfflineFragment.newInstance(Gson().toJson(modelNavbar)))
        }
    }

    private fun checkEndManifest() {
        var processId = sharedPref.getString(dataManifestNo ?: "0") ?: "0"
        if (processId.equals("0")) {
            startManifest(true)
        } else {
            dialogLoading.show()
            var requestManifestEndCheck = RequestManifestEndCheck(manifestNo = dataManifestNo, processId = processId, subTitle = locale.language)
            manifestViewModel.getCheckEndManifest(authToken, requestManifestEndCheck).observe(viewLifecycleOwner) {
                if (it != null) {
                    when (it) {
                        is RequestState.Loading -> {}

                        is RequestState.Success -> {
                            Log.e("RespManEndCheck", "" + Gson().toJson(it.data))
                            dialogLoading.dismiss()
                            it.data?.let { data ->
                                var message = (data.data?.messageType ?: "")
                                when {
                                    message.equals("I", ignoreCase = true) -> {
                                        endManifest()
                                    }

                                    message.equals("C", ignoreCase = true) -> {
                                        if (isOthers) {
                                            endManifest()
                                        } else {
                                            var dialogKanban = DialogKanban(requireActivity(), DialogKanban.MODE_MULTIPLE, it.data?.data?.message ?: getString(R.string.ErrorServer), object : OptionListener {
                                                override fun onClicked(isApproved: Boolean) {
                                                    if (isApproved == true) {
                                                        endManifest()
                                                    }
                                                }
                                            })
                                            dialogKanban.show()
                                        }
                                    }

                                    message.equals("E", ignoreCase = true) -> {
                                        var dialogKanban = DialogKanban(requireActivity(), DialogKanban.MODE_BASIC, it.data?.data?.message ?: getString(R.string.ErrorServer), object : OptionListener {
                                            override fun onClicked(isApproved: Boolean) {}
                                        })
                                        dialogKanban.show()
                                    }

                                    else -> {
                                        var dialogKanban = DialogKanban(requireActivity(), DialogKanban.MODE_BASIC, it.data?.data?.message ?: getString(R.string.ErrorServer), object : OptionListener {
                                            override fun onClicked(isApproved: Boolean) {}
                                        })
                                        dialogKanban.show()
                                    }
                                }
                            }
                        }

                        is RequestState.Error -> {
                            dialogLoading.dismiss()
                            var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), it.message, object : OptionListener {
                                override fun onClicked(isApproved: Boolean) {}
                            })
                            dialogInfo.show()
                        }
                    }
                }
            }
        }
    }

    private fun endManifest() {
        dialogLoading.show()
        var requestManifestEnd = RequestManifestEnd(manifestNo = dataManifestNo, processId = sharedPref.getString(dataManifestNo ?: "0") ?: "0", subTitle = locale.language)
        manifestViewModel.postEndManifest(authToken, requestManifestEnd).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespManEndPost", "" + Gson().toJson(it.data))
                        dialogLoading.dismiss()
                        it.data?.let { data ->
                            if (isOthers) {
                                postAssign()
                            } else {
                                parentActivity?.let {
                                    var dialogCompleted = DialogManifestCompleted(parentActivity!!)
                                    dialogCompleted.show()
                                }
                            }
                        } ?: run {
                            var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), getString(R.string.ErrorProcessManifest), object : OptionListener {
                                override fun onClicked(isApproved: Boolean) {}
                            })
                            dialogInfo.show()
                        }
                    }

                    is RequestState.Error -> {
                        dialogLoading.dismiss()
                        var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), it.message, object : OptionListener {
                            override fun onClicked(isApproved: Boolean) {}
                        })
                        dialogInfo.show()
                    }
                }
            }
        }
    }

    private fun getOptionAssign() {
        dialogLoading.show()
        var requestAssignCombo = RequestAssignCombo(supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE) ?: "", supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT) ?: "", subTitle = locale.language)
        manifestViewModel.getComboManifestOthers(authToken, requestAssignCombo).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespAssOption", "" + Gson().toJson(it.data))
                        dialogLoading.dismiss()
                        var size = it.data?.data?.size ?: 0
                        if (size > 0) {
                            val items = mutableListOf<String>()
                            items.clear()
                            items.add("Select Driver")
                            for (job in it.data?.data!!) {
                                var name = job?.label ?: ""
                                items.add(name)
                            }
                            val dialogAssign = DialogAssign(requireActivity(), items, "", object : AssignListener {
                                override fun onClicked(index: Int) {
                                    selectedDelivery = it.data.data?.get(index)?.value ?: ""
                                    getCheckAssign()
                                }
                            })
                            dialogAssign.show(requireActivity().supportFragmentManager, "DIALOG_ASSIGN")
                        } else {
                            var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), "Option Not Found !", object : OptionListener {
                                override fun onClicked(isApproved: Boolean) {}
                            })
                            dialogInfo.show()
                        }
                    }

                    is RequestState.Error -> {
                        dialogLoading.dismiss()
                        var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), it.message, object : OptionListener {
                            override fun onClicked(isApproved: Boolean) {}
                        })
                        dialogInfo.show()
                    }
                }
            }
        }
    }

    private fun getCheckAssign() {
        dialogLoading.show()
        var requestAssign = RequestAssign(deliveryNo = selectedDelivery, manifestNo = dataManifestNo, subTitle = locale.language)
        manifestViewModel.getCheckManifestAssign(authToken, requestAssign).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespAssCheck", "" + Gson().toJson(it.data))
                        dialogLoading.dismiss()
                        it.data?.let { data ->
                            var message = (data.data?.messageType ?: "")
                            when {
                                message.equals("I", ignoreCase = true) -> {
                                    checkEndManifest()
                                }

                                message.equals("C", ignoreCase = true) -> {
                                    var dialogKanban = DialogKanban(requireActivity(), DialogKanban.MODE_ASSIGN, it.data?.data?.message ?: getString(R.string.ErrorServer), object : OptionListener {
                                        override fun onClicked(isApproved: Boolean) {
                                            if (isApproved == true) {
                                                checkEndManifest()
                                            }
                                        }
                                    })
                                    dialogKanban.show()
                                }

                                message.equals("E", ignoreCase = true) -> {
                                    var dialogKanban = DialogKanban(requireActivity(), DialogKanban.MODE_BASIC, it.data?.data?.message ?: getString(R.string.ErrorServer), object : OptionListener {
                                        override fun onClicked(isApproved: Boolean) {}
                                    })
                                    dialogKanban.show()
                                }

                                else -> {
                                    var dialogKanban = DialogKanban(requireActivity(), DialogKanban.MODE_BASIC, it.data?.data?.message ?: getString(R.string.ErrorServer), object : OptionListener {
                                        override fun onClicked(isApproved: Boolean) {}
                                    })
                                    dialogKanban.show()
                                }
                            }
                        }
                    }

                    is RequestState.Error -> {
                        dialogLoading.dismiss()
                        var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), it.message, object : OptionListener {
                            override fun onClicked(isApproved: Boolean) {}
                        })
                        dialogInfo.show()
                    }
                }
            }
        }
    }

    private fun postAssign() {
        dialogLoading.show()
        var requestAssign = RequestAssign(deliveryNo = selectedDelivery, manifestNo = dataManifestNo, subTitle = locale.language)
        manifestViewModel.postManifestAssign(authToken, requestAssign).observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespAssPost", "" + Gson().toJson(it.data))
                        dialogLoading.dismiss()
                        it.data?.let { data ->
                            parentActivity?.let {
                                var dialogCompleted = DialogManifestCompleted(parentActivity!!)
                                dialogCompleted.show()
                            }
                        } ?: run {
                            var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), getString(R.string.ErrorProcessManifest), object : OptionListener {
                                override fun onClicked(isApproved: Boolean) {}
                            })
                            dialogInfo.show()
                        }
                    }

                    is RequestState.Error -> {
                        dialogLoading.dismiss()
                        var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), it.message, object : OptionListener {
                            override fun onClicked(isApproved: Boolean) {}
                        })
                        dialogInfo.show()
                    }
                }
            }
        }
    }
}