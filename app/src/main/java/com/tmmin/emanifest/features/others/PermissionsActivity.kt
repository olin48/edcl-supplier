package com.tmmin.emanifest.features.others

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.tmmin.emanifest.R
import com.tmmin.emanifest.databinding.ActivityPermissionsBinding
import com.tmmin.emanifest.features.operator.main.OperatorMainActivity
import com.tmmin.emanifest.util.BaseActivity

class PermissionsActivity : BaseActivity(), View.OnClickListener {
    lateinit var binding: ActivityPermissionsBinding
    val Request_Code = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_permissions)
        setup()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (hasPermissions()) {
            goToApp()
        } else {
            Toast.makeText(this@PermissionsActivity, getString(R.string.ErrorPermission), Toast.LENGTH_LONG).show()
            finish()
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btnSubmit -> {
                if (hasPermissions()) {
                    goToApp()
                } else {
                    requestPermission()
                }
            }
        }
    }

    private fun setup() {
        binding.btnSubmit.setOnClickListener(this)
        Glide.with(this).asGif().load(R.raw.gif_permission).into(binding.ivPermission)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
            binding.layoutStorage.visibility = View.GONE
            binding.layoutCamera.visibility = View.VISIBLE
            binding.layoutBluetooth.visibility = View.VISIBLE
            binding.layoutLocation.visibility = View.VISIBLE
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            binding.layoutCamera.visibility = View.VISIBLE
            binding.layoutStorage.visibility = View.VISIBLE
            binding.layoutBluetooth.visibility = View.VISIBLE
            binding.layoutLocation.visibility = View.VISIBLE
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            binding.layoutStorage.visibility = View.GONE
            binding.layoutCamera.visibility = View.VISIBLE
            binding.layoutBluetooth.visibility = View.VISIBLE
            binding.layoutLocation.visibility = View.VISIBLE
        } else {
            binding.layoutBluetooth.visibility = View.GONE
            binding.layoutCamera.visibility = View.VISIBLE
            binding.layoutStorage.visibility = View.VISIBLE
            binding.layoutLocation.visibility = View.VISIBLE
        }
    }

    private fun goToApp() {
        val intent = Intent(this@PermissionsActivity, OperatorMainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
        finish()
    }

    private fun permissions(): Array<String>? {
        val permission: Array<String>
        permission = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            arrayOf(Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.BLUETOOTH_CONNECT, Manifest.permission.BLUETOOTH_SCAN)
        } else {
            arrayOf(Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
        }
        return permission
    }

    private fun hasPermissions(): Boolean {
        return permissions()!!.all {
            ContextCompat.checkSelfPermission(this, it) == PackageManager.PERMISSION_GRANTED
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(this@PermissionsActivity, permissions()!!, Request_Code)
    }
}