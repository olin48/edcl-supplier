package com.tmmin.emanifest.features.operator.manifest.others

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.tmmin.emanifest.R
import com.tmmin.emanifest.adapters.ManifestAdapter
import com.tmmin.emanifest.databinding.FragmentManifestOthersEoBinding
import com.tmmin.emanifest.helpers.PreferenceHelper
import com.tmmin.emanifest.models.others.ModelNavbar
import com.tmmin.emanifest.models.requests.manifest.others.RequestManifestOthers
import com.tmmin.emanifest.network.RequestState
import com.tmmin.emanifest.util.BaseFragment
import com.tmmin.emanifest.util.Constant
import com.tmmin.emanifest.viewmodels.ManifestViewModel

class ManifestOthersEOFragment : BaseFragment(), View.OnClickListener {
    private val viewModel: ManifestViewModel by viewModels()
    lateinit var binding: FragmentManifestOthersEoBinding
    lateinit var manifestAdapter: ManifestAdapter
    lateinit var manifestSearchAdapter: ManifestAdapter
    lateinit var modelNavbar: ModelNavbar
    var parentActivity: ManifestOthersActivity? = null
    var runnable: Runnable? = null
    val handler = Handler()
    var query = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_manifest_others_eo, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        setup()
    }

    override fun onPause() {
        super.onPause()
        viewModel.manifestOthersListEOResponse.removeObserver {}
        viewModel.manifestOthersListEOSearchResponse.removeObserver {}
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.manifestOthersListEOResponse.removeObserver {}
        viewModel.manifestOthersListEOSearchResponse.removeObserver {}
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ManifestOthersActivity) {
            parentActivity = context
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btnSearchClose -> {
                binding.etSearch.setText("")
            }
        }
    }

    fun setup() {
        parentActivity = activity as? ManifestOthersActivity
        sharedPref = PreferenceHelper(requireActivity())
        manifestAdapter = ManifestAdapter(requireActivity(), false)
        manifestSearchAdapter = ManifestAdapter(requireActivity(), false)
        binding.rcyManifest.adapter = manifestAdapter
        binding.rcyManifest.layoutManager = LinearLayoutManager(requireActivity())
        handleGetManifestsOthersEO()
        handleSearchManifestsOthersEO()
        getManifestOthersEO()
        binding.btnSearchClose.setOnClickListener(this)
        binding.etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                runnable?.let { handler.removeCallbacks(it) }
            }

            override fun afterTextChanged(s: Editable?) {
                binding.tvInfoManifest.visibility = View.GONE
                binding.rcyManifest.visibility = View.VISIBLE
                if (s.toString().length == 0) {
                    binding.btnSearchClose.visibility = View.GONE
                    binding.rcyManifest.adapter = manifestAdapter
                } else {
                    binding.btnSearchClose.visibility = View.VISIBLE
                    binding.rcyManifest.adapter = manifestSearchAdapter
                    runnable = Runnable {
                        searchManifestOthersEO()
                    }
                    handler.postDelayed(runnable!!, 2000)
                }
            }
        })
    }

    private fun handleGetManifestsOthersEO() {
        viewModel.manifestOthersListEOResponse.observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespManEO", "" + Gson().toJson(it.data))
                        binding.layoutLoading.stopShimmer()
                        var size = it.data?.data?.size ?: 0
                        if (size > 0) {
                            it.data?.data?.let { data -> manifestAdapter?.differ?.submitList(data.toList()) }
                            binding.rcyManifest.visibility = View.VISIBLE
                            binding.layoutLoading.visibility = View.GONE
                            binding.tvInfoManifest.visibility = View.GONE
                        } else {
                            binding.tvInfoManifest.text = "No Manifests"
                            binding.rcyManifest.visibility = View.GONE
                            binding.layoutLoading.visibility = View.GONE
                            binding.tvInfoManifest.visibility = View.VISIBLE
                        }
                    }

                    is RequestState.Error -> {
                        binding.layoutLoading.stopShimmer()
                        binding.tvInfoManifest.text = it.message
                        binding.rcyManifest.visibility = View.GONE
                        binding.layoutLoading.visibility = View.GONE
                        binding.tvInfoManifest.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    private fun handleSearchManifestsOthersEO() {
        viewModel.manifestOthersListEOSearchResponse.observe(viewLifecycleOwner) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        binding.layoutLoading.stopShimmer()
                        var size = it.data?.data?.size ?: 0
                        if (size > 0) {
                            it.data?.data?.let { data -> manifestSearchAdapter?.differ?.submitList(data.toList()) }
                            binding.rcyManifest.visibility = View.VISIBLE
                            binding.layoutLoading.visibility = View.GONE
                            binding.tvInfoManifest.visibility = View.GONE
                        } else {
                            binding.tvInfoManifest.text = "Manifest Not Found"
                            binding.rcyManifest.visibility = View.GONE
                            binding.layoutLoading.visibility = View.GONE
                            binding.tvInfoManifest.visibility = View.VISIBLE
                        }
                    }

                    is RequestState.Error -> {
                        binding.layoutLoading.stopShimmer()
                        binding.tvInfoManifest.text = it.message
                        binding.rcyManifest.visibility = View.GONE
                        binding.layoutLoading.visibility = View.GONE
                        binding.tvInfoManifest.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    private fun getManifestOthersEO() {
        binding.layoutLoading.visibility = View.VISIBLE
        binding.tvInfoManifest.visibility = View.GONE
        binding.rcyManifest.visibility = View.GONE
        binding.layoutLoading.startShimmer()
        var requestManifestOthers = RequestManifestOthers(supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE) ?: "", supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT) ?: "", subTitle = locale.language)
        viewModel.getListManifestEO(authToken, requestManifestOthers)
    }

    private fun searchManifestOthersEO() {
        query = binding.etSearch.text.toString()
        if (!query.isEmpty()) {
            binding.layoutLoading.visibility = View.VISIBLE
            binding.tvInfoManifest.visibility = View.GONE
            binding.rcyManifest.visibility = View.GONE
            binding.layoutLoading.startShimmer()
            var requestManifestOthers = RequestManifestOthers(supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE) ?: "", supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT) ?: "", keyword = query, subTitle = locale.language)
            viewModel.searchListManifestEO(authToken, requestManifestOthers)
        }
    }
}