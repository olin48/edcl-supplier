package com.tmmin.emanifest.features.operator.job.supplier

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.google.android.material.tabs.TabLayoutMediator
import com.google.gson.Gson
import com.tmmin.emanifest.R
import com.tmmin.emanifest.adapters.PagerJobSupplierAdapter
import com.tmmin.emanifest.databinding.ActivityJobSupplierBinding
import com.tmmin.emanifest.features.operator.main.OperatorMainActivity
import com.tmmin.emanifest.helpers.DataHelper
import com.tmmin.emanifest.models.requests.job.RequestJobSupplierTotal
import com.tmmin.emanifest.network.RequestState
import com.tmmin.emanifest.util.BaseActivity
import com.tmmin.emanifest.util.Constant
import com.tmmin.emanifest.viewmodels.JobViewModel

class JobSupplierActivity : BaseActivity(), View.OnClickListener {
    private val jobViewModel: JobViewModel by viewModels()
    lateinit var binding: ActivityJobSupplierBinding
    var dataList: ArrayList<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_job_supplier)
        setup()
    }

    override fun onResume() {
        super.onResume()
        val selectedItem = binding.navigation.menu.findItem(R.id.navHome)
        selectedItem?.isChecked = true
        getJobSupplierTotal()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1) {
            super.onBackPressed()
        } else {
            finish()
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.layoutToolbar.btnBack -> {
                finish()
            }
        }
    }

    private fun setup() {
        binding.layoutToolbar.tvTitle.visibility = View.VISIBLE
        binding.layoutToolbar.btnBack.visibility = View.VISIBLE
        binding.layoutToolbar.tvTitle.text = "Prepared On Supplier"
        binding.layoutToolbar.btnBack.setOnClickListener(this)
        dataList = intent.getStringArrayListExtra(DataHelper.DATA_LIST) ?: arrayListOf("ON PROGRESS", "READY TO PICKUP")
        val adapter = PagerJobSupplierAdapter(supportFragmentManager, lifecycle)
        binding.viewPager.adapter = adapter
        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position -> tab.text = dataList!![position] }.attach()
        binding.navigation.setOnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navHome -> {
                    val intent = Intent(this, OperatorMainActivity::class.java)
                    intent.putExtra(DataHelper.DATA_NAVTO_FRAGMENT, 0)
                    startActivity(intent)
                    true
                }

                R.id.navJobList -> {
                    val intent = Intent(this, OperatorMainActivity::class.java)
                    intent.putExtra(DataHelper.DATA_NAVTO_FRAGMENT, 1)
                    startActivity(intent)
                    true
                }

                R.id.navTrackOrder -> {
                    val intent = Intent(this, OperatorMainActivity::class.java)
                    intent.putExtra(DataHelper.DATA_NAVTO_FRAGMENT, 2)
                    startActivity(intent)
                    true
                }

                R.id.navJobHistory -> {
                    val intent = Intent(this, OperatorMainActivity::class.java)
                    intent.putExtra(DataHelper.DATA_NAVTO_FRAGMENT, 3)
                    startActivity(intent)
                    true
                }

                R.id.navProfile -> {
                    val intent = Intent(this, OperatorMainActivity::class.java)
                    intent.putExtra(DataHelper.DATA_NAVTO_FRAGMENT, 4)
                    startActivity(intent)
                    true
                }

                else -> false
            }
        }
    }

    private fun getJobSupplierTotal() {
        var requestJobSupplierTotal = RequestJobSupplierTotal(supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE) ?: "", supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT) ?: "", keywordOnProgress = "", keywordReadyToPickup = "", subTitle = locale.language)
        jobViewModel.getJobSupplierProgressTotal(sharedPref.getString(Constant.AUTH_TOKEN_ACCESS) ?: "", requestJobSupplierTotal).observe(this) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("RespJobSupTotal", "" + Gson().toJson(it.data))
                        it.data.data?.let {
                            val tabTitles = listOf("ON PROGRESS ", "READY TO PICKUP ")
                            val tabTotals = arrayListOf<String>()
                            var totalProgress = "(" + it.totalDeliveryOnProgress + ")"
                            var totalReady = "(" + it.totalDeliveryReadyToPickup + ")"
                            tabTotals.add(totalProgress)
                            tabTotals.add(totalReady)
                            for (i in 0 until tabTitles.size) {
                                val tabView = LayoutInflater.from(this).inflate(R.layout.layout_tab_custom, null) as LinearLayout
                                val titleTextView = tabView.findViewById<TextView>(R.id.tabText)
                                val subtitleTextView = tabView.findViewById<TextView>(R.id.tabTextTotal)
                                titleTextView.text = tabTitles[i]
                                subtitleTextView.text = tabTotals[i]
                                when (i) {
                                    0 -> {
                                        if ((it.totalDeliveryOnProgress ?: 0) > 0) {
                                            subtitleTextView.setTextColor(ContextCompat.getColor(this, R.color.ColorGreenDark))
                                        } else {
                                            subtitleTextView.setTextColor(ContextCompat.getColor(this, R.color.ColorPurple))
                                        }
                                    }

                                    1 -> {
                                        if ((it.totalDeliveryReadyToPickup ?: 0) > 0) {
                                            subtitleTextView.setTextColor(ContextCompat.getColor(this, R.color.ColorRed))
                                        } else {
                                            subtitleTextView.setTextColor(ContextCompat.getColor(this, R.color.ColorPurple))
                                        }
                                    }

                                    else -> subtitleTextView.setTextColor(ContextCompat.getColor(this, R.color.ColorPurple))
                                }
                                binding.tabLayout.getTabAt(i)?.customView = tabView
                            }
                        }
                    }

                    is RequestState.Error -> {}
                }
            }
        }
    }
}