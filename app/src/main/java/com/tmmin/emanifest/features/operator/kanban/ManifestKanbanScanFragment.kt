package com.tmmin.emanifest.features.operator.kanban

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.zxing.BarcodeFormat
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.DefaultDecoderFactory
import com.tmmin.emanifest.R
import com.tmmin.emanifest.databinding.FragmentManifestKanbanScanBinding
import com.tmmin.emanifest.dialogs.DialogKanban
import com.tmmin.emanifest.dialogs.DialogLoading
import com.tmmin.emanifest.features.operator.main.OperatorMainActivity
import com.tmmin.emanifest.helpers.DataHelper
import com.tmmin.emanifest.listeners.OptionListener
import com.tmmin.emanifest.models.others.ModelNavbar
import com.tmmin.emanifest.models.requests.kanban.cancel.RequestKanbanCancel
import com.tmmin.emanifest.models.requests.kanban.check.RequestKanbanCheck
import com.tmmin.emanifest.models.requests.kanban.create.RequestKanbanCreate
import com.tmmin.emanifest.models.requests.kanban.detail.RequestKanbanDetail
import com.tmmin.emanifest.models.requests.kanban.total.RequestKanbanTotal
import com.tmmin.emanifest.models.responses.kanban.detail.ResponseKanbanDetail
import com.tmmin.emanifest.network.RequestState
import com.tmmin.emanifest.util.BaseFragment
import com.tmmin.emanifest.util.Constant
import com.tmmin.emanifest.viewmodels.KanbanViewModel


class ManifestKanbanScanFragment : BaseFragment(), View.OnClickListener, View.OnTouchListener {
    val kanbanViewModel: KanbanViewModel by viewModels()
    private lateinit var dialogLoading: DialogLoading
    lateinit var binding: FragmentManifestKanbanScanBinding
    lateinit var modelNavbar: ModelNavbar
    lateinit var responseKanbanDetail: ResponseKanbanDetail
    private var mediaPlayer: MediaPlayer? = null
    var dataNavbar: String? = null
    var parentActivity: OperatorMainActivity? = null
    val REQUEST_CAMERA = 1
    var isScanning = false
    var flashOn = false
    var currentManifestNo = ""

    companion object {
        fun newInstance(paramNavbar: String): ManifestKanbanScanFragment {
            val fragment = ManifestKanbanScanFragment()
            val args = Bundle()
            args.putString(DataHelper.DATA_NAVBAR, paramNavbar)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            dataNavbar = it.getString(DataHelper.DATA_NAVBAR)
            modelNavbar = Gson().fromJson(dataNavbar, ModelNavbar::class.java);
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_manifest_kanban_scan, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (checkSelfPermission(requireActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            setup()
        } else {
            doRequestPermission()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OperatorMainActivity) {
            parentActivity = context
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        binding.scanner.resume()
        mediaPlayer = MediaPlayer().apply {
            setOnCompletionListener {
                it.reset()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        binding.scanner.pause()
        mediaPlayer?.release()
        mediaPlayer = null
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_CAMERA -> {
                setup()
            }
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btnSubmit -> {
                submitKanban()
            }

            binding.btnModeCamera -> {
                switchMode(false)
            }

            binding.btnModeScanner -> {
                switchMode(true)
            }

            binding.btnFlash -> {
                flashOn = !flashOn
                binding.scanner.barcodeView.setTorch(flashOn)
                if (flashOn) {
                    binding.btnFlash.setImageResource(R.drawable.ic_flash_on)
                } else {
                    binding.btnFlash.setImageResource(R.drawable.ic_flash_off)
                }
            }
        }
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        when (v) {
            binding.btnSwipe -> {
                var initialX = 0f
                when (event?.action) {
                    MotionEvent.ACTION_DOWN -> {
                        initialX = v.x
                    }

                    MotionEvent.ACTION_MOVE -> {
                        val newX = event.rawX - v.width
                        if (newX > binding.tvDescSwipe.left && newX + v.width < binding.tvDescSwipe.right) {
                            v.x = newX
                        }
                    }

                    MotionEvent.ACTION_UP -> {
                        if (event.rawX >= binding.tvDescSwipe.right) {
                            v.x = initialX
                            parentActivity?.let {
                                it.supportFragmentManager.popBackStack()
                            }
                        } else {
                            v.x = initialX
                        }
                    }
                }
            }
        }
        return true
    }

    private fun setup() {
        parentActivity = activity as? OperatorMainActivity
        parentActivity?.binding?.navigation?.visibility = View.GONE
        binding.layoutToolbar.tvTitle.visibility = View.VISIBLE
        binding.layoutToolbar.btnBack.visibility = View.GONE
        binding.layoutToolbar.tvTitle.text = "Kanban Scan"
        binding.tvRoute.text = modelNavbar.route
        binding.tvRate.text = modelNavbar.rate
        binding.tvPartner.text = modelNavbar.partner
        binding.tvPickupDate.text = modelNavbar.date
        binding.tvSKIDNumber.text = modelNavbar.skidNo
        binding.btnFlash.setOnClickListener(this)
        binding.btnSubmit.setOnClickListener(this)
        binding.btnSwipe.setOnTouchListener(this)
        binding.btnModeCamera.setOnClickListener(this)
        binding.btnModeScanner.setOnClickListener(this)
        dialogLoading = DialogLoading(requireActivity())
        initScannerView()
        initScannerExternal()
        getTotalKanban()
        if (hasLaserScanner()) {
            Glide.with(this).asGif().load(R.raw.gif_scanner).into(binding.ivScanner)
            var mode = sharedPref.getInt(Constant.SET_SCANNER) ?: 0
            if (mode == 0) {
                switchMode(false)
            } else {
                switchMode(true)
            }
        } else {
            switchMode(false)
            binding.layoutMode.visibility = View.GONE
        }
    }

    private fun doRequestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), REQUEST_CAMERA)
        } else {
            setup()
        }
    }

    private fun initScannerExternal() {
        binding.etResultScanner.isCursorVisible = false
        binding.etResultScanner.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                val imm = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(binding.etResultScanner.windowToken, 0)
            }
        }
        binding.etResultScanner.requestFocus()
        binding.etResultScanner.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                binding.etResultScanner.removeTextChangedListener(this)
                binding.etResultScanner.setText("")
                binding.etResultScanner.addTextChangedListener(this)
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                getDetailKanban(s.toString(), "")
            }
        })
    }

    private fun initScannerView() {
        val formats = listOf(BarcodeFormat.QR_CODE)
        binding.scanner.barcodeView.decoderFactory = DefaultDecoderFactory(formats)
        binding.scanner.resume()
        binding.scanner.decodeContinuous(object : BarcodeCallback {
            override fun barcodeResult(result: BarcodeResult) {
                if (!isScanning) {
                    isScanning = true
                    var kanban = result?.text ?: ""
                    getDetailKanban(kanban, "")
                }
            }

            override fun possibleResultPoints(resultPoints: List<ResultPoint>) {}
        })
    }

    private fun switchMode(isExternal: Boolean) {
        binding.etResultScanner.isEnabled = isExternal
        if (isExternal) {
            binding.layoutScanExternal.visibility = View.VISIBLE
            binding.layoutScanInternal.visibility = View.GONE
            binding.btnModeCamera.visibility = View.VISIBLE
            binding.btnModeScanner.visibility = View.GONE
            binding.scanner.pause()
            binding.etResultScanner.requestFocus()
        } else {
            binding.layoutScanExternal.visibility = View.GONE
            binding.layoutScanInternal.visibility = View.VISIBLE
            binding.btnModeCamera.visibility = View.GONE
            binding.btnModeScanner.visibility = View.VISIBLE
            binding.scanner.resume()
        }
    }

    private fun submitKanban() {
        var kanban = binding.etResult.text.toString().toUpperCase()
        if (!kanban.isEmpty()) {
            isScanning = true
            getDetailKanban("", kanban)
        }
    }

    private fun initDefaultView() {
        binding.etResult.setText("")
        binding.tvScanNumber.text = ""
        binding.tvScanDiv.text = ""
        binding.tvScanTotal.text = ""
    }

    private fun playBeepSound() {
        var sound = R.raw.scanner_beep
        mediaPlayer?.apply {
            if (isPlaying) {
                stop()
                reset()
            }
            setDataSource(requireActivity(), Uri.parse("android.resource://${requireActivity().packageName}/$sound"))
            prepare()
            start()
        }
    }

    private fun playBeepSoundError() {
        var sound = R.raw.scanner_beep_error
        mediaPlayer?.apply {
            if (isPlaying) {
                stop()
                reset()
            }
            setDataSource(requireActivity(), Uri.parse("android.resource://${requireActivity().packageName}/$sound"))
            prepare()
            start()
        }
    }

    private fun getTotalKanban() {
        isScanning = true
        initDefaultView()
        dialogLoading.show()
        Log.e("Loading1", "Show")
        var requestKanbanTotal = RequestKanbanTotal(manifestNo = modelNavbar.manifestNo ?: "")
        kanbanViewModel.getTotalKanban(authToken, requestKanbanTotal).observe(viewLifecycleOwner) {
            if (it != null) {
                Log.e("HandTotalKanban", "" + it)
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("Loading1", "Dismiss")
                        dialogLoading.dismiss()
                        isScanning = false
                        it.data?.let { data ->
                            var totalScannedKanban = data.data?.totalScannedKanban ?: 0
                            var totalKanban = data.data?.totalKanban ?: 0
                            binding.tvScanNumber.setText(totalScannedKanban.toString())
                            binding.tvScanDiv.setText("/")
                            binding.tvScanTotal.setText(totalKanban.toString())
                            binding.tvScanNumber.setTextColor(getColor(totalScannedKanban, totalKanban))
                            binding.tvScanDiv.setTextColor(getColor(totalScannedKanban, totalKanban))
                            binding.tvScanTotal.setTextColor(getColor(totalScannedKanban, totalKanban))
                        } ?: run {
                            isScanning = false
                            binding.tvScanNumber.setText("")
                            binding.tvScanDiv.setText("")
                            binding.tvScanTotal.setText("")
                        }
                    }

                    is RequestState.Error -> {
                        Log.e("Loading1Err", "Dismiss")
                        dialogLoading.dismiss()
                        isScanning = false
                    }
                }
            }
        }
    }

    private fun getDetailKanban(kanbanId: String, kanbanCd: String) {
        dialogLoading.show()
        Log.e("Loading2", "Show")
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            var requestKanbanDetail: RequestKanbanDetail
            if (kanbanCd.isEmpty()) {
                requestKanbanDetail = RequestKanbanDetail(kanbanId = kanbanId, kanbanCd = "", supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE), supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT), subTitle = locale.language)
            } else {
                requestKanbanDetail = RequestKanbanDetail(kanbanId = "", kanbanCd = kanbanCd, supplierCd = sharedPref.getString(Constant.PROFILE_SUPPLIER_CODE), supplierPlant = sharedPref.getString(Constant.PROFILE_SUPPLIER_PLANT), subTitle = locale.language)
            }
            kanbanViewModel.getDetailKanban(authToken, requestKanbanDetail).observe(viewLifecycleOwner) {
                if (it != null) {
                    Log.e("HandDetailKanban", "" + it)
                    when (it) {
                        is RequestState.Loading -> {}

                        is RequestState.Success -> {
                            it.data?.let { data ->
                                responseKanbanDetail = data
                                currentManifestNo = responseKanbanDetail.data?.manifestNo ?: ""
                                postCheckKanban()
                            }
                        }

                        is RequestState.Error -> {
                            Log.e("Loading2Err", "Dismiss")
                            dialogLoading.dismiss()
                            playBeepSoundError()
                            var dialogKanban = DialogKanban(requireActivity(), DialogKanban.MODE_BASIC, it.message ?: getString(R.string.MessageError), object : OptionListener {
                                override fun onClicked(isApproved: Boolean) {
                                    binding.etResult.setText("")
                                    binding.etResult.clearFocus()
                                    isScanning = false
                                }
                            })
                            dialogKanban.show()
                        }
                    }
                }
            }
        }, 2000)
    }

    private fun postCheckKanban() {
        var data = responseKanbanDetail.data
        var processId = (sharedPref.getString(modelNavbar.manifestNo ?: "0") ?: "0").toLong()
        var requestKanbanCheck = RequestKanbanCheck(manifestNo = currentManifestNo, skidNo = modelNavbar.skidNo, itemNo = data?.itemNo ?: 0, seqNo = data?.seqNo ?: 0, qty = data?.totalQuantityPlan ?: 0, processId = processId, subTitle = locale.language)
        kanbanViewModel.getCheckKanban(authToken, requestKanbanCheck).observe(viewLifecycleOwner) {
            if (it != null) {
                Log.e("HandCheckKanban", "" + it)
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("Loading2.2", "Dismiss")
                        dialogLoading.dismiss()
                        it.data?.let { data ->
                            var message = (data.data?.messageType ?: "")
                            when {
                                message.equals("I", ignoreCase = true) -> {
                                    playBeepSound()
                                    postAddKanban()
                                }

                                message.equals("C", ignoreCase = true) -> {
                                    playBeepSound()
                                    var dialogKanban = DialogKanban(requireActivity(), DialogKanban.MODE_MULTIPLE, it.data?.data?.message ?: getString(R.string.ErrorServer), object : OptionListener {
                                        override fun onClicked(isApproved: Boolean) {
                                            if (isApproved == true) {
                                                postCancelKanban()
                                            } else {
                                                binding.etResult.setText("")
                                                binding.etResult.clearFocus()
                                                isScanning = false
                                            }
                                        }
                                    })
                                    dialogKanban.show()
                                }

                                message.equals("E", ignoreCase = true) -> {
                                    playBeepSoundError()
                                    var dialogKanban = DialogKanban(requireActivity(), DialogKanban.MODE_BASIC, it.data?.data?.message ?: getString(R.string.ErrorServer), object : OptionListener {
                                        override fun onClicked(isApproved: Boolean) {
                                            binding.etResult.setText("")
                                            binding.etResult.clearFocus()
                                            isScanning = false
                                        }
                                    })
                                    dialogKanban.show()
                                }

                                else -> {
                                    playBeepSoundError()
                                    var dialogKanban = DialogKanban(requireActivity(), DialogKanban.MODE_BASIC, it.data?.data?.message ?: getString(R.string.ErrorServer), object : OptionListener {
                                        override fun onClicked(isApproved: Boolean) {
                                            binding.etResult.setText("")
                                            binding.etResult.clearFocus()
                                            isScanning = false
                                        }
                                    })
                                    dialogKanban.show()
                                }
                            }
                        } ?: run {
                            var dialogKanban = DialogKanban(requireActivity(), DialogKanban.MODE_BASIC, getString(R.string.ErrorKanban), object : OptionListener {
                                override fun onClicked(isApproved: Boolean) {
                                    binding.etResult.setText("")
                                    binding.etResult.clearFocus()
                                    isScanning = false
                                }
                            })
                            dialogKanban.show()
                        }
                    }

                    is RequestState.Error -> {
                        Log.e("Loading2.2Err", "Dismiss")
                        dialogLoading.dismiss()
                        var dialogKanban = DialogKanban(requireActivity(), DialogKanban.MODE_BASIC, it.message ?: "", object : OptionListener {
                            override fun onClicked(isApproved: Boolean) {
                                binding.etResult.setText("")
                                binding.etResult.clearFocus()
                                isScanning = false
                            }
                        })
                        dialogKanban.show()
                    }
                }
            }
        }
    }

    private fun postAddKanban() {
        Log.e("Loading3", "Show")
        dialogLoading.show()
        var data = responseKanbanDetail.data
        var processId = (sharedPref.getString(modelNavbar.manifestNo ?: "0") ?: "0").toLong()
        var requestKanbanCreate = RequestKanbanCreate(manifestNo = currentManifestNo, skidNo = modelNavbar.skidNo, itemNo = (data?.itemNo ?: 0).toString(), seqNo = (data?.seqNo ?: 0).toString(), qty = data?.totalQuantityPlan ?: 0, processId = processId, subTitle = locale.language)
        kanbanViewModel.createKanban(authToken, requestKanbanCreate).observe(viewLifecycleOwner) {
            if (it != null) {
                Log.e("HandAddKanban", "" + it)
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("Loading3", "Dismiss")
                        dialogLoading.dismiss()
                        it.data?.let { data ->
                            binding.etResult.setText("")
                            binding.etResult.clearFocus()
                            getTotalKanban()
                        } ?: run {
                            dialogLoading.dismiss()
                            var dialogKanban = DialogKanban(requireActivity(), DialogKanban.MODE_BASIC, getString(R.string.ErrorAddKanban), object : OptionListener {
                                override fun onClicked(isApproved: Boolean) {
                                    binding.etResult.setText("")
                                    binding.etResult.clearFocus()
                                    isScanning = false
                                }
                            })
                            dialogKanban.show()
                        }
                    }

                    is RequestState.Error -> {
                        Log.e("Loading3Err", "Dismiss")
                        dialogLoading.dismiss()
                        var dialogKanban = DialogKanban(requireActivity(), DialogKanban.MODE_BASIC, it.message ?: getString(R.string.MessageError), object : OptionListener {
                            override fun onClicked(isApproved: Boolean) {
                                binding.etResult.setText("")
                                binding.etResult.clearFocus()
                                isScanning = false
                            }
                        })
                        dialogKanban.show()
                    }
                }
            }
        }
    }

    private fun postCancelKanban() {
        Log.e("Loading4", "Show")
        dialogLoading.show()
        var data = responseKanbanDetail.data
        var processId = sharedPref.getString(modelNavbar.manifestNo ?: "0") ?: "0"
        var requestKanbanCancel = RequestKanbanCancel(manifestNo = currentManifestNo, skidNo = modelNavbar.skidNo, itemNo = (data?.itemNo ?: 0).toString(), seqNo = (data?.seqNo ?: 0).toString(), processId = processId, subTitle = locale.language)
        kanbanViewModel.postCancelKanban(authToken, requestKanbanCancel).observe(viewLifecycleOwner) {
            if (it != null) {
                Log.e("HandCancelKanban", "" + it)
                when (it) {
                    is RequestState.Loading -> {}

                    is RequestState.Success -> {
                        Log.e("Loading4", "Dismiss")
                        dialogLoading.dismiss()
                        it.data?.let { data ->
                            binding.etResult.setText("")
                            binding.etResult.clearFocus()
                            getTotalKanban()
                        }
                    }

                    is RequestState.Error -> {
                        Log.e("Loading4Err", "Dismiss")
                        dialogLoading.dismiss()
                        binding.etResult.setText("")
                        binding.etResult.clearFocus()
                        isScanning = false
                    }
                }
            }
        }
    }

    private fun getColor(kanbanActual: Int, kanbanPlant: Int): Int {
        if (kanbanActual == 0) {
            return ContextCompat.getColor(requireActivity(), R.color.ColorRed)
        } else if (kanbanActual == kanbanPlant) {
            return ContextCompat.getColor(requireActivity(), R.color.ColorGreenDark)
        } else if (kanbanActual != kanbanPlant) {
            return ContextCompat.getColor(requireActivity(), R.color.ColorYellow)
        } else {
            return ContextCompat.getColor(requireActivity(), R.color.ColorBlackSecond)
        }
    }
}