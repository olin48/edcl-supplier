package com.tmmin.emanifest.features.operator.manifest.others

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.google.android.material.tabs.TabLayoutMediator
import com.tmmin.emanifest.R
import com.tmmin.emanifest.adapters.PagerManifestAdapter
import com.tmmin.emanifest.databinding.ActivityOthersManifestBinding
import com.tmmin.emanifest.features.operator.main.OperatorMainActivity
import com.tmmin.emanifest.helpers.DataHelper

class ManifestOthersActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var binding: ActivityOthersManifestBinding
    var dataList: ArrayList<String>? = null
    var dataType: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_others_manifest)
        setup()
    }

    override fun onResume() {
        super.onResume()
        val selectedItem = binding.navigation.menu.findItem(R.id.navHome)
        selectedItem?.isChecked = true
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1) {
            super.onBackPressed()
        } else {
            finish()
        }
    }

    private fun setup() {
        binding.layoutToolbar.tvTitle.visibility = View.VISIBLE
        binding.layoutToolbar.btnBack.visibility = View.VISIBLE
        binding.layoutToolbar.tvTitle.text = "Others Manifest"
        binding.layoutToolbar.btnBack.setOnClickListener(this)
        dataList = intent.getStringArrayListExtra(DataHelper.DATA_LIST) ?: arrayListOf("EO", "Shortage", "History")
        dataType = intent.getIntExtra(DataHelper.DATA_TYPE, 0)
        val adapter = PagerManifestAdapter(supportFragmentManager, lifecycle)
        binding.viewPager.adapter = adapter
        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position -> tab.text = dataList!![position] }.attach()
        when (dataType) {
            1 -> adapter.createFragment(1)
            2 -> adapter.createFragment(2)
            else -> adapter.createFragment(0)
        }
        binding.navigation.setOnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navHome -> {
                    val intent = Intent(this, OperatorMainActivity::class.java)
                    intent.putExtra(DataHelper.DATA_NAVTO_FRAGMENT, 0)
                    startActivity(intent)
                    true
                }

                R.id.navJobList -> {
                    val intent = Intent(this, OperatorMainActivity::class.java)
                    intent.putExtra(DataHelper.DATA_NAVTO_FRAGMENT, 1)
                    startActivity(intent)
                    true
                }

                R.id.navTrackOrder -> {
                    val intent = Intent(this, OperatorMainActivity::class.java)
                    intent.putExtra(DataHelper.DATA_NAVTO_FRAGMENT, 2)
                    startActivity(intent)
                    true
                }

                R.id.navJobHistory -> {
                    val intent = Intent(this, OperatorMainActivity::class.java)
                    intent.putExtra(DataHelper.DATA_NAVTO_FRAGMENT, 3)
                    startActivity(intent)
                    true
                }

                R.id.navProfile -> {
                    val intent = Intent(this, OperatorMainActivity::class.java)
                    intent.putExtra(DataHelper.DATA_NAVTO_FRAGMENT, 4)
                    startActivity(intent)
                    true
                }

                else -> false
            }
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.layoutToolbar.btnBack -> {
                finish()
            }
        }
    }
}