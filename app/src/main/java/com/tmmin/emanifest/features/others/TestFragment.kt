package com.tmmin.emanifest.features.others

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.tscdll.TSCActivity
import com.google.zxing.BarcodeFormat
import com.google.zxing.WriterException
import com.google.zxing.qrcode.QRCodeWriter
import com.mazenrashed.printooth.Printooth.getPairedPrinter
import com.mazenrashed.printooth.Printooth.hasPairedPrinter
import com.mazenrashed.printooth.Printooth.printer
import com.mazenrashed.printooth.Printooth.removeCurrentPrinter
import com.mazenrashed.printooth.ui.ScanningActivity
import com.mazenrashed.printooth.utilities.Printing
import com.mazenrashed.printooth.utilities.PrintingCallback
import com.tmmin.emanifest.R
import com.tmmin.emanifest.databinding.FragmentTestBinding
import com.tmmin.emanifest.dialogs.DialogInfo
import com.tmmin.emanifest.dialogs.DialogSlip
import com.tmmin.emanifest.features.operator.main.OperatorMainActivity
import com.tmmin.emanifest.listeners.OptionListener
import com.tmmin.emanifest.listeners.SlipPreviewListener
import com.tmmin.emanifest.models.others.ModelSlip
import com.tmmin.emanifest.util.BaseFragment
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class TestFragment : BaseFragment(), View.OnClickListener {
    private lateinit var binding: FragmentTestBinding
    var parentActivity: OperatorMainActivity? = null
    var tscActivity = TSCActivity()
    var printing: Printing? = null
    val message = "Hello World"
    var qrCodeBitmap: Bitmap? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_test, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ScanningActivity.SCANNING_FOR_PRINTER && resultCode == Activity.RESULT_OK) {
            initDevices()
            initPrinterListener()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OperatorMainActivity) {
            parentActivity = context
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btnBrowse -> {
                if (hasPairedPrinter()) {
                    removeCurrentPrinter()
                } else {
                    startActivityForResult(Intent(requireActivity(), ScanningActivity::class.java), ScanningActivity.SCANNING_FOR_PRINTER)
                    initDevices()
                }
            }

            binding.btnPrint -> {
                slipPreview()
            }
        }
    }

    private fun setup() {
        binding.btnBrowse.setOnClickListener(this)
        binding.btnPrint.setOnClickListener(this)
        qrCodeBitmap = generateQRCode(message, 500, 500)
        if (hasPairedPrinter()) {
            printing = printer()
        }
        initDevices()
        initPrinterListener()
    }

    private fun initDevices() {
        if (getPairedPrinter() == null) {
            binding.etAddress.setText("No Printer Selected")
//            binding.btnPrint.isEnabled = false
        } else {
            if (hasPairedPrinter()) {
                binding.etAddress.setText(getPairedPrinter()?.name ?: "-")
//                binding.btnPrint.isEnabled = true
            } else {
                binding.etAddress.setText("No Printer Selected")
//                binding.btnPrint.isEnabled = false
            }
        }
    }

    private fun initPrinterListener() {
        if (hasPairedPrinter()) {
            printer().printingCallback = object : PrintingCallback {
                override fun connectingWithPrinter() {}
                override fun printingOrderSentSuccessfully() {}
                override fun connectionFailed(error: String) {}
                override fun disconnected() {}
                override fun onError(error: String) {}
                override fun onMessage(message: String) {}
            }
        }
    }

    private fun slipPreview() {
        var modelSlip = ModelSlip(supCodePlant = "5011 - 1", dockCode = "1N", suppName = "AISIN INDONESIA", suppDockCode = "Y2P", orderNumber = "2024070101", manifestNumber = "1200123456", routeCycle = "RS23-01", planeNumber = "04", conveyance = "D", kanbanCode = "k0A12b", kanbanId = "Hello World")
        var dialogSlip = DialogSlip(requireActivity(), modelSlip, object : SlipPreviewListener {
            override fun onCanceled() {
                parentActivity?.let {
                    it.supportFragmentManager.popBackStack()
                }
            }

            override fun onConfirmed(bitmap: Bitmap) {
                val currentTimeSeconds = System.currentTimeMillis() / 1000
                val fileName = modelSlip.orderNumber + "_" + currentTimeSeconds
                var file = saveBitmapToFile(bitmap, fileName)
                if (file == null) {
                    var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_FAILED, getString(R.string.MessageError), getString(R.string.ErrorPrintSlip), object : OptionListener {
                        override fun onClicked(isApproved: Boolean) {}
                    })
                    dialogInfo.show()
                } else {
//                    printImage(file)
                    var dialogInfo = DialogInfo(requireActivity(), DialogInfo.MODE_SUCCESS, getString(R.string.MessageSuccess), getString(R.string.InfoPrintSlip), object : OptionListener {
                        override fun onClicked(isApproved: Boolean) {
                            file.delete()
                            parentActivity?.let {
                                it.supportFragmentManager.popBackStack()
                            }
                        }
                    })
                    dialogInfo.show()
                }
            }
        })
        dialogSlip.show()
    }

    private fun slipDownload() {
        printer()
    }

    private fun print() {
        try {
            tscActivity.openport("printerName");
            tscActivity.setup(80, 51, 4, 15, 0, 3, 0);
            tscActivity.clearbuffer();

            tscActivity.sendcommand("PUTBMP 19,15,\"logo-bmp.BMP\"");
            tscActivity.printlabel(1, 1);
            tscActivity.closeport()

            tscActivity.openport(getPairedPrinter()?.address ?: "")
            tscActivity.sendcommand("SIZE 75 mm, 50 mm\r\n")
            tscActivity.sendcommand("SPEED 4\r\n")
            tscActivity.sendcommand("DENSITY 12\r\n")
            tscActivity.sendcommand("CODEPAGE UTF-8\r\n")
            tscActivity.sendcommand("SET TEAR ON\r\n")
            tscActivity.sendcommand("SET COUNTER @1 1\r\n")
            tscActivity.sendcommand("@1 = \"0001\"\r\n")
            tscActivity.clearbuffer()
            tscActivity.sendcommand("TEXT 100,300,\"ROMAN.TTF\",0,12,12,@1\r\n")
            tscActivity.sendcommand("TEXT 100,400,\"ROMAN.TTF\",0,12,12,\"TEST FONT\"\r\n")
            tscActivity.sendbitmap(100, 100, qrCodeBitmap)
            tscActivity.barcode(100, 100, "128", 100, 1, 0, 3, 3, "123456789")
            tscActivity.printerfont(100, 250, "3", 0, 1, 1, "987654321")
            tscActivity.printlabel(1, 1)
            tscActivity.closeport(5000)
        } catch (e: Exception) {
            Log.e("ErrPrint", "" + e.message)
        }
    }

    private fun printImage(file: File) {
        tscActivity.openport(getPairedPrinter()?.address ?: "")
        tscActivity.setup(100, 60, 4, 15, 0, 3, 0)
        tscActivity.clearbuffer()
        tscActivity.sendpicture(200, 200, file.absolutePath)
        tscActivity.printlabel(1, 1)
        tscActivity.closeport()
    }

    fun generateQRCode(text: String, width: Int, height: Int): Bitmap? {
        val writer = QRCodeWriter()
        return try {
            val bitMatrix = writer.encode(text, BarcodeFormat.QR_CODE, width, height)
            val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
            for (x in 0 until width) {
                for (y in 0 until height) {
                    bitmap.setPixel(x, y, if (bitMatrix[x, y]) android.graphics.Color.BLACK else android.graphics.Color.TRANSPARENT)
                }
            }
            bitmap
        } catch (e: WriterException) {
            e.printStackTrace()
            null
        }
    }

    private fun saveBitmapToFile(bitmap: Bitmap, fileName: String): File? {
        val directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
        if (!directory.exists()) {
            directory.mkdirs()
        }
        val file = File(directory, "$fileName.png")
        var fos: FileOutputStream? = null
        try {
            fos = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos)
            fos.flush()
            return file
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            fos?.close()
        }
        return null
    }
}