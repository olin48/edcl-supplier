package com.tmmin.emanifest.repositories

import com.tmmin.emanifest.models.requests.assign.RequestAssign
import com.tmmin.emanifest.models.requests.assign.RequestAssignCombo
import com.tmmin.emanifest.models.requests.job.RequestJobList
import com.tmmin.emanifest.models.requests.job.RequestJobSupplierTotal
import com.tmmin.emanifest.models.requests.job.driver.RequestDriverPickup
import com.tmmin.emanifest.network.ApiConfig

class JobRepository {
    private val client = ApiConfig.getJobService()

    suspend fun getJobHistory(authorization: String, requestJobList: RequestJobList) = client.getJobHistory(
        authorization,
        requestJobList.supplierCd,
        requestJobList.supplierPlant,
        requestJobList.pickupDtFrom,
        requestJobList.pickupDtTo,
        requestJobList.sortBy,
        requestJobList.keyword,
        requestJobList.pageNumber,
        requestJobList.pageSize,
        requestJobList.subTitle,
    )

    suspend fun getJobList(authorization: String, requestJobList: RequestJobList) = client.getJobList(
        authorization,
        requestJobList.supplierCd,
        requestJobList.supplierPlant,
        requestJobList.pickupDtFrom,
        requestJobList.pickupDtTo,
        requestJobList.sortBy,
        requestJobList.keyword,
        requestJobList.pageNumber,
        requestJobList.pageSize,
        requestJobList.subTitle,
    )

    suspend fun getJobDirectPreparation(authorization: String, requestJobList: RequestJobList) = client.getJobDirectPreparation(
        authorization,
        requestJobList.supplierCd,
        requestJobList.supplierPlant,
        requestJobList.pickupDtFrom,
        requestJobList.pickupDtTo,
        requestJobList.sortBy,
        requestJobList.keyword,
        requestJobList.pageNumber,
        requestJobList.pageSize,
        requestJobList.subTitle,
    )

    suspend fun getJobSupplierProgressList(authorization: String, requestJobList: RequestJobList) = client.getJobSupplierProgressList(
        authorization,
        requestJobList.supplierCd,
        requestJobList.supplierPlant,
        requestJobList.pickupDtFrom,
        requestJobList.pickupDtTo,
        requestJobList.sortBy,
        requestJobList.keyword,
        requestJobList.deliveryType,
        requestJobList.pageNumber,
        requestJobList.pageSize,
        requestJobList.subTitle,
    )

    suspend fun getJobSupplierReadyList(authorization: String, requestJobList: RequestJobList) = client.getJobSupplierReadyList(
        authorization,
        requestJobList.supplierCd,
        requestJobList.supplierPlant,
        requestJobList.pickupDtFrom,
        requestJobList.pickupDtTo,
        requestJobList.sortBy,
        requestJobList.keyword,
        requestJobList.deliveryType,
        requestJobList.pageNumber,
        requestJobList.pageSize,
        requestJobList.subTitle,
    )

    suspend fun getJobDeliverMilkrunList(authorization: String, requestJobList: RequestJobList) = client.getJobDeliverMilkrunList(
        authorization,
        requestJobList.supplierCd,
        requestJobList.supplierPlant,
        requestJobList.pickupDtFrom,
        requestJobList.pickupDtTo,
        requestJobList.sortBy,
        requestJobList.keyword,
        requestJobList.pageNumber,
        requestJobList.pageSize,
        requestJobList.subTitle,
    )

    suspend fun getJobDeliverDirectList(authorization: String, requestJobList: RequestJobList) = client.getJobDeliverDirectList(
        authorization,
        requestJobList.supplierCd,
        requestJobList.supplierPlant,
        requestJobList.pickupDtFrom,
        requestJobList.pickupDtTo,
        requestJobList.sortBy,
        requestJobList.keyword,
        requestJobList.pageNumber,
        requestJobList.pageSize,
        requestJobList.subTitle,
    )

    suspend fun getJobDriverDeliverDirectList(authorization: String, requestJobList: RequestJobList) = client.getJobDriverDeliverDirectList(
        authorization,
        requestJobList.supplierCd,
        requestJobList.supplierPlant,
        requestJobList.pickupDtFrom,
        requestJobList.pickupDtTo,
        requestJobList.sortBy,
        requestJobList.keyword,
        requestJobList.pageNumber,
        requestJobList.pageSize,
        requestJobList.subTitle,
    )

    suspend fun getJobSupplierProgressTotal(authorization: String, requestJobSupplierTotal: RequestJobSupplierTotal) = client.getJobSupplierProgressTotal(authorization, requestJobSupplierTotal.supplierCd, requestJobSupplierTotal.supplierPlant, requestJobSupplierTotal.keywordOnProgress, requestJobSupplierTotal.keywordReadyToPickup, requestJobSupplierTotal.subTitle)

    suspend fun getJobCombo(authorization: String, subtitle: String) = client.getJobCombo(authorization, subtitle)

    suspend fun getAssignCombo(authorization: String, requestAssignCombo: RequestAssignCombo) = client.getComboAssignJob(
        authorization,
        requestAssignCombo.supplierCd ?: "",
        requestAssignCombo.supplierPlant ?: "",
        requestAssignCombo.subTitle ?: "",
    )

    suspend fun getAssignedDriver(authorization: String, requestAssign: RequestAssign) = client.getAssignedDriver(authorization, requestAssign.deliveryNo ?: "", requestAssign.supplierCd ?: "", requestAssign.supplierPlant ?: "", requestAssign.subTitle ?: "")

    suspend fun getAssignCheck(authorization: String, requestAssign: RequestAssign) = client.getCheckAssignJob(authorization, requestAssign.deliveryNo ?: "", requestAssign.supplierCd ?: "", requestAssign.supplierPlant ?: "", requestAssign.driverUsername ?: "", requestAssign.subTitle ?: "")

    suspend fun postAssign(authorization: String, requestAssign: RequestAssign) = client.postAssignJob(authorization, requestAssign)

    suspend fun getDriverJobCheck(authorization: String, requestDriverPickup: RequestDriverPickup) = client.getDriverJobCheck(authorization, requestDriverPickup.deliveryNo ?: "", requestDriverPickup.directSupplierSts ?: "", requestDriverPickup.directSupplierNote ?: "",requestDriverPickup.supplierCd ?: "", requestDriverPickup.supplierPlant ?: "", requestDriverPickup.subTitle ?: "")

    suspend fun postDriverJobConfirm(authorization: String, requestDriverPickup: RequestDriverPickup) = client.postDriverJobConfirm(authorization, requestDriverPickup)
}