package com.tmmin.emanifest.repositories

import com.tmmin.emanifest.models.requests.order.RequestOrderCombo
import com.tmmin.emanifest.models.requests.order.RequestOrderRoute
import com.tmmin.emanifest.models.requests.order.RequestOrderStatus
import com.tmmin.emanifest.network.ApiConfig

class OrderRepository {
    private val client = ApiConfig.getOrderService()

    suspend fun getOrderCombo(authorization: String, requestOrderCombo: RequestOrderCombo) = client.getOrderCombo(
        authorization,
        requestOrderCombo.supplierCd,
        requestOrderCombo.supplierPlant,
        requestOrderCombo.subTitle,
    )

    suspend fun getOrderStatus(authorization: String, requestOrderStatus: RequestOrderStatus) = client.getOrderStatus(
        authorization,
        requestOrderStatus.deliveryNo,
        requestOrderStatus.subTitle,
    )

    suspend fun getOrderRoute(authorization: String, requestOrderRoute: RequestOrderRoute) = client.getOrderRoute(
        authorization,
        requestOrderRoute.deliveryNo
    )

    suspend fun getOrderLastPosition(authorization: String, requestOrderRoute: RequestOrderRoute) = client.getOrderLastPosition(
        authorization,
        requestOrderRoute.deliveryNo,
        requestOrderRoute.subTitle
    )
}