package com.tmmin.emanifest.repositories

import com.tmmin.emanifest.models.requests.settings.RequestSettingPlant
import com.tmmin.emanifest.network.ApiConfig

class SettingRepository {
    private val client = ApiConfig.getSettingsService()
    suspend fun getSettingPlant(auth: String, requestSettingPlant: RequestSettingPlant) = client.getSupplierPlant(auth, requestSettingPlant.supplierCd, requestSettingPlant.subTitle)
}