package com.tmmin.emanifest.repositories

import com.tmmin.emanifest.models.requests.manifest.RequestManifestDetail
import com.tmmin.emanifest.models.requests.manifest.RequestManifestEnd
import com.tmmin.emanifest.models.requests.manifest.RequestManifestEndCheck
import com.tmmin.emanifest.models.requests.manifest.RequestManifestList
import com.tmmin.emanifest.models.requests.manifest.RequestManifestStart
import com.tmmin.emanifest.models.requests.manifest.completeness.RequestManifestCompletenessItem
import com.tmmin.emanifest.models.requests.manifest.others.RequestManifestOthers
import com.tmmin.emanifest.models.requests.assign.RequestAssign
import com.tmmin.emanifest.models.requests.assign.RequestAssignCombo
import com.tmmin.emanifest.network.ApiConfig

class ManifestRepository {
    private val client = ApiConfig.getManifestService()
    suspend fun getListManifest(authorization: String, reqManifestList: RequestManifestList) = client.getListManifest(
        authorization,
        reqManifestList.deliveryNo ?: "",
        reqManifestList.supplierCd ?: "",
        reqManifestList.supplierPlant ?: "",
        reqManifestList.pageNumber,
        reqManifestList.pageSize,
        reqManifestList.subTitle ?: "",
    )

    suspend fun getDetailManifest(authorization: String, reqManifestDetail: RequestManifestDetail) = client.getDetailManifest(
        authorization,
        reqManifestDetail.manifestNo ?: "",
        reqManifestDetail.subTitle ?: "",
    )

    suspend fun postManifestPreparation(authorization: String, requestManifestStart: RequestManifestStart) = client.postManifestPreparation(authorization, requestManifestStart)

    suspend fun getCheckManifest(authorization: String, reqManifestEndCheck: RequestManifestEndCheck) = client.getCheckManifest(
        authorization,
        reqManifestEndCheck.manifestNo ?: "",
        reqManifestEndCheck.processId ?: "",
        reqManifestEndCheck.subTitle ?: "",
    )

    suspend fun postEndManifest(authorization: String, requestManifestEnd: RequestManifestEnd) = client.postEndManifest(authorization, requestManifestEnd)

    suspend fun postManifestCompletenessAdd(authorization: String, requestManifestCompleteness: List<RequestManifestCompletenessItem>) = client.postManifestCompletenessAdd(authorization, requestManifestCompleteness)

    suspend fun postManifestCompletenessCancel(authorization: String, requestManifestCompleteness: List<RequestManifestCompletenessItem>) = client.postManifestCompletenessCancel(authorization, requestManifestCompleteness)

    suspend fun getTotalManifestOthers(authorization: String, requestManifestOthers: RequestManifestOthers) = client.getTotalManifestOthers(
        authorization,
        requestManifestOthers.supplierCd ?: "",
        requestManifestOthers.supplierPlant ?: "",
        requestManifestOthers.subTitle ?: "",
    )

    suspend fun getListManifestEO(authorization: String, requestManifestOthers: RequestManifestOthers) = client.getListManifestEO(
        authorization,
        requestManifestOthers.supplierCd ?: "",
        requestManifestOthers.supplierPlant ?: "",
        requestManifestOthers.keyword ?: "",
        requestManifestOthers.subTitle ?: "",
    )

    suspend fun searchListManifestEO(authorization: String, requestManifestOthers: RequestManifestOthers) = client.getListManifestEO(
        authorization,
        requestManifestOthers.supplierCd ?: "",
        requestManifestOthers.supplierPlant ?: "",
        requestManifestOthers.keyword ?: "",
        requestManifestOthers.subTitle ?: "",
    )

    suspend fun getListManifestShortage(authorization: String, requestManifestOthers: RequestManifestOthers) = client.getListManifestShortage(
        authorization,
        requestManifestOthers.supplierCd ?: "",
        requestManifestOthers.supplierPlant ?: "",
        requestManifestOthers.keyword ?: "",
        requestManifestOthers.subTitle ?: "",
    )

    suspend fun searchListManifestShortage(authorization: String, requestManifestOthers: RequestManifestOthers) = client.getListManifestShortage(
        authorization,
        requestManifestOthers.supplierCd ?: "",
        requestManifestOthers.supplierPlant ?: "",
        requestManifestOthers.keyword ?: "",
        requestManifestOthers.subTitle ?: "",
    )

    suspend fun getListManifestHistory(authorization: String, requestManifestOthers: RequestManifestOthers) = client.getListManifestHistory(
        authorization,
        requestManifestOthers.supplierCd ?: "",
        requestManifestOthers.supplierPlant ?: "",
        requestManifestOthers.sortBy ?: "",
        requestManifestOthers.keyword ?: "",
        requestManifestOthers.subTitle ?: "",
    )

    suspend fun searchListManifestHistory(authorization: String, requestManifestOthers: RequestManifestOthers) = client.getListManifestHistory(
        authorization,
        requestManifestOthers.supplierCd ?: "",
        requestManifestOthers.supplierPlant ?: "",
        requestManifestOthers.sortBy ?: "",
        requestManifestOthers.keyword ?: "",
        requestManifestOthers.subTitle ?: "",
    )

    suspend fun getComboManifestOthers(authorization: String, requestAssignCombo: RequestAssignCombo) = client.getComboManifestOthers(
        authorization,
        requestAssignCombo.supplierCd ?: "",
        requestAssignCombo.supplierPlant ?: "",
        requestAssignCombo.subTitle ?: "",
    )

    suspend fun getCheckManifestAssign(authorization: String, requestAssign: RequestAssign) = client.getCheckManifestAssign(authorization, requestAssign.deliveryNo ?: "", requestAssign.manifestNo ?: "", requestAssign.subTitle ?: "")

    suspend fun postManifestAssign(authorization: String, requestAssign: RequestAssign) = client.postManifestAssign(authorization, requestAssign)
}