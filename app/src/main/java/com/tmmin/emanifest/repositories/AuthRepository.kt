package com.tmmin.emanifest.repositories

import com.tmmin.emanifest.models.requests.auth.RequestLogin
import com.tmmin.emanifest.models.requests.auth.RequestLogout
import com.tmmin.emanifest.models.requests.auth.RequestRefreshToken
import com.tmmin.emanifest.models.requests.auth.RequestSupplierInfo
import com.tmmin.emanifest.network.ApiConfig

class AuthRepository {
    private val client = ApiConfig.getAuthService()
    suspend fun loginSupplier(requestLogin: RequestLogin) = client.loginSupplier(requestLogin)
    suspend fun getSupplierInfo(auth: String, requestSupplierInfo: RequestSupplierInfo) = client.getSupplierInfo(auth, requestSupplierInfo.supplierCd, requestSupplierInfo.supplierPlant, requestSupplierInfo.subTitle)
    suspend fun logout(requestLogout: RequestLogout) = client.logout(requestLogout)
    suspend fun refreshToken(auth:String, requestRefreshToken: RequestRefreshToken) = client.refreshToken(auth, requestRefreshToken)
    suspend fun getSettingDockCode(auth: String, subTitle: String) = client.getSettingDockCode(auth, subTitle)
}