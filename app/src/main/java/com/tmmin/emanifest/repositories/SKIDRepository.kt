package com.tmmin.emanifest.repositories

import com.tmmin.emanifest.models.requests.skid.RequestSKIDCreate
import com.tmmin.emanifest.models.requests.skid.RequestSkidDetail
import com.tmmin.emanifest.models.requests.skid.RequestSKIDList
import com.tmmin.emanifest.network.ApiConfig

class SKIDRepository {
    private val client = ApiConfig.getSKIDService()
    suspend fun getListSKID(authorization: String, requestListSKID: RequestSKIDList) = client.getListSKID(
        authorization,
        requestListSKID.manifestNo ?: "",
        requestListSKID.subTitle ?: "")

    suspend fun createSKID(authorization: String, requestSKIDCreate: RequestSKIDCreate) = client.createSKID(authorization, requestSKIDCreate)

    suspend fun getDetailSKID(authorization: String, requestSkidDetail: RequestSkidDetail) = client.getDetailSKID(
        authorization,
        requestSkidDetail.skidNo ?: "",
        requestSkidDetail.subTitle ?: "")
}