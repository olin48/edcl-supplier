package com.tmmin.emanifest.repositories

import com.tmmin.emanifest.models.requests.part.RequestPartList
import com.tmmin.emanifest.network.ApiConfig

class PartRepository {
    private val client = ApiConfig.getPartService()
    suspend fun getListPart(authorization: String, requestPartList: RequestPartList) = client.getListPart(authorization, requestPartList.manifestNo ?: "", requestPartList.subTitle ?: "")
}