package com.tmmin.emanifest.repositories

import com.tmmin.emanifest.models.requests.kanban.cancel.RequestKanbanCancel
import com.tmmin.emanifest.models.requests.kanban.check.RequestKanbanCheck
import com.tmmin.emanifest.models.requests.kanban.create.RequestKanbanCreate
import com.tmmin.emanifest.models.requests.kanban.detail.RequestKanbanDetail
import com.tmmin.emanifest.models.requests.kanban.list.RequestKanbanList
import com.tmmin.emanifest.models.requests.kanban.total.RequestKanbanTotal
import com.tmmin.emanifest.network.ApiConfig

class KanbanRepository {
    private val client = ApiConfig.getKanbanService()

    suspend fun getTotalKanban(authorization: String, requestKanbanTotal: RequestKanbanTotal) = client.getTotalKanban(authorization, requestKanbanTotal.manifestNo)

    suspend fun getDetailKanban(authorization: String, requestKanbanDetail: RequestKanbanDetail) = client.getDetailKanban(authorization, requestKanbanDetail.supplierCd ?: "", requestKanbanDetail.supplierPlant ?: "", requestKanbanDetail.kanbanId ?: "", requestKanbanDetail.kanbanCd ?: "", requestKanbanDetail.subTitle ?: "")

    suspend fun getCheckKanban(authorization: String, requestKanbanCheck: RequestKanbanCheck) = client.getCheckKanban(authorization, requestKanbanCheck.manifestNo ?: "", requestKanbanCheck.itemNo ?: 0, requestKanbanCheck.seqNo ?: 0, requestKanbanCheck.skidNo ?: "", requestKanbanCheck.qty ?: 0, requestKanbanCheck.processId ?: 0, requestKanbanCheck.subTitle ?: "")

    suspend fun createKanban(authorization: String, requestKanbanCreate: RequestKanbanCreate) = client.createKanban(authorization, requestKanbanCreate)

    suspend fun postCancelKanban(authorization: String, requestKanbanCancel: RequestKanbanCancel) = client.postCancelKanban(authorization, requestKanbanCancel)

    suspend fun getListKanban(authorization: String, requestKanbanList: RequestKanbanList) = client.getListKanban(authorization, requestKanbanList.manifestNo ?: "", requestKanbanList.itemNo ?: "", requestKanbanList.skidNo ?: "", requestKanbanList.subTitle ?: "")

    suspend fun getListKanbanManifest(authorization: String, requestKanbanList: RequestKanbanList) = client.getListKanbanManifest(authorization, requestKanbanList.manifestNo ?: "", requestKanbanList.subTitle ?: "")
}