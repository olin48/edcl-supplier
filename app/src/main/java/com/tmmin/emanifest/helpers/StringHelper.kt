package com.tmmin.emanifest.helpers

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class StringHelper {
    fun isSupCodeValid(text: String): Boolean {
        val parts = text.split(".")
        if (parts.isNotEmpty()) {
            return true
        } else {
            return false
        }
    }

    fun convertDateTime(datetime: String): String {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val outputFormat = SimpleDateFormat("dd-MM-yyyy, HH:mm", Locale.getDefault())
        try {
            val date: Date = inputFormat.parse(datetime)
            return outputFormat.format(date)
        } catch (e: Exception) {
            return datetime
        }
    }
}