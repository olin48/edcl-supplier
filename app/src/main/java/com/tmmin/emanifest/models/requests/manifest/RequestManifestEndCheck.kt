package com.tmmin.emanifest.models.requests.manifest

import com.google.gson.annotations.SerializedName

data class RequestManifestEndCheck(

	@field:SerializedName("ManifestNo")
	val manifestNo: String? = null,

	@field:SerializedName("ProcessId")
	val processId: String? = null,

	@field:SerializedName("SubTitle")
	val subTitle: String? = null
)