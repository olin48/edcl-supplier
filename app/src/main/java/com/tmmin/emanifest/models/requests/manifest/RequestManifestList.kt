package com.tmmin.emanifest.models.requests.manifest

import com.google.gson.annotations.SerializedName

data class RequestManifestList(

	@field:SerializedName("supplierPlant")
	val supplierPlant: String? = null,

	@field:SerializedName("PageNumber")
	var pageNumber: Int = 1,

	@field:SerializedName("subTitle")
	val subTitle: String? = null,

	@field:SerializedName("deliveryNo")
	val deliveryNo: String? = null,

	@field:SerializedName("supplierCd")
	val supplierCd: String? = null,

	@field:SerializedName("pageSize")
	var pageSize: Int = 10,
)