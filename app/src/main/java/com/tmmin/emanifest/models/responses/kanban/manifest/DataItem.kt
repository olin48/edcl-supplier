package com.tmmin.emanifest.models.responses.kanban.manifest

import com.google.gson.annotations.SerializedName

data class DataItem(

	@field:SerializedName("uniqueNo")
	val uniqueNo: String? = null,

	@field:SerializedName("kanbanId")
	val kanbanId: String? = null,

	@field:SerializedName("supplierPlant")
	val supplierPlant: String? = null,

	@field:SerializedName("manifestNo")
	val manifestNo: String? = null,

	@field:SerializedName("skidNo")
	var skidNo: String? = null,

	@field:SerializedName("supplierCd")
	val supplierCd: String? = null,

	@field:SerializedName("boxType")
	val boxType: String? = null,

	@field:SerializedName("kanbanCd")
	val kanbanCd: String? = null,

	@field:SerializedName("itemNo")
	val itemNo: String? = null,

	@field:SerializedName("seqNo")
	val seqNo: String? = null,

	@field:SerializedName("partNo")
	val partNo: String? = null
)