package com.tmmin.emanifest.models.responses.dockCode

import com.google.gson.annotations.SerializedName

data class DataItem(

	@field:SerializedName("label")
	val label: String? = null,

	@field:SerializedName("value")
	val value: String? = null
)