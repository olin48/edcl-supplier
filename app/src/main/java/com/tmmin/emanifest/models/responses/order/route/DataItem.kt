package com.tmmin.emanifest.models.responses.order.route

import com.google.gson.annotations.SerializedName

data class DataItem(

	@field:SerializedName("noKTP")
	val noKTP: String? = null,

	@field:SerializedName("deliveryNo")
	val deliveryNo: String? = null,

	@field:SerializedName("deviceId")
	val deviceId: String? = null,

	@field:SerializedName("gpsVendor")
	val gpsVendor: String? = null,

	@field:SerializedName("platNo")
	val platNo: String? = null,

	@field:SerializedName("speed")
	val speed: Int? = null,

	@field:SerializedName("datetime")
	val datetime: String? = null,

	@field:SerializedName("streetName")
	val streetName: String? = null,

	@field:SerializedName("positionId")
	val positionId: Int? = null,

	@field:SerializedName("lpcd")
	val lpcd: String? = null,

	@field:SerializedName("x")
	val x: Double? = null,

	@field:SerializedName("y")
	val y: Double? = null,

	@field:SerializedName("course")
	val course: Int? = null
)