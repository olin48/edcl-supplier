package com.tmmin.emanifest.models.responses.auth.login

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("refreshTokenExpired")
	val refreshTokenExpired: String? = null,

	@field:SerializedName("accessToken")
	val accessToken: String? = null,

	@field:SerializedName("refreshToken")
	val refreshToken: String? = null
)