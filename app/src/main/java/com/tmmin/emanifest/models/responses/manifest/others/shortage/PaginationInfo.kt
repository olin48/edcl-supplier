package com.tmmin.emanifest.models.responses.manifest.others.shortage

import com.google.gson.annotations.SerializedName

data class PaginationInfo(

	@field:SerializedName("pageNumber")
	val pageNumber: Int? = null,

	@field:SerializedName("pageSize")
	val pageSize: Int? = null,

	@field:SerializedName("moreRecord")
	val moreRecord: Boolean? = null
)