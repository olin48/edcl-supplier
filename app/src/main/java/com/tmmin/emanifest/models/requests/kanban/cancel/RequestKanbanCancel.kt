package com.tmmin.emanifest.models.requests.kanban.cancel

import com.google.gson.annotations.SerializedName

data class RequestKanbanCancel(

	@field:SerializedName("seqNo")
	val seqNo: String? = null,

	@field:SerializedName("subTitle")
	val subTitle: String? = null,

	@field:SerializedName("processId")
	val processId: String? = null,

	@field:SerializedName("manifestNo")
	val manifestNo: String? = null,

	@field:SerializedName("skidNo")
	val skidNo: String? = null,

	@field:SerializedName("itemNo")
	val itemNo: String? = null
)