package com.tmmin.emanifest.models.responses.job.driver.check

import com.google.gson.annotations.SerializedName

data class Data(
	@field:SerializedName("deliveryNo") var deliveryNo: String? = null,
	@field:SerializedName("supplierCD") var supplierCD: String? = null,
	@field:SerializedName("supplierPlant") var supplierPlant: String? = null,
	@field:SerializedName("directSupplierStatus") var directSupplierStatus: String? = null,
	@field:SerializedName("directSupplierStatusDescription") var directSupplierStatusDescription: String? = null,
	@field:SerializedName("loadingDt") var loadingDt: String? = null,
	@field:SerializedName("completePickupDt") var completePickupDt: String? = null,
	@field:SerializedName("returnStatus") var returnStatus: String? = null,
	@field:SerializedName("messageType") var messageType: String? = null,
	@field:SerializedName("message") var message: String? = null)