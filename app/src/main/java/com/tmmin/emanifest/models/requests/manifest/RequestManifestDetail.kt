package com.tmmin.emanifest.models.requests.manifest

import com.google.gson.annotations.SerializedName

data class RequestManifestDetail(

	@field:SerializedName("subTitle")
	val subTitle: String? = null,

	@field:SerializedName("manifestNo")
	val manifestNo: String? = null
)