package com.tmmin.emanifest.models.responses.kanban.detail

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("kanbanId")
	val kanbanId: String? = null,

	@field:SerializedName("totalQuantityActual")
	val totalQuantityActual: Int? = null,

	@field:SerializedName("seqNo")
	val seqNo: Int? = null,

	@field:SerializedName("manifestNo")
	val manifestNo: String? = null,

	@field:SerializedName("shortage")
	val shortage: Int? = null,

	@field:SerializedName("kanbanCd")
	val kanbanCd: String? = null,

	@field:SerializedName("totalQuantityPlan")
	val totalQuantityPlan: Int? = null,

	@field:SerializedName("itemNo")
	val itemNo: Int? = null,

	@field:SerializedName("partNo")
	val partNo: String? = null
)