package com.tmmin.emanifest.models.responses.skid.detail

import com.google.gson.annotations.SerializedName

data class DataItem(

	@field:SerializedName("uniqueNo")
	val uniqueNo: String? = null,

	@field:SerializedName("totalQuantity")
	val totalQuantity: Int? = null,

	@field:SerializedName("totalScannedQuantity")
	val totalScannedQuantity: Int? = null,

	@field:SerializedName("manifestNo")
	val manifestNo: String? = null,

	@field:SerializedName("piecesPerKanban")
	val piecesPerKanban: Int? = null,

	@field:SerializedName("boxType")
	val boxType: String? = null,

	@field:SerializedName("itemNo")
	val itemNo: Int? = null,

	@field:SerializedName("partNo")
	val partNo: String? = null,

	@field:SerializedName("totalKanban")
	val totalKanban: Int? = null,

	@field:SerializedName("totalScannedKanban")
	val totalScannedKanban: Int? = null
)