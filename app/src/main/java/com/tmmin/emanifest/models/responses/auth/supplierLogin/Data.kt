package com.tmmin.emanifest.models.responses.auth.supplierLogin

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("supplierPlant")
	val supplierPlant: String? = null,

	@field:SerializedName("supplierCd")
	val supplierCd: String? = null,

	@field:SerializedName("supplierRole")
	val supplierRole: String? = null,

    @field:SerializedName("supplierUser")
    val supplierUser: String? = null,

	@field:SerializedName("accessToken")
	val accessToken: String? = null,

	@field:SerializedName("refreshTokenExpired")
	val refreshTokenExpired: String? = null,

	@field:SerializedName("refreshToken")
	val refreshToken: String? = null
)