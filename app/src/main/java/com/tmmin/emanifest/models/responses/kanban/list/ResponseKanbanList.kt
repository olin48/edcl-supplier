package com.tmmin.emanifest.models.responses.kanban.list

import com.google.gson.annotations.SerializedName

data class ResponseKanbanList(

	@field:SerializedName("data")
	val data: MutableList<DataItem>,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)