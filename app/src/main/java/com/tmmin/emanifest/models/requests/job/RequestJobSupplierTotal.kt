package com.tmmin.emanifest.models.requests.job

import com.google.gson.annotations.SerializedName

data class RequestJobSupplierTotal(

	@field:SerializedName("SupplierPlant")
	val supplierPlant: String,

	@field:SerializedName("KeywordReadyToPickup")
	val keywordReadyToPickup: String,

	@field:SerializedName("KeywordOnProgress")
	val keywordOnProgress: String,

	@field:SerializedName("SupplierCd")
	val supplierCd: String,

	@field:SerializedName("SubTitle")
	val subTitle: String
)