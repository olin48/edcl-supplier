package com.tmmin.emanifest.models.responses.auth.logout

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("isSuccess")
	val isSuccess: Boolean? = null
)