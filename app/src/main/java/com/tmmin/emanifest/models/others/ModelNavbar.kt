package com.tmmin.emanifest.models.others

import com.google.gson.annotations.SerializedName

data class ModelNavbar(
    @field:SerializedName("deliveryNo") var deliveryNo: String? = null,
    @field:SerializedName("orderNo") var orderNo: String? = null,
    @field:SerializedName("manifestNo") var manifestNo: String? = null,
    @field:SerializedName("skidNo") var skidNo: String? = null,
    @field:SerializedName("route") var route: String? = null,
    @field:SerializedName("rate") var rate: String? = null,
    @field:SerializedName("partner") var partner: String? = null,
    @field:SerializedName("dockCode") var dockCode: String? = null,
    @field:SerializedName("conveyanceRoute") var conveyanceRoute: String? = null,
    @field:SerializedName("pLaneSeq") var pLaneSeq: String? = null,
    @field:SerializedName("shippingDock") var shippingDock: String? = null,
    @field:SerializedName("deliveryPreparationStatus") var deliveryPreparationStatus: String? = null,
    @field:SerializedName("date") var date: String? = null
)