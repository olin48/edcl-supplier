package com.tmmin.emanifest.models.requests.assign

import com.google.gson.annotations.SerializedName

data class RequestAssignCombo(

	@field:SerializedName("SupplierPlant")
	val supplierPlant: String? = null,

	@field:SerializedName("SupplierCd")
	val supplierCd: String? = null,

	@field:SerializedName("SubTitle")
	val subTitle: String? = null
)