package com.tmmin.emanifest.models.responses.job.list

import com.google.gson.annotations.SerializedName
import com.tmmin.emanifest.models.responses.manifest.list.PaginationInfo

data class ResponseJobList(

	@field:SerializedName("data")
	val data: MutableList<DataItem>,

	@field:SerializedName("paginationInfo")
	val paginationInfo: PaginationInfo? = null,

	@field:SerializedName("paginationTotalInfo")
	val paginationTotalInfo: PaginationTotalInfo? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)