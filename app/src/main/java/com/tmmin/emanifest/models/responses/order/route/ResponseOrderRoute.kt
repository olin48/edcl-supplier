package com.tmmin.emanifest.models.responses.order.route

import com.google.gson.annotations.SerializedName

data class ResponseOrderRoute(

	@field:SerializedName("data")
	val data: List<DataItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)