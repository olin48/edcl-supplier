package com.tmmin.emanifest.models.responses.manifest.end

import com.google.gson.annotations.SerializedName

data class ResponseManifestEnd(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)