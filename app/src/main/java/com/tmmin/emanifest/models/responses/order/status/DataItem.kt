package com.tmmin.emanifest.models.responses.order.status

import com.google.gson.annotations.SerializedName

data class DataItem(

	@field:SerializedName("supplierPlant")
	val supplierPlant: String? = null,

	@field:SerializedName("departurePlanDt")
	val departurePlanDt: String? = null,

	@field:SerializedName("isTmmin")
	val isTmmin: String? = null,

	@field:SerializedName("departureActualDt")
	val departureActualDt: String? = null,

	@field:SerializedName("latitude")
	val latitude: Double? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("arrivalActualDt")
	val arrivalActualDt: String? = null,

	@field:SerializedName("supplierCD")
	val supplierCD: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("arrivalPlanDt")
	val arrivalPlanDt: String? = null,

	@field:SerializedName("longitude")
	val longitude: Double? = null
)