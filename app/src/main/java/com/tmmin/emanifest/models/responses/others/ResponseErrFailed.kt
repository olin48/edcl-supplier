package com.tmmin.emanifest.models.responses.others

import com.google.gson.annotations.SerializedName

data class ResponseErrFailed(

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)