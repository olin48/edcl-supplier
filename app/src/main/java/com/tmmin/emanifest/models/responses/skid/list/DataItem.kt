package com.tmmin.emanifest.models.responses.skid.list

import com.google.gson.annotations.SerializedName

data class DataItem(

	@field:SerializedName("totalQuantity")
	val totalQuantity: Int? = null,

	@field:SerializedName("skidNo")
	val skidNo: String? = null,

	@field:SerializedName("isScannedByLP")
	val isScannedByLP: Boolean? = null,

	@field:SerializedName("totalKanban")
	val totalKanban: Int? = null
)