package com.tmmin.emanifest.models.requests.skid

import com.google.gson.annotations.SerializedName

data class RequestSKIDCreate(

	@field:SerializedName("supplierPlant")
	val supplierPlant: String? = null,

	@field:SerializedName("subTitle")
	val subTitle: String? = null,

	@field:SerializedName("processId")
	val processId: String? = null,

	@field:SerializedName("manifestNo")
	val manifestNo: String? = null,

	@field:SerializedName("supplierCd")
	val supplierCd: String? = null
)