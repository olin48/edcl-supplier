package com.tmmin.emanifest.models.responses.assign.combo

import com.google.gson.annotations.SerializedName

data class DataItem(

	@field:SerializedName("label")
	val label: String? = null,

	@field:SerializedName("value")
	val value: String? = null
)