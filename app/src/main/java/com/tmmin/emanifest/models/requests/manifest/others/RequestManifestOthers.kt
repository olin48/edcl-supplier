package com.tmmin.emanifest.models.requests.manifest.others

import com.google.gson.annotations.SerializedName

data class RequestManifestOthers(

	@field:SerializedName("SupplierPlant")
	val supplierPlant: String? = null,

	@field:SerializedName("SupplierCd")
	val supplierCd: String? = null,

	@field:SerializedName("SortBy")
	val sortBy: String? = null,

	@field:SerializedName("Keyword")
	val keyword: String? = null,

	@field:SerializedName("SubTitle")
	val subTitle: String? = null
)