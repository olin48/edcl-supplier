package com.tmmin.emanifest.models.requests.order

import com.google.gson.annotations.SerializedName

data class RequestOrderStatus(

	@field:SerializedName("DeliveryNo")
	val deliveryNo: String,

	@field:SerializedName("SubTitle")
	val subTitle: String
)