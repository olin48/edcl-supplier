package com.tmmin.emanifest.models.requests.auth

import com.google.gson.annotations.SerializedName

data class RequestRefreshToken(

	@field:SerializedName("refreshToken")
	val refreshToken: String? = null
)