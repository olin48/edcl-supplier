package com.tmmin.emanifest.models.requests.assign

import com.google.gson.annotations.SerializedName

data class RequestAssign(

    @field:SerializedName("SubTitle") val subTitle: String? = null,

    @field:SerializedName("ManifestNo") val manifestNo: String? = null,

    @field:SerializedName("DeliveryNo") val deliveryNo: String? = null,

    @field:SerializedName("SupplierPlant") val supplierPlant: String? = null,

    @field:SerializedName("DriverUsername") val driverUsername: String? = null,

    @field:SerializedName("SupplierCd") val supplierCd: String? = null)