package com.tmmin.emanifest.models.responses.manifest.start

import com.google.gson.annotations.SerializedName

data class ResponseManifestStart(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)