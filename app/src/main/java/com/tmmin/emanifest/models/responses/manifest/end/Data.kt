package com.tmmin.emanifest.models.responses.manifest.end

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("isSuccess")
	val isSuccess: Boolean? = null
)