package com.tmmin.emanifest.models.others

import com.google.gson.annotations.SerializedName

data class ModelSlip(

    @field:SerializedName("kanbanId") var kanbanId: String = "-",

    @field:SerializedName("supCodePlant") var supCodePlant: String = "-",

    @field:SerializedName("suppName") var suppName: String = "-",

	@field:SerializedName("suppNumber") var suppNumber: String = "-",

    @field:SerializedName("orderNumber") var orderNumber: String = "-",

    @field:SerializedName("routeCycle") var routeCycle: String = "-",

    @field:SerializedName("conveyance") var conveyance: String = "-",

    @field:SerializedName("manifestNumber") var manifestNumber: String = "-",

    @field:SerializedName("planeNumber") var planeNumber: String = "-",

    @field:SerializedName("dockCode") var dockCode: String = "-",

    @field:SerializedName("kanbanCode") var kanbanCode: String = "-",

    @field:SerializedName("suppDockCode") var suppDockCode: String = "-"
)