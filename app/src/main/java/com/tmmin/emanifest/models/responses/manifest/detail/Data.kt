package com.tmmin.emanifest.models.responses.manifest.detail

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("route")
	val route: String? = null,

	@field:SerializedName("rate")
	val rate: String? = null,

	@field:SerializedName("lpcd")
	val lpcd: String? = null,

	@field:SerializedName("lpName")
	val lpName: String? = null,

	@field:SerializedName("pickupDt")
	val pickupDt: String? = null,

	@field:SerializedName("supplierPreparationDescription")
	val supplierPreparationDescription: String? = null,

	@field:SerializedName("supplierCd")
	val supplierCd: String? = null,

	@field:SerializedName("supplierArrivalPlanDt")
	val supplierArrivalPlanDt: String? = null,

	@field:SerializedName("supplierPlant")
	val supplierPlant: String? = null,

	@field:SerializedName("tmminDeparturePlanDt")
	val tmminDeparturePlanDt: String? = null,

	@field:SerializedName("manifestCategoryFlag")
	val manifestCategoryFlag: String? = null,

	@field:SerializedName("manifestCategory")
	val manifestCategory: String? = null,

	@field:SerializedName("totalQuantityPlan")
	val totalQuantityPlan: Int? = null,

	@field:SerializedName("dockCd")
	val dockCd: String? = null,

	@field:SerializedName("vehicleType")
	val vehicleType: String? = null,

	@field:SerializedName("supplierDeparturePlanDt")
	val supplierDeparturePlanDt: String? = null,

	@field:SerializedName("supplierName")
	val supplierName: String? = null,

	@field:SerializedName("supplierAddress")
	val supplierAddress: String? = null,

	@field:SerializedName("supplierPreparationStatus")
	val supplierPreparationStatus: String? = null,

	@field:SerializedName("orderNo")
	val orderNo: String? = null,

	@field:SerializedName("manifestNo")
	val manifestNo: String? = null,

	@field:SerializedName("noKtp")
	val noKtp: String? = null,

	@field:SerializedName("deliveryNo")
	val deliveryNo: String? = null,

	@field:SerializedName("tmminName")
	val tmminName: String? = null,

	@field:SerializedName("platNo")
	val platNo: String? = null,

	@field:SerializedName("totalQuantityActual")
	val totalQuantityActual: Int? = null,

	@field:SerializedName("totalKanbanPlan")
	val totalKanbanPlan: Int? = null,

	@field:SerializedName("tmminArrivalPlanDt")
	val tmminArrivalPlanDt: String? = null,

	@field:SerializedName("tmminPlant")
	val tmminPlant: String? = null,

	@field:SerializedName("totalKanbanActual")
	val totalKanbanActual: Int? = null,

	@field:SerializedName("supplierShippingDock")
	val supplierShippingDock: String? = null,

	@field:SerializedName("driverName")
	val driverName: String? = null,

	@field:SerializedName("lastPreparedBy")
	val lastPreparedBy: String? = null,

	@field:SerializedName("conveyanceRoute")
	val conveyanceRoute: String? = null,

	@field:SerializedName("pLaneSeq")
	val pLaneSeq: String? = null,

	@field:SerializedName("shippingDock")
	val shippingDock: String? = null,

	@field:SerializedName("totalSKID")
	val totalSKID: Int? = null,

	@field:SerializedName("isPickedUp")
	val isPickedUp: Boolean? = null,

	@field:SerializedName("tmminAddress")
	val tmminAddress: String? = null,

	@field:SerializedName("tmminCd")
	val tmminCd: String? = null
)