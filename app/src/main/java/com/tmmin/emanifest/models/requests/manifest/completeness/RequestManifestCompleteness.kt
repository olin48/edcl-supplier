package com.tmmin.emanifest.models.requests.manifest.completeness

import com.google.gson.annotations.SerializedName

data class RequestManifestCompleteness(

	@field:SerializedName("RequestManifestCompleteness")
	val requestManifestCompleteness: List<RequestManifestCompletenessItem>
)