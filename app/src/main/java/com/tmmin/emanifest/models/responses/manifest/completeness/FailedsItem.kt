package com.tmmin.emanifest.models.responses.manifest.completeness

import com.google.gson.annotations.SerializedName

data class FailedsItem(

	@field:SerializedName("kanbanId")
	val kanbanId: String? = null,

	@field:SerializedName("supplierPlant")
	val supplierPlant: String? = null,

	@field:SerializedName("seqNo")
	val seqNo: Int? = null,

	@field:SerializedName("skidNo")
	val skidNo: String? = null,

	@field:SerializedName("manifestNo")
	val manifestNo: String? = null,

	@field:SerializedName("supplierCode")
	val supplierCode: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("itemNo")
	val itemNo: Int? = null
)