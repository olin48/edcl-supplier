package com.tmmin.emanifest.models.requests.part

import com.google.gson.annotations.SerializedName

data class RequestPartList(

	@field:SerializedName("subTitle")
	val subTitle: String? = null,

	@field:SerializedName("manifestNo")
	val manifestNo: String? = null
)