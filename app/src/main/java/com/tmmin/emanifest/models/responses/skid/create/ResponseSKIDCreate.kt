package com.tmmin.emanifest.models.responses.skid.create

import com.google.gson.annotations.SerializedName

data class ResponseSKIDCreate(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)