package com.tmmin.emanifest.models.responses.assign.check

import com.google.gson.annotations.SerializedName

data class ResponseAssignCheck(

    @field:SerializedName("data")
	val data: Data? = null,

    @field:SerializedName("message")
	val message: String? = null,

    @field:SerializedName("status")
	val status: Int? = null
)