package com.tmmin.emanifest.models.requests.delivery

import com.google.gson.annotations.SerializedName

data class RequestDeliverySubmit(

	@field:SerializedName("supplierPlant")
	val supplierPlant: String? = null,

	@field:SerializedName("subTitle")
	val subTitle: String? = null,

	@field:SerializedName("deliveryNo")
	val deliveryNo: String? = null,

	@field:SerializedName("supplierCd")
	val supplierCd: String? = null
)