package com.tmmin.emanifest.models.requests.auth

import com.google.gson.annotations.SerializedName

data class RequestSupplierInfo(

	@field:SerializedName("supplierPlant")
	var supplierPlant: String,

	@field:SerializedName("subTitle")
	var subTitle: String,

	@field:SerializedName("supplierCd")
	var supplierCd: String
)