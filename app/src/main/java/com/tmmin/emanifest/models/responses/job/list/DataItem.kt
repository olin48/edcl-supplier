package com.tmmin.emanifest.models.responses.job.list

import com.google.gson.annotations.SerializedName

data class DataItem(

	@field:SerializedName("deliveryPreparationStatus")
	val deliveryPreparationStatus: String? = null,

	@field:SerializedName("noKTP")
	val noKTP: String? = null,

	@field:SerializedName("deliveryNo")
	val deliveryNo: String? = null,

	@field:SerializedName("arrivalPlanDt")
	val arrivalPlanDt: String? = null,

	@field:SerializedName("platNo")
	val platNo: String? = null,

	@field:SerializedName("pickupDt")
	val pickupDt: String? = null,

	@field:SerializedName("deliveryPreparationDescription")
	val deliveryPreparationDescription: String? = null,

	@field:SerializedName("departurePlanDt")
	val departurePlanDt: String? = null,

	@field:SerializedName("route")
	val route: String? = null,

	@field:SerializedName("rate")
	val rate: String? = null,

	@field:SerializedName("lpName")
	val lpName: String? = null,

	@field:SerializedName("lpCd")
	val lpCd: String? = null,

	@field:SerializedName("isNoOrder")
	val isNoOrder: Boolean? = null,

	@field:SerializedName("deliveryType")
	val deliveryType: String? = null,

	@field:SerializedName("driverName")
	val driverName: String? = null
)