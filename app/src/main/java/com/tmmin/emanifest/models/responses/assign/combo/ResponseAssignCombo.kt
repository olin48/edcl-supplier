package com.tmmin.emanifest.models.responses.assign.combo

import com.google.gson.annotations.SerializedName

data class ResponseAssignCombo(

	@field:SerializedName("data")
	val data: List<DataItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)