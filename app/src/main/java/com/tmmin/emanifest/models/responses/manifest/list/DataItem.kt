package com.tmmin.emanifest.models.responses.manifest.list

import com.google.gson.annotations.SerializedName

data class DataItem(

	@field:SerializedName("supplierPreparationStatus")
	val supplierPreparationStatus: String? = null,

	@field:SerializedName("orderNo")
	val orderNo: String? = null,

	@field:SerializedName("supplierPreparationDescription")
	val supplierPreparationDescription: String? = null,

	@field:SerializedName("isNoOrder")
	val isNoOrder: String? = null,

	@field:SerializedName("totalScannedKanban")
	val totalScannedKanban: Int? = null,

	@field:SerializedName("manifestCategory")
	val manifestCategory: String? = null,

	@field:SerializedName("manifestNo")
	val manifestNo: String? = null,

	@field:SerializedName("manifestCategoryFlag")
	val manifestCategoryFlag: String? = null,

	@field:SerializedName("dockCd")
	val dockCd: String? = null,

	@field:SerializedName("lastPreparedBy")
	val lastPreparedBy: String? = null,

	@field:SerializedName("totalSKID")
	val totalSKID: Int? = null,

	@field:SerializedName("totalKanban")
	val totalKanban: Int? = null
)