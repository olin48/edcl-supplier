package com.tmmin.emanifest.models.responses.part.list

import com.google.gson.annotations.SerializedName

data class ResponsePartList(

	@field:SerializedName("data")
	val data: List<DataItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)