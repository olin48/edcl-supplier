package com.tmmin.emanifest.models.requests.settings

import com.google.gson.annotations.SerializedName

data class RequestSettingPlant(
	@field:SerializedName("supplierCd")
	var supplierCd: String,

	@field:SerializedName("subTitle")
	var subTitle: String
)