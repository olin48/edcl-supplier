package com.tmmin.emanifest.models.responses.job.combo

import com.google.gson.annotations.SerializedName

data class ResponseJobCombo(

	@field:SerializedName("data")
	val data: ArrayList<DataItem>,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)