package com.tmmin.emanifest.models.responses.manifest.check

import com.google.gson.annotations.SerializedName

data class ResponseManifestEndCheck(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)