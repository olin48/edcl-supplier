package com.tmmin.emanifest.models.responses.order.combo

import com.google.gson.annotations.SerializedName

data class ResponseOrderCombo(

	@field:SerializedName("data")
	val data: MutableList<DataItem>,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)