package com.tmmin.emanifest.models.requests.auth

import com.google.gson.annotations.SerializedName

data class RequestLogin(
    @field:SerializedName("password") val password: String,
    @field:SerializedName("username") val username: String,
    @field:SerializedName("subTitle") var subTitle: String)