package com.tmmin.emanifest.models.responses.part

import com.google.gson.annotations.SerializedName

data class ResponsePartItem(

	@field:SerializedName("pcs")
	val pcs: String? = null,

	@field:SerializedName("kanbanNumber")
	val kanbanNumber: String? = null,

	@field:SerializedName("partNumber")
	val partNumber: String? = null,

	@field:SerializedName("uniqueNumber")
	val uniqueNumber: String? = null
)