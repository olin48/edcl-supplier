package com.tmmin.emanifest.models.responses.manifest.others

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("totalEOManifest")
	val totalEOManifest: Int? = null,

	@field:SerializedName("totalShortageManifest")
	val totalShortageManifest: Int? = null,

	@field:SerializedName("totalOthersManifest")
	val totalOthersManifest: Int? = null
)