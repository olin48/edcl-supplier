package com.tmmin.emanifest.models.responses.auth.profile

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("supplierName")
	val supplierName: String? = null,

	@field:SerializedName("supplierPlant")
	val supplierPlant: String? = null,

	@field:SerializedName("supplierCd")
	val supplierCd: String? = null,

	@field:SerializedName("supplierAbbreviation")
	val supplierAbbreviation: String? = null
)