package com.tmmin.emanifest.models.responses.manifest.others.shortage

import com.google.gson.annotations.SerializedName

data class ResponseManifestOthersListShortage(

	@field:SerializedName("data")
	val data: List<DataItem?>? = null,

	@field:SerializedName("paginationInfo")
	val paginationInfo: PaginationInfo? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)