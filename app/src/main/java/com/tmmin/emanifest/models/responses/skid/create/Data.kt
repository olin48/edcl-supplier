package com.tmmin.emanifest.models.responses.skid.create

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("skidNoOutput")
	val skidNoOutput: String? = null
)