package com.tmmin.emanifest.models.responses.manifest.start

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("manifestNoOutput")
	val manifestNoOutput: String? = null,

	@field:SerializedName("processIdOutput")
	val processIdOutput: Long? = null
)