package com.tmmin.emanifest.models.responses.kanban.total

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("totalScannedKanban")
	val totalScannedKanban: Int? = null,

	@field:SerializedName("totalKanban")
	val totalKanban: Int? = null
)