package com.tmmin.emanifest.models.responses.job.driver.post

import com.google.gson.annotations.SerializedName

data class ResponseDriverPickup(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)