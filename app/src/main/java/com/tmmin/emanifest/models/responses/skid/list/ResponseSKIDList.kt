package com.tmmin.emanifest.models.responses.skid.list

import com.google.gson.annotations.SerializedName

data class ResponseSKIDList(

	@field:SerializedName("data")
	val data: List<DataItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)