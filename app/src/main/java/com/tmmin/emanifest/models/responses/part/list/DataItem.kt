package com.tmmin.emanifest.models.responses.part.list

import com.google.gson.annotations.SerializedName

data class DataItem(

	@field:SerializedName("uniqueNo")
	val uniqueNo: String? = null,

	@field:SerializedName("totalQuantityActual")
	val totalQuantityActual: Int? = null,

	@field:SerializedName("totalKanbanPlan")
	val totalKanbanPlan: Int? = null,

	@field:SerializedName("totalKanbanActual")
	val totalKanbanActual: Int? = null,

	@field:SerializedName("manifestNo")
	val manifestNo: String? = null,

	@field:SerializedName("piecesPerKanban")
	val piecesPerKanban: Int? = null,

	@field:SerializedName("boxType")
	val boxType: String? = null,

	@field:SerializedName("totalQuantityPlan")
	val totalQuantityPlan: Int? = null,

	@field:SerializedName("itemNo")
	val itemNo: String? = null,

	@field:SerializedName("partNo")
	val partNo: String? = null
)