package com.tmmin.emanifest.models.responses.kanban.list

import com.google.gson.annotations.SerializedName

data class DataItem(
	@field:SerializedName("seqNo")
	val seqNo: Int? = null,

	@field:SerializedName("manifestNo")
	val manifestNo: String? = null,

	@field:SerializedName("skidNo")
	val skidNo: String? = null,

	@field:SerializedName("kanbanCD")
	val kanbanCd: String? = null,

	@field:SerializedName("totalQuantityActual")
	val totalQuantityActual: Int? = null,

	@field:SerializedName("totalQuantityPlan")
	val totalQuantityPlan: Int? = null,

	@field:SerializedName("itemNo")
	val itemNo: Int? = null,

	@field:SerializedName("isScanned")
	val isScanned: Boolean? = null
)