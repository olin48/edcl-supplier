package com.tmmin.emanifest.models.requests.kanban.create

import com.google.gson.annotations.SerializedName

data class RequestKanbanCreate(

	@field:SerializedName("seqNo")
	val seqNo: String? = null,

	@field:SerializedName("SubTitle")
	val subTitle: String? = null,

	@field:SerializedName("processId")
	val processId: Long? = null,

	@field:SerializedName("manifestNo")
	val manifestNo: String? = null,

	@field:SerializedName("skidNo")
	val skidNo: String? = null,

	@field:SerializedName("qty")
	val qty: Int? = null,

	@field:SerializedName("itemNo")
	val itemNo: String? = null
)