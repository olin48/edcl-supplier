package com.tmmin.emanifest.models.responses.manifest.completeness

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("successes")
	val successes: List<SuccessesItem?>? = null,

	@field:SerializedName("faileds")
	val faileds: List<FailedsItem?>? = null
)