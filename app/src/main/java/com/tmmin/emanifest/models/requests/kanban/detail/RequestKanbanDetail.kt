package com.tmmin.emanifest.models.requests.kanban.detail

import com.google.gson.annotations.SerializedName

data class RequestKanbanDetail(

	@field:SerializedName("kanbanId")
	val kanbanId: String? = null,

	@field:SerializedName("supplierPlant")
	val supplierPlant: String? = null,

	@field:SerializedName("subTitle")
	val subTitle: String? = null,

	@field:SerializedName("supplierCd")
	val supplierCd: String? = null,

	@field:SerializedName("kanbanCd")
	val kanbanCd: String? = null
)