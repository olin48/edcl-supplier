package com.tmmin.emanifest.models.responses.kanban.create

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("isSuccess")
	val isSuccess: Boolean? = null
)