package com.tmmin.emanifest.models.requests.delivery

import com.google.gson.annotations.SerializedName

data class RequestDeliveryCheck(

	@field:SerializedName("SupplierPlant")
	val supplierPlant: String? = null,

	@field:SerializedName("DeliveryNo")
	val deliveryNo: String? = null,

	@field:SerializedName("SupplierCd")
	val supplierCd: String? = null,

	@field:SerializedName("SubTitle")
	val subTitle: String? = null
)