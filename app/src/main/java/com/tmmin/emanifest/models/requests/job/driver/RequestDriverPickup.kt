package com.tmmin.emanifest.models.requests.job.driver

import com.google.gson.annotations.SerializedName

data class RequestDriverPickup(

	@field:SerializedName("supplierPlant")
	val supplierPlant: String? = null,

	@field:SerializedName("supplierCd")
	val supplierCd: String? = null,

	@field:SerializedName("deliveryNo")
	val deliveryNo: String? = null,

	@field:SerializedName("directSupplierSts")
	val directSupplierSts: String? = null,

	@field:SerializedName("directSupplierNote")
	val directSupplierNote: String? = null,

	@field:SerializedName("subTitle")
	val subTitle: String? = null
)