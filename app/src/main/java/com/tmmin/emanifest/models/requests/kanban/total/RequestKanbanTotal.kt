package com.tmmin.emanifest.models.requests.kanban.total

import com.google.gson.annotations.SerializedName

data class RequestKanbanTotal(

	@field:SerializedName("ManifestNo")
	val manifestNo: String
)