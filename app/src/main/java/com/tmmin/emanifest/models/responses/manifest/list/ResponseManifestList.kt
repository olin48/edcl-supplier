package com.tmmin.emanifest.models.responses.manifest.list

import com.google.gson.annotations.SerializedName

data class ResponseManifestList(

	@field:SerializedName("data")
	val data: MutableList<DataItem>,

	@field:SerializedName("paginationInfo")
	val paginationInfo: PaginationInfo? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)