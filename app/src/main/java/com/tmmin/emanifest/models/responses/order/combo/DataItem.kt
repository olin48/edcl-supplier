package com.tmmin.emanifest.models.responses.order.combo

import com.google.gson.annotations.SerializedName

data class DataItem(

	@field:SerializedName("pickupDt")
	val pickupDt: String? = null,

	@field:SerializedName("lpName")
	val lpName: String? = null,

	@field:SerializedName("lpcd")
	val lpcd: String? = null,

	@field:SerializedName("noKTP")
	val noKTP: String? = null,

	@field:SerializedName("deliveryNo")
	val deliveryNo: String? = null,

	@field:SerializedName("driverName")
	val driverName: String? = null,

	@field:SerializedName("label")
	val label: String? = null,

	@field:SerializedName("value")
	val value: String? = null
)