package com.tmmin.emanifest.models.responses.delivery.check

import com.google.gson.annotations.SerializedName

data class ResponseDeliveryCheck(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)