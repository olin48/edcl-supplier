package com.tmmin.emanifest.models.responses.job.current

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("totalDeliveryOnProgress")
	val totalDeliveryOnProgress: Int? = null,

	@field:SerializedName("totalDeliveryReadyToPickup")
	val totalDeliveryReadyToPickup: Int? = null
)