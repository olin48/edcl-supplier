package com.tmmin.emanifest.models.requests.skid

import com.google.gson.annotations.SerializedName

data class RequestSKIDList(

	@field:SerializedName("subTitle")
	val subTitle: String? = null,

	@field:SerializedName("manifestNo")
	val manifestNo: String? = null
)