package com.tmmin.emanifest.models.requests.skid

import com.google.gson.annotations.SerializedName

data class RequestSkidDetail(

	@field:SerializedName("subTitle")
	val subTitle: String? = null,

	@field:SerializedName("skidNo")
	val skidNo: String? = null
)