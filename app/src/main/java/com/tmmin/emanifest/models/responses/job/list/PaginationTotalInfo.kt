package com.tmmin.emanifest.models.responses.job.list

import com.google.gson.annotations.SerializedName

data class PaginationTotalInfo(

	@field:SerializedName("pageNumber")
	val pageNumber: Int? = null,

	@field:SerializedName("pageSize")
	val pageSize: Int? = null,

	@field:SerializedName("totalItem")
	val totalItem: Int? = null,

	@field:SerializedName("totalPage")
	val totalPage: Int? = null,

	@field:SerializedName("moreRecord")
	val moreRecord: Boolean? = null
)