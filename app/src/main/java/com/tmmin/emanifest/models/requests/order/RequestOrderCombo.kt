package com.tmmin.emanifest.models.requests.order

import com.google.gson.annotations.SerializedName

data class RequestOrderCombo(

    @field:SerializedName("SupplierCd") val supplierCd: String,

    @field:SerializedName("SupplierPlant") val supplierPlant: String,

    @field:SerializedName("SubTitle") val subTitle: String)