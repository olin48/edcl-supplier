package com.tmmin.emanifest.models.responses.settings.plant

import com.google.gson.annotations.SerializedName

data class DataItem(
	@field:SerializedName("label")
	var label: String,

	@field:SerializedName("value")
	var value: String
)