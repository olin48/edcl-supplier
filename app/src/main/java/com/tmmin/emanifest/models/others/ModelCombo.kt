package com.tmmin.emanifest.models.others

import com.google.gson.annotations.SerializedName

data class ModelCombo(

	@field:SerializedName("sortBySelected")
	var sortBySelected: Int,

	@field:SerializedName("dateFromSelected")
	var dateFromSelected: String,

	@field:SerializedName("dateToSelected")
	var dateToSelected: String
)