package com.tmmin.emanifest.models.requests.kanban.list

import com.google.gson.annotations.SerializedName

data class RequestKanbanList(

	@field:SerializedName("subTitle")
	val subTitle: String? = null,

	@field:SerializedName("manifestNo")
	val manifestNo: String? = null,

	@field:SerializedName("skidNo")
	val skidNo: String? = null,

	@field:SerializedName("itemNo")
	val itemNo: String? = null,
)