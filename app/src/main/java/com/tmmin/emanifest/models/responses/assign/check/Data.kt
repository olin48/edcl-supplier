package com.tmmin.emanifest.models.responses.assign.check

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("messageType")
	val messageType: String? = null,

	@field:SerializedName("returnStatus")
	val returnStatus: String? = null,

	@field:SerializedName("message")
	val message: String? = null
)