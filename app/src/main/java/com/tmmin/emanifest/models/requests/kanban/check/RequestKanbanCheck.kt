package com.tmmin.emanifest.models.requests.kanban.check

import com.google.gson.annotations.SerializedName

data class RequestKanbanCheck(

	@field:SerializedName("seqNo")
	val seqNo: Int? = null,

	@field:SerializedName("subTitle")
	val subTitle: String? = null,

	@field:SerializedName("processId")
	val processId: Long? = null,

	@field:SerializedName("manifestNo")
	val manifestNo: String? = null,

	@field:SerializedName("skidNo")
	val skidNo: String? = null,

	@field:SerializedName("qty")
	val qty: Int? = null,

	@field:SerializedName("itemNo")
	val itemNo: Int? = null
)