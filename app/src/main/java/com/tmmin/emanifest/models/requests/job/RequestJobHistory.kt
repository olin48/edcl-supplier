package com.tmmin.emanifest.models.requests.job

import com.google.gson.annotations.SerializedName

data class RequestJobHistory(

	@field:SerializedName("SupplierPlant")
	var supplierPlant: String = "",

	@field:SerializedName("PickupDtTo")
	var pickupDtTo: String = "",

	@field:SerializedName("PageSize")
	var pageSize: Int = 10,

	@field:SerializedName("PickupDtFrom")
	var pickupDtFrom: String = "",

	@field:SerializedName("PageNumber")
	var pageNumber: Int = 1,

	@field:SerializedName("SortBy")
	var sortBy: String = "",

	@field:SerializedName("Keyword")
	var keyword: String = "",

	@field:SerializedName("SupplierCd")
	var supplierCd: String = "",

	@field:SerializedName("SubTitle")
	var subTitle: String = ""
)