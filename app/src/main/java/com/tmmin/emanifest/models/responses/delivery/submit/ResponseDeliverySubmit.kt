package com.tmmin.emanifest.models.responses.delivery.submit

import com.google.gson.annotations.SerializedName

data class ResponseDeliverySubmit(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)