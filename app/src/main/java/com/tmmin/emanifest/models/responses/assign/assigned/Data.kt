package com.tmmin.emanifest.models.responses.assign.assigned

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("supplierPlant")
	val supplierPlant: String? = null,

	@field:SerializedName("loadingDt")
	val loadingDt: String? = null,

	@field:SerializedName("directSupplierStatusDescription")
	val directSupplierStatusDescription: String? = null,

	@field:SerializedName("completePickupDt")
	val completePickupDt: String? = null,

	@field:SerializedName("directSupplierNote")
	val directSupplierNote: String? = null,

	@field:SerializedName("deliveryNo")
	val deliveryNo: String? = null,

	@field:SerializedName("supplierCd")
	val supplierCd: String? = null,

	@field:SerializedName("driverName")
	val driverName: String? = null,

	@field:SerializedName("username")
	val username: String? = null,

	@field:SerializedName("directSupplierStatus")
	val directSupplierStatus: String? = null
)