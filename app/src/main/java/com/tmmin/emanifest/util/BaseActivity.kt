package com.tmmin.emanifest.util

import android.content.Context
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.os.StrictMode.ThreadPolicy
import androidx.appcompat.app.AppCompatActivity
import com.tmmin.emanifest.helpers.PreferenceHelper
import java.io.IOException
import java.net.ConnectException
import java.net.HttpURLConnection
import java.net.SocketTimeoutException
import java.net.URL
import java.util.Locale


abstract class BaseActivity : AppCompatActivity() {
    lateinit var sharedPref: PreferenceHelper
    lateinit var authToken: String
    lateinit var locale: Locale

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setup()
    }

    private fun setup() {
        sharedPref = PreferenceHelper(this)
        authToken = sharedPref.getString(Constant.AUTH_TOKEN_ACCESS) ?: ""
        locale = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            resources.configuration.locales.get(0)
        } else {
            resources.configuration.locale
        }
        initializeSSLContext()
    }

    fun isConnected(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        return activeNetwork?.isConnected == true
    }

    fun isInternetAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.M -> {
                val activeNetwork = connectivityManager.activeNetwork ?: return false
                val cap = connectivityManager.getNetworkCapabilities(activeNetwork) ?: return false
                when {
                    cap.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                    cap.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                    else -> false
                }
            }

            else -> {
                val activeNetwork = connectivityManager.activeNetworkInfo ?: return false
                return when (activeNetwork.type) {
                    ConnectivityManager.TYPE_MOBILE -> true
                    ConnectivityManager.TYPE_WIFI -> true
                    ConnectivityManager.TYPE_VPN -> true
                    else -> false
                }
            }
        }
    }

    fun isConnectedToInternet(): Boolean{
        return true
    }

    fun isConnectedWW(): Boolean {
        val SDK_INT = Build.VERSION.SDK_INT
        return if (SDK_INT > 8) {
            val policy = ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)
            val cm = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            if (activeNetwork != null && activeNetwork.isConnected) {
                return try {
                    val url = URL("https://www.google.com/")
                    val urlc = url.openConnection() as HttpURLConnection
                    urlc.setRequestProperty("User-Agent", "test")
                    urlc.setRequestProperty("Connection", "close")
                    urlc.connectTimeout = 5000
                    urlc.readTimeout = 5000
                    urlc.connect()
                    if (urlc.responseCode == 200) {
                        true
                    } else {
                        false
                    }
                } catch (e: SocketTimeoutException) {
                    false
                } catch (e: ConnectException) {
                    false
                } catch (e: IOException) {
                    false
                }
            }
            false
        } else {
            false
        }
    }

    private fun initializeSSLContext() {
//        val trustAllCerts = arrayOf<TrustManager>(
//            object : X509TrustManager {
//                override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {}
//                override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {}
//                override fun getAcceptedIssuers(): Array<X509Certificate> = arrayOf()
//            }
//        )
//
//        val sslContext: SSLContext = SSLContext.getInstance("SSL")
//        sslContext.init(null, trustAllCerts, java.security.SecureRandom())
//        HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.socketFactory)

//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
//            try {
//                ProviderInstaller.installIfNeeded(applicationContext)
//                val sslContext: SSLContext = SSLContext.getInstance("TLSv1.2")
//                sslContext.init(null, null, null)
//                sslContext.createSSLEngine()
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
//        }
//
//        Log.e("Install", "TLSv1.2")
//        try {
//            SSLContext.getInstance("TLSv1.2")
//        } catch (e: NoSuchAlgorithmException) {
//            e.printStackTrace()
//        }
//        try {
//            ProviderInstaller.installIfNeeded(this)
//        } catch (e: GooglePlayServicesRepairableException) {
//            e.printStackTrace()
//        } catch (e: GooglePlayServicesNotAvailableException) {
//            e.printStackTrace()
//        }
    }

    fun hasLaserScanner(): Boolean {
        return packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_LEVEL_FULL)
    }
}