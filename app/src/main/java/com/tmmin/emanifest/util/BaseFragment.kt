package com.tmmin.emanifest.util

import android.content.pm.PackageManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.tmmin.emanifest.BuildConfig
import com.tmmin.emanifest.helpers.PreferenceHelper
import java.util.Locale

abstract class BaseFragment : Fragment() {
    lateinit var sharedPref: PreferenceHelper
    lateinit var authToken: String
    lateinit var locale: Locale

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setup()
    }

    private fun setup() {
        sharedPref = PreferenceHelper(requireActivity())
        authToken = sharedPref.getString(Constant.AUTH_TOKEN_ACCESS) ?: ""
        locale = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            resources.configuration.locales.get(0)
        } else {
            resources.configuration.locale
        }
    }

    fun hasLaserScanner(): Boolean {
        return requireActivity().packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_LEVEL_FULL)
    }

    fun isProduction(): Boolean {
        if (BuildConfig.BASE_URL.equals(BuildConfig.BASE_URL_PROD)) {
            return true
        } else {
            return false
        }
    }
}