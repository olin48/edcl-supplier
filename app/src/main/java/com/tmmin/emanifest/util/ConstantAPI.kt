package com.tmmin.emanifest.util

class ConstantAPI {
    companion object {
        const val API_V1 = "api/v1/"

        //Auth
        const val API_AUTH_LOGIN_SUPPLIER = API_V1 + "auth/supplier-login"
        const val API_AUTH_INFO = API_V1 + "supplier/supplier-info"
        const val API_AUTH_LOGOUT = API_V1 + "auth/logout"
        const val API_AUTH_REFRESH_TOKEN = API_V1 + "auth/supplier-refresh-token"

        //Setting
        const val API_AUTH_DOCK_CODE = API_V1 + "dock-packing/daily-order-label"
        const val API_SETTING_PLANT = API_V1 + "supplier/supplier-plant-label"

        //Job
        const val API_JOB_DIRECT_PREPARATION = API_V1 + "delivery-ctlh/delivery-order-direct-preparation"
        const val API_JOB_SUPPLIER_TOTAL = API_V1 + "delivery-ctlh/total-delivery-order-prepared-on-supplier"
        const val API_JOB_SUPPLIER_PREPARATION = API_V1 + "delivery-ctlh/delivery-order-preparation"
        const val API_JOB_SUPPLIER_PROGRESS = API_V1 + "delivery-ctlh/delivery-order-on-progress"
        const val API_JOB_SUPPLIER_READY = API_V1 + "delivery-ctlh/delivery-order-ready-to-pickup"
        const val API_JOB_DELIVERY_MILKRUN = API_V1 + "delivery-ctlh/delivery-order-milkrun"
        const val API_JOB_DELIVERY_DIRECT = API_V1 + "delivery-ctlh/delivery-order-direct-transporter"
        const val API_JOB_DRIVER_DELIVERY_DIRECT = API_V1 + "delivery-ctlh/delivery-order-direct-transporter-assigned"
        const val API_JOB_HISTORY = API_V1 + "delivery-ctlh/delivery-order-history"
        const val API_JOB_COMBO = API_V1 + "delivery-ctlh/delivery-order-history-label"
        const val API_JOB_DRIVER_CHECK_DELIVERY_DIRECT = API_V1 + "direct-supplier/check-complete-picked-up"
        const val API_JOB_DRIVER_CONFIRM_DELIVERY_DIRECT = API_V1 + "direct-supplier/complete-picked-up"
        const val API_ASSIGN_COMBO = API_V1 + "direct-supplier/driver-assign-label"
        const val API_ASSIGNED_DRIVER = API_V1 + "direct-supplier/get-assigned-driver"
        const val API_ASSIGN_CHECK = API_V1 + "direct-supplier/check-assign-driver"
        const val API_ASSIGN_POST = API_V1 + "direct-supplier/assign-driver"

        //Manifest
        const val API_MANIFEST_END_CHECK = API_V1 + "manifest-order-preparation/manifest-check"
        const val API_MANIFEST_START = API_V1 + "manifest-order-preparation/manifest-start"
        const val API_MANIFEST_END = API_V1 + "manifest-order-preparation/manifest-end"
        const val API_MANIFEST_LIST = API_V1 + "daily-order-manifest/manifests"
        const val API_MANIFEST_DETAIL = API_V1 + "daily-order-manifest/manifests/detail"
        const val API_MANIFEST_OTHERS_COMBO = API_V1 + "daily-order-manifest/assign-order-label"
        const val API_MANIFEST_OTHERS_TOTAL = API_V1 + "daily-order-manifest/total-others"
        const val API_MANIFEST_OTHERS_EO = API_V1 + "daily-order-manifest/eo"
        const val API_MANIFEST_OTHERS_SHORTAGE = API_V1 + "daily-order-manifest/shortage"
        const val API_MANIFEST_OTHERS_HISTORY = API_V1 + "daily-order-manifest/manifest-history-list"
        const val API_MANIFEST_OTHERS_ASSIGN_CHECK = API_V1 + "daily-order-manifest/check-assign-order"
        const val API_MANIFEST_OTHERS_ASSIGN_POST = API_V1 + "daily-order-manifest/assign-order"
        const val API_KANBAN_TOTAL = API_V1 + "daily-order-manifest/total-kanban"
        const val API_MANIFEST_COMPLETENESS_ADD = API_V1 + "manifestCompleteness/confirm"
        const val API_MANIFEST_COMPLETENESS_CANCEL = API_V1 + "manifestCompleteness/cancel"

        //SKID
        const val API_SKID_LIST = API_V1 + "daily-order-skid/skid"
        const val API_SKID_CREATE = API_V1 + "daily-order-skid/skid"
        const val API_SKID_DETAIL = API_V1 + "daily-order-skid/skid/detail"

        //Kanban
        const val API_KANBAN_DETAIL = API_V1 + "daily-order-kanban/kanban/detail"
        const val API_KANBAN_MANIFEST_LIST = API_V1 + "daily-order-kanban/kanban/list"
        const val API_KANBAN_LIST = API_V1 + "kanban-order-preparation/list-kanban"
        const val API_KANBAN_CANCEL = API_V1 + "kanban-order-preparation/cancel-kanban"
        const val API_KANBAN_CHECK = API_V1 + "kanban-order-preparation/check-add-kanban"
        const val API_KANBAN_ADD = API_V1 + "kanban-order-preparation/add-kanban"

        //Parts
        const val API_PART_LIST = API_V1 + "daily-order-part/parts"


        //Track My Order
        const val API_ORDER_COMBO = API_V1 + "delivery-ctlh/delivery-order-tracking-label"
        const val API_ORDER_STATUS = API_V1 + "delivery-ctlh/delivery-order-tracking"
        const val API_ORDER_ROUTE = API_V1 + "gps-data-edcl/track-delivery"
        const val API_ORDER_LAST_POSITION = API_V1 + "route-planning/last-position-gps-delivery"

        //Takeout
        const val API_DELIVERY_CHECK = API_V1 + "delivery-ctlh/delivery/supplier/check"
        const val API_DELIVERY_SUBMIT = API_V1 + "delivery-ctlh/delivery/supplier"
    }
}