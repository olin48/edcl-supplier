package com.tmmin.emanifest.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import com.tmmin.emanifest.helpers.PreferenceHelper
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import javax.net.ssl.SSLException
import javax.net.ssl.SSLHandshakeException


class ConnectionInterceptor(private val context: Context) : Interceptor {
    var sharedPref = PreferenceHelper(context)

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        var token = sharedPref.getString(Constant.AUTH_TOKEN_ACCESS) ?: ""
        if (token.isEmpty()) {
            throw NoSessionException()
        }
        if (!isInternetAvailable(context)) {
            throw NoConnectivityException()
        }
        request = request.newBuilder().header("Content-Type", "application/json").header("Authorization", "Bearer $token").build()
        return try {
            chain.proceed(request)
        } catch (e: SSLHandshakeException) {
            e.printStackTrace()
            throw SSLException("SSL Handshake failed", e)
        } catch (e: IOException) {
            e.printStackTrace()
            throw e
        }
    }

    private fun isInternetAvailable(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.M -> {
                val activeNetwork = connectivityManager.activeNetwork ?: return false
                val cap = connectivityManager.getNetworkCapabilities(activeNetwork) ?: return false
                when {
                    cap.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                    cap.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                    else -> false
                }
            }

            else -> {
                val activeNetwork = connectivityManager.activeNetworkInfo ?: return false
                return when (activeNetwork.type) {
                    ConnectivityManager.TYPE_MOBILE -> true
                    ConnectivityManager.TYPE_WIFI -> true
                    ConnectivityManager.TYPE_VPN -> true
                    else -> false
                }
            }
        }
    }

    class NoConnectivityException : IOException() {
        override val message: String get() = "No Internet Connection"
    }

    class NoSessionException : IOException() {
        override val message: String get() = "Invalid Session"
    }
}